<?php
/**
 * interface for ldap plugins
 *
 * @package     Addressbook
 * @subpackage  Backend
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2015 SERPRO (http://www.serpro.gov.br)
 *
 */
/**
 * @package     Addressbook
 * @subpackage  Backend
 */
interface Addressbook_Backend_Ldap_Plugin_Interface
{
    /**
     * Use options from backend and Ldap proxy for returning additional options
     * @param array $ldapBackendOptions
     * @param array $tinebaseLdapOptions
     * @return array
     */
    public function getOptions(array $ldapBackendOptions, array $tinebaseLdapOptions);
}