<?php
/**
 * Tine 2.0
 *
 * @package     Addressbook
 * @subpackage  Setup
 * @license     http://www.gnu.org/licenses/agpl.html AGPL3
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Philipp Schüle <p.schuele@metaways.de>
 */
class Addressbook_Setup_Update_Release7 extends Setup_Update_Abstract
{
    /**
     * update to 7.1
     * - add seq
     * 
     * @see 0000554: modlog: records can't be updated in less than 1 second intervals
     */
    public function update_0()
    {
        $declaration = Tinebase_Setup_Update_Release7::getRecordSeqDeclaration();
        try {
            $this->_backend->addCol('addressbook', $declaration);
        } catch (Zend_Db_Statement_Exception $zdse) {
            // ignore
        }
        $this->setTableVersion('addressbook', 17);
        
        // update contacts: set max seq from modlog + update modlog
        Tinebase_Setup_Update_Release7::updateModlogSeq('Addressbook_Model_Contact', 'addressbook');
        
        $this->setApplicationVersion('Addressbook', '7.1');
    }

    /**
     * update to 7.2
     * - make name in address_book_lists non-unique
     * 
     */
    public function update_1()
    {        
        $this->_backend->dropIndex('addressbook_lists', 'name');
        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>name</name>
                <unique>false</unique>
                <field>
                    <name>name</name>
                </field>
            </index>
        ');
        $this->_backend->addIndex('addressbook_lists', $declaration);

        $this->setTableVersion('addressbook_lists', 3);
        $this->setApplicationVersion('Addressbook', '7.2');
    }
    
    /**
     * update to 7.3
     * - make name and creator an unique index
     * 
     */
    public function update_2()
    {        
        $this->_backend->dropIndex('addressbook_lists', 'name');
        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>created_by-name</name>
                <unique>true</unique>
                <field>
                    <name>created_by</name>
                </field>
                <field>
                    <name>name</name>
                </field>
            </index>
        ');
        $this->_backend->addIndex('addressbook_lists', $declaration);

        $this->setTableVersion('addressbook_lists', 4);
        $this->setApplicationVersion('Addressbook', '7.3');
    }
    
    /**
     * update to 7.4
     * - make name and creator an unique index
     * 
     */
    public function update_3()
    {
        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>container_id</name>
                <field>
                    <name>container_id</name>
                </field>
            </index>
        ');
        $this->_backend->addIndex('addressbook_lists', $declaration);
        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>created_by</name>
                <field>
                    <name>created_by</name>
                </field>
            </index>
        ');
        $this->_backend->addIndex('addressbook_lists', $declaration);
        
        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>list_id</name>
                <field>
                    <name>list_id</name>
                </field>
            </index>
        ');
        $this->_backend->addIndex('addressbook_list_members', $declaration);
        
        $this->setTableVersion('addressbook_lists', 5);
        $this->setTableVersion('addressbook_list_members', 3);
        $this->setApplicationVersion('Addressbook', '7.4');
    }
    
    /**
     * update to 7.5
     * - create table addressbook_certificates
     * 
     */
    public function update_4()
    {
        $table = Setup_Backend_Schema_Table_Factory::factory('String', '
            <table>
                <name>addressbook_certificates</name>
                <version>1</version>
                <declaration>
                    <field>
                        <name>hash</name>
                        <type>text</type>
                        <length>64</length>
                        <notnull>true</notnull>
                    </field>
                    <field>
                        <name>auth_key_identifier</name>
                        <type>text</type>
                        <length>64</length>
                        <notnull>true</notnull>
                    </field>
                    <field>
                        <name>email</name>
                        <type>text</type>
                        <length>320</length>
                        <notnull>true</notnull>
                    </field>
                    <field>
                        <name>certificate</name>
                        <type>text</type>
                        <notnull>true</notnull>
                    </field>
                    <field>
                        <name>invalid</name>
                        <type>boolean</type>
                        <default>false</default>
                    </field>
                    <index>
                        <name>hash-auth_key_identifier</name>
                        <primary>true</primary>
                        <field>
                            <name>hash</name>
                        </field>
                        <field>
                            <name>auth_key_identifier</name>
                        </field>
                    </index>
                    <index>
                        <name>email</name>
                        <field>
                            <name>email</name>
                        </field>
                    </index>
                </declaration>
            </table>
        ');
        $this->_backend->createTable($table);
        
        $this->setTableVersion('addressbook_certificates', 1);
        $this->setApplicationVersion('Addressbook', '7.5');
    }
    
    /**
     * update to 7.6
     * - Update hash values from openssl hash to a sha256 hash from a der certificate
     * 
     */
    public function update_5()
    {
        
        $db = Tinebase_Core::getDb();
        
            $select = $db->select()
                ->from( SQL_TABLE_PREFIX . 'addressbook_certificates');
            $stmt = $select->query();
            $certificates = $stmt->fetchAll();
            
            foreach($certificates as $certificateEntry) {
                // Now $certificate->hash is a sha256 hash from the der certificate's representation
                $certificate = new Custom_Auth_ModSsl_Certificate_X509($certificateEntry['certificate'], true);
                $update = array();
                $update['hash'] = $certificate->getHash();
                $where = array(
                    $this->_db->quoteInto($this->_db->quoteIdentifier('hash') . ' = ?', $certificateEntry['hash']),
                    $this->_db->quoteInto($this->_db->quoteIdentifier('auth_key_identifier') . ' = ?', $certificateEntry['auth_key_identifier']),
                );
                
                // update table
                $changes = $db->update(SQL_TABLE_PREFIX . 'addressbook_certificates',
                    $update, $where);
                
                if ($changes != 1) {
                    throw new Zend_Db_Exception("Changes should affect just one row");
                }
            }
            
            $this->setTableVersion('addressbook_certificates', 2);
            $this->setApplicationVersion('Addressbook', '7.6');
        
        
    }

    /**
     * update to 7.7
     * - add new thunderbird import definition
     *
     * @return void
     */
    public function update_6()
    {
        Setup_Controller::getInstance()->createImportExportDefinitions(Tinebase_Application::getInstance()->getApplicationByName('Addressbook'));

        $this->setApplicationVersion('Addressbook', '7.7');
    }

    /**
     * update to 7.8
     *
     * @see 0011000: Cannot accept invitation to meeting when organiser email is too long
     *
     * @return void
     */
    public function update_7()
    {
        $colsToChange = array('email', 'email_home', 'n_given', 'n_fn');
        foreach ($colsToChange as $col) {
            $declaration = new Setup_Backend_Schema_Field_Xml('
                <field>
                    <name>' . $col . '</name>
                    <type>text</type>
                    <length>255</length>
                    <notnull>false</notnull>
                </field>');
            $this->_backend->alterCol('addressbook', $declaration);
        }

        $this->setTableVersion('addressbook', 18);

        $this->setApplicationVersion('Addressbook', '7.8');
    }
}
