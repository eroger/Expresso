<?php
/**
 * Tine 2.0
 * 
 * @package     Addressbook
 * @subpackage  Model
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mário César Kolling <mario.kolling@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2009-2014 Serpro (http://serpro.gov.br)
 */

/**
 * class to hold contact data
 * 
 * @package     Addressbook
 * @subpackage  Model
 * @property    email       e-mail value inside certificate
 * @property    subject     certificate owner subject
 * @property    issuer_cn   CN of certificate's Issuer
 * @property    hash        certificate's digital fingerprint
 * @property    certificate certificate's data in PEM format
 * @property    not_after   certificate's expiration date
 * @property    expired     true if certificate is expired
 * @property    revoked     true if certificate was revoked
 */
class Addressbook_Model_Certificate extends Tinebase_Record_Abstract
{
    
    /**
     * Default certificate class type
     */
    const CERTIFICATE_CLASS = 'Custom_Auth_ModSsl_Certificate_X509';
    
    /**
     * key in $_validators/$_properties array for the filed which 
     * represents the identifier
     * 
     * @var string
     */
    protected $_identifier = 'hash';
    
    /**
     * application the record belongs to
     *
     * @var string
     */
    protected $_application = 'Addressbook';
    
    /**
     * list of zend validator
     * this validators get used when validating user generated content with Zend_Input_Filter
     * @var array
     * @todo should we use validators? Data comes from valid digital certificates.
     */
    protected $_validators = array(
        'hash'                      => array(Zend_Filter_Input::PRESENCE_REQUIRED => true), // primary key
        'auth_key_identifier'       => array(Zend_Filter_Input::PRESENCE_REQUIRED => true), // primary key
        'email'                     => array(Zend_Filter_Input::PRESENCE_REQUIRED => true),
        'certificate'               => array(Zend_Filter_Input::PRESENCE_REQUIRED => true),
        'invalid'                   => array(Zend_Filter_Input::ALLOW_EMPTY => true, Zend_Filter_Input::DEFAULT_VALUE => 'false'),
    );
        
    
    /**
     *
     * @param mixed $_data array | Custom_Auth_ModSsl_Certificate_X509 $_certificate
     * @param boolean $_bypassFilters
     * @param boolean $_convertDates 
     */
    public function __construct($_data = NULL, $_bypassFilters = false, $_convertDates = true) {
        
        $_data = $_data instanceof Custom_Auth_ModSsl_Certificate_X509 ? array(
            'hash'                  => $_data->getHash(),
            'auth_key_identifier'   => $_data->getAuthorityKeyIdentifier(),
            'email'                 => $_data->getEmail(),
            'certificate'           => $_data->getPemCertificateData(),
            'invalid'               => !$_data->isValid(),
        ) : $_data;
        
        parent::__construct($_data, $_bypassFilters, $_convertDates);
    }
    
    
}

?>
