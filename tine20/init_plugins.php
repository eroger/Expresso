<?php
/**
 * Tine 2.0
 *
 * Use this script to initialize plugins for frontend, controller and backend layers
 * or for groups and user class
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2008-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014 Serpro (http://www.serpro.gov.br)
 *
 */
Addressbook_Backend_Ldap::addOptionPlugin('Custom_Addressbook_Backend_Ldap_ExternalCatalog');
Tinebase_Frontend_Abstract::attachPlugins(array(
        'verifyCertificate' => 'Custom_Plugins_DigitalCertificate',
        'getKeyEscrowCertificates' => 'Custom_Plugins_DigitalCertificate',
		'getUrlLogoutSSC' => 'Prodemge_Tinebase_Frontend_Json',
    ), 'Custom_Plugins_DigitalCertificate');

// session validator and accessLog initialization
Tinebase_Session::setIpAddressValidator('Custom_Session_Validator_IpAddress');
Tinebase_AccessLog::setStrategy('Custom_AccessLog_Strategy_Default');
Tinebase_User::addPlugin('Tinebase_User_Plugin_Samba');
Tinebase_Group::addPlugin('Tinebase_Group_Plugin_Samba');

Syncroton_Command_Sync::setPlugin('Custom_Syncroton_Command_Sync');
Syncroton_Model_Folder::addPlugin('Custom_Syncroton_Model_Folder');

Zend_Db_Adapter_Pdo_Pgsql::setCachePlugin('Custom_Db_Adapter_Pdo_Pgsql_Cache');

// customization of accesslog validators
Tinebase_Model_AccessLog::setAdditionalValidators(array(
    'passwordhash'  => array('allowEmpty' => true)
));

Tinebase_Model_AccessLogFilter::setAdditionalFilterModels(array(
    'passwordhash'  => array('filter' => 'Tinebase_Model_Filter_Text')
));

/*
* This plugin checks if request is from mobile device and redirects user
*/
Tinebase_Http_UserAgent::init();

// Write and read configuration from config.inc.php
Tinebase_Config::setConfigOnlyIntoFilesystem(TRUE);

// Multidomain configuration
Tinebase_Config_Manager::setMultidomain(FALSE);

Tinebase_Auth::addBackendPlugin('Ssc','Prodemge_Tinebase_Auth_Ssc');

Tinebase_Server_Http::setFrontendClass('Prodemge_Tinebase_Frontend_Http','Ssc');

Tinebase_Server_Http::addLoginPlugin('Prodemge_Tinebase_Server_Http');

Tinebase_View::setAdditionalJsFilesFromDirectory('library/Prodemge/Tinebase/js');