<?php
/**
 * AppLauncherRecord controller for AppLauncher application
 * 
 * @package     AppLauncher
 * @subpackage  Controller
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Philipp Schüle <p.schuele@metaways.de>
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * AppLauncherRecord controller class for AppLauncher application
 * 
 * @package     AppLauncher
 * @subpackage  Controller
 */
class AppLauncher_Controller_AppLauncherRecord extends Tinebase_Controller_Record_Abstract
{
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */
    private function __construct() {
        $this->_applicationName = 'AppLauncher';
        $this->_backend = new AppLauncher_Backend_AppLauncherRecord();
        $this->_modelName = 'AppLauncher_Model_AppLauncherRecord';
        $this->_purgeRecords = FALSE;
        // activate this if you want to use containers
        $this->_doContainerACLChecks = FALSE;
        $this->_resolveCustomFields = TRUE;
    }
    
    /**
     * holds the instance of the singleton
     *
     * @var AppLauncher_Controller_AppLauncherRecord
     */
    private static $_instance = NULL;
    
    /**
     * the singleton pattern
     *
     * @return AppLauncher_Controller_AppLauncherRecord
     */
    public static function getInstance() 
    {
        if (self::$_instance === NULL) {
            self::$_instance = new AppLauncher_Controller_AppLauncherRecord();
        }
        
        return self::$_instance;
    }        

    /****************************** overwritten functions ************************/    
    
}
