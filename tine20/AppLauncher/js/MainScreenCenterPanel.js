/* 
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Rommel Cysne <rommel.cysne@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2008 Metaways Infosystems GmbH (http://www.metaways.de)
 */

Ext.ns('Tine.AppLauncher');

Tine.AppLauncher.MainScreenCenterPanel = Ext.extend(Ext.Panel, {
    border: false,
    app: null,
    id: 'appLauncher-mainscreen-panel',
    createForm: null,
    grid: null,
    layout: 'fit',
       
    initComponent: function () {
	var me = this;
            
	this.app = Tine.Tinebase.appMgr.get('AppLauncher');
        this.title = this.app.i18n._('Applications');
        this.cls = 'x-applauncher-mainscreen';
		
	//this.recordClass = Tine.Calendar.Model.Event;
        
	this.initActions();
	this.initLayout();
        
	Tine.AppLauncher.MainScreenCenterPanel.superclass.initComponent.call(this);
    },
    
    initActions: function () {
    },
    
    initLayout: function () {
	this.items = [ 
            this.getLauncherPanel()
	];
    },
    
    getLauncherPanel: function() {
	if (! this.launcherPanel) {
            var button_width = 120;
            var click_to_load = this.app.i18n._('Clique para iniciar a aplicação') + ' ';

            if (Tine.Tinebase.registry.get('applauncher') && Tine.Tinebase.registry.get('applauncher').domain=='planejamento') {
                var action_listadmin = new Ext.Action({
                    text: this.app.i18n._('Administração de Listas'),
                    iconCls: 'x-applauncher-app-icon-listadmin',
                    handler: this.renderForm.createDelegate(this,['listadmin',Tine.Tinebase.registry.get('applauncher').applications.listadmin]),
                    scope: this
                });

                var panel1 = new Ext.Toolbar({
                    defaults: {height: 110, padding: 10},
                    items: [{
                        xtype: 'buttongroup',
                        columns: 3,
                        items: [
                            Ext.apply(new Ext.Button(action_listadmin), {
                                scale: 'medium',
                                height: 80, 
                                rowspan: 2,
                                width: button_width,
                                iconAlign: 'top',
                                labelAlign: 'bottom',
                                tooltip: click_to_load + this.app.i18n._('Administração de Listas')
                            })
                        ]
                    }]
                });

                this.launcherPanel = new Ext.Panel({
                    layout: 'vbox',
                    layoutConfig: {
                        align:'stretch'
                    },
                    border: false,
                    items: [
                        panel1
                    ]
                });
            }
            else {
                var action_pd = new Ext.Action({
                    text: this.app.i18n._('Decisive process'),
                    iconCls: 'x-applauncher-app-icon-pd',
                    handler: this.renderForm.createDelegate(this,['pd']),
                    scope: this
                });

                var action_svpc = new Ext.Action({
                    text: this.app.i18n._('SVPC'),
                    iconCls: 'x-applauncher-app-icon-svpc',
                    handler: this.renderForm.createDelegate(this,['svpc']),
                    scope: this
                });

                var action_listadmin = new Ext.Action({
                    text: this.app.i18n._('Administração de Listas'),
                    iconCls: 'x-applauncher-app-icon-listadmin',
                    handler: this.renderForm.createDelegate(this,['listadmin']),
                    scope: this
                });

                var action_calendar = new Ext.Action({
                    text: this.app.i18n._('Eventos'),
                    iconCls: 'x-applauncher-app-icon-calendar',
                    handler: this.renderForm.createDelegate(this,['calendar']),
                    scope: this
                });

                var action_adminserpro = new Ext.Action({
                    text: this.app.i18n._('AdminSerpro'),
                    iconCls: 'x-applauncher-app-icon-adminserpro',
                    handler: this.renderForm.createDelegate(this,['adminserpro']),
                    scope: this
                });

                var action_adminwf = new Ext.Action({
                    text: this.app.i18n._('Administração do WF'),
                    iconCls: 'x-applauncher-app-icon-adminwf',
                    handler: this.renderForm.createDelegate(this,['adminwf']),
                    scope: this
                });

                var buttons1 = [];
                buttons1.push(Ext.apply(new Ext.Button(action_pd), {
                                scale: 'medium',
                                height: 80, 
                                rowspan: 2,
                                width: button_width,
                                iconAlign: 'top',
                                labelAlign: 'bottom',
                                tooltip: click_to_load + this.app.i18n._('Decisive process')
                            }));
                buttons1.push(Ext.apply(new Ext.Button(action_svpc), {
                                scale: 'medium',
                                height: 80, 
                                rowspan: 2,
                                width: button_width,
                                iconAlign: 'top',
                                labelAlign: 'bottom',
                                tooltip: click_to_load + this.app.i18n._('SVPC')
                            }));
                buttons1.push(Ext.apply(new Ext.Button(action_listadmin), {
                                scale: 'medium',
                                height: 80, 
                                rowspan: 2,
                                width: button_width,
                                iconAlign: 'top',
                                labelAlign: 'bottom',
                                tooltip: click_to_load + this.app.i18n._('Administração de Listas')
                            }));
                var buttons2 = [];
                buttons2.push(Ext.apply(new Ext.Button(action_calendar), {
                                scale: 'medium',
                                height: 80, 
                                rowspan: 2,
                                width: button_width,
                                iconAlign: 'top',
                                labelAlign: 'bottom',
                                tooltip: click_to_load + this.app.i18n._('Eventos')
                            }));
                buttons2.push(Ext.apply(new Ext.Button(action_adminserpro), {
                                scale: 'medium',
                                height: 80, 
                                rowspan: 2,
                                width: button_width,
                                iconAlign: 'top',
                                labelAlign: 'bottom',
                                tooltip: click_to_load + this.app.i18n._('AdminSerpro')
                            }));
                buttons2.push(Ext.apply(new Ext.Button(action_adminwf), {
                                scale: 'medium',
                                height: 80, 
                                rowspan: 2,
                                width: button_width,
                                iconAlign: 'top',
                                labelAlign: 'bottom',
                                tooltip: click_to_load + this.app.i18n._('Administração do WF')
                            }));

                var panel1 = new Ext.Toolbar({
                    defaults: {height: 110, padding: 10},
                    items: [{
                        xtype: 'buttongroup',
                        columns: 3,
                        items: buttons1
                    }]
                });
                var panel2 = new Ext.Toolbar({
                    defaults: {height: 110, padding: 10},
                    items: [{
                        xtype: 'buttongroup',
                        columns: 3,
                        items: buttons2            
                    }]
                });

                this.launcherPanel = new Ext.Panel({
                    layout: 'vbox',
                    layoutConfig: {
                        align:'stretch'
                    },
                    border: false,
                    items: [
                        panel1,
                        panel2
                    ]
                });
            }

	}

	return this.launcherPanel;
    },
    
    onRender: function(ct, position) {
	Tine.AppLauncher.MainScreenCenterPanel.superclass.onRender.apply(this, arguments);
    },
    
    renderForm: function(app_to_launch, url_to_call) {

        switch (app_to_launch) {
            case 'pd':
                this.loginBoxEl = this.getLoginBoxEl('https://aplicacoesexpresso.serpro.gov.br/login.php');
                this.loginBoxEl.submit();
                break;

            case 'svpc':
                this.loginBoxEl = this.getLoginBoxEl('https://svpc.portalcorporativo.serpro/');
                this.loginBoxEl.submit();
                break;
                
            case 'listadmin':
                url_to_call = url_to_call || 'https://aplicacoesexpresso.serpro.gov.br/listAdmin/index.php';
                this.loginBoxEl = this.getLoginBoxEl(url_to_call);
                this.loginBoxEl.submit();
                break;
                
            case 'calendar':
                this.loginBoxEl = this.getLoginBoxEl('https://aplicacoesexpresso.serpro.gov.br/calendar/index.php');
                this.loginBoxEl.submit();
                break;

            case 'adminserpro':
                this.loginBoxEl = this.getLoginBoxEl('https://aplicacoesexpresso.serpro.gov.br/expressoAdminSerpro/index.php');
                this.loginBoxEl.submit();
                break;

            case 'adminwf':
                this.loginBoxEl = this.getLoginBoxEl('https://aplicacoesexpresso.serpro.gov.br/login.php');
                this.loginBoxEl.submit();
                break;

            default:
                break;
        }

    },
    
    getLoginBoxEl: function(link_action) {
        if (this.loginBoxEl) {
            var flogin = Ext.get('flogin');
            if (flogin) {
                flogin.remove();
            }
        }
        var loginBoxEl = Ext.DomHelper.append(Ext.getBody(), {
            tag: 'form',
            method: 'post',
            target: '_blank',
            id: 'flogin',
            action: link_action,
            style: 'position: absolute; left: -100px; top: -100px; width: 100px; height: 100px'
        });
        return loginBoxEl;
    }

});