/*
 * Tine 2.0
 * 
 * @package     AppLauncher
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Philipp Schüle <p.schuele@metaways.de>
 * @copyright   Copyright (c) 2009-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */
 
Ext.ns('Tine.AppLauncher.Model');

/**
 * @namespace Tine.AppLauncher.Model
 * @class     Tine.AppLauncher.Model.User
 * @extends   Tine.AppLauncher.data.Record
 * 
 * Model of a user
 */
Tine.AppLauncher.Model.User = Tine.Tinebase.data.Record.create([
    {name: 'id'},
    {name: 'name'},
    {name: 'description'},
    {name: 'container_id'},
    {name: 'visibility'}
    //{name: 'groupMembers'}
], {
    appName: 'AppLauncher',
    modelName: 'User',
    idProperty: 'id',
    titleProperty: 'name',
    // ngettext('Group', 'Groups', n); gettext('Groups');
    recordName: 'User',
    recordsName: 'Users'
});

///**
// * returns default group data
// * 
// * @namespace Tine.AppLauncher.Model.Group
// * @static
// * @return {Object} default data
// */
//Tine.AppLauncher.Model.Group.getDefaultData = function () {
//    var internalAddressbook = Tine.AppLauncher.registry.get('defaultInternalAddressbook');
//    
//    return {
//        visibility: (internalAddressbook !== null) ? 'displayed' : 'hidden',
//        container_id: internalAddressbook
//    };
//};

/**
 * @namespace Tine.AppLauncher.Model
 * @class     Tine.AppLauncher.Model.Application
 * @extends   Tine.AppLauncher.data.Record
 * 
 * Model of an application
 */
Tine.AppLauncher.Model.Application = Tine.Tinebase.data.Record.create([
    {name: 'id'},
    {name: 'name'},
    {name: 'status'},
    {name: 'order'},
    {name: 'app_tables'},
    {name: 'version'}
], {
    appName: 'AppLauncher',
    modelName: 'Application',
    idProperty: 'id',
    titleProperty: 'name',
    // ngettext('Application', 'Applications', n); gettext('Applications');
    recordName: 'Application',
    recordsName: 'Applications'
});
