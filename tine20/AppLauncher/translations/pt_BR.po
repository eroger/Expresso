msgid ""
msgstr ""
"Project-Id-Version: ExpressoBr- AppLauncher\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-17 22:12+0100\n"
"PO-Revision-Date: 2015-08-24 15:02-0300\n"
"Last-Translator: Cassiano Dal Pizzol <cassiano.dalpizzol@serpro.gov.br>\n"
"Language-Team: ExpressoBr Translators\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2012-03-02 09:32+0000\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: js/MainScreenCenterPanel.js:23
msgid "Applications"
msgstr "Aplicativos"

#: js/MainScreenCenterPanel.js:55
msgid "Decisive process"
msgstr "Processo Decisório"

#: js/AppLauncher.js:93
msgid "Could not create northPanel"
msgstr "Não foi possível criar painel superior"

#: Setup/setup.xml:4 js/Model.js:68
msgid "AppLauncher"
msgstr "Lançador de Aplicativos"
