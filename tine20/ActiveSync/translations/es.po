#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: ExpressoBr - Activesync\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-04-11 11:43-0300\n"
"PO-Revision-Date: 2015-08-24 15:31-0300\n"
"Last-Translator: Cassiano Dal Pizzol <cassiano.dalpizzol@serpro.gov.br>\n"
"Language-Team: ExpressoBr Translators\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _\n"
"X-Poedit-Basepath: ..\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-SearchPath-0: js\n"

#: js/Application.js:27
msgid "Active Sync"
msgstr "Active Sync"

#: js/Application.js:57
msgid "No ActiveSync Device registered"
msgstr "No hay registrado ningún dispositvo ActiveSync"

#: js/Application.js:90
msgid "Resetted Sync Filter"
msgstr "Filtro de Sync reseteado"

#: js/Application.js:43
msgid "Select a Device"
msgstr "Seleccione un dispositivo"

#: js/Application.js:102
msgid "Set Sync Filter"
msgstr "Defina filtro de Sync"

#: js/Application.js:65
msgid "Set as {0} Filter"
msgstr "Filtro {0} seteado"

#: js/Application.js:94
msgid "resetted"
msgstr "reseteado"

#: js/Application.js:91 js/Application.js:103
msgid "{0} filter for device \"{1}\" is now \"{2}\""
msgstr "Filtro {0} para dispositivo \"{1}\" ahora es \"{2}\""
