<?php
/**
 * Tine 2.0
 *
 * @package     ActiveSync
 * @subpackage  Setup
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2013 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Philipp Schüle <p.schuele@metaways.de>
 */

/**
 * updates for major release 7
 *
 * @package     ActiveSync
 * @subpackage  Setup
 */
class ActiveSync_Setup_Update_Release7 extends Setup_Update_Abstract
{
    /**
     * update to 7.1
     * 
     * @see 0007452: use json encoded array for saving of policy settings
     * 
     * @return void
     */
    public function update_0()
    {
        if ($this->getTableVersion('acsync_policy') != 3) {
            $release6 = new ActiveSync_Setup_Update_Release6($this->_backend);
            $release6->update_3();
        }
        
        $this->setApplicationVersion('ActiveSync', '7.1');
    }

    /**
     * update to 7.2
     * 
     * @see 0007942: reset device pingfolder in update script
     * 
     * @return void
     */
    public function update_1()
    {
        $release6 = new ActiveSync_Setup_Update_Release6($this->_backend);
        $release6->update_4();
        
        $this->setApplicationVersion('ActiveSync', '7.2');
    }
    
    /**
     * update to 7.x
     * 
     * Add fields required by Expressomail module
     * 
     * @return void
     */
    public function update_2()
    {
        if (! $this->_backend->columnExists('lastimapmodseq', 'acsync_folder')) {
            $declaration = new Setup_Backend_Schema_Field_Xml('
                <field>
                    <name>lastping</name>
                    <type>datetime</type>
                    <default>null</default>
                </field>
            ');

            $this->_backend->addCol('acsync_device', $declaration);

            $declaration = new Setup_Backend_Schema_Field_Xml('
                <field>
                    <name>imapstatus</name>
                    <type>text</type>
                    <length>254</length>
                    <notnull>true</notnull>
                    <default></default>
                </field>
            ');

            $this->_backend->addCol('acsync_folder', $declaration);

            $declaration = new Setup_Backend_Schema_Field_Xml('
                <field>
                    <name>lastimapmodseq</name>
                    <type>integer</type>
                    <notnull>true</notnull>
                    <default>-1</default>
                </field>
            ');

            $this->_backend->addCol('acsync_folder', $declaration);

            $declaration = new Setup_Backend_Schema_Field_Xml('
                <field>
                    <name>contentid</name>
                    <type>text</type>
                    <length>254</length>
                </field>
            ');

            $this->_backend->alterCol('acsync_content', $declaration);

        }

        $this->setTableVersion('acsync_device', 6);
        $this->setTableVersion('acsync_folder', 4);
        $this->setTableVersion('acsync_content', 5);
        $this->setApplicationVersion('ActiveSync', '7.3');
    }

    /**
     * update to 7.4
     *
     * Drop Constraints and Create Indexes make it compatible with separate backend
     *
     * @return void
     */
    public function update_3()
    {
        if (! $this->_backend->tableExists('acsync_device')) {
            throw new Tinebase_Exception_Backend_Database('It was not possible to access database to update application.');
        }

        $this->_backend->dropForeignKey('acsync_device', 'acsync_device::calendarfilter_id--filter::id');
        $this->_backend->dropForeignKey('acsync_device', 'acsync_device::contactsfilter_id--filter::id');
        $this->_backend->dropForeignKey('acsync_device', 'acsync_device::emailfilter_id--filter::id');
        $this->_backend->dropForeignKey('acsync_device', 'acsync_device::tasksfilter_id--filter::id');

        $declaration = new Setup_Backend_Schema_Index_Xml('
                <index>
                    <name>acsync_device::calendarfilter_id--filter::id</name>
                    <field>
                        <name>calendarfilter_id</name>
                    </field>
                </index>
        ');
        $this->_backend->addIndex('acsync_device', $declaration);

        $declaration = new Setup_Backend_Schema_Index_Xml('
                <index>
                    <name>acsync_device::contactsfilter_id--filter::id</name>
                    <field>
                        <name>contactsfilter_id</name>
                    </field>
                </index>
        ');
        $this->_backend->addIndex('acsync_device', $declaration);

        $declaration = new Setup_Backend_Schema_Index_Xml('
                <index>
                    <name>acsync_device::emailfilter_id--filter::id</name>
                    <field>
                        <name>emailfilter_id</name>
                    </field>
                </index>
        ');
         $this->_backend->addIndex('acsync_device', $declaration);

         $declaration = new Setup_Backend_Schema_Index_Xml('
                <index>
                    <name>acsync_device::tasksfilter_id--filter::id</name>
                    <field>
                        <name>tasksfilter_id</name>
                    </field>
                </index>
        ');
        $this->_backend->addIndex('acsync_device', $declaration);

        $this->setTableVersion('acsync_device', 7);
        $this->setApplicationVersion('ActiveSync', '7.4');
    }

    /**
     * update to 7.5
     *
     * Adapt Expressomail big folderIds and big contentIds
     *
     * @return void
     */
    public function update_4()
    {
        if (! $this->_backend->tableExists('acsync_device')) {
            throw new Tinebase_Exception_Backend_Database('It was not possible to access database to update application.');
        }

        $declaration = new Setup_Backend_Schema_Field_Xml('
                <field>
                    <name>bigfolderid</name>
                    <type>text</type>
                    <length>4100</length>
                    <notnull>false</notnull>
                </field>
        ');

        $this->_backend->addCol('acsync_folder', $declaration);

        $declaration = new Setup_Backend_Schema_Field_Xml('
                <field>
                    <name>bigcontentid</name>
                    <type>text</type>
                    <length>4100</length>
                    <notnull>false</notnull>
                </field>
        ');

        $this->_backend->addCol('acsync_content', $declaration);

        $this->setTableVersion('acsync_folder', 5);
        $this->setTableVersion('acsync_content', 6);
        $this->setApplicationVersion('ActiveSync', '7.5');
    }
}