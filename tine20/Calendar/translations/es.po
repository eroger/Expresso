#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: ExpressoBr - Calendar\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-04-09 17:10-0300\n"
"PO-Revision-Date: 2015-08-24 15:04-0300\n"
"Last-Translator: Cassiano Dal Pizzol <cassiano.dalpizzol@serpro.gov.br>\n"
"Language-Team: ExpressoBr Translators\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _\n"
"X-Poedit-Basepath: ..\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-SearchPath-0: js\n"

#: js/AttendeeGridPanel.js:630 js/AttendeeSelCombo.js:403
msgid "(as a group member)"
msgstr "(como un miembro del grupo)"

#: js/AddToEventPanel.js:109
msgid "Adding {0} Attendee to event"
msgstr "Agregando al asistente {0} al evento"

#: js/AttendeeFilterModel.js:35 js/AttendeeGridPanel.js:82
#: js/AttendeeSelCombo.js:76
msgid "Attendee"
msgstr "Asistentes"

#: js/CalDAVContainerPropertiesHookField.js:35
msgid "CalDAV URL"
msgstr "URL de CalDAV"

#: js/Calendar.js:39
msgid "Calendar"
msgid_plural "Calendars"
msgstr[0] "Calendario"
msgstr[1] "Calendarios"

#: js/AttendeeFilterGrid.js:60
msgid "Check to filter for this attendee"
msgstr ""

#: js/AttendeeSelCombo.js:378
msgid "Click here to delegate attendance to another attender..."
msgstr "Haga clic aquí para delegar asistencia a otro asistente..."

#: js/AttendeeGridPanel.js:470
msgid "Delegate Attendance"
msgstr "Delegar asistente"

#: js/ContactEventsGridPanel.js:50
msgid "Events"
msgstr "Eventos"

#: js/AttendeeGridPanel.js:259 js/AttendeeGridPanel.js:721
msgid "External Attendee"
msgstr "Asistente externo"

#: js/AdminPanel.js:45
msgid "Manage Resources"
msgstr "Administrar Recursos"

#: js/AttendeeGridPanel.js:205 js/AttendeeSelCombo.js:169
msgid "Name"
msgstr "Nombre"

#: js/AttendeeGridPanel.js:625 js/AttendeeGridPanel.js:643
#: js/AttendeeGridPanel.js:660 js/AttendeeGridPanel.js:672
#: js/AttendeeSelCombo.js:398 js/AttendeeSelCombo.js:416
#: js/AttendeeSelCombo.js:433 js/AttendeeSelCombo.js:445
msgid "No Information"
msgstr "Sin Información"

#: js/CalendarSelectWidget.js:124
msgid "Originally"
msgstr "Originalmente"

#: js/AddToEventPanel.js:35
msgid "Please choose the Event to add the contacts to"
msgstr "Por favor elija el evento al que agregarle contactos"

#: js/AttendeeGridPanel.js:274
msgid "Please wait"
msgstr "Por favor espere"

#: js/AttendeeGridPanel.js:461
msgid "Remove Attender"
msgstr "Eliminar Asistente"

#: js/AttendeeGridPanel.js:197 js/AttendeeSelCombo.js:161
msgid "Resource"
msgstr "Recurso"

#: js/AddToEventPanel.js:78 js/AttendeeGridPanel.js:130
#: js/AttendeeSelCombo.js:102
msgid "Role"
msgstr "Cargo"

#: js/AttendeeFilterModel.js:100
msgid "Select Attendee"
msgstr "Elija asistente"

#: js/AddToEventPanel.js:79
msgid "Select Role"
msgstr "Elija rol"

#: js/AddToEventPanel.js:89
msgid "Select Status"
msgstr "Elija estado"

#: js/AttendeeFilterGrid.js:47
msgid "Sort Attendee"
msgstr "Ordenar Asistentes"

#: js/ContactEventsGridPanel.js:119
msgid "Start Time"
msgstr "Hora de Inicio"

#: js/AddToEventPanel.js:88 js/AttendeeGridPanel.js:213
#: js/AttendeeSelCombo.js:177 js/ContactEventsGridPanel.js:126
msgid "Status"
msgstr "Estado"

#: js/ContactEventsGridPanel.js:113
msgid "Summary"
msgstr "Resumen"

#: js/calendarRenderer.js:33
msgid "This event is additionally displayed in your personal calendar {0}"
msgstr "Es evento se muestra en su calendario personal {0}"

#: js/calendarRenderer.js:30
msgid "This event is originally stored in a calendar you don't have access to."
msgstr "Este evento está guardado en un calendario al cual Ud. no tiene acceso"

#: js/calendarRenderer.js:31
msgid "This event is originally stored in {0}"
msgstr "Este evento está originalmente guardado en {0}"

#: js/AttendeeGridPanel.js:153 js/AttendeeSelCombo.js:116
msgid "This is the calendar where the attender has saved this event in"
msgstr "Este es el calendario donde el asistente ha guardado este evento en"

#: js/AttendeeGridPanel.js:188 js/AttendeeSelCombo.js:152
msgid "Type"
msgstr "Tipo"

#: js/AttendeeGridPanel.js:195 js/AttendeeSelCombo.js:159
msgid "User"
msgstr "Usuario"

#: js/AttendeeGridPanel.js:274
msgid "Verifying account"
msgstr "Verificando cuenta"

#: js/AttendeeGridPanel.js:260
msgid "You have selected an external attendee!"
msgstr "Ud. ha seleccionado un asistente externo"

#: js/iMIPDetailsPanel.js:487
msgid "An invited attendee responded to the invitation."
msgstr "Un asistente invitado ha respondido a su invitación"

#: js/EventEditDialog.js:115
msgid "Attachments"
msgstr "Adjuntos"

#: js/Model.js:227
msgid "Attendee Role"
msgstr "Rol de asistente"

#: js/Model.js:218
msgid "Attendee Status"
msgstr "Estado del asistente"

#: js/EventDetailsPanel.js:262 js/GridView.js:129
msgid "Blocking"
msgstr "Bloquear"

#: js/OptionsDialog.js:121
msgid "Cancel"
msgstr "Cancelar"

#: js/iMIPDetailsPanel.js:346 js/MainScreenCenterPanel.js:1270
msgid "Cancel this action"
msgstr "Cancelar esta acción"

#: js/iMIPDetailsPanel.js:76
msgid "Checking Calendar Data..."
msgstr "Revisando datos de calendario..."

#: js/MainScreenCenterPanel.js:605
msgid "Copy {0}"
msgstr "Copiar {0}"

#: js/MainScreenCenterPanel.js:1100
msgid "Could not Print"
msgstr "No pude imprimir"

#: js/MainScreenCenterPanel.js:622
msgid "Create or update this event"
msgstr "Crear o actualizar este evento"

#: js/MainScreenCenterPanel.js:621
msgid "Creating or updating event in the past"
msgstr "Creando o actualizando un evento en el pasado"

#: js/MainScreenCenterPanel.js:222
msgid "Day"
msgstr "Día"

#: js/MainScreenCenterPanel.js:898
msgid "Delete Event"
msgstr "Eliminar Evento"

#: js/MainScreenCenterPanel.js:905
msgid "Delete nothing"
msgstr "No Eliminar"

#: js/MainScreenCenterPanel.js:903
msgid "Delete this and all future events"
msgstr "Eliminar éste y todos los eventos futuros"

#: js/MainScreenCenterPanel.js:902
msgid "Delete this event only"
msgstr "Eliminar sólo este evento"

#: js/MainScreenCenterPanel.js:904
msgid "Delete whole series"
msgstr "Eliminar toda la serie"

#: js/EventEditDialog.js:332 js/GridView.js:163 js/Model.js:214
#: js/ResourceEditDialog.js:84
msgid "Description"
msgstr "Descripción"

#: js/EventEditDialog.js:187
msgid "Details"
msgstr "Detalles"

#: js/PerspectiveCombo.js:110
msgid "Displayed in"
msgstr "Mostrado en"

#: js/MainScreenCenterPanel.js:623
msgid "Do not create or update this event"
msgstr "No crear o actualizar este evento"

#: js/iMIPDetailsPanel.js:108
msgid "Do not respond"
msgstr "No responder"

#: js/iMIPDetailsPanel.js:250
msgid "Do not update this event"
msgstr "No actualizar este evento"

#: js/MainScreenCenterPanel.js:956
msgid "Do you really want to delete this event?"
msgid_plural "Do you really want to delete the {0} selected events?"
msgstr[0] "¿Realmente desea eliminar este evento?"
msgstr[1] "¿Realmente desea eliminar los {0} eventos seleccioniados?"

#: js/iMIPDetailsPanel.js:345 js/MainScreenCenterPanel.js:1269
msgid "Edit Event"
msgstr "Editar Evento"

#: js/ResourceEditDialog.js:73 js/ResourcesGridPanel.js:49
msgid "Email"
msgstr "Email"

#: js/EventDetailsPanel.js:257 js/EventEditDialog.js:220 js/GridView.js:117
msgid "End Time"
msgstr "Hora de Término"

#: js/EventEditDialog.js:618
msgid "End date is not valid"
msgstr "La fecha de término no es válida"

#: js/EventEditDialog.js:621
msgid "End date must be after start date"
msgstr "La fecha de término debe ser mayor a la fecha de inicio"

#: js/EventEditDialog.js:346
msgid "Enter description"
msgstr "Ingrese descripción"

#: js/ResourceEditDialog.js:85
msgid "Enter description..."
msgstr "Ingrese descripción..."

#: js/EventEditDialog.js:105 js/EventEditDialog.js:654
msgid "Errors"
msgstr "Errores"

#: js/EventEditDialog.js:441
msgid "Files are still uploading."
msgstr "Los archivos todavía se están subiendo."

#: js/ResourceEditDialog.js:117
msgid "Grants"
msgstr "Derechos"

#: js/MainScreenCenterPanel.js:187
msgid "Grid"
msgstr "Cuadrícula"

#: js/iMIPDetailsPanel.js:344 js/MainScreenCenterPanel.js:1268
msgid "Ignore Conflict"
msgstr "Ignorar Conflicto"

#: js/OptionsDialog.js:147
msgid "Inform inicial date!"
msgstr "Informe la fecha inicial!"

#: js/OptionsDialog.js:139
msgid "Inform the number of days!"
msgstr "Informe el número de días"

#: js/ResourceEditDialog.js:79
msgid "Is a location"
msgstr "Es una ubicación"

#: js/EventDetailsPanel.js:272 js/EventEditDialog.js:200 js/GridView.js:150
#: js/Model.js:213 js/ResourcesGridPanel.js:54
msgid "Location"
msgstr "Ubicación"

#: js/OptionsDialog.js:142
msgid "Maximum value is 45!"
msgstr "El valor máximo es 45!"

#: js/MainScreenCenterPanel.js:240
msgid "Month"
msgstr "Mes"

#: js/OptionsDialog.js:94
msgid "Number of days:"
msgstr "Número de días:"

#: js/OptionsDialog.js:114
msgid "Ok"
msgstr "Ok"

#: js/iMIPDetailsPanel.js:397
msgid "Open/Delegate"
msgstr "Abrir/Delegar"

#: js/EventDetailsPanel.js:276 js/EventEditDialog.js:389 js/GridView.js:156
#: js/Model.js:233 js/PerspectiveCombo.js:127
msgid "Organizer"
msgstr "Organizador"

#: js/MainScreenCenterPanel.js:249
msgid "Period"
msgstr "Período"

#: js/MainScreenCenterPanel.js:884
msgid "Please Change Selection"
msgstr "Por favor Cambie la Selección"

#: js/MainScreenCenterPanel.js:159
msgid "Print Page"
msgstr "Imprimir página"

#: js/MainScreenCenterPanel.js:387
msgid "Print period view"
msgstr "Imprimir período en vista"

#: js/EventEditDialog.js:288 js/GridView.js:96
msgid "Private"
msgstr "Privado"

#: js/Model.js:211
msgid "Quick Search"
msgstr "Búsqueda rápida"

#: js/iMIPDetailsPanel.js:102
msgid "Reply to Recurring Event"
msgstr "Responder a evento recurrente"

#: js/iMIPDetailsPanel.js:107
msgid "Respond to whole series"
msgstr "Responder a toda la serie"

#: js/EventEditDialog.js:236
msgid "Saved in"
msgstr "Guardado en"

#: js/iMIPDetailsPanel.js:338 js/MainScreenCenterPanel.js:1262
msgid "Scheduling Conflict"
msgstr "Conflicto de Planificación"

#: js/MainScreenCenterPanel.js:576
msgid "Set my response"
msgstr "Setear mi respuesta"

#: js/MainScreenCenterPanel.js:172
msgid "Sheet"
msgstr "Hoja"

#: js/MainScreenCenterPanel.js:1100
msgid "Sorry, your current view does not support printing."
msgstr "Disculpe, la vista actual no soporta impresión."

#: js/OptionsDialog.js:88
msgid "Start Date"
msgstr "Fecha de Inicio"

#: js/EventEditDialog.js:634
msgid "Start date is not valid"
msgstr "La fecha de inicio no es válida"

#: js/DaysView.js:737
msgid "Summary too Long"
msgstr "Resumen muy largo"

#: js/DaysView.js:747
msgid "Summary too Short"
msgstr "Resumen muy corto"

#: js/GridView.js:104
msgid "Tags"
msgstr "Etiquetas"

#: js/EventDetailsPanel.js:267 js/EventEditDialog.js:274 js/GridView.js:137
msgid "Tentative"
msgstr "Tentativo"

#: js/iMIPDetailsPanel.js:446
msgid "The event of this message does not exist"
msgstr "El evento de este mensaje no existe"

#: js/iMIPDetailsPanel.js:340 js/MainScreenCenterPanel.js:1264
msgid "The following attendee are busy at the requested time:"
msgstr "El siguiente asistente se encuentra ocupado para la fecha solicitada:"

#: js/iMIPDetailsPanel.js:451
msgid "The sender is not authorised to update the event"
msgstr "El remitente no está autorizado a actualizar este evento"

#: js/DaysView.js:700
msgid "The summary must have at least 1 character."
msgstr "El resumen debe tener por lo menos 1 caracter."

#: js/DaysView.js:698
msgid "The summary must not be longer than 255 characters."
msgstr "El resumen no debe contener más que 255 caracteres."

#: js/iMIPDetailsPanel.js:475
msgid "This is an event invitation for someone else."
msgstr "Este es una invitación a un evento de alguien más."

#: js/iMIPDetailsPanel.js:456
msgid "This message is already processed"
msgstr "Este mensaje ya fue procesado"

#: js/iMIPDetailsPanel.js:465
msgid "Unsupported message"
msgstr "Mensaje no soportado"

#: js/iMIPDetailsPanel.js:491
msgid "Unsupported method"
msgstr "Método no soportado"

#: js/iMIPDetailsPanel.js:187 js/MainScreenCenterPanel.js:744
msgid "Update Event"
msgstr "Actualizar Evento"

#: js/iMIPDetailsPanel.js:194 js/MainScreenCenterPanel.js:751
msgid "Update nothing"
msgstr "No actualizar"

#: js/iMIPDetailsPanel.js:192 js/MainScreenCenterPanel.js:749
msgid "Update this and all future events"
msgstr "Actualizar este evento y todos los futuros"

#: js/iMIPDetailsPanel.js:249
msgid "Update this event"
msgstr "Actualizar este evento"

#: js/iMIPDetailsPanel.js:191 js/MainScreenCenterPanel.js:748
msgid "Update this event only"
msgstr "Actualizar este evento solamente"

#: js/iMIPDetailsPanel.js:193 js/MainScreenCenterPanel.js:750
msgid "Update whole series"
msgstr "Actualizar toda la serie"

#: js/iMIPDetailsPanel.js:248
msgid "Updating event in the past"
msgstr "Actualizando evento en el pasado"

#: js/EventEditDialog.js:171
msgid "View"
msgstr "Vista"

#: js/MainScreenCenterPanel.js:231 js/PagingToolbar.js:315
msgid "Week"
msgstr "Semana"

#: js/iMIPDetailsPanel.js:461
msgid "You are not an attendee of this event"
msgstr "Ud. no es un asistente de este evento"

#: js/iMIPDetailsPanel.js:103
msgid "You are responding to an recurring event. What would you like to do?"
msgstr "Está respondiendo a un evento recurrente. ¿Qué quiere hacer?"

#: js/iMIPDetailsPanel.js:477
msgid "You have already replied to this event invitation."
msgstr "Ud. ya respondió a la invitación de este evento."

#: js/iMIPDetailsPanel.js:479
msgid "You received an event invitation. Set your response to:"
msgstr "Ud. recibió una invitación a un evento. Definir su respuesta como:"

#: js/MainScreenCenterPanel.js:885
msgid ""
"Your selection contains recurring events. Recuring events must be deleted "
"seperatly!"
msgstr ""
"Los elementos seleccionados contienen eventos repetitivos. ¡Los eventos "
"repetitivos deben eliminarse por separado!"

#: js/EventUI.js:256
msgid "has alarm"
msgstr "tiene alarma"

#: js/EventEditDialog.js:258 js/PerspectiveCombo.js:84
msgid "non-blocking"
msgstr "no bloquear"

#: js/EventUI.js:235
msgid "private classification"
msgstr "clasificación privada"

#: js/EventDetailsPanel.js:89 js/EventUI.js:242
msgid "recurring event"
msgstr "evento recurrente"

#: js/EventDetailsPanel.js:91 js/EventUI.js:247
msgid "recurring event exception"
msgstr "excepción a evento recurrente"

#: js/EventEditDialog.js:214 js/GridView.js:123
msgid "whole day"
msgstr "todo el día"

#: js/EventDetailsPanel.js:65
msgid "{0} {1} o'clock"
msgstr "{0} {1} en punto"

#: js/RrulePanel.js:79
msgid "Daily"
msgstr "Diariamente"

#: js/RrulePanel.js:679
msgid "Day must be given"
msgstr "El día debe ser informado"

#: js/RrulePanel.js:322
msgid "End"
msgstr "Termina"

#: js/SearchCombo.js:79
msgid "Event"
msgstr "Evento"

#: js/RrulePanel.js:412
msgid "Every {0}. Day"
msgstr "Cada {0}. Día"

#: js/RrulePanel.js:515
msgid "Every {0}. Month"
msgstr "Cada {0}. Mes"

#: js/RrulePanel.js:445
msgid "Every {0}. Week at"
msgstr "Cada {0}. Semana en"

#: js/RrulePanel.js:714
msgid "Every {0}. Year"
msgstr "Cada {0}. Año"

#: js/RrulePanel.js:178
msgid "Exceptions of reccuring events can't have recurrences themselves."
msgstr ""
"Excepciones de eventos recurrentes no pueden tener recurrencias propias."

#: js/RrulePanel.js:683
msgid "Invalid day"
msgstr "día inválido"

#: js/RrulePanel.js:908
msgid "Invalid day for the month of {0}"
msgstr "día del mes {0} inválido"

#: js/WestPanel.js:32
msgid "Mini Calendar"
msgstr "Calendario mini"

#: js/RrulePanel.js:93
msgid "Monthly"
msgstr "Mensualmente"

#: js/RrulePanel.js:36
msgid "No recurring rule defined"
msgstr "No se definió una regla de repetición"

#: js/RrulePanel.js:72
msgid "None"
msgstr "Ninguno"

#: js/RrulePanel.js:28
msgid "Recurrances"
msgstr "Repeticiones"

#: js/SearchCombo.js:80
msgid "Search Event"
msgstr "Buscar evento"

#: js/SearchCombo.js:71
msgid "Searching..."
msgstr "Buscando..."

#: js/RrulePanel.js:381
msgid "Until has to be after event start"
msgstr "Debe ocurrir hasta que el evento comience"

#: js/WestPanel.js:55
msgid "WK"
msgstr "SEM"

#: js/Printer/DaysView.js:36 js/Printer/GridViewAlternative.js:38
msgid "Week {0} :"
msgstr "Semana {0}:"

#: js/RrulePanel.js:86
msgid "Weekly"
msgstr "Semanalmente"

#: js/RrulePanel.js:100
msgid "Yearly"
msgstr "Anualmente"

#: js/RrulePanel.js:262
msgid "after {0} occurrences"
msgstr "después de {0} ocurrencias"

#: js/RrulePanel.js:241
msgid "at"
msgstr "a"

#: js/RrulePanel.js:521 js/RrulePanel.js:573 js/RrulePanel.js:721
#: js/RrulePanel.js:774
msgid "at the"
msgstr "en el (la)"

#: js/RrulePanel.js:540 js/RrulePanel.js:740
msgid "first"
msgstr "primero"

#: js/RrulePanel.js:543 js/RrulePanel.js:743
msgid "fourth"
msgstr "cuarto"

#: js/RrulePanel.js:544 js/RrulePanel.js:744
msgid "last"
msgstr "último"

#: js/RrulePanel.js:253
msgid "never"
msgstr "nunca"

#: js/SearchCombo.js:70
msgid "no events found"
msgstr "no se encontró ningún evento"

#: js/RrulePanel.js:826
msgid "of"
msgstr "de"

#: js/RrulePanel.js:541 js/RrulePanel.js:741
msgid "second"
msgstr "segundo"

#: js/RrulePanel.js:542 js/RrulePanel.js:742
msgid "third"
msgstr "tercero"
