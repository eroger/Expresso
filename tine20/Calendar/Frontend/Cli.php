<?php
/**
 * Tine 2.0
 * @package     Calendar
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009-2013 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * Cli frontend for Calendar
 *
 * This class handles cli requests for the Calendar
 *
 * @package     Calendar
 */
class Calendar_Frontend_Cli extends Tinebase_Frontend_Cli_Abstract
{
    /**
     * the internal name of the application
     * 
     * @var string
     */
    protected $_applicationName = 'Calendar';
    
    /**
     * help array with function names and param descriptions
     * 
     * @return void
     */
    protected $_help = array(
        'importegw14' => array(
            'description'   => 'imports calendars/events from egw 1.4',
            'params'        => array(
                'host'     => 'dbhost',
                'username' => 'username',
                'password' => 'password',
                'dbname'   => 'dbname'
            )
        ),
        'exportICS' => array(  
            'description' => "export calendar as ics", 
            'params' => array('container_id') 
        ),
    );
    
    /**
     * import events
     *
     * @param Zend_Console_Getopt $_opts
     */
    public function import($_opts)
    {
        parent::_import($_opts);
    }
    
    /**
     * exports calendars as ICS
     *
     * @param Zend_Console_Getopt $_opts
     */
    public function exportICS($_opts)
    {
        $opts = $_opts->getRemainingArgs();
        $container_id = $opts[0];
        $filter = new Calendar_Model_EventFilter(array(
            array(
                'field'     => 'container_id',
                'operator'  => 'equals',
                'value'     => $container_id
            )

        ));
        $result = Calendar_Controller_MSEventFacade::getInstance()->search($filter, null, false, false, 'get');
        $converter = Calendar_Convert_Event_VCalendar_Factory::factory("generic");
        $result = $converter->fromTine20RecordSet($result);
        print $result->serialize();
    }
    
    /**
     * delete duplicate events
     *  - allowed params:
     *      organizer=ORGANIZER_CONTACTID (equals)
     *      created_by=USER_ID (equals)
     *      dtstart="2014-10-28" (after)
     *      summary=EVENT_SUMMARY (contains)
     *      -d (dry run)
     *
     * @param Zend_Console_Getopt $opts
     *
     * @see 0008182: event with lots of exceptions breaks calendar sync
     *
     * @todo allow to pass json encoded filter
     */
    public function deleteDuplicateEvents($opts)
    {
        $this->_addOutputLogWriter(6);

        $args = $this->_parseArgs($opts);

        $be = new Calendar_Backend_Sql();
        $filterData = array(array(
            'field'    => 'dtstart',
            'operator' => 'after',
            'value'    => isset($args['dtstart']) ? new Tinebase_DateTime($args['dtstart']) : Tinebase_DateTime::now(),
        ));
        if (isset($args['organizer'])) {
            $filterData[] = array(
                'field'    => 'organizer',
                'operator' => 'equals',
                'value'    => $args['organizer'],
            );
        }
        if (isset($args['created_by'])) {
            $filterData[] = array(
                'field'    => 'created_by',
                'operator' => 'equals',
                'value'    => $args['created_by'],
            );
        }
        if (isset($args['summary'])) {
            $filterData[] = array(
                'field'    => 'summary',
                'operator' => 'contains',
                'value'    => $args['summary'],
            );
        }
        $filter = new Calendar_Model_EventFilter($filterData);

        $result = $be->deleteDuplicateEvents($filter, $opts->d);
        exit($result > 0 ? 1 : 0);
    }
    
    /**
     * repair dangling attendee records (no displaycontainer_id)
     * 
     * @see https://forge.tine20.org/mantisbt/view.php?id=8172
     */
    public function repairDanglingDisplaycontainerEvents()
    {
        $writer = new Zend_Log_Writer_Stream('php://output');
        $writer->addFilter(new Zend_Log_Filter_Priority(5));
        Tinebase_Core::getLogger()->addWriter($writer);
        
        $be = new Calendar_Backend_Sql();
        $be->repairDanglingDisplaycontainerEvents();
    }

    /**
     * move external organizer events to new containers
     *
     */
    public function migrateExternalEvents()
    {

        $backend = Setup_Backend_Factory::factory();
        $setup = new Calendar_Setup_Update_Release7($backend);
        if ($setup->getApplicationVersion('Calendar') != '7.5') {
            echo "\nExternal events migration can be executed only in 7.5 version of Calendar application\n";
            return;
        }

        $config = Tinebase_Config::getInstance()->get('externalEventId');

        if ($config && $config['externalEventId'] && $config['externalEventId'] != 0) {
            try {
                $containerId = (int) $config['externalEventId'];
                $externalContainerId = Tinebase_Container::getInstance()->getContainerById($containerId)->id;

                echo "\nStarting moving external events from container with id=" . $externalContainerId . "\n";

                $eventBE = new Tinebase_Backend_Sql(array(
                    'modelName'    => 'Calendar_Model_Event',
                    'tableName'    => 'cal_events',
                    'modlogActive' => false
                ));
                $db = Tinebase_Core::getDb();

                // find all events from old external container
                $eventIds = $db->query(
                        "SELECT " . $db->quoteIdentifier('id') .
                        " FROM " . $db->quoteIdentifier(SQL_TABLE_PREFIX . "cal_events") .
                        " WHERE " . $db->quoteInto($db->quoteIdentifier("container_id") . ' = ?', $externalContainerId)
                )->fetchAll(Zend_Db::FETCH_ASSOC);

                $eventsCounter = 0;
                foreach ($eventIds as $eventId) {
                    $event = $eventBE->get($eventId['id']);

                    if (!empty($event->organizer)) {
                        $organizer = $event->resolveOrganizer();
                        if ($organizer instanceof Addressbook_Model_Contact) {
                            $newContainer = Calendar_Controller::getInstance()->getInvitationContainer($organizer);

                            $where  = array(
                                $db->quoteInto($db->quoteIdentifier('id') . ' = ?', $eventId),
                            );

                            try {
                                $db->update(SQL_TABLE_PREFIX . "cal_events", array('container_id' => $newContainer->getId()), $where);
                                $eventsCounter++;
                            } catch (Tinebase_Exception_Record_Validation $terv) {
                                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                                    . ' Could not fix invalid event record: ' . print_r($event->toArray(), true));
                                Tinebase_Exception::log($terv);
                            }
                        }
                    }
                }

                echo $eventsCounter . " event(s) moved!\n";

            } catch (Tinebase_Exception_NotFound $tenf) {
                Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' External events container not found. Nothing to do.' );
                echo "External events container not found. Is it configured and exists in database?\n";
            }
        } else {
            Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' External events container not found. Nothing to do.' );
            echo "External events container not found. Is it configured and exists in database?\n";
        }

        $setup->setApplicationVersion('Calendar', '7.6');
    }
}
