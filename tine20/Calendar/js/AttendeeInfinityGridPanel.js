/*
 * Tine 2.0
 *
 * @package     Calendar
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

Ext.ns('Tine.Calendar');

/**
 * @namespace   Tine.Calendar
 * @class       Tine.Calendar.AttendeeInfinityGridPanel
 * @extends     Ext.grid.EditorInfinityGridPanel
 * @author      Gabriel Malheiros <gabriel.malheiros@serpro.gov.br>
 */
Tine.Calendar.AttendeeInfinityGridPanel = Ext.extend(Ext.grid.EditorGridPanel, {
    autoExpandColumn: 'freebusy',
    clicksToEdit: 1,
    enableHdMenu: false,

    /**
     * @cfg defaut text for new attendee combo
     * _('Click here to invite another attender...')
     */
    addNewAttendeeText: 'Click here to invite another attender...',

    /**
     * @cfg {String} loadMaskText
     * _('Loading events, please wait...')
     */
    loadMaskText: 'Loading FreeBusy, please wait...',

    /**
     * @cfg {Boolean} showGroupMemberType
     * show user_type groupmember in type selection
     */
    showMemberOfType: false,

    /**
     * @cfg {Boolean} showNamesOnly
     * true to only show types and names in the list
     */
    showNamesOnly: false,

    /**
     * The record currently being edited
     *
     * @type Tine.Calendar.Model.Event
     * @property record
     */
    record: null,

    /**
     * id of current account
     *
     * @type Number
     * @property currentAccountId
     */
    currentAccountId: null,

    /**
     * ctx menu
     *
     * @type Ext.menu.Menu
     * @property ctxMenu
     */
    ctxMenu: null,

    /**
     * Events of user to freebuzy
     * @type Tine.Calendar.Model.Event
     *
     */
    eventsStore: null,

    /**
     * @cfg {Number} freebusyColumn
     * width of Freebusy Column
     */
    freebusyColumn: 982,

    /**
     * @cfg {Date} startDate
     * start date
     */
    startDate: new Date(),
    /**
     * @cfg {Date} dtStart
     * start date of the event
     */
    dtStart: null,
    /**
     * @cfg {Date} dtEnd
     * end date of the event
     */
    dtEnd: null,
    /**
     * @cfg {boolean} is_all_day_event
     * is all day event?
     */
    isAllDayEvent: false,
     /**
     * @cfg {Number} numOfDays
     * number of days to display
     */
    numOfDays: 14,
    /**
     * @cfg {String} newEventSummary
     * _('New Event')
     */
    newEventSummary: 'New Event',
    /**
     * @cfg {String} dayFormatString
     * _('{0}, the {1}. of {2}')
     */
    dayFormatString: '{0}, the {1}. of {2}',
    /**
     * @cfg {Number} timeGranularity
     * granularity of timegrid in minutes
     */
    timeGranularity: 60,
    /**
     * @cfg {Number} granularityUnitHeights
     * heights in px of a granularity unit
     */
    granularityUnitHeights: 18,
    /**
     * @cfg {Number} granularityUnitWidht
     * widht in px of a granularity unit time on freebusy tab
     */
    granularityUnitWidht: 80,
    /**
     * @cfg {Boolean} denyDragOnMissingEditGrant
     * deny drag action if edit grant for event is missing
     */
    denyDragOnMissingEditGrant: true,
    /**
     * store holding timescale
     * @type {Ext.data.Store}
     */
    timeScale: null,
    /**
     * store holding dayscale
     * @type {Ext.data.Store}
     */
    dayScale: null,
    /**
     * The amount of space to reserve for the scrollbar (defaults to 19 pixels)
     * @type {Number}
     */
    scrollOffset: 19,
    /**
     * @property {bool} editing
     * @private
     */
    editing: false,
    /**
     * currently active event
     * $type {Tine.Calendar.Model.Event}
     */
    activeEvent: null,
    /**
     * @property {Ext.data.Store}
     * @private
     */
    ds: null,
    /**
     * @cfg {Number} workweekday
     * days indexs for which day of the week should be show
     */
    work: 127,
    /**
     * @cfg {string} StartDayTime
     * start hour to show in sheet week and day view
    */
    startDayTime: '00',
    /**
     * @cfg {string} EndDayTime
     * End hour to show in sheet week and day view
    */
    endDayTime: '24',
    /**
     * @cfg {Number} PrintConvert
     * Const to ajust the print to the page size
    */
    printConvert: 1,
    /**
     * @cfg {Number} NumberWholeDayEvents
     * Const to ajust the print to the page size
    */
    numberWholeDayEvents: 0,
    /**
     * @cfg {Number} columnWidht
     * column Widht of freebusy on grid
    */
    columnWidht: 866,

    /**
     * @cfg {Number} mousedownx
     * mouse down px for do the drag scroll
    */
    mousedownx: 0,

    /**
     * @cfg {Number} hposition
     * horizontal position of event time in overflow bar
    */
    hposition: 0,

    /**
     * @cfg {Number} scrollDay
     * day show on scroll horizontal bar
    */
    scrollDay: 0,

    /**
     * @cfg {Number} visibleGridRows
     * number of rows that is visible in scroll grid
    */
    visibleGridRows: 0,

    /**
     * @cfg {Number} scrollTimePosition
     * the position of the bar  on freeBusy View overload
    */
    scrollTimePosition: 0,

    /**
     *@cfg {bollean} freebusyscrollbuttom
     * bottom for scroll in freebusy grid
    */
    freebusyscrollbuttom: false,

    /**
     *@cfg {bollean} loadstore
     * indicate if the store is loaded.
    */
    loadstore: false,

    /**
     *@cfg {bollean} loadstore
     * indicate if the eventstore has to be load.
    */
    loadeventstore: false,

    /**
     * store to hold all attendee
     *
     * @type Ext.data.Store
     * @property attendeeStore
     */
    attendeeStore: null,

    stateful: true,
    stateId: 'cal-attendeeinfinitygridpanel',

    initComponent: function() {
        this.app = Tine.Tinebase.appMgr.get('Calendar');

        this.recordClass = Tine.Calendar.Model.Event;
        if(!Ext.isDate(this.startDate)){
            this.startDate = new Date.parseDate(this.startDate,"Y-m-d H:i:s");
        }
        this.loadMaskText = this.app.i18n._hidden(this.loadMaskText);
        this.currentAccountId = Tine.Tinebase.registry.get('currentAccount').accountId;
        this.dayFormatString  = this.app.i18n._hidden(this.dayFormatString);
        this.title = this.app.i18n._('FreeBusy');
        this.plugins = this.plugins || [];
        if (! this.showNamesOnly) {
            this.plugins.push(new Ext.ux.grid.GridViewMenuPlugin({}));
        }

        this.eventsStore = new Ext.data.JsonStore({
                id: 'events',
                fields: Tine.Calendar.Model.Event,
                proxy: Tine.Calendar.backend,
                reader: new Ext.data.JsonReader({}),
                listeners: {
                    scope: this,
                    'beforeload': this.onStoreBeforeload,
                    'load': this.onStoreLoad,
                    'loadexception': this.onStoreLoadException
                }
        });
        this.defaultFilters = [
            {field: 'attender', operator: 'in' , value: []},
            {field: 'attender_status', operator: 'notin', value: ['DECLINED']}
        ];
        this.filterToolbar =  this.getFilterToolbar();

        this.store = new Ext.data.SimpleStore({
            fields: Tine.Calendar.Model.Attender.getFieldDefinitions().concat('sort'),
            sortInfo: {field: 'user_id', direction: 'ASC'},
            sortData : function(f, direction){
                direction = direction || 'ASC';
                var st = this.fields.get(f).sortType;
                var fn = function(r1, r2){
                    // make sure new-attendee line is on the bottom
                    if (!r1.data.user_id) return direction == 'ASC';
                    if (!r2.data.user_id) return direction != 'ASC';
                    var v1 = st(r1.data[f]), v2 = st(r2.data[f]);
                    return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
                };
                this.data.sort(direction, fn);
                if(this.snapshot && this.snapshot != this.data){
                    this.snapshot.sort(direction, fn);
                }
            }
        });

        this.startDayTime = this.app.getRegistry().get('preferences').get('daystimeviewstarttime');
        this.startDayTime = this.startDayTime.substring(0,(this.startDayTime.length - 2));
        this.endDayTime = this.app.getRegistry().get('preferences').get('daystimeviewendtime');
        this.endDayTime = this.endDayTime.substring(0,(this.endDayTime.length - 2));
        this.composeGranularityTime();

        this.on('beforeedit', this.onBeforeAttenderEdit, this);
        this.on('afteredit', this.onAfterAttenderEdit, this);
        this.on('mouseup', this.onMouseUp, this);
        this.on('keydown', this.onKeyDown, this);
        this.on('mouseover', this.onMouseOver, this);
        this.on('columnresize', this.onResize, this);

        this.initTemplates();
        this.initTimeScale();
        this.initColumns();

        this.mon(Ext.getBody(), 'click', this.stopEditingIf, this);

        this.viewConfig = {
            getRowClass: this.getRowClass
        };

        Tine.Calendar.AttendeeInfinityGridPanel.superclass.initComponent.call(this);
    },

    initColumns: function() {
        this.columns = [{
            id: 'user_type',
            dataIndex: 'user_type',
            width: 40, // if change , change the afterrender function
            sortable: true,
            resizable: false,
            hideable: false,
            header: this.app.i18n._('Type'),
            renderer: this.renderAttenderType.createDelegate(this),
            editor: new Ext.form.ComboBox({
                blurOnSelect  : true,
                expandOnFocus : true,
                mode          : 'local',
                store         : [
                    ['user',     this.app.i18n._('User')   ],
                    ['resource', this.app.i18n._('Resource')]
                ]
            })
        }, {
            id: 'user_id',
            dataIndex: 'user_id',
            header: this.app.i18n._('Name'),
            width: 150,  // if change , change the afterrender function
            resizable: false,
            hideable: false,
            renderer: this.renderAttenderName.createDelegate(this),
            editor: true
        }, {
            id: 'freebusy',
            dataIndex: 'freebusy',
            resizable: false,
            hideable: false,
            header: this.getFreebusyHeaders(),
            renderer: this.renderFreebusy.createDelegate(this)
        }, {
            id: 'status',
            dataIndex: 'status',
            width: 100,
            sortable: true,
            header: this.app.i18n._('Status'),
            resizable: false,
            hidden: false,
            renderer: this.renderAttenderStatus.createDelegate(this),
            editor: {
                xtype: 'widget-keyfieldcombo',
                app:   'Calendar',
                keyFieldName: 'attendeeStatus'
            }
        }];
    },

    validateInsertStore: function(o){
        var isDuplicate = false;
        this.store.each(function(attender) {
            if (o.record.getUserId() == attender.getUserId()
                    && o.record.get('user_type') == attender.get('user_type')
                    && o.record != attender) {
                attender.set('checked', true);
                var row = this.getView().getRow(this.store.indexOf(attender));
                Ext.fly(row).highlight();
                isDuplicate = true;
                return false;
            }
        }, this);

        if (isDuplicate) {
            o.record.reject();
            this.startEditing(o.row, o.column);
        } else if (o.value) {
            // set status authkey for contacts and recources so user can edit status directly
            // NOTE: we can't compute if the user has editgrant to the displaycontainer of an account here!
            //       WELL we could add the info to search attendee somehow
            if (   (o.record.get('user_type') == 'user' && ! o.value.account_id )
                || (o.record.get('user_type') == 'resource' && o.record.get('user_id') && o.record.get('user_id').container_id && o.record.get('user_id').container_id.account_grants && o.record.get('user_id').container_id.account_grants.editGrant)) {
                o.record.set('status_authkey', 1);
            }

            o.record.explicitlyAdded = true;
            o.record.set('checked', true);

            var newAttender = new Tine.Calendar.Model.Attender(Tine.Calendar.Model.Attender.getDefaultData(), 'new-' + Ext.id() );
            this.store.add([newAttender]);
            this.startEditing(o.row +1, o.column);

            if (o.record.get('user_id').account_id === null) {
                Ext.Msg.show({
                    title: this.app.i18n._('External Attendee'),
                    msg: this.app.i18n._('You have selected an external attendee!'),
                    icon: Ext.MessageBox.INFO,
                    buttons: Ext.Msg.OK,
                    scope: this
                });
            }
        }
    },

    /**
     * @private
     * @param {Tine.Calendar.Model.Event} event
     */
    insertEvent: function(event) {
        if ( event.get('transp') == 'OPAQUE' ) {
            event.ui = new Tine.Calendar.FreebusyViewEventUI(event);
            event.ui.render(this);
        }
    },

    /**
     * @private
     * @param {Tine.Calendar.Model.Event} event
     */
    insertEventTime: function(start,end) {
        var numElements = this.el.select('.freebusy-col-line').elements.length -1;
        Ext.each(this.el.select('.freebusy-col-line').elements, function(row) {
            if (this.el.select('.freebusy-col-line').elements[numElements] != row ) {
                var baseDate = this.startDate.clone();
                this.dtStart = start;
                this.dtEnd = end;
                if(!Ext.isDate(this.dtStart)) {
                    this.dtStart = new Date.parseDate(this.dtStart,"Y-m-d H:i:s");
                }
                if(!Ext.isDate(this.dtEnd)) {
                    this.dtEnd = new Date.parseDate(this.dtEnd,"Y-m-d H:i:s");
                }
                if(this.dtEnd.getHours() > this.endDayTime) {
                    var freeBusyDtend = this.dtEnd;
                    freeBusyDtend.clearTime(true);
                    freeBusyDtend.setHours(this.endDayTime);
                    freeBusyDtend.setMinutes('00');
                    this.dtEnd = freeBusyDtend;
                }
                if(this.dtStart.getHours() < this.startDayTime) {
                    var freeBusyDtstart = this.dtStart;
                    freeBusyDtstart.setHours(this.startDayTime);
                    freeBusyDtstart.setMinutes('00');
                    this.dtStart = freeBusyDtstart;
                }
                var startDay = Math.floor((this.dtStart.getTime() - this.startDate.getTime())/(1000*60*60*24));
                var endDay = Math.floor((this.dtEnd.getTime() - this.startDate.getTime())/(1000*60*60*24));
                var granularityBrowserUnitWidht;
                if(Ext.isGecko) {
                    granularityBrowserUnitWidht = this.granularityUnitWidht +1.11;
                } else {
                    granularityBrowserUnitWidht = this.granularityUnitWidht +1;
                }
                if (this.isAllDayEvent) {
                    var widht = this.timeScale.getCount() * granularityBrowserUnitWidht;
                    var startPosition = 0;
                } else {
                    if ( startDay == endDay){
                        var widht = (granularityBrowserUnitWidht / this.timeGranularity) * ((this.dtEnd.getTime() - this.dtStart.getTime())/60000);
                    } else {
                        var j = (endDay - startDay)-1;
                        var setTimeDate = Date.parseDate(this.dtStart.format('Y-m-d') + ' ' + this.endDayTime  + ':00:00', Date.patterns.ISO8601Long);
                        var riseTimeDate = Date.parseDate(this.dtEnd.format('Y-m-d') + ' ' + this.startDayTime  + ':00:00', Date.patterns.ISO8601Long);
                        var widht = ((granularityBrowserUnitWidht/ this.timeGranularity) * ((setTimeDate.getTime() - this.dtStart.getTime())/60000)) + ((this.timeScale.getCount() * j * granularityBrowserUnitWidht)) + ((this.granularityUnitWidht / this.timeGranularity) * ((this.dtEnd.getTime() - riseTimeDate.getTime())/60000));
                    }
                    for (var i = 0; i < this.timeScale.getCount(); i++) {
                        var currentDate = Date.parseDate(baseDate.add(Date.DAY, 1).format('Y-m-d') + ' ' + this.timeScale.data.items[i].data.time + ':00', Date.patterns.ISO8601Long);
                        var currentMin = ((currentDate.getHours() * 60) + currentDate.getMinutes())-(this.timeGranularity/2);
                        if( i == (this.timeScale.getCount()-1)){
                              var nextDate = Date.parseDate(baseDate.add(Date.DAY, 1).format('Y-m-d') + ' ' + this.timeScale.data.items[i].data.time + ':00', Date.patterns.ISO8601Long);
                              if(nextDate.getMinutes() != 0 ) {
                                 var nextMin = ((nextDate.getHours() * 60) + nextDate.getMinutes())+(this.timeGranularity/2);
                              } else {
                                 var nextMin = ((nextDate.getHours() * 60))+(this.timeGranularity);
                              }
                        } else {
                            var nextDate = Date.parseDate(baseDate.add(Date.DAY, 1).format('Y-m-d') + ' ' + this.timeScale.data.items[i+1].data.time + ':00', Date.patterns.ISO8601Long);
                            var nextMin = ((nextDate.getHours() * 60) + nextDate.getMinutes())-(this.timeGranularity/2);
                        }
                        var dtStartMin = (this.dtStart.getHours() * 60) + this.dtStart.getMinutes();
                        if( dtStartMin >= currentMin && dtStartMin < nextMin ) {
                            var startPosition = (((this.granularityUnitWidht / this.timeGranularity) * (dtStartMin - currentMin)) + (i * granularityBrowserUnitWidht)) + (this.timeScale.getCount() * startDay * granularityBrowserUnitWidht);
                            break;
                        }
                    }
                }
                var freebusyRow = Ext.get(row);
                var domId = Ext.id() + '-time:' + freebusyRow.id;
                this.scrollTimePosition = startPosition -200;
                var eventEl = this.templates.timeEvent.append(freebusyRow, {
                     id: domId,
                     height: 100 + '%',
                     left: startPosition + 'px',
                     width: widht - 4 + 'px'
                }, true);
            }
        }, this);

    },

    onStoreLoad: function() {
        this.hideMask();
        this.insertEventTime(this.dtStart,this.dtEnd);
        this.eventsStore.each(this.insertEvent, this);
    },

    onRefresh: function() {
        this.scrollTimePosition = 0;
        this.updateFreebusyHeader();
        this.insertEventTime(this.dtStart,this.dtEnd);
        this.eventsStore.each(this.insertEvent, this);
    },

    updateEventTime: function(){
        Ext.each(this.el.select('.freebusy-event-time').elements, function(row) {
            Ext.get(row).remove();
        }, this);
        this.insertEventTime(this.dtStart,this.dtEnd);
    },

    getCustomfieldFilters: Tine.widgets.grid.GridPanel.prototype.getCustomfieldFilters,
    getFilterToolbar: Tine.widgets.grid.GridPanel.prototype.getFilterToolbar,
    /**
     * called before store queries for data
     */
    onStoreBeforeload: function (store, options) {
        options.params = options.params || {};

        // define a transaction
        this.lastStoreTransactionId = options.transactionId = Ext.id();

        // allways start with an empty filter set!
        // this is important for paging and sort header!
        options.params.filter = [];

        this.filterToolbar.onBeforeLoad.call(this.filterToolbar, store, options);
        try {
            if(!options.add){
                var filterPanel = this.filterToolbar.activeFilterPanel;
                this.store.each(function(attender) {
                    options.params.filter[0].filters[0].filters[0].value.push(attender.data);
                }, this);
            } else {
                var filterPanel = this.filterToolbar.activeFilterPanel;
                options.params.filter[0].filters[0].filters[0].value.pop();
                options.params.filter[0].filters[0].filters[0].value.push(this.store.getAt(this.store.getCount() - 2).data);
            }
        } catch (e) {}

        // add period filter as last filter to not conflict with first OR filter
        if (! options.noPeriodFilter) {
            options.params.filter.push({field: 'period', operator: 'within', value: {
                from: this.startDate,
                until: this.endDate
            }});

        }
        this.showMask();
    },

    onStoreLoadException: function(e) {
        this.hideMask();
        Ext.MessageBox.alert('Error',this.app.i18n._('Error when loading FreeBuzy'));
    },
    onAfterAttenderEdit: function(o) {
        switch (o.field) {
            case 'user_id' :
                if (o.value && o.record.get('user_id') && o.record.get('user_id').account_id === null) {
                    Ext.MessageBox.wait(_('Please wait'), this.app.i18n._('Verifying account'));
                    var filter = [{field: 'email', operator: 'equals', value: o.record.get('user_id').email}];
                    filter.push({field: 'type', operator: 'equals', value: 'user'});
                    Tine.Addressbook.searchContacts(filter, null, function(response) {
                        Ext.MessageBox.hide();
                        if (response.results.length > 0) {
                            var contact = new Tine.Calendar.Model.Attender(response.results[0]);
                            o.record.reject();
                            o.record.set('user_id',contact.data);
                        }
                        this.validateInsertStore(o);
                    }, this);
                } else {
                    this.validateInsertStore(o);
                    Ext.each(this.el.select('.freebusy-col').elements, function(row) {
                        Ext.get(row).scrollTo("r", 0);
                        this.scrollTimePosition = 0;
                    }, this);
                    this.setTextHeaderLocation();
                    this.getView().refresh(false);
                    this.eventsStore.load({add:true});
                }
                break;
            case 'user_type' :
                this.startEditing(o.row, o.column +1);
                break;
        }
    },

    onBeforeAttenderEdit: function(o) {
        if (o.field == 'status') {
            // allow status setting if status authkey is present
            o.cancel = ! o.record.get('status_authkey');
            return;
        }

        if (o.field == 'displaycontainer_id') {
            if (! o.value || ! o.value.account_grants || ! o.value.account_grants.deleteGrant) {
                o.cancel = true;
            }
            return;
        }

        // for all other fields user need editGrant
        if (! this.record.get('editGrant')) {
            o.cancel = true;
            return;
        }

        // don't allow to set anything besides quantity and role for already set attendee
        if (o.record.get('user_id')) {
            o.cancel = true;
            if (o.field == 'quantity' && o.record.get('user_type') == 'resource') {
                o.cancel = false;
            }
            if (o.field == 'role') {
                o.cancel = false;
            }
            return;
        }

        if (o.field == 'user_id') {
            // switch editor
            var colModel = o.grid.getColumnModel();
            switch(o.record.get('user_type')) {
                case 'user' :
                colModel.config[o.column].setEditor(new Tine.Addressbook.SearchCombo({
                    allowBlank: true,
                    blurOnSelect: true,
                    selectOnFocus: true,
                    forceSelection: false,
                    verifyUserAccount: true,
                    renderAttenderName: this.renderAttenderName,
                    getValue: function() {
                        var value = this.selectedRecord ? this.selectedRecord.data : ((this.getRawValue() && Ext.form.VTypes.email(this.getRawValue())) ? this.getRawValue() : null);
                        return value;
                    }
                }));
                break;
                case 'resource':
                colModel.config[o.column].setEditor(new Tine.Tinebase.widgets.form.RecordPickerComboBox({
                    minListWidth: 350,
                    blurOnSelect: true,
                    recordClass: Tine.Calendar.Model.Resource,
                    getValue: function() {
                        return this.selectedRecord ? this.selectedRecord.data : null;
                    }
                }));
                break;
            }
            colModel.config[o.column].editor.selectedRecord = null;
        }
    },

    /*
     * test if time pass, conflict with any event in eventStore
     */
    hasConflictEvent: function(start,end) {
        var conflict = false;
        this.eventsStore.each(function(event) {
            if( event.data.transp == 'OPAQUE' ) {
                if(event.data.dtstart < start && start < event.data.dtend) {
                    conflict = true;
                    return conflict;
                } else if (end < event.data.dtend && end > event.data.dtstart) {
                    conflict = true;
                    return conflict;
                } else if (event.data.dtstart > start && event.data.dtstart < end && event.data.dtend > start && event.data.dtend < end ) {
                    conflict = true;
                    return conflict;
                } else if (event.data.dtstart.getTime() == start.getTime()) {
                    conflict = true;
                    return conflict;
                } else if (end.getTime() == event.data.dtend.getTime()) {
                    conflict = true;
                    return conflict;
                } else if (start.getDate() == event.data.dtstart.getDate() && event.data.is_all_day_event) {
                    conflict = true;
                    return conflict;
                }
           }
        }, this);
        return conflict;
    },

    previusButton: function(start,end) {
        if (!this.hasConflictEvent(start,end)) {
            if (start.getTime() < Date.parseDate(this.dtStart.add(Date.DAY, 0).format('Y-m-d') + ' ' + this.startDayTime  + ':00:00', Date.patterns.ISO8601Long).getTime()) {
                var diff = (this.dtEnd.getTime() - this.dtStart.getTime())/60000;
                this.dtEnd = Date.parseDate(this.dtEnd.add(Date.DAY, -1).format('Y-m-d') + ' ' + this.endDayTime  + ':00:00', Date.patterns.ISO8601Long);
                this.dtStart = this.dtEnd.add(Date.MINUTE,-diff);
                return this.previusButton(this.dtStart,this.dtEnd);
            } else if (start.getTime() >= this.startDate.getTime()) {
                this.dtStart = start;
                this.dtEnd = end;
                return this;
            } else {
                return false;
            }
        } else {
            if (start.getTime() < Date.parseDate(this.dtStart.add(Date.DAY, 0).format('Y-m-d') + ' ' + this.startDayTime  + ':00:00', Date.patterns.ISO8601Long).getTime()) {
                var diff = (this.dtEnd.getTime() - this.dtStart.getTime())/60000;
                this.dtEnd = Date.parseDate(this.dtEnd.add(Date.DAY, -1).format('Y-m-d') + ' ' + this.endDayTime  + ':00:00', Date.patterns.ISO8601Long);
                this.dtStart = this.dtEnd.add(Date.MINUTE,-diff);
                return this.previusButton(this.dtStart,this.dtEnd);
            } else if (start.getTime() >= this.startDate.getTime()) {
                return this.previusButton(start.add(Date.MINUTE,-this.timeGranularity), end.add(Date.MINUTE,-this.timeGranularity));
            } else {
                return false;
            }
        }
    },

    nextButton: function(start,end) {
        if (!this.hasConflictEvent(start,end)) {
            if (end.getTime() > Date.parseDate(this.dtEnd.add(Date.DAY, 0).format('Y-m-d') + ' ' + this.endDayTime  + ':00:00', Date.patterns.ISO8601Long).getTime()) {
                var diff = (this.dtEnd.getTime() - this.dtStart.getTime())/60000;
                this.dtStart = Date.parseDate(this.dtStart.add(Date.DAY, 1).format('Y-m-d') + ' ' + this.startDayTime  + ':00:00', Date.patterns.ISO8601Long);
                this.dtEnd = this.dtStart.add(Date.MINUTE,diff);
                return this.nextButton(this.dtStart,this.dtEnd);
            } else if (end.getTime() < this.startDate.add(Date.DAY,this.numOfDays).getTime()) {
                this.dtStart = start;
                this.dtEnd = end;
                return this;
            } else {
                return false;
            }
        } else {
            if (end.getTime() > Date.parseDate(this.dtEnd.add(Date.DAY, 0).format('Y-m-d') + ' ' + this.endDayTime  + ':00:00', Date.patterns.ISO8601Long).getTime()) {
                diff = (this.dtEnd.getTime() - this.dtStart.getTime())/60000;
                this.dtStart = Date.parseDate(this.dtStart.add(Date.DAY, 1).format('Y-m-d') + ' ' + this.startDayTime  + ':00:00', Date.patterns.ISO8601Long);
                this.dtEnd = this.dtStart.add(Date.MINUTE,diff);
                return this.previusButton(this.dtStart,this.dtEnd);
            } else if (end.getTime() < this.startDate.add(Date.DAY,this.numOfDays).getTime()) {
                return  this.nextButton(start.add(Date.MINUTE,this.timeGranularity), end.add(Date.MINUTE,this.timeGranularity));
            } else {
                return false;
            }
        }
    },

    /**
     * give new attendee an extra cls
     */
    getRowClass: function(record, index) {
        if (! record.get('user_id')) {
            return 'x-cal-add-attendee-row';
        }
    },

    /**
     * stop editing if user clicks else where
     *
     * FIXME this breaks the paging in search combos, maybe we should check if search combo paging buttons are clicked, too
     */
    stopEditingIf: function(e) {
        if (! e.within(this.getGridEl())) {
        }
    },

    onMouseUp: function(e, target)  {

        if ((e.xy[0] - this.mousedownx)> 50 ){
          if ((e.xy[0] - this.mousedownx)> 300 ) {
              this.scrollTimePosition -= 400;
              Ext.each(this.el.select('.freebusy-col').elements, function(row) {
                 Ext.get(row).scrollTo("r", this.scrollTimePosition, true);
              }, this);
              if ( this.scrollTimePosition < 0 ) {
                 this.scrollTimePosition = 0;
              }
              this.setTextHeaderLocation();
          } else {
              this.scrollTimePosition -= 100;
              Ext.each(this.el.select('.freebusy-col').elements, function(row) {
                  Ext.get(row).scrollTo("r", this.scrollTimePosition, true);
              }, this);
              if ( this.scrollTimePosition < 0 ) {
                  this.scrollTimePosition = 0;
              }
              this.setTextHeaderLocation();
          }
        }
        if (this.mousedownx -e.xy[0] > 50){
            if (this.mousedownx - e.xy[0] > 300) {
                this.scrollTimePosition += 400;
                Ext.each(this.el.select('.freebusy-col').elements, function(row) {
                    Ext.get(row).scrollTo("l", this.scrollTimePosition, true);
                }, this);
                if ( this.scrollTimePosition > (Ext.get(this.el.select('.freebusy-col-line').elements[0]).getWidth() - 1000)) {
                    this.scrollTimePosition = Ext.get(this.el.select('.freebusy-col-line').elements[0]).getWidth() - 1000;
                }
                this.setTextHeaderLocation();
            } else {
                this.scrollTimePosition += 100;
                Ext.each(this.el.select('.freebusy-col').elements, function(row) {
                    Ext.get(row).scrollTo("l", this.scrollTimePosition, true);
                }, this);
                if ( this.scrollTimePosition > (Ext.get(this.el.select('.freebusy-col-line').elements[0]).getWidth() - 1000)) {
                    this.scrollTimePosition = Ext.get(this.el.select('.freebusy-col-line').elements[0]).getWidth() - 1000;
                }
                this.setTextHeaderLocation();
            }
        }
    },

    onMouseDown: function(e, target)  {
        this.mousedownx = e.xy[0];
    },

    // NOTE: Ext docu seems to be wrong on arguments here
    onContextMenu: function(e, target) {
        e.preventDefault();
        var row = this.getView().findRowIndex(target);
        var attender = this.store.getAt(row);
        if (attender && ! this.disabled) {
            // don't delete 'add' row
            if (! attender.get('user_id')) {
                return;
            }

            this.ctxMenu = new Ext.menu.Menu({
                items: [{
                    text: this.app.i18n._('Remove Attender'),
                    iconCls: 'action_delete',
                    scope: this,
                    disabled: !this.record.get('editGrant'),
                    handler: function() {
                        this.eventsStore.each(function(event) {
                            if (event.get('container_id').owner_id == attender.get('user_id').account_id) {
                                this.eventsStore.remove(event);
                            }
                        }, this);
                        this.store.removeAt(row);
                    }
                }]
            });
            this.ctxMenu.showAt(e.getXY());
        }
    },

    /**
     * loads this panel with data from given record
     * called by edit dialog when record is loaded
     *
     * @param {Tine.Calendar.Model.Event} record
     * @param Array addAttendee
     */
    onRecordLoad: function(record) {
        this.record = record;
        this.store.removeAll();
        var attendee = record.get('attendee');
        Ext.each(attendee, function(attender) {

            var record = new Tine.Calendar.Model.Attender(attender, attender.id);
            this.store.addSorted(record);

            if (attender.displaycontainer_id  && this.record.get('container_id') && attender.displaycontainer_id.id == this.record.get('container_id').id) {
                this.eventOriginator = record;
            }
        }, this);

        this.eventsStore.load({});
        if (record.get('editGrant')) {
            // Add new attendee
            this.store.add([new Tine.Calendar.Model.Attender(Tine.Calendar.Model.Attender.getDefaultData(), 'new-' + Ext.id() )]);
        }
    },

    /**
     * Updates given record with data from this panel
     * called by edit dialog to get data
     *
     * @param {Tine.Calendar.Model.Event} record
     */
    onRecordUpdate: function(record) {
        this.stopEditing(false);

        var attendee = [];
        this.store.each(function(attender) {
            var user_id = attender.get('user_id');
            if (user_id) {
                if (typeof user_id.get == 'function') {
                    attender.data.user_id = user_id.data;
                }
               attendee.push(attender.data);
            }
        }, this);
        record.set('attendee', attendee);
    },

    onResize: function(newWidth, newHeight) {
       Tine.Calendar.AttendeeInfinityGridPanel.superclass.onResize.apply(this, arguments);
       if(this.freebusyscrollbuttom) {
        Ext.get(this.el.select('.freebusy-rightbuttom').elements[0]).remove();
        Ext.get(this.el.select('.freebusy-leftbuttom').elements[0]).remove();
        this.freebusyscrollbuttom = false;
       }
       var newWidht =  Ext.get(this.el.select('.x-grid3-td-freebusy').elements[0]).getWidth();
       Ext.each(this.el.select('.freebusy-col').elements, function(row) {
           Ext.get(row).setWidth(newWidht,true);
       }, this);
    },

    onHiddenChange: function(){
       if(this.freebusyscrollbuttom) {
           Ext.get(this.el.select('.freebusy-rightbuttom').elements[0]).remove();
           Ext.get(this.el.select('.freebusy-leftbuttom').elements[0]).remove();
           this.freebusyscrollbuttom = false;
       }
       var newWidht =  Ext.get(this.el.select('.x-grid3-td-freebusy').elements[0]).getWidth();
       Ext.each(this.el.select('.freebusy-col').elements, function(row) {
           Ext.get(row).setWidth(newWidht,true);
       }, this);
    },

    onKeyDown: function(e) {
        switch(e.getKey()) {

            case e.DELETE:
                if (this.record.get('editGrant')) {
                    var selection = this.getSelectionModel().getSelectedCell();

                    if (selection) {
                        var row = selection[0];

                        // don't delete 'add' row
                        var attender = this.store.getAt(row);
                        if (! attender.get('user_id')) {
                            return;
                        }

                        this.store.removeAt(row);
                    }
                }
                break;
           case e.LEFT:
                this.scrollTimePosition -= 100;
                Ext.each(this.el.select('.freebusy-col').elements, function(row) {
                   Ext.get(row).scrollTo("r", this.scrollTimePosition, true);
                }, this);
                if ( this.scrollTimePosition < 0 ) {
                    this.scrollTimePosition = 0;
                }
                this.setTextHeaderLocation();
                break;
           case e.RIGHT:
                this.scrollTimePosition += 100;
                Ext.each(this.el.select('.freebusy-col').elements, function(row) {
                    Ext.get(row).scrollTo("l", this.scrollTimePosition, true);
                }, this);
                if ( this.scrollTimePosition > (Ext.get(this.el.select('.freebusy-col-line').elements[0]).getWidth() - 1000)) {
                    this.scrollTimePosition = Ext.get(this.el.select('.freebusy-col-line').elements[0]).getWidth() - 1000;
                }
                this.setTextHeaderLocation();
                break;
        }
    },

    composeGranularityTime: function() {
        var diffTime = this.endDayTime - this.startDayTime;
        if ( diffTime < 4) {
            this.tbar[1].getStore().removeAt(3);
            this.tbar[1].getStore().removeAt(2);
            this.tbar[1].getStore().removeAt(1);
        } else if(diffTime < 12) {
            this.tbar[1].getStore().removeAt(3);
            this.tbar[1].getStore().removeAt(2);
        } else if(diffTime < 24) {
            this.tbar[1].getStore().removeAt(3);
        }
    },

    renderAttenderName: function(name, metaData, record) {
        if (name) {
            var type = record ? record.get('user_type') : 'user';
            return this['renderAttender' + Ext.util.Format.capitalize(type) + 'Name'].apply(this, arguments);
        }

        // add new user:
        if (arguments[1]) {
            arguments[1].css = 'x-form-empty-field';
            return this.app.i18n._(this.addNewAttendeeText);
        }
    },
    renderFreebusy: function(name, metaData, record) {

         if(!Ext.isDate(this.startDate)){
             this.startDate = new Date.parseDate(this.startDate,"Y-m-d H:i:s");
         }
         var width = this.freebusyColumn -2;
         this.app = Tine.Tinebase.appMgr.get('Calendar');
         this.scrollDay = 0;
         this.numOfDays = 14;

         this.startDate.setHours(0);
         this.startDate.setMinutes(0);
         this.startDate.setSeconds(0);
         if (this.startDate.getHours() == 23){
             this.startDate.setTime(this.startDate.getTime()+(3600000));
         }
         this.endDate = this.startDate.add(Date.DAY, this.numOfDays-1);

         if (this.endDate.getHours() == 01){
             this.endDate.setTime(this.startDate.getTime()-(3600000));
         }

         this.initTimeScale();
         this.updateFreebusyHeader();
         if (! this.selModel) {
             this.selModel = this.selModel || new Tine.Calendar.EventSelectionModel();
         }
         var totalwidth = (this.numOfDays * (this.granularityUnitWidht +1.1)  * this.timeScale.getCount()) + (this.numOfDays * 1.5);
         var timerows="";
         for (i=0;i<this.numOfDays;i++){
             timerows += this.getTimeRows();
         }
         this.getColumnModel().on('hiddenchange',this.onHiddenChange,this);
         return this.templates.freebusy.applyTemplate({
                 timeRows: timerows,
                 width: width + "px",
                 totalwidth: totalwidth + "px"
         });
    },

    /**
     * gets HTML fragment of the horizontal time columns for freebusy column
     */
    getTimeRows: function() {
        var html = '';
        var baseId = Ext.id();
        this.timeScale.each(function(time){
            var index = time.get('index');
            html += this.templates.timeRow.applyTemplate({
                width: this.granularityUnitWidht + 'px',
                id: baseId + ':' + index,
                time: time.get('time'),
                border: index ==  this.timeScale.getCount()? 'solid 1px black' : 'dotted 1px grey'
            });
        }, this);

        return html;
    },

    /**
     * gets HTML fragment of the horizontal days columns for header frebusy column
     */
    getDayRows: function() {
        var html = '';
        var baseId = Ext.id();
        for (var i=0, date; i<this.numOfDays; i++) {
            var day = this.startDate.add(Date.DAY, i);
                html += this.templates.dayRow.applyTemplate({
                    day: String.format(this.dayFormatString, day.format('l'), day.format('j'), day.format('F')),
                    id: baseId + ':' + i,
                    width: this.granularityUnitWidhtHeader + 2.57 + 'px',
                    border: 'solid 1px black'

                });
        }
        return html;
    },

    /**
     * gets freebusy column headers
     *
     */
    getFreebusyHeaders: function() {
       var width = (this.granularityUnitWidht +1.1) * this.timeScale.getCount();
       this.granularityUnitWidhtHeader = width;
       var totalwidth = (this.numOfDays * this.granularityUnitWidhtHeader) +2;
       var daysRow= this.getDayRows();
       return this.templates.freebusyheaders.applyTemplate({
                 daysRow: daysRow,
                 width: this.freebusyColumn -2 + "px",
                 totalwidth: totalwidth + "px"
       });
    },

    /**
     * update freebusy column header
     *
     */
    updateFreebusyHeader: function() {
        if(Ext.isGecko) {
            this.granularityUnitWidhtHeader = (this.granularityUnitWidht +1.1)  * this.timeScale.getCount();
        } else {
            this.granularityUnitWidhtHeader = (this.granularityUnitWidht + 1)  * this.timeScale.getCount();
        }
        var i=0;
        Ext.each(this.el.select('.header-text-element').elements, function(row) {
            var day = this.startDate.add(Date.DAY,i);
            Ext.get(row).update(String.format(this.dayFormatString, day.format('l'), day.format('j'), day.format('F')));
            if (this.timeGranularity <= 120 ){
                 Ext.get(row).setStyle('margin-left','10px');
                 Ext.get(row).setStyle('text-align','left');
            } else {
                 Ext.get(row).setStyle('margin-left','0px');
                 Ext.get(row).setStyle('text-align','center');
            }
            i++;
        }, this);
        Ext.each(this.el.select('.header-element').elements, function(row) {
            Ext.get(row).setWidth(this.granularityUnitWidhtHeader,false);
        }, this);
        Ext.get(this.el.select('.freebusy-col-header').elements[0]).setWidth((this.numOfDays * this.granularityUnitWidhtHeader)+2,false);
        Ext.get(this.el.select('.freebusy-col').elements[0]).scrollTo("r", 0);
        Ext.get(this.el.select('.freebusy-col').elements[0]).setWidth(this.freebusyColumn -2 , false);
    },

    /**
     * update text day header location
     */
    setTextHeaderLocation: function() {
        if(this.timeGranularity <= 120) {
            var day=Math.floor(this.scrollTimePosition/this.granularityUnitWidhtHeader);
            var leftText=this.scrollTimePosition % this.granularityUnitWidhtHeader;
            var beforeLeftText = Number(Ext.get(this.el.select('.header-text-element').elements[day]).getStyle('margin-left').slice(0,-2));
            if((leftText+120) < this.granularityUnitWidhtHeader){
                Ext.get(this.el.select('.header-text-element').elements[day]).animate({'margin-left': {to: leftText,from: beforeLeftText,unit: 'px'},},0.30);
            }
        }
    },

    /**
     * inits time scale
     * @private
     */
    initTimeScale: function() {
        var data = [];
        var scaleSize = ((parseInt(this.endDayTime) * Date.msHOUR) - (parseInt(this.startDayTime) * Date.msHOUR))/(this.timeGranularity * Date.msMINUTE);
        var baseDate = this.startDate.clone();

        if (baseDate.isDST){
            baseDate = Date.parseDate(baseDate.add(Date.DAY, 1).format('Y-m-d') + ' ' + this.startDayTime + ':00:00', Date.patterns.ISO8601Long);
        }

        var minutes;
        for (var i=1; i<=Math.ceil(scaleSize); i++) {
            minutes = (i * this.timeGranularity) - (30 * (this.timeGranularity/60));
            data.push([i, minutes, minutes * Date.msMINUTE, baseDate.add(Date.MINUTE, minutes).format('H:i')]);
        }

        this.timeScale = new Ext.data.SimpleStore({
            fields: ['index', 'minutes', 'milliseconds', 'time'],
            data: data,
            id: 'index'
        });
    },
    /**
     * inits all tempaltes of this view
     */
    initTemplates: function() {
        var ts = this.templates || {};
        ts.freebusy = new Ext.XTemplate(
            '<div class="freebusy-col" style="width: {width}; overflow: hidden; ">',
                '<div class="freebusy-col-line" style="width: {totalwidth}; overflow:auto;position: relative;" >{timeRows}</div></div>'
        );
        ts.freebusyheaders = new Ext.XTemplate(
            '<div class="freebusy-col" style="width: {width}; overflow: hidden; ">',
                '<div  class="freebusy-col-header" style="width: {totalwidth}; overflow:auto;position: relative;" >{daysRow}</div></div>'
        );
        ts.timeRow = new Ext.XTemplate(
            '<div id="{id}" style="text-align: center;float: left; width: {width}; border-right: {border}">{time}',
                '<div style="margin-left: 50%; border-left: solid 1px black; height: 4px;"></div>',
            '</div>'
        );
        ts.dayRow = new Ext.XTemplate(
            '<div id="{id}" class="header-element" style="float: left; width: {width}; border-right: {border}">',
                '<div class="header-text-element" style="text-align: left;margin-left: 10px;">{day}',
                '</div>',
            '</div>'
        );
        ts.event = new Ext.XTemplate(
            '<div id="{id}" class="freebusy-event" style="width: {width}; left: {left}; height: {height};">',
                '<div class="freebusy-event-front" style="width: {width}; height: {height};" title="{status}">',
                '</div>',
                '<div class="freebusy-event-back" style="width: {width}; height: {height};">',
                '</div>',
            '</div>'
        );
        ts.timeEvent = new Ext.XTemplate(
            '<div id="{id}" class="freebusy-event-time" style="width: {width}; left: {left}; height: {height};">',
            '</div>'
        );
        for(var k in ts){
            var t = ts[k];
            if(t && typeof t.compile == 'function' && !t.compiled){
                t.disableFormats = true;
                t.compile();
            }
        }

        this.templates = ts;
    },

    onMouseOver: function(o) {
        var rowsQuantity = this.el.select('.x-grid3-row').elements.length;;
        var bodyHeight = rowsQuantity * Ext.get(this.el.select('.x-grid3-row').elements[0]).getHeight();
        if(!this.freebusyscrollbuttom) {
            var freeBusyColumnWidth = Ext.get(this.el.select('.x-grid3-td-freebusy').elements[0]).getWidth();
            var rightShift = Ext.get(this.el.select('.x-grid3-td-user_type').elements[0]).getWidth()+Ext.get(this.el.select('.x-grid3-td-user_id').elements[0]).getWidth()+freeBusyColumnWidth-60;
            var leftButtom = new Ext.Element(document.createElement('div'));
            var rightButtom = new Ext.Element(document.createElement('div'));
            leftButtom.addClass("freebusy-leftbuttom");
            rightButtom.addClass("freebusy-rightbuttom");
            leftButtom.setHeight(bodyHeight,false);
            rightButtom.setHeight(bodyHeight,false);
            leftButtom.setLeft(Ext.get(this.el.select('.x-grid3-td-user_type').elements[0]).getWidth()+Ext.get(this.el.select('.x-grid3-td-user_id').elements[0]).getWidth()+"px");
            rightButtom.setLeft(rightShift+"px");
            leftButtom.setWidth(60,false);
            rightButtom.setWidth(60,false);
            leftButtom.setStyle('position','absolute');
            rightButtom.setStyle('position','absolute');
            leftButtom.setTop(0);
            rightButtom.setTop(0);

            leftButtom.on('mouseenter',function (evt,el,o) {
                    Ext.get(this.el.select('.freebusy-arrowleftbuttom').elements[0]).show();
            },this);
            leftButtom.on('mouseleave',function (evt,el,o) {
                    Ext.get(this.el.select('.freebusy-arrowleftbuttom').elements[0]).hide();
            },this);
            rightButtom.on('mouseenter',function (evt,el,o) {
                    Ext.get(this.el.select('.freebusy-arrowrightbuttom').elements[0]).show();
            },this);
            rightButtom.on('mouseleave',function (evt,el,o) {
                    Ext.get(this.el.select('.freebusy-arrowrightbuttom').elements[0]).hide();
            },this);

            this.el.select('.x-grid3-scroller').appendChild(leftButtom);
            this.el.select('.x-grid3-scroller').appendChild(rightButtom);
            var arrowleftButtom = new Ext.Element(document.createElement('div'));
            var arrowrightButtom = new Ext.Element(document.createElement('div'));
            arrowleftButtom.addClass("freebusy-arrowleftbuttom");
            arrowrightButtom.addClass("freebusy-arrowrightbuttom");
            arrowleftButtom.setStyle('position','absolute');
            arrowrightButtom.setStyle('position','absolute');
            arrowleftButtom.setVisible(false);
            arrowrightButtom.setVisible(false);
            arrowleftButtom.on('click',function () {
                    this.scrollTimePosition -= 100;
                    Ext.each(this.el.select('.freebusy-col').elements, function(row) {
                        Ext.get(row).scrollTo("r", this.scrollTimePosition, true);
                    }, this);
                    if ( this.scrollTimePosition < 0 ) {
                        this.scrollTimePosition = 0;
                    }
                    this.setTextHeaderLocation();
            },this);
            arrowrightButtom.on('click',function () {
                    this.scrollTimePosition += 100;
                    Ext.each(this.el.select('.freebusy-col').elements, function(row) {
                        Ext.get(row).scrollTo("l", this.scrollTimePosition, true);
                    }, this);
                    if ( this.scrollTimePosition > (Ext.get(this.el.select('.freebusy-col-line').elements[0]).getWidth() - 1000)) {
                        this.scrollTimePosition = Ext.get(this.el.select('.freebusy-col-line').elements[0]).getWidth() - 1000;
                    }
                    this.setTextHeaderLocation();
            },this);
            this.visibleGridRows = Ext.get(this.el.select('.x-grid3-scroller').elements[0]).getHeight() / Ext.get(this.el.select('.x-grid3-row').elements[0]).getHeight();
            if( this.el.select('.x-grid3-row').elements.length > this.visibleGridRows) {
                arrowleftButtom.setStyle('background-attachment','fixed');
                arrowrightButtom.setStyle('background-attachment','fixed');
                arrowleftButtom.setStyle('background-size','4% 20%');
                arrowrightButtom.setStyle('background-size','4% 20%');
                var topPosition = Ext.get(this.el.select('.x-grid3-td-freebusy').elements[0]).getTop() + ((this.visibleGridRows * Ext.get(this.el.select('.x-grid3-row').elements[0]).getHeight())/2);
                var leftPosition = Ext.get(this.el.select('.x-grid3-td-freebusy').elements[0]).getLeft() + 'px ' + topPosition  + 'px';
                var rightPosition = Ext.get(this.el.select('.x-grid3-td-freebusy').elements[0]).getLeft()  + Ext.get(this.el.select('.x-grid3-td-freebusy').elements[0]).getWidth() - 60  + 'px ' + topPosition + 'px';
                arrowrightButtom.setStyle('background-position', rightPosition);
                arrowleftButtom.setStyle('background-position', leftPosition);

            }
            leftButtom.appendChild(arrowleftButtom);
            rightButtom.appendChild(arrowrightButtom);
            this.freebusyscrollbuttom = true;
        } else {
            Ext.get(this.el.select('.freebusy-leftbuttom').elements[0]).setHeight(bodyHeight,true);
            Ext.get(this.el.select('.freebusy-rightbuttom').elements[0]).setHeight(bodyHeight,true);
        }
    },

    renderAttenderUserName: function(name) {
        name = name || "";
        if (typeof name.get == 'function' && name.get('n_fileas')) {
            return Ext.util.Format.htmlEncode(name.get('n_fileas'));
        }
        if (name.n_fileas) {
            return Ext.util.Format.htmlEncode(name.n_fileas);
        }
        if (name.accountDisplayName) {
            return Ext.util.Format.htmlEncode(name.accountDisplayName);
        }
        // how to detect hash/string ids
        if (Ext.isString(name) && ! name.match('^[0-9a-f-]{40,}$') && ! parseInt(name, 10)) {
            return Ext.util.Format.htmlEncode(name);
        }
        // NOTE: this fn gets also called from other scopes
        return Tine.Tinebase.appMgr.get('Calendar').i18n._('No Information');
    },

    renderAttenderGroupmemberName: function(name) {
        var name = Tine.Calendar.AttendeeGridPanel.prototype.renderAttenderUserName.apply(this, arguments);
        return name + ' ' + Tine.Tinebase.appMgr.get('Calendar').i18n._('(as a group member)');
    },

    renderAttenderGroupName: function(name) {
        if (typeof name.getTitle == 'function') {
            return Ext.util.Format.htmlEncode(name.getTitle());
        }
        if (name.name) {
            return Ext.util.Format.htmlEncode(name.name);
        }
        if (Ext.isString(name)) {
            return Ext.util.Format.htmlEncode(name);
        }
        return Tine.Tinebase.appMgr.get('Calendar').i18n._('No Information');
    },

    renderAttenderMemberofName: function(name) {
        return Tine.Calendar.AttendeeGridPanel.prototype.renderAttenderGroupName.apply(this, arguments);
    },

    renderAttenderResourceName: function(name) {
        if (typeof name.getTitle == 'function') {
            return Ext.util.Format.htmlEncode(name.getTitle());
        }
        if (name.name) {
            return Ext.util.Format.htmlEncode(name.name);
        }
        if (Ext.isString(name)) {
            return Ext.util.Format.htmlEncode(name);
        }
        return Tine.Tinebase.appMgr.get('Calendar').i18n._('No Information');
    },

    renderAttenderDispContainer: function(displaycontainer_id, metadata, attender) {
        metadata.attr = 'style = "overflow: none;"';
        if (displaycontainer_id) {
            if (displaycontainer_id.name) {
                return Ext.util.Format.htmlEncode(displaycontainer_id.name).replace(/ /g,"&nbsp;");
            } else {
                metadata.css = 'x-form-empty-field';
                return this.app.i18n._('No Information');
            }
        }
    },

    renderAttenderQuantity: function(quantity, metadata, attender) {
        return quantity > 1 ? quantity : '';
    },

    renderAttenderRole: function(role, metadata, attender) {
        var i18n = Tine.Tinebase.appMgr.get('Calendar').i18n,
            renderer = Tine.widgets.grid.RendererManager.get('Calendar', 'Attender', 'role', Tine.widgets.grid.RendererManager.CATEGORY_GRIDPANEL);

        if (this.record && this.record.get('editGrant')) {
            metadata.attr = 'style = "cursor:pointer;"';
        } else {
            metadata.css = 'x-form-empty-field';
        }
        return renderer(role);
    },

    renderAttenderStatus: function(status, metadata, attender) {
        var i18n = Tine.Tinebase.appMgr.get('Calendar').i18n,
            renderer = Tine.widgets.grid.RendererManager.get('Calendar', 'Attender', 'status', Tine.widgets.grid.RendererManager.CATEGORY_GRIDPANEL);

        if (! attender.get('user_id')) {
            return '';
        }

        if (attender.get('status_authkey')) {
            metadata.attr = 'style = "cursor:pointer;"';
        } else {
            metadata.css = 'x-form-empty-field';
        }

        return renderer(status);
    },

    renderAttenderType: function(type, metadata, attender) {
        metadata.css = 'tine-grid-cell-no-dirty';
        var cssClass = '',
            qtipText =  '',
            userId = attender.get('user_id'),
            hasAccount = userId && ((userId.get && userId.get('account_id')) || userId.account_id);

        switch (type) {
            case 'user':
                cssClass = hasAccount || ! userId ? 'renderer_typeAccountIcon' : 'renderer_typeContactIcon';
                qtipText = hasAccount || ! userId ? '' : Tine.Tinebase.appMgr.get('Calendar').i18n._('External Attendee');
                break;
            case 'group':
                cssClass = 'renderer_accountGroupIcon';
                break;
            default:
                cssClass = 'cal-attendee-type-' + type;
                break;
        }

        var qtip = qtipText ? 'ext:qtip="' + Tine.Tinebase.common.doubleEncode(qtipText) + '" ': '';
        var result = '<div ' + qtip + 'style="background-position:0px;" class="' + cssClass + '">&#160</div>';

        if (! attender.get('user_id')) {
            result = Tine.Tinebase.common.cellEditorHintRenderer(result);
        }

        return result;
    },

    /**
     * disable contents not panel
     */
    setDisabled: function(v) {
        if (v) {
            // remove "add new attender" row
            this.store.filterBy(function(r) {return ! (r.id && typeof r.id === 'string' && r.id.match(/^new-/))});
        } else {
            this.store.clearFilter();
        }
    },

    showMask: function() {
       if (!this.loadMask) {
            this.loadMask = new Ext.LoadMask(this.ownerCt.body,{msg:this.loadMaskText});
       }

       this.loadMask.show();
    },

    hideMask: function() {
        if (this.loadMask) {
            this.loadMask.hide();
        }
    }
});
