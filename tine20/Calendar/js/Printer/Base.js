Ext.ns('Tine.Calendar.Printer');


/**
 * @class   Tine.Calendar.Printer.BaseRenderer
 * @extends Ext.ux.Printer.BaseRenderer
 * 
 * Printig renderer for Ext.ux.printing
 */
Tine.Calendar.Printer.BaseRenderer = Ext.extend(Ext.ux.Printer.BaseRenderer, {
    stylesheetPath: 'Calendar/css/print.css',
    
    extraTitle: '',
    titleStyle: '',
    dayFormatString: '{0}, the {1}. of {2}',
//    generateBody: function(view) {
//        var days = [];
//        
//        // iterate days
//        for (var dayStart, dayEnd, dayEvents, i=0; i<view.numOfDays; i++) {
//            dayStart = view.startDate.add(Date.DAY, i);
//            dayEnd   = dayStart.add(Date.DAY, 1).add(Date.SECOND, -1);
//            
//            // get events in this day
//            dayEvents = view.store.queryBy(function(event){
//                return event.data.dtstart.getTime() < dayEnd.getTime() && event.data.dtend.getTime() > dayStart.getTime();
//            });
//            
//            days.push(this.generateDay(dayStart, dayEnd, dayEvents));
//        }
//        
//        var topic = this.generateHeader(view);
//        var body  = 
//        return view.numOfDays === 1 ? days[0] : String.format('<table>{0}</table>', this.generateCalRows(days, view.numOfDays < 9 ? 2 : 7));
//    },
    
    generateTitle: function(view) {
        var container = view.app.mainScreen.WestPanel.TreePanel.lastClickedNode;
        var user = container && container.attributes.owner ? container.attributes.owner.accountFullName : Tine.Tinebase.registry.get('currentAccount').accountDisplayName;
        return ['<table style="', this.titleStyle, '" class="mini-cal-header-table" cellspacing="0"><tr><th class="cal-print-title">', this.extraTitle,  this.getTitle(view), '<br><br><br><span style="font-size: 12px;">', user,'</span></th>',this.generateMiniCal(view),'</tr></table>'].join('');
    },
    getPageSize: function(){
        var size = "",
            width = null,
            height = null;
        this.paperHeight = this.app.getRegistry().get('preferences').get('papersize');
        switch(this.paperHeight) {
            case "326":
                size = "A3 portrait";
                width = "297mm";
                height = "420mm";
                break;
            case "223":
                size = "A4 portrait";
                width = "210mm";
                height = "297mm";
                break;
            case "136":
                size = "A5 portrait";
                width = "148mm";
                height = "210mm";
                break;
            case "224":
                size = "A3 landscape";
                width = "420mm";
                height = "297mm";
                break;
            case "137":
                size = "A4 landscape";
                width = "297mm";
                height = "210mm";
                break;
            case "74":
                size = "A5 landscape";
                width = "210mm";
                height = "148mm";
                break;
            default:
                size="A4 portrait";
                width = "210mm";
                height = "297mm";
        }

        return String.format(
                '<style type="text/css"> @page { margin: 12mm 9mm 12mm 9mm;size:{0}; } @media print and (-webkit-min-device-pixel-ratio:0) {html, body {width: {1}; height: {2};}} </style>',
                size,
                width,
                height);
    },

    generateMiniCal: function(view){
        var miniCalNum = this.app.getRegistry().get('preferences').get('minicalprint');
        if(miniCalNum == 1){

            return String.format('<th class ="cal-print-minical-second">{0}</th>',this.getMiniCal(0,view.startDate));;
        } else if(miniCalNum == 2){
              return String.format('<th class ="cal-print-minical-first">{0}</th><th class ="cal-print-minical-second">{1}</th>',this.getMiniCal(0,view.startDate),this.getMiniCal(1,view.startDate));
        } else if(miniCalNum == 3){
              return String.format('<th class ="cal-print-minical-first">{0}</th><th class ="cal-print-minical-first">{1}</th><th class ="cal-print-minical-second">{2}</th>',this.getMiniCal(-1,view.startDate),this.getMiniCal(0,view.startDate),this.getMiniCal(1,view.startDate));
        }
    },

    generateCalRows: function(days, numCols, alignHorizontal) {
        var row, col, cellsHtml, idx,
            numRows = Math.ceil(days.length/numCols),
            rowsHtml = '';
        for (row=0; row<numRows; row++) {
            cellsHtml = '';
            //offset = row*numCols;
            
            for (col=0; col<numCols; col++) {
                idx = alignHorizontal ? row*numCols + col: col*numRows + row;
                cellsHtml += String.format('<td class="cal-print-daycell" style="vertical-align: top;">{0}</td>', days[idx] || '');
            }
            
            rowsHtml += String.format('<tr class="cal-print-dayrow" style="height: {1}mm">{0}</tr>', cellsHtml, this.paperHeight/numRows);
        }
        
        return rowsHtml;
    },

    getDayColumns: function(view){
        var html = '';
        var width = "65";
        for (var i=0; i<view.numOfDays; i++) {
            y <<= i;
            g = this.work & y;
            if(g > 0) {
                html += this.dayColumn.apply({
                    width: width + '%',
                    left: j * width + '%',
                    overRows: this.getOverRows(i)
                });
                j++;
            }
            y=1;
        }

        return html
    },
    generateDayHeader: function(view,data,height){
        return this.eventDayHeader.apply({
            dayname: view.startDate.format('l'),
            data: data,
            height: height + 'px'
        });
    },

    generateDayCalRows: function(data, view) {
        var rowsHtml = '';
        if(this.app.getRegistry().get('preferences').get('printannotations') == 1)
        {
            rowsHtml += String.format('<table><tr><td class="cal-daysviewpanel-body-scroller"><div class="cal-daysviewpanel-scroller" id="ext-gen626" style="height: {1}mm;"><div class="cal-daysviewpanel-body" id="ext-gen627"><div class="cal-daysviewpanel-body-inner">{0}</div></div</div></td>',this.getTimeRows(view,data),this.paperHeight);
            rowsHtml += String.format('<td class="cal-daysviewpanel-body-annotation"><table>{0}</table></td></tr></table>',this.getNotes(view));
        } else {
            rowsHtml += String.format('<div class="cal-daysviewpanel-scroller" id="ext-gen626" style="height: {1}mm;"><div class="cal-daysviewpanel-body" id="ext-gen627"><div class="cal-daysviewpanel-body-inner">{0}</div></div</div>',this.getTimeRows(view,data),this.paperHeight);
        }
        return rowsHtml;

    },

    /*
    * get minical calendar
    *  month is the numder of month, 0 is current month
    *
    */
    getMiniCal: function(month,date) {
        var firstDay = new Date(date.getFullYear(), date.getMonth()+ month, 1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + month +1, 0);
        monthName = String.format('<td align="center" class="mini-date-title" id="firstMonthName">{0}</td>', firstDay.format('F Y'));
        tBody = '<tr>';
        for (j = 0 ; j < firstDay.getDay(); j++){
            tBody += '<td class="mini-date-inner"></td>';
        }
        var currentDate = firstDay.getDay();
        for (i = firstDay.getDate(); i <= lastDay.getDate();i++){
            if(currentDate === 7){
                tBody += '</tr><tr border-collapse="collapse">';
                currentDate = 0;
            }
            tBody += String.format('<td class="mini-date-inner"><span>{0}</span></td>',i)
            currentDate += 1;
        }
        tBody += '</tr>';
        minical = String.format('<table cellspacing="0" class="mini-cal-table" id="firstmini" align="right"><tbody><tr>{0}</tr><tr><td colspan="3" class="mini-date-inner"><table cellspacing="0" class="mini-date-inner"><thead><tr><th class="mini-cal-span"><span>D</span></th><th class="mini-cal-span"><span>S</span></th><th class="mini-cal-span"><span>T</span></th><th class="mini-cal-span"><span>Q</span></th><th class="mini-cal-span"><span>Q</span></th><th class="mini-cal-span"><span>S</span></th><th class="mini-cal-span"><span>S</span></th></tr></thead><tbody class="mini-cal-span">{1}</tbody></table></td></tr></tbody></table>',monthName,tBody);
        return minical;
    },

    generateDay: function(dayStart, dayEnd, dayEvents) {
        var dayBody = '';
        this.app = Tine.Tinebase.appMgr.get('Calendar');
        this.dayFormatString      =  this.app.i18n._hidden(this.dayFormatString);
        var dayHeaders = Ext.DomQuery.select('div[class=cal-daysviewpanel-dayheader-day]', this.innerHd);
        var mainPanel = this.app.getMainScreen().getCenterPanel();
        dayEvents.each(function(event){
            var start = event.data.dtstart.getTime() <= dayStart.getTime() ? dayStart : event.data.dtstart,
                end   = event.data.dtend.getTime() > dayEnd.getTime() ? dayEnd : event.data.dtend;
            if ( String(mainPanel.activeView).match(/^month/i)){
                dayBody += this.eventMonthTpl.apply({
                    color: event.colorSet.color,
                    startTime: event.data.is_all_day_event ? '' : start.format('H:i'),
                    untilseperator: event.data.is_all_day_event ? '' : '-',
                    endTime: event.data.is_all_day_event ? '' : end.format('H:i'),
                    summary: Ext.util.Format.htmlEncode(event.data.summary),
                    duration: event.data.is_all_day_event ? Tine.Tinebase.appMgr.get('Calendar').i18n._('whole day') :
                        Tine.Tinebase.common.minutesRenderer(Math.round((end.getTime() - start.getTime())/(1000*60)), '{0}:{1}', 'i')
                });
            } else{
                dayBody += this.eventWeekTpl.apply({
                    color: event.colorSet.color,
                    startTime: event.data.is_all_day_event ? '' : start.format('H:i'),
                    untilseperator: event.data.is_all_day_event ? '' : '-',
                    endTime: event.data.is_all_day_event ? '' : end.format('H:i'),
                    summary: Ext.util.Format.htmlEncode(event.data.summary),
                    duration: event.data.is_all_day_event ? Tine.Tinebase.appMgr.get('Calendar').i18n._('whole day') :
                        Tine.Tinebase.common.minutesRenderer(Math.round((end.getTime() - start.getTime())/(1000*60)), '{0}:{1}', 'i'),
                    attendees: this.getAttendees(event),
                    width: 200,
                    description: Ext.util.Format.htmlEncode(event.data.description)
                });
            }
        }, this);
        if ( String(mainPanel.activeView).match(/^month/i)) {
            var dayHeader = this.dayTpl.apply({
                dayOfMonth: dayStart.format('j')
            });
        } else {
            var dayHeader = this.dayTpl.apply({
                weekDay: String.format(this.dayFormatString, dayStart.format('l'), dayStart.format('j'), dayStart.format('F'))
            });
        }
        return String.format('<table class="cal-print-daysview-day"><tr>{0}</tr>{1}</table>', dayHeader, dayBody);
    },

    generateDayData: function(view){
        var html = [];
        var eventToPrint = [];
        var k=0;
        view.store.each(function(event){
             if (event.ui) {
                 eventToPrint = event.ui.printEvent(view);
                 html[k] = eventToPrint;
                 k++;
             }
        }, this);
        return html;
    },

    splitDays: function(ds, dateMesh, numOfDays, returnData) {
        var days = [];
        
        // iterate days
        for (var dayStart, dayEnd, dayEvents, i=0; i<numOfDays; i++) {
            dayStart = dateMesh[i];
            dayEnd   = dayStart.add(Date.DAY, 1).add(Date.SECOND, -1);
            
            // get events in this day
            dayEvents = ds.queryBy(function(event){
                return event.data.dtstart.getTime() < dayEnd.getTime() && event.data.dtend.getTime() > dayStart.getTime();
            });
            
            days.push(returnData ? {
                dayStart: dayStart,
                dayEnd: dayEnd,
                dayEvents: dayEvents
            } : this.generateDay(dayStart, dayEnd, dayEvents));
        }
        
        return days;
    },

    /**
     * gets HTML fragment of the horizontal time rows
     */
    getTimeRows: function(view,data) {
        var html = '';
        i=0;
        view.timeScale.each(function(time){
            var index = time.get('index');
            //height adjustment
            if (index%2 === 0){
                var top = (view.printConvert * view.granularityUnitHeights) * i;
                var topOff = (view.printConvert * view.granularityUnitHeights) * ++i;
                html += this.timeRow.apply({
                    height:  view.printConvert * view.granularityUnitHeights + 'px',
                    time:  time.get('time'),
                    top : top + 'px',
                    topOff: topOff + 'px'
                });
                i++;
            }
        }, this);


        html += this.getOverRows(view,data);

        return html;
    },

    getNotes: function(view) {
        var rowsNumber = view.timeScale.data.length;
        html = String.format( '<tr><th>{0}</th></tr><tr><td colspan="{1}" ></td></tr>',Tine.Tinebase.appMgr.get('Calendar').i18n._('Annotations'),rowsNumber);
        return html;
    },

    /**
     * gets HTML fragment of the time over rows
     */
    getOverRows: function(view,data) {
        var html = '<div class="cal-daysviewpanel-body-daycolumns"><div style="left: 0%; width: 100%;" class="cal-daysviewpanel-body-daycolumn cal-daysviewpanel-body-daycolumn-today">';
        var baseId = Ext.id();
        var dayIndex =0;
        var offOn = 'on';
        view.timeScale.each(function(time){
            var index = time.get('index');
            html += this.overRow.apply({
                id: baseId + ':' + dayIndex + ':' + index,
                height: view.printConvert * view.granularityUnitHeights + 'px',
                offOn: offOn
            });
            dayIndex++;
            if(offOn == 'on'){
               offOn = 'off';
            }else{
               offOn = 'on';
            }
        }, this);
        html += data;
        html += '</div></div>';

        return html;
    },


    getAttendees: function(event){
        var attendees ="";
        Ext.each(event.data.attendee, function(attender) {
            attendees += attender.user_id.n_fn + ", ";
        }, this);

        return attendees.substring(0,attendees.length -2 );
    },

    dayTpl: new Ext.XTemplate(
        '<tr>',
            '<th  colspan="5" style="border-bottom: 1px solid black;">',
                '<span class="cal-print-daysview-day-dayOfMonth">{dayOfMonth}</span>',
                '<span class="cal-print-daysview-day-weekDay">{weekDay}</span>',
            '</th>', 
        '</tr>'
    ),

    timeRow:new Ext.XTemplate(
         '<div style="height: {height}; top: {top}" class="cal-daysviewpanel-timeRow-on">',
             '<div class="cal-daysviewpanel-timeRow-time">{time}</div>',
         '</div>',
         '<div style="height:  {height}; top: {topOff};" class="cal-daysviewpanel-timeRow-off">',
             '<div class="cal-daysviewpanel-timeRow-time"></div>',
         '</div>'
    ),

    bodyHourTpl: new Ext.XTemplate(
         '<table clas="outter">',
             '<tr>',
                 '<td class="semana">{dia}</td>',
                 '<td class="anotacoes">{notes}</td>',
             '</tr>',
         '</table>'
    ),

    dayColumn: new Ext.XTemplate(
         '<div class="cal-daysviewpanel-body-daycolumn" style="left: {left}; width: {width};">',
              '<div class="cal-daysviewpanel-body-daycolumn-inner">&#160;</div>',
              '{overRows}',
              '<img src="', Ext.BLANK_IMAGE_URL, '" class="cal-daysviewpanel-body-daycolumn-hint-above" />',
              '<img src="', Ext.BLANK_IMAGE_URL, '" class="cal-daysviewpanel-body-daycolumn-hint-below" />',
         '</div>'
     ),

    overRow: new Ext.XTemplate(
         '<div id="{id}" class="cal-daysviewpanel-datetime cal-daysviewpanel-daycolumn-row" style="height: {height};">',
             '<div class="cal-daysviewpanel-daycolumn-row-{offOn}"></div>',
         '</div>'
      ),


    /**
     * @property eventTpl
     * @type Ext.XTemplate
     * The XTemplate used to create the headings row. By default this just uses <th> elements, override to provide your own
     */
    eventWeekTpl: new Ext.XTemplate(
        '<tr>',
            '<td class="cal-print-daysview-day-color" rowspan="4" valign="top"><span style="color: {color};">&#9673;</span></td>',
            '<td class="cal-print-daysview-day-starttime">{startTime}</td>',
            '<td class="cal-print-daysview-day-untilseperator">{untilseperator}</td>',
            '<td class="cal-print-daysview-day-endtime">{endTime}</td>',
        '</tr>',
        '<tr>',
            '<td class="cal-print-daysview-day-summary" colspan="3">{summary}</td>',
        '</tr>',
        '<tr>',
             '<td colspan="3" class="cal-print-daysview-day-description"><p style="width: {width}px;">{description}</p></td>',
        '</tr>',
        '<tr>',
             '<td colspan="3" class="cal-print-daysview-day-attendees">{attendees}</td>',
        '</tr>'
    ),

    eventMonthTpl: new Ext.XTemplate(
        '<tr>',
            '<td class="cal-print-daysview-day-color" rowspan="2" valign="top"><span style="color: {color};">&#9673;</span></td>',
            '<td class="cal-print-daysview-day-starttime">{startTime}</td>',
            '<td class="cal-print-daysview-day-untilseperator">{untilseperator}</td>',
            '<td class="cal-print-daysview-day-endtime">{endTime}</td>',
         '</tr>',
         '<tr>',
            '<td class="cal-print-daysview-day-summary" colspan="3">{summary}</td>',
        '</tr>'
    ),

    eventDayHeader: new Ext.XTemplate(
        '<div class="cal-daysviewpanel-header" id="ext-gen625">',
            '<div class="cal-daysviewpanel-header-inner">',
               '<div class="cal-daysviewpanel-header-offset">',
                 '<div class="cal-daysviewpanel-daysheader">',
                   '<div style="height: 18; left: 0%; width: 100%;" class="cal-daysviewpanel-dayheader">',
                     '<div class="cal-daysviewpanel-dayheader-day-wrap cal-daysviewpanel-dayheader-today" id="ext-gen621:0">',
                         ' <div class="cal-daysviewpanel-dayheader-day" id="ext-gen632">{dayname}</div>',
                     '</div>',
                   '</div>',
                 '</div>',
                 '<div class="cal-daysviewpanel-wholedayheader-scroller" style="height: {height}; left: 0%;">',
                   '<div class="cal-daysviewpanel-wholedayheader" id="ext-gen642" style="height: 18px;">',
                      '{data}',
                     '<div class="cal-daysviewpanel-wholedayheader-daycols">',
                       '<div style="left: 0%; width: 100%;" class="cal-daysviewpanel-body-wholedaycolumn">',
                           '<div class="cal-daysviewpanel-datetime cal-daysviewpanel-body-wholedaycolumn-over" id="ext-gen622:0">&nbsp;</div>',
                       '</div>',
                     '</div>',
                   '</div>',
                 '</div>',
               '</div>',
            '</div>',
            '<div class="x-clear"></div>',
        '</div>'
    )
    
});
