/*
 * Tine 2.0
 * 
 * @package     Setup
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/*global Ext, Tine*/

Ext.ns('Tine', 'Tine.Setup');
 
/**
 * Setup Authentication Manager
 * 
 * @namespace   Tine.Setup
 * @class       Tine.Setup.AuthenticationPanel
 * @extends     Tine.Tinebase.widgets.form.ConfigPanel
 * 
 * <p>Authentication Panel</p>
 * <p><pre>
 * TODO         move to next step after install?
 * TODO         make default is valid mechanism with 'allowEmpty' work
 * TODO         add port for ldap hosts
 * </pre></p>
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 * @param       {Object} config
 * @constructor
 * Create a new Tine.Setup.AuthenticationPanel
 */
Tine.Setup.AuthenticationPanel = Ext.extend(Tine.Tinebase.widgets.form.ConfigPanel, {
    
    /**
     * authProviderPrefix DOM Id prefix
     * 
     * @property authProviderIdPrefix
     * @type String
     */
    authProviderIdPrefix: null,
    
    /**
     * accountsStorageIdPrefix DOM Id prefix
     * 
     * @property accountsStorageIdPrefix
     * @type String
     */
    accountsStorageIdPrefix: null,
    
    /**
     * combo box containing the authentication backend selection
     * 
     * @property authenticationBackendCombo
     * @type Ext.form.ComboBox 
     */
    authenticationBackendCombo: null,

    /**
     * combo box containing the accounts storage selection
     * 
     * @property accountsStorageCombo
     * @type Ext.form.ComboBox
     */
    accountsStorageCombo: null,
    
    /**
     * combo box containing the expiration password control
     * 
     * @property authenticationCheckExpiredPassword
     * @type Ext.form.ComboBox
     */
    authenticationCheckExpiredPassword: null,    
    
    /**
     * combo box containing the mail list control
     *
     * @property ldapMailListControl
     * @type Ext.form.ComboBox
     */
    ldapMailListControl: null,

    /**
     * The currently active accounts storage backend
     * 
     * @property originalAccountsStorage
     * @type String
     */
    originalAccountsStorage: null,

    /**
     * @private
     * panel cfg
     */
    saveMethod: 'Setup.saveAuthentication',
    registryKey: 'authenticationData',
    
    /**
     * @private
     * field index counter
     */
    tabIndexCounter: 1,
    
    /**
     * @private
     */
    initComponent: function () {
        this.idPrefix                              = Ext.id();
        this.authProviderIdPrefix                  = this.idPrefix + '-authProvider-';
        this.accountsStorageIdPrefix               = this.idPrefix + '-accountsStorage-';
        this.originalAccountsStorage               = (this.getRegistryData(this.registryKey).accounts) ? this.getRegistryData(this.registryKey).accounts.backend : 'Sql';
        this.passwordExpirationdPrefix             = this.idPrefix + '-passwordExpiration-';
        this.ldapMailListPrefix                    = this.idPrefix + '-ldapmailList-';
        this.originalCheckForPasswordExpiration    = '0'; 
        this.originalMailListControl               = '0';
        Tine.Setup.AuthenticationPanel.superclass.initComponent.call(this);
    },
    
    /**
     * Change card layout depending on selected combo box entry
     */
    onChangeAuthProvider: function () {
        var authProvider = this.authenticationBackendCombo.getValue();
        
        var cardLayout = Ext.getCmp(this.authProviderIdPrefix + 'CardLayout').getLayout();
        cardLayout.setActiveItem(this.authProviderIdPrefix + authProvider);
    },
    
    /**
     * Change card layout depending on selected combo box entry
     */
    onChangeAccountsStorage: function () {
        var AccountsStorage = this.accountsStorageCombo.getValue();

        if (AccountsStorage !== 'Sql' && AccountsStorage !== this.originalAccountsStorage) {
            Ext.Msg.confirm(this.app.i18n._('Delete all existing users and groups'), this.app.i18n._('Switching from SQL to other backend type will delete all existing User Accounts, Groups and Roles. Do you really want to switch the accounts storage backend ?'), function (confirmbtn, value) {
                if (confirmbtn === 'yes') {
                    this.doOnChangeAccountsStorage(AccountsStorage);
                } else {
                    this.accountsStorageCombo.setValue(this.originalAccountsStorage);
                }
            }, this);
        } else {
            this.doOnChangeAccountsStorage(AccountsStorage);
        }
        this.LDAPVisualAdds(AccountsStorage);
    },
    
    /**
     * Change card layout depending on selected combo box entry for passoword expiration control
     */
    onChangePasswordExpiration: function () {
        var checkForExpiration = this.authenticationCheckExpiredPassword.getValue();
        var AccountsStorage = this.accountsStorageCombo.getValue();
        
        if (checkForExpiration === '0' && checkForExpiration !== this.originalCheckForPasswordExpiration && AccountsStorage === 'Ldap') {
            Ext.Msg.confirm(this.app.i18n._('Remove LDAP password expiration control'), this.app.i18n._('Disabling this feature authentication mechanism becomes more vulnerable. Are you sure you want to do this?'), function (confirmbtn, value) {
                if (confirmbtn === 'yes') {
                    this.doOnChangePasswordExpiration(checkForExpiration);
                } else {
                    this.authenticationCheckExpiredPassword.setValue(this.originalCheckForPasswordExpiration);
                }
            }, this);
        } else {
            this.doOnChangePasswordExpiration(checkForExpiration);
        }
    },    
    
    /**
     * Change card layout depending on selected combo box entry for LDAP mail list control
     */
    onChangeMailList: function () {
        var mailListControl = this.ldapMailListControl.getValue();
        var AccountsStorage = this.accountsStorageCombo.getValue();

        if (mailListControl === '0' && mailListControl !== this.originalMailListControl && AccountsStorage === 'Ldap') {
            Ext.Msg.confirm(this.app.i18n._('Remove LDAP mail list control'), this.app.i18n._('Disabling this feature mail list resources will not be avaliable at Administration module. Are you sure you want to do this?'), function (confirmbtn, value) {
                if (confirmbtn === 'yes') {
                    this.doOnChangeMailList(mailListControl);
                } else {
                    this.ldapMailListControl.setValue(this.originalMailListControl);
                }
            }, this);
        } else {
            this.doOnChangeMailList(mailListControl);
        }
    },

    /**
     * Change card layout depending on selected combo box entry
     */
    doOnChangeAccountsStorage: function (AccountsStorage) {
        var cardLayout = Ext.getCmp(this.accountsStorageIdPrefix + 'CardLayout').getLayout();
        
        cardLayout.setActiveItem(this.accountsStorageIdPrefix + AccountsStorage);
        this.showLDAPPasswordPolicyControlCard();
        this.originalAccountsStorage = AccountsStorage;
    },
    
    /**
     * Change card layout depending on selected combo box entry for LDAP password expiration control
     */
    doOnChangePasswordExpiration: function (PasswordExpirationCheck) {
        if(PasswordExpirationCheck == '0'){
            Ext.getCmp(this.idPrefix+'accounts_Ldap_passwordExpirationAttribute').setValue('');
            Ext.getCmp(this.idPrefix+'accounts_Ldap_passwordExpirationInterval').setValue('');
            this.authenticationCheckExpiredPassword.reset();
        }
            
        var cardLayout = Ext.getCmp(this.passwordExpirationdPrefix + 'CardLayout').getLayout();        
        cardLayout.setActiveItem(this.passwordExpirationdPrefix + PasswordExpirationCheck);
        this.originalCheckForPasswordExpiration = PasswordExpirationCheck;
    },    

    /**
     * Change card layout depending on selected combo box entry for LDAP mail list control
     */
    doOnChangeMailList: function (MailListControl) {
        if(MailListControl == '0'){
            Ext.getCmp(this.idPrefix+'accounts_Ldap_mailListDn').setValue('');
            Ext.getCmp(this.idPrefix+'accounts_Ldap_mailListGid').setValue('');
            this.ldapMailListControl.reset();
        }

        var cardLayout = Ext.getCmp(this.ldapMailListPrefix + 'CardLayout').getLayout();
        cardLayout.setActiveItem(this.ldapMailListPrefix + MailListControl);
        this.originalMailListControl = MailListControl;
    },
    
    /**
     * Change card layout depending on selected combo box for account storage 
     */    
    showLDAPPasswordPolicyControlCard: function(){     
        var AccountsStorage = this.accountsStorageCombo.getValue();
        var cardLayoutPass = Ext.getCmp(this.idPrefix+'ldap_password_policy');
        var cardLayoutMailList = Ext.getCmp(this.idPrefix+'ldap_maillist_control');
        
        if (AccountsStorage === 'Ldap'){
            cardLayoutPass.show();      
            cardLayoutMailList.show();
        }
        else{
            this.authenticationCheckExpiredPassword.reset();
            cardLayoutPass.hide();
            cardLayoutMailList.hide();
        }
        this.onChangePasswordExpiration();
    },

    /**
     * Adjust the dynamic fields LDAP related to show/hide aditional pannels: passoword expiration and mail list
     */
    LDAPVisualAdds: function(BackendConfig) {
        if(BackendConfig != 'Ldap')
            return;

        if(Ext.getCmp(this.idPrefix+'accounts_Ldap_passwordExpirationAttribute').getValue() != ''){
            this.authenticationCheckExpiredPassword.setValue('1');
            this.originalCheckForPasswordExpiration = '1';
            this.doOnChangePasswordExpiration(this.originalCheckForPasswordExpiration);
        }

        if(Ext.getCmp(this.idPrefix+'accounts_Ldap_mailListDn').getValue() != ''){
            this.ldapMailListControl.setValue('1');
            this.originalMailListControl = '1';
            this.doOnChangeMailList(this.originalMailListControl);
        }
    },
    
    /**
     * @private
     */
    onRender: function (ct, position) {
        Tine.Setup.AuthenticationPanel.superclass.onRender.call(this, ct, position);
        
        this.onChangeAuthProvider.defer(250, this);
        this.onChangeAccountsStorage.defer(250, this);
    },
        
    /**
     * transforms form data into a config object
     * 
     * @hack   smuggle termsAccept in 
     * @return {Object} configData
     */
    form2config: function () {
        var configData = this.supr().form2config.call(this);
        configData.acceptedTermsVersion = Tine.Setup.registry.get('acceptedTermsVersion');
        
        return configData;
    },
    
    /**
     * get tab index for field
     * 
     * @return {Integer}
     */
    getTabIndex: function () {
        return this.tabIndexCounter++;
    },
    
    /**
     * returns a combo item for sql authentication backend
     * @param {type} commonComboConfig
     * @returns obj
     */
    getAuthenticationSqlItem: function(commonComboConfig) {
        return {
            id: this.authProviderIdPrefix + 'Sql',
            layout: 'form',
            autoHeight: 'auto',
            defaults: {
                width: 300,
                xtype: 'textfield',
                tabIndex: this.getTabIndex
            },
            items: [
                Ext.applyIf({
                    name: 'authentication_Sql_tryUsernameSplit',
                    fieldLabel: this.app.i18n._('Try to split username'),
                    store: [['1', this.app.i18n._('Yes')], ['0', this.app.i18n._('No')]],
                    value: '1'
                }, commonComboConfig), 
                Ext.applyIf({
                    name: 'authentication_Sql_accountCanonicalForm',
                    fieldLabel: this.app.i18n._('Account canonical form'),
                    store: [['2', 'ACCTNAME_FORM_USERNAME'], ['3', 'ACCTNAME_FORM_BACKSLASH'], ['4', 'ACCTNAME_FORM_PRINCIPAL']],
                    value: '2'
                }, commonComboConfig), 
                {
                    name: 'authentication_Sql_accountDomainName',
                    fieldLabel: this.app.i18n._('Account domain name ')
                }, {
                    name: 'authentication_Sql_accountDomainNameShort',
                    fieldLabel: this.app.i18n._('Account domain short name')
                }
            ]
        };
    },
    
    /**
     * returns a combo item for ldap authentication backend
     * @param {type} commonComboConfig
     * @returns obj
     */    
    getAuthenticationLdapItem: function(commonComboConfig) {
        return {
            id: this.authProviderIdPrefix + 'Ldap',
            layout: 'form',
            autoHeight: 'auto',
            defaults: {
                width: 300,
                xtype: 'textfield',
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'authentication_Ldap_host',
                fieldLabel: this.app.i18n._('Host')
            }, {
                name: 'authentication_Ldap_username',
                fieldLabel: this.app.i18n._('Login name')
            }, {
                name: 'authentication_Ldap_password',
                fieldLabel: this.app.i18n._('Password'),
                inputType: 'password'
            }, 
            Ext.applyIf({
                name: 'authentication_Ldap_bindRequiresDn',
                fieldLabel: this.app.i18n._('Bind requires DN'),
                store: [['1', this.app.i18n._('Yes')], ['0', this.app.i18n._('No')]],
                value: '1'
            }, commonComboConfig), 
            {
                name: 'authentication_Ldap_baseDn',
                fieldLabel: this.app.i18n._('Base DN')
            }, {
                name: 'authentication_Ldap_accountFilterFormat',
                fieldLabel: this.app.i18n._('Search filter')
            }, 
            Ext.applyIf({
                name: 'authentication_Ldap_tryUsernameSplit',
                fieldLabel: this.app.i18n._('Try to split username'),
                store: [['1', this.app.i18n._('Yes')], ['0', this.app.i18n._('No')]],
                value: '1'
            }, commonComboConfig), 
            Ext.applyIf({
                name: 'authentication_Ldap_accountCanonicalForm',
                fieldLabel: this.app.i18n._('Account canonical form'),
                store: [['2', 'ACCTNAME_FORM_USERNAME'], ['3', 'ACCTNAME_FORM_BACKSLASH'], ['4', 'ACCTNAME_FORM_PRINCIPAL']],
                value: '2'
            }, commonComboConfig), 
            {
                name: 'authentication_Ldap_accountDomainName',
                fieldLabel: this.app.i18n._('Account domain name ')
            }, {
                name: 'authentication_Ldap_accountDomainNameShort',
                fieldLabel: this.app.i18n._('Account domain short name')
            }]
        };
    },
    
    /**
     * returns a combo item for Imap authentication backend
     * @param {type} commonComboConfig
     * @returns obj
     */
    getAuthenticationImapItem: function(commonComboConfig) {
        return {
            id: this.authProviderIdPrefix + 'Imap',
            layout: 'form',
            autoHeight: 'auto',
            defaults: {
                width: 300,
                xtype: 'textfield',
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'authentication_Imap_host',
                fieldLabel: this.app.i18n._('Hostname')
            }, {
                name: 'authentication_Imap_port',
                fieldLabel: this.app.i18n._('Port'),
                xtype: 'numberfield'
            }, 
            Ext.applyIf({
                fieldLabel: this.app.i18n._('Secure Connection'),
                name: 'authentication_Imap_ssl',
                store: [['none', this.app.i18n._('None')], ['tls', this.app.i18n._('TLS')], ['ssl', this.app.i18n._('SSL')]],
                value: 'none'
            }, commonComboConfig), 
            {
                name: 'authentication_Imap_domain',
                fieldLabel: this.app.i18n._('Append domain to login name')
            }
            ]
        };
    },
    
    getAuthenticationBackendComboItems: function(commonComboConfig) {
        items = new Array();
        Ext.each(this.authenticationBackendComboStore, function(item){
            try {
                items.push(eval('this.getAuthentication'+item[0]+'Item(commonComboConfig)'));
            } catch(e) { 
                // backend plugins has not config items
            }
        }, this);
        return items;
    },
    
    getAccountStorageSqlItem: function(commonComboConfig) {
        return {
            id: this.accountsStorageIdPrefix + 'Sql',
            layout: 'form',
            autoHeight: 'auto',
            defaults: {
                width: 300,
                xtype: 'textfield',
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'accounts_Sql_defaultUserGroupName',
                fieldLabel: this.app.i18n._('Default user group name')                
            }, {
                name: 'accounts_Sql_defaultAdminGroupName',
                fieldLabel: this.app.i18n._('Default admin group name')                
            }]
        };
    },
    
    getAccountStorageLdapItem: function(commonComboConfig) {
        return {
            id: this.accountsStorageIdPrefix + 'Ldap',
            layout: 'form',
            autoHeight: 'auto',
            defaults: {
                width: 300,
                xtype: 'textfield',
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'accounts_Ldap_host',
                fieldLabel: this.app.i18n._('Host')
            }, {
                name: 'accounts_Ldap_username',
                fieldLabel: this.app.i18n._('Login name')
            }, {
                name: 'accounts_Ldap_password',
                fieldLabel: this.app.i18n._('Password'),
                inputType: 'password'
            }, 
            Ext.applyIf({
                name: 'accounts_Ldap_bindRequiresDn',
                fieldLabel: this.app.i18n._('Bind requires DN'),
                store: [['1', this.app.i18n._('Yes')], ['0', this.app.i18n._('No')]],
                value: '1'
            }, commonComboConfig),
            Ext.applyIf({
                name: 'accounts_Ldap_syncWhenNotFound',
                fieldLabel: this.app.i18n._("Sync user if not found in database"),
                store: [['1', this.app.i18n._('Yes')], ['0', this.app.i18n._('No')]],
                value: '1'
            }, commonComboConfig),
            {
                name: 'accounts_Ldap_userDn',
                fieldLabel: this.app.i18n._('User DN')
            },{
                name: 'accounts_Ldap_userOus',
                fieldLabel: this.app.i18n._('User OUs')
            },Ext.applyIf({
                name: 'accounts_Ldap_userLoginForUserIdgeneration',
                fieldLabel: this.app.i18n._('Use Login For Generate User ID'),
                store: [['1', this.app.i18n._('Yes')], ['0', this.app.i18n._('No')]],
                value: '0'
            }, commonComboConfig),{
                name: 'accounts_Ldap_userFilter',
                fieldLabel: this.app.i18n._('User Filter')
            },{
                name: 'accounts_Ldap_extraSchemas',
                fieldLabel: this.app.i18n._('Extra LDAP schema for the user')
            },{
                name: 'accounts_Ldap_extraAttributes',
                fieldLabel: this.app.i18n._('Extra LDAP attribute for the user')
            }, 
            Ext.applyIf({
                name: 'accounts_Ldap_userSearchScope',
                fieldLabel: this.app.i18n._('User Search Scope'),
                store: [['1', 'SEARCH_SCOPE_SUB'], ['2', 'SEARCH_SCOPE_ONE']],
                value: '1'
            }, commonComboConfig), 
            {
                name: 'accounts_Ldap_groupsDn',
                fieldLabel: this.app.i18n._('Groups DN')
            }, {
                name: 'accounts_Ldap_groupFilter',
                fieldLabel: this.app.i18n._('Group Filter')
            },{
                name: 'accounts_Ldap_groupSchemas',
                fieldLabel: this.app.i18n._('Extra LDAP schema for the groups')
            },{
                name: 'accounts_Ldap_groupAttributes',
                fieldLabel: this.app.i18n._('Extra LDAP attribute for the groups')
            },{
                name: 'accounts_Ldap_groupOU',
                fieldLabel: this.app.i18n._('Group OU')
            },
            Ext.applyIf({
                name: 'accounts_Ldap_groupSearchScope',
                fieldLabel: this.app.i18n._('Group Search Scope'),
                store: [['1', 'SEARCH_SCOPE_SUB'], ['2', 'SEARCH_SCOPE_ONE']],
                value: '1'
            }, commonComboConfig), 
            Ext.applyIf({
                name: 'accounts_Ldap_pwEncType',
                fieldLabel: this.app.i18n._('Password encoding'),
                store: [['CRYPT', 'CRYPT'], ['SHA', 'SHA'], ['MD5', 'MD5'], ['PLAIN', 'PLAIN']],
                value: 'CRYPT'
            }, commonComboConfig), 
            Ext.applyIf({
                name: 'accounts_Ldap_useRfc2307bis',
                fieldLabel: this.app.i18n._('Use Rfc 2307 bis'),
                store: [['0', this.app.i18n._('No')], ['1', this.app.i18n._('Yes')]],
                value: '0'
            }, commonComboConfig), 
            {
                name: 'accounts_Ldap_minUserId',
                fieldLabel: this.app.i18n._('Min User Id')
            }, {
                name: 'accounts_Ldap_maxUserId',
                fieldLabel: this.app.i18n._('Max User Id')
            }, {
                name: 'accounts_Ldap_minGroupId',
                fieldLabel: this.app.i18n._('Min Group Id')
            }, { 
               name: 'accounts_Ldap_maxGroupId',
                fieldLabel: this.app.i18n._('Max Group Id')
            }, {
                name: 'accounts_Ldap_groupUUIDAttribute',
                fieldLabel: this.app.i18n._('Group UUID Attribute name')
            }, {
                name: 'accounts_Ldap_userUUIDAttribute',
                fieldLabel: this.app.i18n._('User UUID Attribute name')
            }, {
                name: 'accounts_Ldap_defaultUserGroupName',
                fieldLabel: this.app.i18n._('Default user group name')
            }, {
                name: 'accounts_Ldap_defaultAdminGroupName',
                fieldLabel: this.app.i18n._('Default admin group name')
            },
            Ext.applyIf({
                name: 'accounts_Ldap_saveStatus',
                fieldLabel: this.app.i18n._('Save status information in LDAP'),
                store: [['0', this.app.i18n._('No')], ['1', this.app.i18n._('Yes')]],
                value: '1'
            }, commonComboConfig),
            Ext.applyIf({
                name: 'accounts_Ldap_readonly',
                fieldLabel: this.app.i18n._('Readonly access'),
                store: [['0', this.app.i18n._('No')], ['1', this.app.i18n._('Yes')]],
                value: '0'
            }, commonComboConfig),
            {
            name: 'accounts_Ldap_masterLdapHost',
            fieldLabel: this.app.i18n._('LDAP Master Server')
            }, {
                name: 'accounts_Ldap_masterLdapUsername',
                fieldLabel: this.app.i18n._('LDAP Master Admin')
            },{
                name: 'accounts_Ldap_masterLdapPassword',
                fieldLabel: this.app.i18n._('LDAP Master Admin Password'),
                inputType: 'password'
            },
            Ext.applyIf({
                name: 'accounts_Ldap_displaynameFormat',
                fieldLabel: this.app.i18n._('Display Name'),
                store: [['2', this.app.i18n._('Name surname')], ['1', this.app.i18n._('surname, name')],['0', this.app.i18n._('Use Other Attributes')] ],
                value: '1'
            }, commonComboConfig),
            {
                name: 'accounts_Ldap_displayName',
                fieldLabel: this.app.i18n._('Display Name Atrribute')
            },
            {
                name: 'accounts_Ldap_fullName',
                fieldLabel: this.app.i18n._('Full Name Atrribute')
            },
        ]
        };
    },
    
    getAccountsStorageComboItems: function(commonComboConfig) {
        items = new Array();
        Ext.each(this.accountsStorageComboStore, function(item){            
            items.push(eval('this.getAccountStorage'+item[0]+'Item(commonComboConfig)'));
        }, this);
        return items;
    },
    
   /**
     * returns config manager form
     * 
     * @private
     * @return {Array} items
     */
    getFormItems: function () {
        var setupRequired = Tine.Setup.registry.get('setupRequired');
                
        // common config for all combos in this setup
        var commonComboConfig = {
            xtype: 'combo',
            listWidth: 300,
            mode: 'local',
            forceSelection: true,
            allowEmpty: false,
            triggerAction: 'all',
            editable: false,
            tabIndex: this.getTabIndex
        };
        
        this.authenticationBackendComboStore = Tine.Setup.registry.get('authenticationBackends');
        
        this.accountsStorageComboStore = [['Sql', 'Sql'], ['Ldap', 'Ldap']];
        
        this.authenticationBackendCombo = new Ext.form.ComboBox(Ext.applyIf({
            name: 'authentication_backend',
            fieldLabel: this.app.i18n._('Backend'),
            store: this.authenticationBackendComboStore,
            value: 'Sql',
            width: 300,
            listeners: {
                scope: this,
                change: this.onChangeAuthProvider,
                select: this.onChangeAuthProvider
            }
        }, commonComboConfig));
        
        this.accountsStorageCombo = new Ext.form.ComboBox(Ext.applyIf({
            name: 'accounts_backend',
            fieldLabel: this.app.i18n._('Backend'),
            store: this.accountsStorageComboStore,
            value: 'Sql',
            width: 300,
            listeners: {
                scope: this,
                change: this.onChangeAccountsStorage,
                select: this.onChangeAccountsStorage
            }
        }, commonComboConfig));
        
        this.authenticationCheckExpiredPassword = new Ext.form.ComboBox(Ext.applyIf({
            name: 'accounts_Ldap_checkExpiredPassword',
            fieldLabel: this.app.i18n._('Check for expired password'),
            store: [['0', this.app.i18n._('No')], ['1', this.app.i18n._('Yes')]],
            value: '0',
            width: 300,
            listeners: {
                scope: this,
                change: this.onChangePasswordExpiration,
                select: this.onChangePasswordExpiration
            }
        }, commonComboConfig));

        this.ldapMailListControl = new Ext.form.ComboBox(Ext.applyIf({
            name: 'accounts_Ldap_mailListControl',
            fieldLabel: this.app.i18n._('Activate Mail List Control'),
            store: [['0', this.app.i18n._('No')], ['1', this.app.i18n._('Yes')]],
            value: '0',
            width: 300,
            listeners: {
                scope: this,
                change: this.onChangeMailList,
                select: this.onChangeMailList
            }
        }, commonComboConfig));
        
        this.authenticationBackendComboItems = this.getAuthenticationBackendComboItems(commonComboConfig);
        this.accountsStorageComboItems       = this.getAccountsStorageComboItems(commonComboConfig);

        var formItens = [];
        if (!Tine.Tinebase.registry.get('multidomain') || this.domain == Tine.Setup.registry.get('domainData')['activeDomain']) {
            formItens = formItens.concat({
                xtype: 'fieldset',
                collapsible: true,
                collapsed: !setupRequired,
                autoHeight: true,
                title: this.app.i18n._('Initial Admin User'),
                items: [{
                    layout: 'form',
                    autoHeight: 'auto',
                    border: false,
                    defaults: {
                        width: 300,
                        xtype: 'textfield',
                        inputType: 'password',
                        tabIndex: this.getTabIndex
                    },
                    items: [{
                        name: 'authentication_Sql_adminLoginName',
                        fieldLabel: this.app.i18n._('Initial admin login name'),
                        inputType: 'text',
                        disabled: !setupRequired
                    }, {
                        name: 'authentication_Sql_adminPassword',
                        fieldLabel: this.app.i18n._('Initial admin Password'),
                        disabled: !setupRequired
                    }, {
                        name: 'authentication_Sql_adminPasswordConfirmation',
                        fieldLabel: this.app.i18n._('Password confirmation'),
                        disabled: !setupRequired
                    }]
                }]
            });
        }

        return formItens.concat({
            xtype: 'fieldset',
            collapsible: false,
            autoHeight: true,
            title: this.app.i18n._('Authentication provider'),
            items: [
                this.authenticationBackendCombo,
                {
                    id: this.authProviderIdPrefix + 'CardLayout',
                    layout: 'card',
                    activeItem: this.authProviderIdPrefix + 'Sql',
                    border: false,
                    defaults: {border: false},
                    items: this.getAuthenticationBackendComboItems(commonComboConfig)
                }
            ]
        }, {
            xtype: 'fieldset',
            collapsible: false,
            autoHeight: true,
            title: this.app.i18n._('Accounts storage'),
            items: [
                this.accountsStorageCombo,
                {
                    id: this.accountsStorageIdPrefix + 'CardLayout',
                    layout: 'card',
                    activeItem: this.accountsStorageIdPrefix + 'Sql',
                    border: false,
                    defaults: {
                        border: false
                    },
                    items: this.getAccountsStorageComboItems(commonComboConfig)
                } 
            ]
        }, {
            xtype: 'fieldset',
            id: this.idPrefix+'ldap_password_policy',
            collapsible: false,
            autoHeight: true,
            title: this.app.i18n._('LDAP Password Expiration Policy'),
            items: [
                this.authenticationCheckExpiredPassword,
                {
                            id: this.passwordExpirationdPrefix + 'CardLayout',
                            layout: 'card',
                            activeItem: this.passwordExpirationdPrefix + '0',
                            border: false,
                            defaults: {
                                border: false
                            },
                            items: [{
                                id: this.passwordExpirationdPrefix + '0',
                                layout: 'form',
                                autoHeight: 'auto'
                                },{
                                id: this.passwordExpirationdPrefix + '1',
                                layout: 'form',
                                autoHeight: 'auto',
                                defaults: {
                                    width: 300,
                                    xtype: 'textfield',
                                    tabIndex: this.getTabIndex
                                },                                
                                items: [{
                                        id: this.idPrefix+'accounts_Ldap_passwordExpirationAttribute',
                                        name: 'accounts_Ldap_passwordExpirationAttribute',
                                        fieldLabel: this.app.i18n._('Password expiration attribute name')
                                    },
                                    Ext.applyIf({
                                        id: this.idPrefix+'accounts_Ldap_passwordExpirationInterval',
                                        name: 'accounts_Ldap_passwordExpirationInterval',
                                        fieldLabel: this.app.i18n._('Password expiration interval'),
                                        store: [
                                            ['1', this.app.i18n._('Immediate')],
                                            ['7', String.format(this.app.i18n._('{0} days'), '7')],
                                            ['15', String.format(this.app.i18n._('{0} days'), '15')],
                                            ['30', String.format(this.app.i18n._('{0} days'), '30')],
                                            ['45', String.format(this.app.i18n._('{0} days'), '45')],
                                            ['60', String.format(this.app.i18n._('{0} days'), '60')],
                                            ['90', String.format(this.app.i18n._('{0} days'), '90')],
                                            ['120', String.format(this.app.i18n._('{0} days'), '120')],
                                            ['150', String.format(this.app.i18n._('{0} days'), '150')],
                                            ['180', String.format(this.app.i18n._('{0} days'), '180')],
                                            ['0', this.app.i18n._('Never')]],
                                        value: '0'
                                    }, commonComboConfig)
                                ]
                                }
                            ]
                }
            ]
          }, {
            xtype: 'fieldset',
            id: this.idPrefix+'ldap_maillist_control',
            collapsible: false,
            autoHeight: true,
            title: this.app.i18n._('LDAP Mail List Control'),
            items: [
                this.ldapMailListControl,
                {
                            id: this.ldapMailListPrefix + 'CardLayout',
                            layout: 'card',
                            activeItem: this.ldapMailListPrefix + '0',
                            border: false,
                            defaults: {
                                border: false
                            },
                            items: [{
                                id: this.ldapMailListPrefix + '0',
                                layout: 'form',
                                autoHeight: 'auto'
                                },{
                                id: this.ldapMailListPrefix + '1',
                                layout: 'form',
                                autoHeight: 'auto',
                                defaults: {
                                    width: 300,
                                    xtype: 'textfield',
                                    tabIndex: this.getTabIndex
                                },
                                items: [{
                                        id: this.idPrefix+'accounts_Ldap_mailListDn',
                                        name: 'accounts_Ldap_mailListDn',
                                        fieldLabel: this.app.i18n._('List DN')
                                    },{
                                        id: this.idPrefix+'accounts_Ldap_mailListGid',
                                        name: 'accounts_Ldap_mailListGid',
                                        fieldLabel: this.app.i18n._('List Group Id')
                                    },{
                                        id: this.idPrefix+'accounts_Ldap_mailListFilter',
                                        name: 'accounts_Ldap_mailListFilter',
                                        fieldLabel: this.app.i18n._('Lists Filter')
                                    },{
                                        id: this.idPrefix+'accounts_Ldap_mailListSchemas',
                                        name: 'accounts_Ldap_mailListSchemas',
                                        fieldLabel: this.app.i18n._('Extra LDAP schema for the Lists')
                                    },{
                                        id: this.idPrefix+'accounts_Ldap_mailListAttributes',
                                        name: 'accounts_Ldap_mailListAttributes',
                                        fieldLabel: this.app.i18n._('Extra LDAP attributes for the Lists')
                                    }
                                ]
                                }
                            ]
                }
            ]
          }, {
            xtype: 'fieldset',
            collapsible: false,
            autoHeight: true,
            title: this.app.i18n._('Login panel'),
            defaults: {
                width: 300,
                xtype: 'uxspinner',
                tabIndex: this.getTabIndex,
                strategy: {
                    xtype: 'number',
                    minValue: 0,
                    maxValue: 64
                },
                value: 0
            },
            items: [
            Ext.applyIf({
                name: 'saveusername_saveusername',
                fieldLabel: this.app.i18n._('Reuse last username logged'),
                store: [[1, this.app.i18n._('Yes')], [0, this.app.i18n._('No')]],
                value: 0
            }, commonComboConfig)
            ]
        }, {
            xtype: 'fieldset',
            collapsible: false,
            autoHeight: true,
            title: this.app.i18n._('Password Settings'),
            defaults: {
                width: 300,
                xtype: 'uxspinner',
                tabIndex: this.getTabIndex,
                strategy: {
                    xtype: 'number',
                    minValue: 0,
                    maxValue: 64
                },
                value: 0
            },
            items: [
            Ext.applyIf({
                name: 'password_changepw',
                fieldLabel: this.app.i18n._('User can change password'),
                store: [[1, this.app.i18n._('Yes')], [0, this.app.i18n._('No')]],
                value: 1
            }, commonComboConfig),
            Ext.applyIf({
                name: 'password_pwPolicyActive',
                fieldLabel: this.app.i18n._('Enable password policy'),
                store: [[1, this.app.i18n._('Yes')], [0, this.app.i18n._('No')]],
                value: 0
            }, commonComboConfig),
            Ext.applyIf({
                name: 'password_pwPolicyOnlyASCII',
                fieldLabel: this.app.i18n._('Only ASCII'),
                store: [[1, this.app.i18n._('Yes')], [0, this.app.i18n._('No')]],
                value: 0
            }, commonComboConfig), {
                name: 'password_pwPolicyMinLength',
                fieldLabel: this.app.i18n._('Minimum length')
            }, {
                name: 'password_pwPolicyMinWordChars',
                fieldLabel: this.app.i18n._('Minimum word chars')
            }, {
                name: 'password_pwPolicyMinUppercaseChars',
                fieldLabel: this.app.i18n._('Minimum uppercase chars')
            }, {
                name: 'password_pwPolicyMinSpecialChars',
                fieldLabel: this.app.i18n._('Minimum special chars')
            }, {
                name: 'password_pwPolicyMinNumbers',
                fieldLabel: this.app.i18n._('Minimum numbers')
            },
            Ext.applyIf({
                name: 'password_pwPolicyForbidUsername',
                fieldLabel: this.app.i18n._('Forbid part of username in password'),
                store: [[1, this.app.i18n._('Yes')], [0, this.app.i18n._('No')]],
                value: 0
            }, commonComboConfig),
            Ext.applyIf({
                name: 'password_pwPolicyForbidSamePassword',
                fieldLabel: this.app.i18n._('It is not allowed to repeat the same password'),
                store: [[1, this.app.i18n._('Yes')], [0, this.app.i18n._('No')]],
                value: 0
            }, commonComboConfig)
            ]
        }, {
            xtype: 'fieldset',
            collapsible: false,
            autoHeight: true,
            title: this.app.i18n._('Redirect Settings'),
            defaults: {
                width: 300,
                xtype: 'textfield',
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'redirectSettings_redirectUrl',
                fieldLabel: this.app.i18n._('Redirect Url (redirect to login screen if empty)')
            }, 
            Ext.applyIf({
                name: 'redirectSettings_redirectAlways',
                fieldLabel: this.app.i18n._('Redirect Always (if No, only redirect after logout)'),
                store: [['1', this.app.i18n._('Yes')], ['0', this.app.i18n._('No')]],
                value: '0'
            }, commonComboConfig), 
            Ext.applyIf({
                name: 'redirectSettings_redirectToReferrer',
                fieldLabel: this.app.i18n._('Redirect to referring site, if exists'),
                store: [['1', this.app.i18n._('Yes')], ['0', this.app.i18n._('No')]],
                value: '0'
            }, commonComboConfig)]
        });
    },
    
    /**
     * applies registry state to this cmp
     */
    applyRegistryState: function () {
        this.action_saveConfig.setDisabled(false);
        
        if (!Tine.Tinebase.registry.get('multidomain')) {
            var domainData = Tine.Setup.registry.get('domainData');
            if (this.domain != domainData['activeDomain']) {
                this.action_saveConfig.setText(this.app.i18n._('Save config'));
                this.getForm().findField('authentication_Sql_adminPassword').setDisabled(true);
                this.getForm().findField('authentication_Sql_adminPasswordConfirmation').setDisabled(true);
                this.getForm().findField('authentication_Sql_adminLoginName').setDisabled(true);
                return;
            }
        }

        if (Tine.Setup.registry.get('setupRequired')) {
            this.action_saveConfig.setText(this.app.i18n._('Save config and install'));
        } else {
            this.action_saveConfig.setText(this.app.i18n._('Save config'));
            if (this.domain == Tine.Setup.registry.get('domainData')['activeDomain']) {
                this.getForm().findField('authentication_Sql_adminPassword').setDisabled(true);
                this.getForm().findField('authentication_Sql_adminPasswordConfirmation').setDisabled(true);
                this.getForm().findField('authentication_Sql_adminLoginName').setDisabled(true);
            }
        }
    },
    
    /**
     * checks if form is valid
     * - password fields are equal
     * 
     * @return {Boolean}
     */
    isValid: function () {
        var form = this.getForm();

        var result = form.isValid();

        if (!Tine.Tinebase.registry.get('multidomain') || this.domain == Tine.Setup.registry.get('domainData')['activeDomain']) {
            // check if passwords match
            if (this.authenticationBackendCombo.getValue() === 'Sql' &&
                form.findField('authentication_Sql_adminPassword') &&
                form.findField('authentication_Sql_adminPassword').getValue() !== form.findField('authentication_Sql_adminPasswordConfirmation').getValue())
            {
                form.markInvalid([{
                    id: 'authentication_Sql_adminPasswordConfirmation',
                    msg: this.app.i18n._("Passwords don't match")
                }]);
                result = false;
            }

            // check if initial username/passwords are set
            if (Tine.Setup.registry.get('setupRequired') && form.findField('authentication_Sql_adminLoginName')) {
                if (Ext.isEmpty(form.findField('authentication_Sql_adminLoginName').getValue())) {
                    form.markInvalid([{
                        id: 'authentication_Sql_adminLoginName',
                        msg: this.app.i18n._("Should not be empty")
                    }]);
                    result = false;
                }
                if (Ext.isEmpty(form.findField('authentication_Sql_adminPassword').getValue())) {
                    form.markInvalid([{
                        id: 'authentication_Sql_adminPassword',
                        msg: this.app.i18n._("Should not be empty")
                    }]);
                    form.markInvalid([{
                        id: 'authentication_Sql_adminPasswordConfirmation',
                        msg: this.app.i18n._("Should not be empty")
                    }]);
                    result = false;
                }
            }
        }
        
        if (this.accountsStorageCombo.getValue() === 'Sql' && 
               form.findField('accounts_Sql_defaultUserGroupName') && 
               Ext.isEmpty(form.findField('accounts_Sql_defaultUserGroupName').getValue())) 
          {
            form.markInvalid([{
                id: 'accounts_Sql_defaultUserGroupName',
                msg: this.app.i18n._("Should not be empty")
            }]);
            result = false;
        }
        
        if (this.accountsStorageCombo.getValue() === 'Sql' && 
            form.findField('accounts_Sql_defaultAdminGroupName') && 
            Ext.isEmpty(form.findField('accounts_Sql_defaultAdminGroupName').getValue())) 
        {
            form.markInvalid([{
                id: 'accounts_Sql_defaultAdminGroupName',
                msg: this.app.i18n._("Should not be empty")
            }]);
            result = false;
        }
        
        //Check if LDap password expiration is on, attribute name is not blank
        if (this.authenticationCheckExpiredPassword.getValue() === '1' &&
                form.findField(this.idPrefix+'accounts_Ldap_passwordExpirationAttribute') &&
                Ext.isEmpty(form.findField(this.idPrefix+'accounts_Ldap_passwordExpirationAttribute').getValue()))
        {
            form.markInvalid([{
                    id: this.idPrefix+'accounts_Ldap_passwordExpirationAttribute',
                    msg: this.app.i18n._('Should not be empty')
            }]);
            result = false;
        }
        
        //Check if LDap password expiration is on, interval is not blank
        if (this.authenticationCheckExpiredPassword.getValue() === '1' &&
                form.findField(this.idPrefix+'accounts_Ldap_passwordExpirationInterval') &&
                Ext.isEmpty(form.findField(this.idPrefix+'accounts_Ldap_passwordExpirationInterval').getValue()))
        {
            form.markInvalid([{
                    id: this.idPrefix+'accounts_Ldap_passwordExpirationInterval',
                    msg: this.app.i18n._('Should not be empty')
            }]);
            result = false;
        }

        //Check if LDap mail list control is on, attribute base dn is not blank
        if (this.ldapMailListControl.getValue() === '1' &&
                form.findField(this.idPrefix+'accounts_Ldap_mailListDn') &&
                Ext.isEmpty(form.findField(this.idPrefix+'accounts_Ldap_mailListDn').getValue()))
        {
            form.markInvalid([{
                    id: this.idPrefix+'accounts_Ldap_mailListDn',
                    msg: this.app.i18n._('Should not be empty')
            }]);
            result = false;
        }

        //Check if LDap mail list control is on, attribute gid is not blank
        if (this.ldapMailListControl.getValue() === '1' &&
                form.findField(this.idPrefix+'accounts_Ldap_mailListDn') &&
                Ext.isEmpty(form.findField(this.idPrefix+'accounts_Ldap_mailListGid').getValue()))
        {
            form.markInvalid([{
                    id: this.idPrefix+'accounts_Ldap_mailListGid',
                    msg: this.app.i18n._('Should not be empty')
            }]);
            result = false;
        }
        
        return result;
    },

    /**
     * update setup registry
     */
    afterSaveConfig: function(regData) {
        // replace some registry data
        for (key in regData) {
            if (key != 'status') {
                Tine.Setup.registry.replace(key, regData[key]);
            }
        }
    }
});
