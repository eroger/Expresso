<?php
/**
 * Tine 2.0
 *
 * @package     Expressodriver
 * @subpackage  Session
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 * @copyright   Copyright (c) 2009-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * Session class for Expressodriver
 *
 * @package     Expressodriver
 * @subpackage  Session
 */
class Expressodriver_Session extends Tinebase_Session_Abstract
{
}
