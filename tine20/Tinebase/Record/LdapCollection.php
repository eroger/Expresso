<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Record
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 */

/**
 * class to hold a ldap collection
 *
 * @package     Tinebase
 * @subpackage  Record
 *
 */
class Tinebase_Record_LdapCollection extends Zend_Ldap_Collection
{

    /**
     * Error code for partial or no results
     * @var integer
     */
    private $_errorCode;

    /**
     * The message to be displayed for user
     * @var string
     */
    private $_message;

    /**
     * Constructor of the class
     * @param Zend_Ldap_Collection_Iterator_Interface $iterator
     * @param bool $_partialResults
     */
    public function __construct(Zend_Ldap_Collection_Iterator_Default $iterator, $_errorCode = 0, $_message = "")
    {
        parent::__construct($iterator);
        $this->_errorCode = $_errorCode;
        $this->_message = $_message;
    }

    /**
     * Return error code
     * @return integer
     */
    public function getErrorCode()
    {
        return $this->_errorCode;
    }

    /**
     * Return error message
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->_message;
    }

    /**
     * Returns if the result is partial
     * @return boolean
     */
    public function isPartialResults()
    {
        return (bool)($this->_errorCode == Zend_Ldap_Exception::LDAP_SIZELIMIT_EXCEEDED ||
                $this->_errorCode == Zend_Ldap_Exception::LDAP_PARTIAL_RESULTS);
    }
}