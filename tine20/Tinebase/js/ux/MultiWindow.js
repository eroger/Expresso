/*
 * Tine 2.0
 *
 * @package     Expressomail
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Fernando Lages <fernando.lages@serpro.gov.br>
 * @copyright   Copyright (c) 2009-2014 Serpro (http://serpro.gov.br)
 */

/**
 *
 * @class Tine.MultiWindow
 * @extends Ext.Window
 * for now it works pretty much the same as Ext.Window
 * but implements a notification tool on top of window
 * TODO: implement multiple windows feature
 */
Tine.MultiWindow = Ext.extend(Ext.Window, {
    notifiable : false,
    minimizable: false,
    maximizable: false,
    customized: false,
    minSize: 250,
    maxSize: 300,
    maximized: false,
    minimized: false,
    iconCls: '',

    // private
    initComponent : function(){
        Tine.MultiWindow.superclass.initComponent.call(this);
    },

    /**
     * initialize tools
     */
    initTools : function(){
        if(this.headerAsText && this.notifiable){
            this.addTool({
                id: 'notify',
                handler: this.notify.createDelegate(this, [])
            });
        }
        Tine.MultiWindow.superclass.initTools.call(this);
    },

    /**
     * fire event notify
     */
    notify : function(){
        this.fireEvent('notify', this);
        return this;
    },

    /**
     * set tools notify
     */
    setNotify : function(notify, what){
        if(this.header && this.headerAsText && this.notifiable) {
            if (what) {
                notify_class = 'x-tool-notify-'+what;
            }
            else {
                if (notify==='') {
                    notify_class = 'x-tool-notify-clear';
                }
                else {
                    notify_class = 'x-tool-notify-default';
                }
            }
            this.tools.notify.removeClass('x-tool-notify-clear');
            this.tools.notify.removeClass('x-tool-notify-default');
            this.tools.notify.removeClass('x-tool-notify-error');
            this.tools.notify.addClass(notify_class);
            this.tools.notify.dom.innerHTML = notify;
            this.fireEvent('notifychange', this, notify);
        }
        return this;
    },

    /**
     * <p>Closes the Window, removes it from the DOM, {@link Ext.Component#destroy destroy}s
     * the Window object and all its descendant Components. The {@link Ext.Panel#beforeclose beforeclose}
     * event is fired before the close happens and will cancel the close action if it returns false.<p>
     * <p><b>Note:</b> This method is not affected by the {@link #closeAction} setting which
     * only affects the action triggered when clicking the {@link #closable 'close' tool in the header}.
     * To hide the Window without destroying it, call {@link #hide}.</p>
     */
    close : function(){
        var manager = this.manager;
        Tine.MultiWindow.superclass.close.call(this);
        this.relocate(manager);
    },

    addWindowToWinBar: function() {
        var minimizedWindowsBar = Ext.getCmp('minimized-windows-bar'),
            id = this.id,
            isTabActive = false;

        minimizedWindowsBar.items.each(function(item, index) {
            if (item.ctid === this.id) {
                // this window is already in tab chat bar...
                minimizedWindowsBar.setActiveTab(item);
                isTabActive = true;
            }
        });

        if (isTabActive) return;

        minimizedWindowsBar.add(
            {
                ctid    : id,
                xtype   : 'windowpanel',
                iconCls : this.iconCls,
                title   : this.title
            }
        );

        var index = minimizedWindowsBar.items.getCount()-1;
        minimizedWindowsBar.setActiveTab(index);
    },

    // private
    beforeShow : function(){
        delete this.el.lastXY;
        delete this.el.lastLT;
        if(this.x === undefined || this.y === undefined){
            if (this.multiple) {
                var xy = this.el.getAlignToXY(this.container, 'br-br');
            } else {
                var xy = this.el.getAlignToXY(this.container, 'c-c');
            }
            var pos = this.el.translatePoints(xy[0], xy[1]);
            this.x = this.x === undefined? pos.left : this.x;
            this.y = this.y === undefined? pos.top : this.y;
        }
        this.el.setLeftTop(this.x, this.y);

        if(this.expandOnShow){
            this.expand(false);
        }

        if(this.modal){
            Ext.getBody().addClass('x-body-masked');
            this.mask.setSize(Ext.lib.Dom.getViewWidth(true), Ext.lib.Dom.getViewHeight(true));
            this.mask.show();
        }
        this.relocate();
    },

    anchorTo : function(el, alignment, offsets, monitorScroll){
      if(this.doAnchor){
            Ext.EventManager.removeResizeListener(this.doAnchor, this);
            Ext.EventManager.un(window, 'scroll', this.doAnchor, this);
            Ext.EventManager.un(Tine.Tinebase.MainScreen.getActiveWinbar(), 'bodyresize', this.doAnchor, this);
      }
      this.doAnchor = function(i){
            this.alignTo(el, alignment, offsets);
      };

      var tm = typeof monitorScroll;
      if(tm != 'undefined'){
            Ext.EventManager.on(window, 'scroll', this.doAnchor, this,
                {buffer: tm == 'number' ? monitorScroll : 50});
            Ext.EventManager.on(Tine.Tinebase.MainScreen.getActiveWinbar(), 'bodyresize', this.doAnchor, this);
      }
      return this;
    },

    relocate : function(manager) {
        if (!this.multiple) {
            return;
        }
        this.relocating = true;
        var winOffset = 0;
        if (!manager || !Ext.isObject(manager)) {
            manager = this.manager;
        }
        var mngr = [];
        if (!manager) {
            this.relocating = false;
            return;
        }
        if (manager.getCount()===1) {
            manager.windowIndex = 0;
        }
        manager.each(
            function(w) {
                mngr.push(w);
            }
        );
        var mw = 0; // number of minimized windows
        manager.each(function(w){if(w.minimized)mw++;});
        var ww = Tine.Tinebase.viewport.getWidth()/mw;
        ww = (ww > this.maxSize ? this.maxSize : (ww < this.minSize ? this.minSize : ww));
        var winCount = manager.windowIndex;
        Ext.each(
            mngr,
            function(w) {
                if (!w.hidden) {
                    if (w.minimized) {
                        // organize the minimized windows
                        w.setWidth(ww);
                        w.anchorTo(this.container, 'br-br', [-winOffset,0]);
                        winOffset += ww;
                        w.doAnchor();
                    } else {
                        if (w.customized) {
                            // do nothing, keep the windows as the user had set
                        } else {
                            if (!w.restorePos) {
                                // show window in cascade mode
                                w.anchorTo(this.container, 'br-br', [-winCount*30,-30-(winCount*25)]);
                                w.doAnchor();
                                w.restorePos = w.getPosition(true);
                                w.manager.windowIndex++;
                            }
                        }
                    }
                }
            },
            this, true
        );
        this.relocating = false;
    },

    /**
     * Fits the window within its current container and automatically replaces
     * the {@link #maximizable 'maximize' tool button} with the 'restore' tool button.
     * Also see {@link #toggleMaximize}.
     * @return {Ext.Window} this
     */
    maximize : function(){
        if(!this.maximized){
            this.expand(false);
            if (!this.minimized) {
                this.restoreSize = this.getSize();
                this.restorePos = this.getPosition(true);
            }
            if (this.maximizable){
                this.tools.minimize.show();
                this.tools.maximize.hide();
                this.tools.restore.show();
            }
            this.minimized = false;
            this.maximized = true;
            this.el.disableShadow();

            if(this.dd){
                this.dd.lock();
            }
            if(this.collapsible){
                this.tools.toggle.hide();
            }
            this.el.addClass('x-window-maximized');
            this.container.addClass('x-window-maximized-ct');

            this.setPosition(0, 0);
            this.fitContainer();
            this.fireEvent('maximize', this);
        }
        return this;
    },

    /**
     * Fits the window within its current minimized windows bar and automatically replaces
     * the {@link #minimizable 'minimize' tool button} with the 'restore' tool button.
     * Also see {@link #toggleMinimize}.
     * @return {Ext.Window} this
     */
    minimize : function(){
        if(!this.minimized){
            if (!this.maximized) {
                this.restoreSize = this.getSize();
                this.restorePos = this.getPosition(true);
            }
            this.collapse(false);
            if (this.minimizable){
                this.tools.minimize.hide();
                this.tools.maximize.show();
                this.tools.restore.show();
            }
            this.maximized = false;
            this.minimized = true;
            this.el.disableShadow();

            if(this.dd){
                this.dd.lock();
            }
            if(this.collapsible){
                this.tools.toggle.hide();
            }
            this.el.addClass('x-window-minimized');
            this.container.addClass('x-window-minimized-ct');

            this.setWidth(this.minSize);
            this.relocate();
            this.hide();
            this.addWindowToWinBar();
            this.fireEvent('minimize', this);
        }
        return this;
    },

    /**
     * Restores a {@link #maximizable maximized}  window back to its original
     * size and position prior to being maximized and also replaces
     * the 'restore' tool button with the 'maximize' tool button.
     * Also see {@link #toggleMaximize}.
     * @return {Ext.Window} this
     */
    restore : function(){
        if(this.maximized){
            var t = this.tools;
            this.el.removeClass('x-window-maximized');
            if(t.restore){
                t.restore.hide();
            }
            if(t.maximize){
                t.maximize.show();
            }
            this.maximized = false;
            this.setPosition(this.restorePos[0], this.restorePos[1]);
            this.setSize(this.restoreSize.width, this.restoreSize.height);
            this.relocate();
            this.el.enableShadow(true);

            if(this.dd){
                this.dd.unlock();
            }
            if(this.collapsible && t.toggle){
                t.toggle.show();
            }
            this.container.removeClass('x-window-maximized-ct');

            this.doConstrain();
            this.fireEvent('restore', this);
        }
        else if(this.minimized) {
            var t = this.tools;
            this.expand(false);
            this.el.removeClass('x-window-minimized');
            if(t.restore){
                t.restore.hide();
            }
            if(t.minimize){
                t.minimize.show();
            }
            this.minimized = false;
            this.relocating = true;
            this.setPosition(this.restorePos[0], this.restorePos[1]);
            this.setSize(this.restoreSize.width, this.restoreSize.height);
            this.relocate();
            this.el.enableShadow(true);

            if(this.dd){
                this.dd.unlock();
            }
            if(this.collapsible && t.toggle){
                t.toggle.show();
            }
            this.container.removeClass('x-window-minimized-ct');

            this.doConstrain();
            this.fireEvent('restore', this);
        }
        this.degrade();
        return this;
    },

    degrade : function() {
        this.callSetBgColor = new Ext.util.DelayedTask(this.setBgColor, this);
        this.bgColor = this.header.getStyle('background-color');
        this.steps = 20;
        this.setBgColor();
    },

    blink : function() {
        this.callSetBgColor = new Ext.util.DelayedTask(this.setBgColor, this);
        this.bgColor = this.header.getStyle('background-color');
        this.blinks = 6;
        this.setBgColor();
    },

    setBgColor : function() {
        if (this.blinks>0) {
            if (Math.round(this.blinks/2) !== (this.blinks/2)) {
                this.el.dom.style.setProperty('background-color','');
            } else {
                this.el.dom.style.setProperty('background-color','#f4f4a4','important');
            }
            this.callSetBgColor.delay(300);
        } else if (this.steps>0) {
            var bgc = (228+this.steps).toString()+','+(228+this.steps).toString()+','+(228-this.steps*2).toString();
            this.el.dom.style.setProperty('background-color','rgb('+bgc+')','important');
            this.callSetBgColor.delay(100);
        } else {
            this.el.dom.style.setProperty('background-color','');
        }
        this.blinks--;
        this.steps--;
    },

    onPosition : function(w, h) {
        Tine.MultiWindow.superclass.onPosition.call(this, w, h);
        if (this.restorePos && !this.minimized && !this.maximized && !this.relocating) {
            this.customized = true;
        }
    },

    onResize : function(w, h) {
        Tine.MultiWindow.superclass.onResize.call(this, w, h);
        if (this.restorePos && !this.minimized && !this.maximized && !this.relocating) {
            this.customized = true;
        }
        if (this.manager) {
            this.relocate();
        }
    },

    onWindowResize : function(e, w) {
        if (this.restorePos && !this.minimized && !this.maximized && !this.relocating) {
            this.customized = true;
        }
        if (this.manager) {
            this.relocate();
        }
    }

});
