<?php
/**
* Connectionconfig operations
*
* @package Tinebase
* @subpackage Shard
* @license http://www.gnu.org/licenses/agpl.html AGPL Version 3
* @author Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
* @copyright Copyright (c); 2015 Metaways Infosystems GmbH (http://www.metaways.de);
*
*/

/**
* Filesystem backend class for Connectionconfig
* @package Tinebase
* @subpackage Shard
*/
class Tinebase_Shard_Connectionconfig_Filesystem implements Tinebase_Shard_Connectionconfig_Interface
{
   /**
    * Full path of file where content of this backend is persisted
    *
    * @var string
    */
    private $_fileName;

    /**
    * holds the instance of the singleton
    *
    * @var Tinebase_Shard_Connectionconfig_Filesystem
    */
    private static $_instance = NULL;

    /**
    * the constructor
    *
    * don't use the constructor. use the singleton
    */
    private function __construct() {}

    /**
    * don't clone. Use the singleton.
    */
    private function __clone() {}

    /**
    * the singleton pattern
    *
    * @param array  $_options
    * @param string $_database
    * @return Tinebase_Shard_Connectionconfig_Filesystem
    */
    public static function getInstance(array $_options, $_database)
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Tinebase_Shard_Connectionconfig_Filesystem;
            self::$_instance->_fileName = $_options['filePath'] . DIRECTORY_SEPARATOR
                                        . 'connectionConfig.' . $_database . '.'
                                        . Tinebase_Config::getDomain();
            if (!file_exists(self::$_instance->_fileName)) {
                $connectionConfigs = serialize(array('connectionConfig' => array()));
                $result = file_put_contents(self::$_instance->_fileName, $connectionConfigs, LOCK_EX);
                if ($result === FALSE) {
                    throw new Exception('Can not create Connection Config file: ' . $this->_fileName);
                }
            }
        }
        return self::$_instance;
    }

   /**
    * get Connectionconfig
    *
    * @param string $_key
    * @return array | FALSE
    */
    public function getConnectionConfig($_key)
    {
        $result = unserialize(file_get_contents($this->_fileName));
        if ($result === FALSE) {
            throw new Exception('Can not read from Connection Config file: ' . $this->_fileName);
        }

        if (is_array($result['connectionConfig']) && array_key_exists($_key, $result['connectionConfig'])) {
            return $result['connectionConfig'][$_key];
        }

        return FALSE;
    }

    /**
    * set Connectionconfig
    *
    * @param string $_key
    * @param array $_parameters
    * @return boolean
    */
    public function setConnectionConfig($_key, $_parameters)
    {
        $result = unserialize(file_get_contents($this->_fileName));
        if ($result === FALSE) {
            throw new Exception('Can not read from Connection Config file: ' . $this->_fileName);
        }

        $result['connectionConfig'][$_key] = $_parameters;
        $resultPut = file_put_contents($this->_fileName, serialize($result), LOCK_EX);

        if ($resultPut === FALSE) {
            throw new Exception('Can not write to Connection Config file: ' . $this->_fileName);
        }

        return $resultPut;
    }

    /**
    * remove Connectionconfig
    *
    * @param string $_key
    * @return boolean
    */
    public function removeConnectionConfig($_key)
    {
        $result = unserialize(file_get_contents($this->_fileName));
        if ($result === FALSE) {
            throw new Exception('Can not read from Connection Config file: ' . $this->_fileName);
        }

        if (isset($result['connectionConfig'][$_key])) {
            unset($result['connectionConfig'][$_key]);

            $resultPut = file_put_contents($this->_fileName, serialize($result), LOCK_EX);

            if ($resultPut === FALSE) {
                throw new Exception('Can not write to Connection Config file: ' . $this->_fileName);
            }

            return $resultPut;
        }  else {
            return TRUE; //nothing to unset
        }
    }
}