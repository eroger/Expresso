<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy
 * @subpackage  Context
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 *
 */

/**
 * Implements the Context to select strategy that associate Shardkeys and Virtual Shards
 *
 * @package     Tinebase_Shard
 */
class Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy_Context
{
    /**
     * Class name that implements selected strategy
     *
     * @param  string
     */
    private  $_strategy = NULL;

    /**
     * Enforce interface restrictions
     *
     * @param  Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy_Interface $_strategy
     */
    private function instantiateStrategy(Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy_Interface $_strategy)
    {
        unset($this->_strategy);
        $this->_strategy = $_strategy;
    }

    /**
     * Select strategy
     *
     * @param  string $_strategy
     * @return boolean || NULL
     */
    public function setStrategy($_strategy)
    {
        $strategyClass = 'Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy_' . $_strategy;

        if (class_exists($strategyClass)) {
            $this->instantiateStrategy(new $strategyClass());
            return true;
        }
        return NULL;
    }

    /**
     * Get VirtualShard number associated with shardKey from selected strategy
     *
     * @param  string $_database
     * @param  string $_shardKey
     * @return integer || NULL
     */
    public function getVirtualShard($_database, $_shardKey)
    {
        return $this->_strategy->getVirtualShard($_database, $_shardKey);
    }
}