<?php
/**
 * interface for Redistributer class
 *
 * @package     Shard
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright   Copyright (c); 2015 Metaways Infosystems GmbH (http://www.metaways.de);
 *
 */

/**
 * interface for Redistributer class
 *
 * @package     Shard
 */
interface Tinebase_Shard_Redistributer_Interface
{
    /**
     * Constructor
     *
     * @param  string    $_database
     */
    function __construct($_database);

    /**
     * Fetch all Shard Key data
     *
     * @param string $_shardKey
     * @param string $_backend_connection_config
     * @return array
     */
    public function &fetchAllShardKeyData($_shardKey, $_backend_connection_config);

    /**
     * Delete all Shard Key data
     *
     * @param array $_rows
     * @param string $_backend_connection_config
     * @return boolean
     */
    public function deleteAllShardKeyData(&$_rows, $_backend_connection_config);

    /**
     * Insert all Shard Key data
     *
     * @param array $_rows
     * @param string $_backend_connection_config
     * @return boolean
     */
    public function insertAllShardKeyData(&$_rows, $_backend_connection_config);

    /**
     * Compare Shard Key data
     *
     * @param array $_rowsOrigin
     * @param string $_shardKey
     * @param string $_backend_connection_config
     * @return boolean
     */
    public function compareAllShardKeyData(&$_rowsOrigin, $_shardKey, $_backend_connection_config);
}