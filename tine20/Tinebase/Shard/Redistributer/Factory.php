<?php

/**
 * backend factory class for Redistributer
 *
 * @package     Shard
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * backend factory class for Redistributer
 *
 * @package     Shard
 */
class Tinebase_Shard_Redistributer_Factory
{
    /**
     * factory function to return a Redistributer class
     *
     * @param string $_database
     * @param string | optional $_type
     * @return Tinebase_Shard_Redistributer_Interface
     */
    static public function factory($_database, $_type = null)
    {
        if (empty($_type)) {
            $db = Tinebase_Core::getDb($_database);
            $adapterName = get_class($db);

            // get last part of class name
            if (empty($adapterName) || strpos($adapterName, '_') === FALSE) {
                throw new Setup_Exception('Could not get DB adapter name.');
            }
            $adapterNameParts = explode('_',$adapterName);
            $type = array_pop($adapterNameParts);

            // special handling for Oracle
            $type = str_replace('Oci', Tinebase_Core::ORACLE, $type);

            $className = 'Tinebase_Shard_Redistributer_' . ucfirst($type);
        } else {
            $className = 'Tinebase_Shard_Redistributer_' . ucfirst($_type);
        }

        if (!class_exists($className)) {
            throw new InvalidArgumentException('Invalid database backend type defined.');
        }

        $instance = new $className($_database);

        return $instance;
    }
}
