<?php
/**
 * Flag backend
 *
 * @package     Shard
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright   Copyright (c); 2015 Metaways Infosystems GmbH (http://www.metaways.de);
 *
 */

/**
 * interface for Flag backend class
 *
 * @package     Shard
 */
interface Tinebase_Shard_Resharding_Flag_Interface
{
    /**
     * the singleton pattern
     *
     * @param array $_options
     * @param string $_database
     * @return Tinebase_Shard_Resharding_Flag_Filesystem
     */
    public static function getInstance(array $_options, $_database);

    /**
     * get shardkey that is in resharding process or 'disabled'
     *
     * @return string $_shardkey | string "disabled"
     */
    public function getResharding();

    /**
     * Set resharding shardkey or 'disabled'
     *
     * @param string $_shardKey | NULL
     */
    public function setResharding($_shardKey = NULL);
}