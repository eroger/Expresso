<?php
/**
 * @package     Tinebase
 * @subpackage  Config
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 */

/**
 * base for config classes
 *
 * @package     Tinebase
 * @subpackage  Config
 *
 * @todo support protected function interceptor for get property: _get<PropertyName>(&$data)
 * @todo support protected function interceptor for set property: _set<PropertyName>(&$data)
 * @todo update db to json encode all configs
 * @todo support array collections definitions
 */
abstract class Tinebase_Config_Abstract
{
    /**
     * object config type
     *
     * @var string
     */
    const TYPE_OBJECT = 'object';

    /**
     * integer config type
     *
     * @var string
     */
    const TYPE_INT = 'int';
    
    /**
     * boolean config type
     *
     * @var string
     */
    const TYPE_BOOL = 'bool';
    
    /**
     * string config type
     *
     * @var string
     */
    const TYPE_STRING = 'string';
    
    /**
     * float config type
     *
     * @var string
     */
    const TYPE_FLOAT = 'float';
    
    /**
     * dateTime config type
     *
     * @var string
     */
    const TYPE_DATETIME = 'dateTime';
    
    /**
     * keyFieldConfig config type
     *
     * @var string
     */
    const TYPE_KEYFIELD = 'keyFieldConfig';

    /**
     * config key for enabled features / feature switch
     *
     * @var string
     */
    const ENABLED_FEATURES = 'features';
    
    /**
     * application name this config belongs to
     *
     * @var string
     */
    protected $_appName;
    
    /**
     * config file data.
     *
     * @var array
     */
    protected static $_configFileData = array();
    protected static $_configFileDataDomain = array();
    protected static $_configFileDataGlobal = array();

    /**
     * config database backend
     *
     * @var Tinebase_Backend_Sql
     */
    protected static $_backend;

    /**
     * path and name of config file used by this class
     */
    protected static $_configFileName = 'config.inc.php';

    /**
     * domain of user
     *
     * @var string
     */
    protected static $_domain = '';

    /**
     * stores login name
     *
     * @var string
     */
    protected static $_loginName = null;

    /**
     * application config class cache (name => config record)
     *
     * @var array
     */
    protected $_cachedApplicationConfig = NULL;

    /**
     * store config data to prevent wrong storage in case of multidomain
     *
     * @var array
     */
    protected $_saveConfigCache = array();

    /**
     * Store values into class for avoiding multiple readings in a same request
     *
     * @var array
     */
    protected static $_classCache = array();

    /**
     * source of configuration storage
     *
     * @var boolean
     */
    protected static $_configOnlyIntoFilesystem = NULL;

    /**
     * server classes
     *
     * @var array
     */
    protected static $_serverPlugins = array();

    /**
     * global configuration keys
     *
     * @var array
     */
    protected static $_globalData = array(
        'bugreportUrl',
        'certificateLoginPath',
        'caching',
        'domaindata',
        'global',
        'logger',
        'helpdoc',
        'session',
        'setupuser',
        'theme',
        'sessionIpValidation'
    );

    /**
     * if domain data override global data
     *
     * @var boolean
    */
    protected static $_domainOverrideGlobal = TRUE;

    /**
     * change precedence of merge configuration
     *
     * @param boolean $enabled
     */
    public static function domainOverrideGlobal($enabled = FALSE)
    {
        self::$_domainOverrideGlobal = $enabled;
    }

    /**
     * Get list of server classes
     *
     * @return array
     */
    public static function getServerPlugins()
    {
        return static::$_serverPlugins;
    }

    /**
     *
     * @return boolean
     */
    public static function isConfigOnlyIntoFilesystem()
    {
        return static::$_configOnlyIntoFilesystem;
    }

    /**
     *
     * @param string $enabled
     */
    public static function setConfigOnlyIntoFilesystem($enabled = TRUE)
    {
        static::$_configOnlyIntoFilesystem = $enabled;
    }

    /**
     * get config object for application
     *
     * @param string $applicationName
     * @return Tinebase_Config_Abstract
     */
    public static function factory($applicationName)
    {
        if ($applicationName === 'Tinebase') {
            $config = Tinebase_Core::getConfig();
            // NOTE: this is a Zend_Config object in the Setup
            if ($config instanceof Tinebase_Config_Abstract) {
                return $config;
            }
        }
        
        $configClassName = $applicationName . '_Config';
        if (@class_exists($configClassName)) {
            $config = call_user_func(array($configClassName, 'getInstance'));
        } else {
            if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__
                . ' Application ' . $applicationName . ' has no config class.');
            $config = NULL;
        }
        
        return $config;
    }
    
    /**
     * gets user domain
     *
     * @param $fromSession boolean
     * @return string
     */
    public static function getDomain($fromSession = TRUE)
    {
        if (empty(static::$_domain)){
            //try to get user from registry (request)
            $user = Tinebase_Core::get(Tinebase_Core::USER);
            if ($user instanceof Tinebase_Model_FullUser){
                static::$_domain = isset($user->domain) ? $user->domain : NULL;
            }
            if (empty(static::$_domain) && $fromSession) {
                try {// try to get user from session
                    if (Tinebase_Session::sessionExists()){
                        $userSessionNamespace = Tinebase_User_Session::getSessionNamespace();
                        if (isset($userSessionNamespace->currentAccount) &&
                            $userSessionNamespace->currentAccount->domain) 
                        {
                            $domain = $userSessionNamespace->currentAccount->domain;
                        }
                    }
                } catch (Exception $e) {
                    Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ' Error while figuring the domain in session');
                    Tinebase_Core::getLogger()->trace(__METHOD__ . "::" . __LINE__ . ' Tracing domain session error ' . print_r($e->getTrace(), true));
                    static::$_domain = NULL;
                }
            }
        }
        return static::$_domain;
    }

    /**
     * @return boolean
     */
    public static function hasDomain()
    {
        $domain = self::getDomain();
        return !empty($domain);
    }

    /**
     * sets user domain
     * doesn't assumes default for ensuring that domain is passed
     *
     * @param string $domain
     * @param boolean $throwException (optional)
     */
    public static function setDomain($domain, $throwException = TRUE)
    {
        static::$_domain = strtolower($domain);
        $domainConfigFilePath = static::getDomainConfigFileNamePath(static::$_domain);
        if(Tinebase_Config_Manager::isMultidomain() && !file_exists($domainConfigFilePath) && $throwException) {
            static::$_domain = '';
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: $domainConfigFilePath not exists.");
            throw new Tinebase_Exception_InvalidArgument("$domain is not a valid domain.");
        }
    }

    /**
     * sets user domain from account login name
     * @param string $accountLoginName
     */
    public static function setDomainFromAccountLoginName($accountLoginName)
    {
        if(!Tinebase_Config_Manager::isMultidomain()) {
            return;
        }

        static::$_loginName = $accountLoginName;
        static::$_configFileData = null;
        $validator = new Zend_Validate_EmailAddress();
        if($validator->isValid($accountLoginName)) {
            $accountLoginData = explode('@', $accountLoginName);
            static::setDomain(array_pop($accountLoginData));
        } else {
            throw new Tinebase_Exception_InvalidDomainName("$accountLoginName is not a valid e-mail address.");
        }
    }

    /**
     * get path and name of config file
     * @return string
     */
    public static function getConfigFileName()
    {
        return static::$_configFileName;
    }

    /**
     * path and name of config file
     * @param string $configFileName
     */
    public static function setConfigFileName($configFileName)
    {
        static::$_configFileName = $configFileName;
    }

    /**
     * retrieve a value and return $default if there is no element set.
     *
     * @param  string $name
     * @param  mixed  $default
     * @return mixed
     */
    public function get($name, $default = NULL)
    {
        $value = Tinebase_Model_Config::NOTSET;

        if (isset(static::$_classCache[$name]) && array_key_exists($name, static::$_classCache))
        {
            return static::$_classCache[$name];
        }

        // NOTE: we check config file data here to prevent db lookup when db is not yet setup
        if ($configFile = $this->_getFromConfigFile($name)) {
            $value = $this->_rawToConfig($configFile, $name);
        }

        // don't read from database if reading is configured only for filesystem or database object is not created
        if ($value == Tinebase_Model_Config::NOTSET &&
                !static::$_configOnlyIntoFilesystem && !Tinebase_Config_Manager::isMultidomain() &&
                    Tinebase_Core::getDb() && $config = $this->_getFromDatabase($name)) {
            $decodedConfigData = json_decode($config->value, TRUE);
            // @todo JSON encode all config data via update script!
            $value = $this->_rawToConfig(($decodedConfigData || is_array($decodedConfigData)) ? $decodedConfigData : $config->value, $name);
        } else if ($value == Tinebase_Model_Config::NOTSET){
            $definition = static::getDefinition($name);
            if ($definition && $definition['type'] === static::TYPE_KEYFIELD) {
                $value = Tinebase_Config_KeyField::create($definition, array_key_exists('options', $definition) ? (array) $definition['options'] : array());
           }
        }

        if (!empty($value) && $value !== Tinebase_Model_Config::NOTSET) {
            static::$_classCache[$name] = $value;
            return $value;
        }

        // get default from definition if needed
        if ($default === NULL) {
            $definition = static::getDefinition($name);
            if ($definition && array_key_exists('default', $definition)) {
                $default = $definition['default'];
            }
        }

        return $default;
    }

    /**
     * store a config value
     *
     * @param  string   $_name      config name
     * @param  mixed    $_value     config value
     * @return void
     */
    public function set($_name, $_value)
    {
        if (Tinebase_Config_Manager::isMultidomain() || static::$_configOnlyIntoFilesystem || $this->_isFromConfigFile($_name)) {
            if (!empty($this->_appName) && $this->_appName !== 'Tinebase'){
                $this->_saveConfig($this->_appName, array($_name => json_encode($_value)));
            } else {
                $this->_saveConfig($_name, $_value);
            }
            $configClass = $_name . '_Config';
            if (@class_exists($configClass)) {
                $configClass::getInstance()->updateProperty($_value);
            }
        } else {
            $configRecord = new Tinebase_Model_Config(array(
                "application_id"    => Tinebase_Application::getInstance()->getApplicationByName($this->_appName)->getId(),
                "name"              => $_name,
                "value"             => json_encode($_value),
            ));

            $this->_saveModelConfig($configRecord);
        }
    }
    
    /**
     * delete a config from database
     *
     * @param  string   $_name
     * @return void
     */
    public function delete($_name)
    {
        if (Tinebase_Config_Manager::isMultidomain()) {
            $config = static::_getConfigFileData(TRUE);
            unset($config[$_name]);
            $this->_writeConfigToFile($config, FALSE);
        } else {
            $config = $this->_getFromDatabase($_name);
            if ($config) {
                $this->_getBackend()->delete($config->getId());
                $this->clearCache();
            }
        }
    }
    
    /**
     * delete all config for a application
     *
     * @param  string   $_applicationId
     * @return integer  number of deleted configs
     *
     * @todo remove param as this should be known?
     */
    public function deleteConfigByApplicationId($_applicationId)
    {
        $count = $this->_getBackend()->deleteByProperty($_applicationId, 'application_id');
        $this->clearCache();
        
        return $count;
    }
    
    /**
     * delete all config for a application
     *
     * @param  string   $_applicationName
     * @return integer  number of deleted configs
     *
     * @todo remove param as this should be known?
     */
    public function deleteConfigByApplicationName($_applicationName)
    {
        $this->delete($_applicationName);
    }

    /**
     * Magic function so that $obj->value will work.
     *
     * @param string $name
     * @return mixed
     */
    public function __get($_name)
    {
        return $this->get($_name);
    }
    
    /**
     * Magic function so that $obj->configName = configValue will work.
     *
     * @param  string   $_name      config name
     * @param  mixed    $_value     config value
     * @return void
     */
    public function __set($_name, $_value)
    {
        $this->set($_name, $_value);
    }
    
    /**
     * checks if a config name is set
     * isset means that the config key is present either in config file or in db
     *
     * @param  string $_name
     * @return bool
     */
    public function __isset($_name)
    {
        // NOTE: we can't test more precise here due to cacheing
        $value = $this->get($_name, Tinebase_Model_Config::NOTSET);
        
        return $value !== Tinebase_Model_Config::NOTSET;
    }
    
    /**
     * returns data from global and domain configuration file
     *
     * @param string $readFromFile
     * @return array
     */
    protected function _getConfigFileData($readFromFile = false) {
        $globalConfigData = array ();
        if ($readFromFile || ! static::$_configFileData) {
            $globalConfigData = $this->_readGlobalConfigFile();
        }

        $domainConfigData = array ();
        if (Tinebase_Config_Manager::isMultidomain() && static::hasDomain()) {
            $domainConfigData = $this->_readDomainConfigFile(static::getDomain());
        }

        if (! empty ( $domainConfigData ) || ! empty ( $globalConfigData )) {
            if (self::$_domainOverrideGlobal){
                $configData = array_merge ( $globalConfigData, $domainConfigData );
            } else {
                $configData = array_merge ( $domainConfigData, $globalConfigData );
            }
            static::$_configFileData = $configData;
        }

        return static::$_configFileData;
    }
    
    /**
     * write config to a file
     *
     * @param array $_data
     * @param boolean $_merge
     * @return Zend_Config
     */
    protected function _writeConfigToFile($_data, $_merge)
    {
        $domain = static::getDomain();
        $domainData = $this->getData($_data, TRUE);
        $globalData = $this->getData($_data);
        // merge config data and active config
        if ($_merge) {
            $formerData = static::_readGlobalConfigFile(TRUE);
            $config = new Zend_Config($formerData, true);
            $config->merge(new Zend_Config($globalData));

            $domainFormerData = static::_readDomainConfigFile($domain, TRUE);
            $domainConfig = new Zend_Config($domainFormerData, true);
            $domainConfig->merge(new Zend_Config($domainData));
        } else {
            $config = new Zend_Config($globalData);
            $domainConfig = new Zend_Config($domainData);
        }

        try {
            // write to file
            $fileNamePath = $this->getGlobalConfigFileNamePath();
            Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' Updating ' . $fileNamePath);
            $writer = new Zend_Config_Writer_Array(array(
                'config'   => $config,
                'filename' => $fileNamePath,
            ));
            $writer->write();
            if (!empty($domain)) {
                $domainFileNamePath = $this->getDomainConfigFileNamePath($domain);
                Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' Updating ' . $domainFileNamePath);
                $writer = new Zend_Config_Writer_Array(array(
                    'config'   => $domainConfig,
                    'filename' => $domainFileNamePath,
                ));
                $writer->write();
            }
        } catch (Tinebase_Exception_SystemGeneric $tes) {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . $tes->getMessage());
        }
    }

    /**
     * returns data from domain config.inc.php file
     *
     * @param $refresh boolean (optional)
     * @return array
     */
    protected function _readGlobalConfigFile($refresh = FALSE)
    {
        if (! static::$_configFileDataGlobal || $refresh) {
            // @todo check path // security
            $configData = include(static::getGlobalConfigFileNamePath());

            if ($configData === false) {
                throw new Tinebase_Exception_NotFound('central configuration file config.inc.php not found in includepath: ' . get_include_path());
            }

            static::$_configFileDataGlobal = $configData;
        }

        return static::$_configFileDataGlobal;
    }

    /**
     * returns data from domain config.inc.php file
     *
     * @param $domain string
     * @param $refresh boolean (optional)
     * @return array
     */
    protected function _readDomainConfigFile($domain, $refresh = FALSE)
    {
        if ((! static::$_configFileDataDomain || $refresh) && $domain) {
            // @todo check path // security
            $domainConfigData = include(static::getDomainConfigFileNamePath($domain));

            if ($domainConfigData === false) {
                Tinebase_Core::getLogger ()->debug ( __METHOD__ . '::' . __LINE__ . ' There is no config file for domain ' . $domain);
                    $domainConfigData = array (); // there is no domain config
                                                 // data before to login, then
                                                 // it must be possible to have
                                                 // only global config
                $domainConfigData = array();
            }

            static::$_configFileDataDomain = $domainConfigData;
        }

        return static::$_configFileDataDomain;
    }

    /**
     * get config file section where config identified by name is in
     *
     * @param  string $_name
     * @return array
     */
    protected function _getFromConfigFile($_name)
    {
        $configFileData = $this->_getConfigFileData(TRUE);

        // appName section overwrites global section in config file
        if (isset($configFileData[$this->_appName]) && (isset($configFileData[$this->_appName][$_name]) || array_key_exists($_name, $configFileData[$this->_appName]))) {
            return $configFileData[$this->_appName][$_name];
        }

        if (isset($configFileData[$_name]) || array_key_exists($_name, $configFileData)) {
            return $configFileData[$_name];
        }

        return null;
    }

    /**
     * returns complete path to domain config file
     *
     * @param string | optional $domain
     * @return string
     */
    public static function getDomainConfigFileNamePath($domain = NULL)
    {
        $path = realpath(__DIR__ . '/../../');
        $fileNamePath = $path . DIRECTORY_SEPARATOR .
        'domains' . DIRECTORY_SEPARATOR .
        (empty($domain) ? static::getDomain() : $domain) . DIRECTORY_SEPARATOR .
        static::$_configFileName;

        return $fileNamePath;
    }

    /**
     * returns complete path to global config file
     *
     * @return string
     */
    public static function getGlobalConfigFileNamePath()
    {
        $path = realpath(__DIR__ . '/../../');
        $fileNamePath = $path . DIRECTORY_SEPARATOR .
        static::$_configFileName;

        if (!file_exists($fileNamePath)) {
            $includePaths = explode(PATH_SEPARATOR, get_include_path());
            foreach ($includePaths as $includePath) {
                $fileNamePath = $includePath . DIRECTORY_SEPARATOR . static::$_configFileName;
                if (file_exists($fileNamePath)) {
                    return $fileNamePath;
                }
            }
        }

        return $fileNamePath;
    }

    /**
     * load a config record from database
     *
     * @param  string                   $_name
     * @return Tinebase_Model_Config|NULL
     */
    protected function _getFromDatabase($name)
    {
        Tinebase_Core::setupCache();
        if (Tinebase_Core::get(Tinebase_Core::CUSTOMEXPIRABLECACHE)
                && (Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) == 'ActiveSync_Server_Http'))
        {
            $params = array($name);
            $cacheId = Tinebase_Helper::arrayHash($params);
            $cache = Tinebase_Core::getCache();
            $conf = $cache->load($cacheId);
            if ($conf === FALSE){
               $filter = new Tinebase_Model_ConfigFilter(array(
                          array('field' => 'name', 'operator' => 'equals', 'value' => $name),
               ));

               $config = $this->_getBackend()->search($filter);
               if($config->count() == 0){
                   $conf = NULL;
               }else{
                   $conf = $config[0];
               }
               $cache->save($conf, $cacheId, array('ASloadConfig', $name));
            }
        } else {
            if ($this->_cachedApplicationConfig === NULL) {
                $this->_loadAllAppConfigsInCache();
            }
            $conf = (isset($this->_cachedApplicationConfig[$name])) ? $this->_cachedApplicationConfig[$name] :  NULL;
        }
        return $conf;
    }

    /**
    * fill class cache with all config records for this app
     */
    protected function _loadAllAppConfigsInCache()
    {
        if (empty($this->_appName)) {
            if (Tinebase_Core::isLogLevel(Zend_Log::WARN)) Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' appName not set');
            $this->_cachedApplicationConfig = array();
        }

        $cache = Tinebase_Core::getCache();
        if (!$cache instanceof Zend_Cache_Core) {
            Tinebase_Core::setupCache();
            $cache = Tinebase_Core::getCache();
        }

        if ($cachedApplicationConfig = $cache->load('cachedAppConfig_' . $this->_appName)) {
            $this->_cachedApplicationConfig = $cachedApplicationConfig;
            return;
        }

        try {
            $applicationId = Tinebase_Model_Application::convertApplicationIdToInt($this->_appName);
        } catch (Zend_Db_Exception $zdae) {
            // DB might not exist or tables are not created, yet
            Tinebase_Exception::log($zdae);
            $this->_cachedApplicationConfig = array();
            return;
        }

        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' Loading all configs for app ' . $this->_appName);

        $filter = new Tinebase_Model_ConfigFilter(array(
            array('field' => 'application_id', 'operator' => 'equals', 'value' => $applicationId),
        ));
        $allConfigs = $this->_getBackend()->search($filter);

        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' Found ' . count($allConfigs) . ' configs.');
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . print_r($allConfigs->toArray(), TRUE));

        foreach ($allConfigs as $config) {
            $this->_cachedApplicationConfig[$config->name] = $config;
        }

        $cache->save($this->_cachedApplicationConfig, 'cachedAppConfig_' . $this->_appName);
    }
    
    /**
     * store a value into central config file
     * @param string $name
     * @param mixed $value
     */
    protected function _saveConfig($name, $value)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' Setting config ' . $name);

        $data = array($name => $value);

        $this->_saveConfigCache = array_replace_recursive($this->_saveConfigCache, $data);

        $config = $this->_writeConfigToFile($this->_saveConfigCache, TRUE);
    }

    /**
     * store a config record in database
     *
     * @param   Tinebase_Model_Config $_config record to save
     * @return  Tinebase_Model_Config
     *
     * @todo only allow to save records for this app ($this->_appName)
     */
    protected function _saveModelConfig(Tinebase_Model_Config $_config)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
            . ' Setting config ' . $_config->name);
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__
            . ' ' . print_r($_config->value, true));

        $config = $this->_getFromDatabase($_config->name);
        
        if ($config) {
            $config->value = $_config->value;
            try {
                $result = $this->_getBackend()->update($config);
            } catch (Tinebase_Exception_NotFound $tenf) {
                // config might be deleted but cache has not been cleaned
                $result = $this->_getBackend()->create($_config);
            }
        } else {
            $result = $this->_getBackend()->create($_config);
        }
        
        $this->clearCache();
        
        return $result;
    }
    
    /**
     * clear the cache
     */
    public function clearCache($appFilter = null)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' Clearing config cache');
        $this->_cachedApplicationConfig = NULL;

        if (isset($appFilter)) {
            list($key, $value) = each($appFilter);
            $appName = $key === 'name' ? $value : Tinebase_Application::getInstance()->getApplicationById($value)->name;
        } else {
            $appName = $this->_appName;
        }
        Tinebase_Core::getCache()->remove('cachedAppConfig_' . $appName);
    }
    
    /**
     * returns config database backend
     *
     * @return Tinebase_Backend_Sql
     */
    protected function _getBackend()
    {
        if (! static::$_backend) {
            static::$_backend = new Tinebase_Backend_Sql(array(
                'modelName' => 'Tinebase_Model_Config',
                'tableName' => 'config',
            ));
        }
        
        return static::$_backend;
    }
    
    /**
     * converts raw data to config values of defined type
     *
     * @TODO support array contents conversion
     * @TODO support interceptors
     *
     * @param   mixed     $_rawData
     * @param   string    $_name
     * @return  mixed
     */
    protected function _rawToConfig($_rawData, $_name)
    {
        $_rawData = !is_array($_rawData) && $this->isJson($_rawData) ? json_decode($_rawData, TRUE) : $_rawData;

        $definition = static::getDefinition($_name);
        
        if (! $definition) {
            return is_array($_rawData) ? new Tinebase_Config_Struct($_rawData) : $_rawData;
        }
        
        if ($definition['type'] === static::TYPE_OBJECT && isset($definition['class']) && @class_exists($definition['class'])) {
            return new $definition['class']($_rawData);
        }
        
        switch ($definition['type']) {
            case static::TYPE_INT:        return (int) $_rawData;
            case static::TYPE_BOOL:       return (bool) (int) $_rawData;
            case static::TYPE_STRING:     return (string) $_rawData;
            case static::TYPE_FLOAT:      return (float) $_rawData;
            case static::TYPE_DATETIME:   return new DateTime($_rawData);
            case static::TYPE_KEYFIELD:   static::isJson($_rawData) ? json_decode($_rawData) : $_rawData; return Tinebase_Config_KeyField::create($_rawData, array_key_exists('options', $definition) ? (array) $definition['options'] : array());
            default:                    return is_array($_rawData) ? new Tinebase_Config_Struct($_rawData) : $_rawData;
        }
    }
    
    /**
     * get definition of given property
     *
     * @param   string  $_name
     * @return  array
     */
    public function getDefinition($_name)
    {
        // NOTE we can't call statecally here (static late binding again)
        $properties = $this->getProperties();
        
        return array_key_exists($_name, $properties) ? $properties[$_name] : NULL;
    }
    
    /**
     * check if config system is ready
     *
     * @todo check db setup
     * @return bool
     */
    public static function isReady()
    {
        return !empty(static::$_configFileData);
    }

    /**
     * Checks if a string is JSON
     * @param string $text
     * @return boolean
     */
    public static function isJson($text)
    {
        if(!is_string($text)) {
            return false;
        }
        
        json_decode($text);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * Merge propeties array with new data
     * @param array $param
     */
    public function updateProperty($param)
    {
        if (is_array($param)) {
            foreach ($param as $key => $value) {
                $param[$key] = json_decode($value, TRUE);
            }
            $properties = static::$_properties;
            $properties = array_replace_recursive($properties, $param);
            static::$_properties = $properties;
        }
    }

    /**
     * Say if a config key is from domain scope
     *
     * @param string $key
     * @return boolean
     */
    public function isDomainData($key)
    {
        return !in_array($key, self::$_globalData);
    }

    /**
     * Content of config file
     *
     * @return array
     */
    public function toArray()
    {
        return self::_getConfigFileData(TRUE);
    }

    /**
     * Return an array with config values according to filter defined by $onlyDomain
     * No multidomain, return all what receive
     * Multidomain, return only global or only domain data, never both of them
     *
     * @param array $_data
     * @return array
     */
    public function getData(array $_data, $onlyDomain = FALSE)
    {
        $data = array();
        if ($this->hasDomain()) { // filter data
            foreach($_data as $key => $value){
                $isFromDomain = $this->isDomainData($key);
                // if both are TRUE, data are DOMAIN, if both are FALSE, data are GLOBAL
                if (($onlyDomain && $isFromDomain) || (!$onlyDomain && !$isFromDomain)){
                    $data[$key] = $value;
                }
            }
        } else { // returns nothing (domain) or all (global)
            $data = ($onlyDomain ? $data : $_data);
        }
        return $data;
    }

    /**
     * Say if config key is from configuration file
     *
     * @param string $name
     * @return boolean
     */
    protected function _isFromConfigFile($name)
    {
        $value = Tinebase_Model_Config::NOTSET;
        if ($configFile = $this->_getFromConfigFile($name)) {
            $value = $this->_rawToConfig($configFile, $name);
        }
        return $value !== Tinebase_Model_Config::NOTSET;
    }

    /**
     * returns true if a certain feature is enabled
     *
     * @param string $featureName
     * @return boolean
     */
    public function featureEnabled($featureName)
    {
        $features = $this->get(self::ENABLED_FEATURES);
        if (isset($features->{$featureName})) {
            return $features->{$featureName};
        }

        return false;
    }
}
