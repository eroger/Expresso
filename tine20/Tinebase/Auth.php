<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Auth
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2010 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Lars Kneschke <l.kneschke@metaways.de>
 */

/**
 * main authentication class
 *
 * @todo 2010-05-20 cweiss: the default option handling looks like a big mess -> someone needs to tidy up here!
 *
 * @package     Tinebase
 * @subpackage  Auth
 */

class Tinebase_Auth
{
    /**
     * constant for Sql auth
     *
     */
    const SQL = 'Sql';
    
    /**
     * constant for LDAP auth
     *
     */
    const LDAP = 'Ldap';

    /**
     * constant for IMAP auth
     *
     */
    const IMAP = 'IMAP';

    /**
     * constant for DigitalCertificate auth / SSL
     *
     */
    const MODSSL = 'ModSsl';

    /**
     * General Failure
     */
    const FAILURE                       =  Zend_Auth_Result::FAILURE;

    /**
     * Failure due to identity not being found.
     */
    const FAILURE_IDENTITY_NOT_FOUND    = Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;

    /**
     * Failure due to user not synced in backend
     */
    const FAILURE_USER_NOT_SYNCED       = -99;

    /**
     * Failure due to identity being ambiguous.
     */
    const FAILURE_IDENTITY_AMBIGUOUS    = Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS;

    /**
     * Failure due to invalid credential being supplied.
     */
    const FAILURE_CREDENTIAL_INVALID    = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;

    /**
     * Failure due to uncategorized reasons.
     */
    const FAILURE_UNCATEGORIZED         = Zend_Auth_Result::FAILURE_UNCATEGORIZED;
    
    /**
     * Failure due the account is disabled
     */
    const FAILURE_DISABLED              = -100;

    /**
     * Failure due the account is expired
     */
    const FAILURE_PASSWORD_EXPIRED      = -101;
    
    /**
     * Failure due the account is temporarly blocked
     */
    const FAILURE_BLOCKED               = -102;
        
    /**
     * database connection failure
     */
    const FAILURE_DATABASE_CONNECTION   = -103;
        
    /**
     * Authentication success.
     */
    const SUCCESS                        =  Zend_Auth_Result::SUCCESS;

    /**
     * Store all backends classes names
     */
    private static $_availableBackends = array(
        self::SQL    => 'Tinebase_Auth_Sql',
        self::LDAP   => 'Tinebase_Auth_Ldap',
        self::IMAP   => 'Tinebase_Auth_Imap',
        self::MODSSL => 'Tinebase_Auth_ModSsl',
    );
    
    /**
     * the name of the authenticationbackend
     *
     * @var string
     */
    protected static $_backendType;
    
    /**
     * Holds the backend configuration options.
     * Property is lazy loaded from {@see Tinebase_Config} on first access via
     * getter {@see getBackendConfiguration()}
     *
     * @var array | optional
     */
    private static $_backendConfiguration;
    
    /**
     * Holds the backend configuration options.
     * Property is lazy loaded from {@see Tinebase_Config} on first access via
     * getter {@see getBackendConfiguration()}
     *
     * @var array | optional
     */
    private static $_backendConfigurationDefaults = array();
    
    /**
     * the instance of the authenticationbackend
     *
     * @var Tinebase_Auth_Interface
     */
    protected $_backend;
    
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton
     */
    private function __construct() {
        $this->setBackend();
    }
    
    /**
     * don't clone. Use the singleton.
     *
     */
    private function __clone() {}

    /**
     * holds the instance of the singleton
     *
     * @var Tinebase_Auth
     */
    private static $_instance = NULL;
    
    /**
     * the singleton pattern
     *
     * @return Tinebase_Auth
     */
    public static function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Tinebase_Auth;
        }
        
        return self::$_instance;
    }
    
    /**
     * authenticate user
     *
     * @param string $_username
     * @param string $_password
     * @param boolean $_expiredPasswordCheck
     * @return Zend_Auth_Result
     */
    public function authenticate($_username, $_password, $_expiredPasswordCheck)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
            . ' Trying to authenticate '. $_username);
        
        $this->_backend->setIdentity($_username);
        $this->_backend->setCredential($_password);
        
        if (Tinebase_Session::isStarted()) {
            Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session());
        } else {
            Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_NonPersistent());
        }
        $result = Zend_Auth::getInstance()->authenticate($this->_backend, $_expiredPasswordCheck);
        
        $username = empty($_username) ? $result->getIdentity() : $_username;
        if ($result->isValid()) {
            if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__
                . ' Authentication of '. $username . ' succeeded');
        } else {
            if (Tinebase_Core::isLogLevel(Zend_Log::WARN)) Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__
                . ' Authentication of '. $username . ' failed');
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                . ' Failure messages: ' . print_r($result->getMessages(), TRUE));
        }
        
        return $result;
    }
    
    /**
     * check if password is valid
     *
     * @param string $_username
     * @param string $_password
     * @return boolean
     */
    public function isValidPassword($_username, $_password)
    {
        $this->_backend->setIdentity($_username);
        $this->_backend->setCredential($_password);
        
        $result = $this->_backend->authenticate();

        if ($result->isValid()) {
            return true;
        }
        
        return false;
    }
    
    /**
     * returns the configured rs backend
     *
     * @return string
     */
    public static function getConfiguredBackend()
    {
        if (!isset(self::$_backendType)) {
            self::setBackendType(Tinebase_Config::getInstance()->get(Tinebase_Config::AUTHENTICATIONBACKENDTYPE, self::SQL));
        }
        
        return self::$_backendType;
    }
    
    /**
     * set the auth backend
     */
    public function setBackend()
    {
        // test mod_ssl server variables
        if (Zend_Auth_Adapter_ModSsl::hasModSsl()) {
            self::setBackendType(self::MODSSL);
            $backendType =  self::MODSSL;
        }
        else {
            $backendType = self::getConfiguredBackend();
        }
        
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ .' authentication backend: ' . $backendType);
        $this->_backend = self::factory($backendType);
    }
    
    /**
     * factory function to return a selected authentication backend class
     *
     * @param   string $_backendType
     * @return  Tinebase_Auth_Interface
     * @throws  Tinebase_Exception_InvalidArgument
     */
    public static function factory($_backendType)
    {
        $options = self::getBackendConfiguration();
        
        if(!array_key_exists($_backendType, self::$_availableBackends)) {
            throw new Tinebase_Exception_NotFound("Backend type $_backendType not found among available backends");
        }
        
        $backendClass = self::$_availableBackends[$_backendType];
        if(!class_exists($backendClass)){
            throw new Tinebase_Exception_NotImplemented("Auth backend type $_backendType not implemented.");
        }
        
        $result = new $backendClass($options);
        
        if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__
            . ' Created auth backend of type ' . $_backendType);
        
        return $result;
    }
    
    /**
     * setter for {@see $_backendType}
     *
     * @todo persist in db
     *
     * @param string $_backendType
     * @return void
     */
    public static function setBackendType($_backendType)
    {
        self::$_backendType = ucfirst($_backendType);
    }
    
    /**
     * Setter for {@see $_backendConfiguration}
     *
     * NOTE:
     * Setting will not be written to Database or Filesystem.
     * To persist the change call {@see saveBackendConfiguration()}
     *
     * @param mixed $_value
     * @param string  optional $_key
     * @return void
     */
    public static function setBackendConfiguration($_value, $_key = null)
    {
        $defaultValues = self::getBackendConfigurationDefaults(self::getConfiguredBackend());

        if (is_null($_key) && !is_array($_value)) {
            throw new Tinebase_Exception_InvalidArgument('To set backend configuration either a key and value parameter are required or the value parameter should be a hash');
        } elseif (is_null($_key) && is_array($_value)) {
            foreach ($_value as $key=> $value) {
                self::setBackendConfiguration($value, $key);
            }
        } else {
            if ( ! array_key_exists($_key, $defaultValues)) {
                if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ .
                    "Cannot set backend configuration option '$_key' for accounts storage " . self::getConfiguredBackend());
                return;
            }
            self::$_backendConfiguration[$_key] = $_value;
        }
    }
    
    /**
     * Creates a connection with auth backend and returns it
     * @return Tinebase_User_Interface
     */
    public static function getAuthBackendConnection()
    {
        $backendClass = self::$_availableBackends[self::getConfiguredBackend()];
        return $backendClass::getBackendConnection(self::getBackendConfiguration());
    }

    /**
     * Delete the given config setting or all config settings if {@param $_key} is not specified
     *
     * @param string | optional $_key
     * @return void
     */
    public static function deleteBackendConfiguration($_key = null)
    {
        if (is_null($_key)) {
            self::$_backendConfiguration = array();
        } elseif (array_key_exists($_key, self::$_backendConfiguration)) {
            unset(self::$_backendConfiguration[$_key]);
        } else {
            Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' configuration option does not exist: ' . $_key);
        }
    }
    
    /**
     * Write backend configuration setting {@see $_backendConfigurationSettings} and {@see $_backendType} to
     * db config table.
     *
     * @return void
     */
    public static function saveBackendConfiguration()
    {
        Tinebase_Config::getInstance()->set(Tinebase_Config::AUTHENTICATIONBACKEND, self::getBackendConfiguration());
        Tinebase_Config::getInstance()->set(Tinebase_Config::AUTHENTICATIONBACKENDTYPE, self::getConfiguredBackend());
    }
    
    /**
     * Getter for {@see $_backendConfiguration}
     *
     * @param boolean $_getConfiguredBackend
     * @return mixed [If {@param $_key} is set then only the specified option is returned, otherwise the whole options hash]
     */
    public static function getBackendConfiguration($_key = null, $_default = null)
    {
        //lazy loading for $_backendConfiguration
        if (!isset(self::$_backendConfiguration)) {
            $rawBackendConfiguration = Tinebase_Config::getInstance()->get(Tinebase_Config::AUTHENTICATIONBACKEND, new Tinebase_Config_Struct())->toArray();
            self::$_backendConfiguration = is_array($rawBackendConfiguration) ? $rawBackendConfiguration : Zend_Json::decode($rawBackendConfiguration);
        }

        if (isset($_key)) {
            return array_key_exists($_key, self::$_backendConfiguration) ? self::$_backendConfiguration[$_key] : $_default;
        } else {
            return self::$_backendConfiguration;
        }
    }
    
    /**
     * Returns default configuration for all supported backends
     * and overrides the defaults with concrete values stored in this configuration
     *
     * @param String | optional $_key
     * @return mixed [If {@param $_key} is set then only the specified option is returned, otherwise the whole options hash]
     */
    public static function getBackendConfigurationWithDefaults($_getConfiguredBackend = TRUE)
    {
        $configs = array();
        $defaultConfig = self::getBackendConfigurationDefaults();
        foreach ($defaultConfig as $backendName => $backendConfig) {
            $configs[$backendName] = ($_getConfiguredBackend && $backendName == self::getConfiguredBackend() ? self::getBackendConfiguration() : array());
            if (is_array($configs[$backendName])) {
                foreach ($backendConfig as $key => $value) {
                    // 2010-05-20 cweiss Zend_Ldap changed and does not longer throw exceptions
                    // on unsupported values, we might skip this cleanup here.
                    if (! array_key_exists($key, $configs[$backendName])) {
                        $configs[$backendName][$key] = $value;
                    }
                }
            } else {
                $configs[$backendName] = $backendConfig;
            }
        }
        return $configs;
    }
    
    /**
     * Getter for {@see $_backendConfigurationDefaults}
     * @param String | optional $_backendName
     * @return array
     */
    public static function getBackendConfigurationDefaults($_backendName = null)
    {
        if(empty(self::$_backendConfigurationDefaults)) {
            foreach(self::$_availableBackends as $name => $backendClass) {
                if(!class_exists($backendClass) || !method_exists($backendClass, 'getBackendConfigurationDefaults')) {
                    throw new Tinebase_Exception_NotImplemented("Backend name $name with class $backendClass not implemented properly");
                }

                self::$_backendConfigurationDefaults[$name] = $backendClass::getBackendConfigurationDefaults();
            }
        }
        
        if ($_backendName) {
            if (!array_key_exists($_backendName, self::$_backendConfigurationDefaults)) {
                throw new Tinebase_Exception_InvalidArgument("Unknown backend name '$_backendName'");
            }
            return self::$_backendConfigurationDefaults[$_backendName];
        } else {
            return self::$_backendConfigurationDefaults;
        }
    }
    
    /**
     * Add a custom backend to stack
     * @param type $_name
     * @param type $_className
     * @throws Tinebase_Exception_InvalidArgument
     */
    public static function addCustomBackend($_name, $_className)
    {
        if(array_key_exists($_name, self::$_availableBackends)) {
            throw new Tinebase_Exception_InvalidArgument("Backend type $_name already exists");
        }
        self::$_availableBackends[$_name] = $_className;
    }

    /**
     * Check if $_authBackend is a valid auth backend connection
     * @param  $authBackend
     * @return boolean
     */
    public static function isBackendValid($authBackend)
    {
        $backendClass = self::$_availableBackends[self::getConfiguredBackend()];
        return (bool) $backendClass::isValid($authBackend);
    }

    /**
     * Force close connection with the backend
     */
    public function closeBackendConnection()
    {
        $this->_backend->closeConnection();
    }

    /**
     * Verify if Zend_Auth storage was created and user object is in session
     *
     * @return bool
     */
    public static function hasIdentity()
    {
       return (Zend_Auth::getInstance()->hasIdentity()
               && isset(Tinebase_User_Session::getSessionNamespace()->currentAccount)
               && Tinebase_User_Session::getSessionNamespace()->currentAccount != null);
    }
    
    /**
     * Add an alternative auth backend
     *
     * @param string $alias
     * @param string $name
     */
    public static function addBackendPlugin($alias, $name)
    {
        self::$_availableBackends[$alias] = $name;
    }
    
    /**
     * Provide authentication backends alias
     * 
     * @return array
     */
    public static function getAuthenticationBackends()
    {
        $authenticationBackends = array();
        foreach(self::$_availableBackends as $backend => $class){
            if ($backend !== self::MODSSL){
                $authenticationBackends[] = array($backend,$backend);
            }
        }
        return $authenticationBackends;
    }
}
