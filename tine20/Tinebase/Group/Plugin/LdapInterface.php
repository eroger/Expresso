<?php
/**
 * Tine 2.0
 * 
 * @package     Tinebase
 * @subpackage  User
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2010 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 */

/**
 * interface for user ldap plugins
 * 
 * @package Tinebase
 * @subpackage Group
 */
interface Tinebase_Group_Plugin_LdapInterface extends Tinebase_Group_Plugin_Interface
{
    /**
     * inspect data used to create user
     * 
     * @param Tinebase_Model_FullUser  $_user
     * @param array                    $_ldapData  the data to be written to ldap
     */
    public function inspectAddGroup(Tinebase_Model_Group $_group, array &$_ldapData);
    
    /**
     * inspect data used to update user
     * 
     * @param Tinebase_Model_FullUser  $_user
     * @param array                    $_ldapData  the data to be written to ldap
     */
    public function inspectUpdateGroup(Tinebase_Model_Group $_group, array &$_ldapData);
      

    public function setLdap(Tinebase_Ldap $_ldap);
}  
