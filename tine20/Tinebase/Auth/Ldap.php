<?php
/**
 * Tine 2.0
 * 
 * @package     Tinebase
 * @subpackage  Auth
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Lars Kneschke <l.kneschke@metaways.de>
 */

/**
 * LDAP authentication backend
 * 
 * @package     Tinebase
 * @subpackage  Auth
 */
class Tinebase_Auth_Ldap extends Zend_Auth_Adapter_Ldap implements Tinebase_Auth_Interface
{
    /**
     * Name of backend
     */
    const NAME = 'Ldap';
    
    /**
     * Type of backend
     */
    const TYPE = 'Ldap';
    
    /**
     * Constructor
     *
     * @param  array  $options  An array of arrays of Zend_Ldap options
     * @return void
     */
    public function __construct(array $options = array(),  $username = null, $password = null)
    {
        $this->setOptions(array('ldap' => $options));
        if ($username !== null) {
            $this->setIdentity($username);
        }
        if ($password !== null) {
            $this->setCredential($password);
        }

        try {
            $this->_ldap = Tinebase_Core::getAuthBackend();
        } catch (Exception $ex) {
            throw new Tinebase_Exception_Backend_Ldap('Could not bind to LDAP: ' . $zle->getMessage());
        }
    }

    /**
     * set loginname
     *
     * @param string $_identity
     * @return Tinebase_Auth_Ldap
     */
    public function setIdentity($_identity)
    {
        parent::setUsername($_identity);
        return $this;
    }
    
    /**
     * set password
     *
     * @param string $_credential
     * @return Tinebase_Auth_Ldap
     */
    public function setCredential($_credential)
    {
        parent::setPassword($_credential);
        return $this;
    }
    
    /**
     * Returns the type of backend
     * @return string
     */
    public static function getType()
    {
        return self::TYPE;
    }
        
    /**
     * Returns default configurations of the backend
     * @return array -> must be like this because of Zend_Auth_Adapter_Ldap 
     * that iterates over several ldaps until be succeded in auth process
     */
    public static function getBackendConfigurationDefaults()
    {
        return array(
            'host' => '',
            'username' => '',
            'password' => '',
            'bindRequiresDn' => true,
            'baseDn' => '',
            'accountFilterFormat' => NULL,
            'accountCanonicalForm' => '2',
            'accountDomainName' => '',
            'accountDomainNameShort' => '',
            'tryUsernameSplit' => '1'
         );
    }

    /**
     * Authenticate the user
     *
     * @param  boolean $_expired_check
     * @throws Zend_Auth_Adapter_Exception
     * @return Zend_Auth_Result
     */
    public function authenticate($_expired_check = NULL)
    {
        /**
         * @see Zend_Ldap_Exception
         */
        require_once 'Zend/Ldap/Exception.php';

        $messages = array();
        $messages[0] = ''; // reserved
        $messages[1] = ''; // reserved

        $username = $this->_username;
        $password = $this->_password;

        if (!$username) {
            $code = Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
            $messages[0] = 'A username is required';
            return new Zend_Auth_Result($code, '', $messages);
        }
        if (!$password) {
            /* A password is required because some servers will
             * treat an empty password as an anonymous bind.
             */
            $code = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
            $messages[0] = 'A password is required';
            return new Zend_Auth_Result($code, '', $messages);
        }

        $code = Zend_Auth_Result::FAILURE;
        $messages[0] = "Authority not found: $username";
        $failedAuthorities = array();

        /* Iterate through each server and try to authenticate the supplied
         * credentials against it.
         */
        foreach ($this->_options as $name => $options) {

            if (!is_array($options)) {
                /**
                 * @see Zend_Auth_Adapter_Exception
                 */
                require_once 'Zend/Auth/Adapter/Exception.php';
                throw new Zend_Auth_Adapter_Exception('Adapter options array not in array');
            }
            $this->_ldap->setOptions($options);
            $dname = '';

            try {
                if ($messages[1])
                    $messages[] = $messages[1];
                $messages[1] = '';
                $messages[] = $this->_optionsToString($options);

                $dname = $this->_getAuthorityName();
                if (isset($failedAuthorities[$dname])) {
                    /* If multiple sets of server options for the same domain
                     * are supplied, we want to skip redundant authentications
                     * where the identity or credentials where found to be
                     * invalid with another server for the same domain. The
                     * $failedAuthorities array tracks this condition (and also
                     * serves to supply the original error message).
                     * This fixes issue ZF-4093.
                     */
                    $messages[1] = $failedAuthorities[$dname];
                    $messages[] = "Skipping previously failed authority: $dname";
                    continue;
                }

                $canonicalName = $this->_ldap->getCanonicalAccountName($username);

                $this->_ldap->bind($canonicalName, $password);

                if($_expired_check)
                {
                    Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . " LDAP backend is setted up to check password expiration");
                    $user_pass_expired = $this->_ldap->passwordExpired($username);

                    if(is_array($user_pass_expired))
                    {
                        throw new Zend_Ldap_Exception($$this->_ldap, $user_pass_expired[1], Zend_Ldap_Exception::LDAP_NO_SUCH_ATTRIBUTE);
                    }

                    if($user_pass_expired)
                    {
                        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " LDAP password for user '$username' is expired");
                        $options = $this->getLdap()->getOptions();
                        $options['passwordExpired'] = true;
                        throw new Tinebase_Exception_PasswordExpired("$canonicalName authentication fails: user password has expired");
                    }
                    else
                    {
                        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " LDAP password for user '$username' is valid, and does not prescribed date");
                        $messages[0] = '';
                        $messages[1] = '';
                        $messages[] = "$canonicalName authentication successful";
                        return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $canonicalName, $messages);
                    }
                }
                else{
                        Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . " LDAP backend is not configured to check password expiration");
                        $messages[0] = '';
                        $messages[1] = '';
                        $messages[] = "$canonicalName authentication successful";
                        return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $canonicalName, $messages);
                }

            } catch (Zend_Ldap_Exception $zle) {

                /* LDAP based authentication is notoriously difficult to diagnose. Therefore
                 * we bend over backwards to capture and record every possible bit of
                 * information when something goes wrong.
                 */

                $err = $zle->getCode();

                if ($err == Zend_Ldap_Exception::LDAP_X_DOMAIN_MISMATCH) {
                    /* This error indicates that the domain supplied in the
                     * username did not match the domains in the server options
                     * and therefore we should just skip to the next set of
                     * server options.
                     */
                    continue;
                } else if ($err == Zend_Ldap_Exception::LDAP_NO_SUCH_OBJECT) {
                    $code = Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
                    $messages[0] = "Account not found: $username";
                    $failedAuthorities[$dname] = $zle->getMessage();
                } else if ($err == Zend_Ldap_Exception::LDAP_INVALID_CREDENTIALS
                        || ($err == Zend_Ldap_Exception::LDAP_UNWILLING_TO_PERFORM
                        &&  preg_match("/Account inactivated\. Contact system administrator\./",$zle->getMessage()))) {
                    $code = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
                    $messages[0] = 'Invalid credentials';
                    $failedAuthorities[$dname] = $zle->getMessage();
                } else if ($err == Zend_Ldap_Exception::LDAP_NO_SUCH_ATTRIBUTE) {
                    $messages[0] = 'Sorry, but there is a problem with your user account. Contact system administration';
                    $failedAuthorities[$dname] = $zle->getMessage();
                } else {
                    $line = $zle->getLine();
                    $messages[] = $zle->getFile() . "($line): " . $zle->getMessage();
                    $messages[] = str_replace($password, '*****', $zle->getTraceAsString());
                    $messages[0] = 'An unexpected failure occurred';
                }
                $messages[1] = $zle->getMessage();
            } catch(Tinebase_Exception_PasswordExpired $tepe) {
                $messages[0] = 'Your password has expired. You must change it. Do you want to do this now?';
                $messages[1] = $tepe->getMessage();
            }
        }

        $msg = isset($messages[1]) ? $messages[1] : $messages[0];
        $messages[] = "$username authentication failed: $msg";

        return new Zend_Auth_Result($code, $username, $messages);
    }

    /**
     * Converts options to string
     *
     * @param  array $options
     * @return string
     */
    private function _optionsToString(array $options)
    {
        $str = '';
        foreach ($options as $key => $val) {
            if ($key === 'password')
                $val = '*****';
            if ($str)
                $str .= ',';
            $str .= $key . '=' . $val;
        }
        return $str;
    }

    /**
     * Returns a connection to user backend
     * @param array $_options
     * @return Tinebase_Ldap
     * @throws Tinebase_Exception_Backend
     */
    public static function getBackendConnection(array $_options = array())
    {
        try {
            $ldap = new Tinebase_Ldap($_options);
            $ldap->bind();
        } catch (Zend_Ldap_Exception $zle) {
            throw new Tinebase_Exception_Backend_Ldap("Cannot connect to ldap: ".$zle->getMessage());
}
        return $ldap;
    }

    /**
     * Checks if user backend is valid
     * @param mixed $_authBackend
     * @return boolean
     */
    public static function isValid($_authBackend)
    {
        return ($_authBackend instanceof Tinebase_Ldap);
    }

    /**
     * Force close connection to Ldap Server
     */
    public function closeConnection()
    {
        $ldap = $this->getLdap();
        $ldap->disconnect();
    }

}
