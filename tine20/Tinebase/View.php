<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  View
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 */

/**
 * View Class
 *
 * @package     Tinebase
 * @subpackage  View
 */
class Tinebase_View
{
	
	/**
	 * additional Javascript files
	 *
	 * @var array
	 *
	 */
	private static $_additionalJsFiles = array ();
	
    public static function getThemeConfig()
    {
        $extJS     = 'ext-all.css';
        $pathTheme = 'tine20';
        $theme_config = array('','','');
        $favicon = 'images/favicon.ico';

        if(isset(Tinebase_Core::getConfig()->theme->load) && !empty(Tinebase_Core::getConfig()->theme->load))
        {
            //if the selected theme exists in the config file, get its path and options
            if (isset(Tinebase_Core::getConfig()->theme->path))
            {
                $pathTheme = Tinebase_Core::getConfig()->theme->path;
                //is useBlueAsBase set?
                if ((!isset(Tinebase_Core::getConfig()->theme->useBlueAsBase)) ||
                        (!Tinebase_Core::getConfig()->theme->useBlueAsBase))
                {
                    $extJS = 'ext-all-notheme.css';
                }
                //is there a customized favicon?
                if (file_exists('themes/' . $pathTheme . '/resources/images/favicon.ico'))
                {
                    $favicon = 'themes/' . $pathTheme . '/resources/images/favicon.ico';
                }
            }
        }

        $theme_config[0] =  $favicon;
        $theme_config[1] =  '<link rel="stylesheet" type="text/css" href="library/ExtJS/resources/css/'.$extJS.'" />';
        $theme_config[2] =  '<link rel="stylesheet" type="text/css" href="themes/'.$pathTheme.'/resources/css/'.$pathTheme.'.css" />';
        return $theme_config;
    }
    
    /**
     * Define additional Javascript files to load
     * 
     * @param array $jsFiles
     */
    public static function setAdditionalJsFiles(array $jsFiles)
    {
    	foreach ($jsFiles as $jsFile) {
    		self::$_additionalJsFiles[] = $jsFile;
    	}
    }
    
    /**
     * Return additional Javascript files content
     *
     * @return array
     */
    public static function getAdditionalJsFiles()
    {
        $contentFiles = array();
        foreach(self::$_additionalJsFiles as $jsFile){
            $contentFiles[] = $jsFile; 
        } 
        return $contentFiles;
    }
    
    /**
     * Define additional Javascript files to load
     * from a specific directory
     * 
     * @param string $directory
     */
    public static function setAdditionalJsFilesFromDirectory($directory)
    {
        self::setAdditionalJsFiles(glob($directory . '/*.js'));
    }
}