<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Server
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Lars Kneschke <l.kneschke@metaways.de>
 *
 */

/**
 * the class provides functions to handle applications
 *
 * @package     Tinebase
 * @subpackage  Server
 */
class Tinebase_Controller extends Tinebase_Controller_Event
{
    /**
     * holds the instance of the singleton
     *
     * @var Tinebase_Controller
     */
    private static $_instance = NULL;

    /**
     * application name
     *
     * @var string
     */
    protected $_applicationName = 'Tinebase';

    /**
     * permission to write into accessLog
     *
     * @var boolean
     */
    protected $_writeAccessLog;

    /**
     * Access Log
     *
     * @var Tinebase_Model_AccessLog
     */
    protected $_accessLog = NULL;

    /**
     * default message error for authentication
     *
     * @var array
     */
    protected $_authErrorMessage = array(FALSE, 'Unknown Authentication Error', -1);

    /**
     * password hash checking
     *
     * @var boolean
     */
    protected $_hashCheck = TRUE;

    /**
     * the constructor
     *
     */
    private function __construct()
    {
        try {
            Tinebase_Config_Manager::getInstance()->checkTinebaseInstalled();
            $isTinebaseInstalled = true;
        } catch (Exception $ex) {
            Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' ' . $ex->getMessage());
            Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . $ex->getTraceAsString());
            $isTinebaseInstalled = false;
        }

        $this->_writeAccessLog = ($isTinebaseInstalled && $this->_isAccessLogEnabled());
    }

    /**
     * don't clone. Use the singleton.
     *
     */
    private function __clone() {}

    /**
     * the singleton pattern
     *
     * @return Tinebase_Controller
     */
    public static function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Tinebase_Controller;
        }

        return self::$_instance;
    }

    /**
     * create new user session
     *
     * @param   string $loginname
     * @param   string $password
     * @param   \Zend\Http\Request $request
     * @param   string $clientIdString
     * @param   string $securitycode the security code(captcha)
     * @param   boolean $_expiredPassword
     * @return  true | Zend_Auth_Result | array (failed)
     */
    public function login($loginName, $password, \Zend\Http\Request $request, $clientIdString = NULL, $securitycode = NULL, $_expiredPassword = NULL)
    {
        $loginName = Tinebase_Config_Manager::getInstance()->getRealLoginName($loginName, $clientIdString);
		
        $authInstance = Tinebase_Auth::getInstance();
        
        try {
            $authResult = $authInstance->authenticate($loginName, $password, $_expiredPassword);
           
        } catch (Exception $e) {
            $this->_authErrorMessage = array(FALSE, $e->getMessage(), -1);
            $authResult = new Zend_Auth_Result(Zend_Auth_Result::FAILURE_UNCATEGORIZED, $loginName, array($e->getMessage()));
            Tinebase_Exception::log($e);
            
        }

        if($_expiredPassword) {
            return($authResult);
        }

        $backendType = $authInstance->getConfiguredBackend();

        Tinebase_Core::set(Tinebase_Session::SESSIONID, Tinebase_Session::isStarted() ? session_id() : Tinebase_Record_Abstract::generateUID());

        $this->_accessLog = $this->_getModelAccessLog($authResult, $clientIdString, $request);

        $user = $this->_validateAuthResult($authResult, $backendType, $loginName, $password, $request);
       
        if (!($user instanceof Tinebase_Model_FullUser)) {
            return $this->_authErrorMessage;
        }
        
        return TRUE;
    }

    /**
     *
     * @param Zend_Auth_Result $authResult
     * @param string $backendType
     * @param string $loginName
     * @param string $password
     * @param Zend\Http\Request $request
     * @return Ambigous <boolean, NULL, Tinebase_Model_FullUser>
     */
    protected function _validateAuthResult($authResult, $backendType, $loginName, $password, $request)
    {
        $user = $this->_getAuthenticatedUser($authResult, $backendType);

        $needToken = FALSE;
        if($user !== NULL) {
            $activeSyncOrWebDAV = (Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) == 'ActiveSync_Server_Http'
                || Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) == 'Tinebase_Server_WebDAV');
            if ($activeSyncOrWebDAV || !($needToken = $this->_checkIfUserIsEnforcedToLoginWithDigitalCertificate($backendType, $user))){
                $this->_checkUserStatus($user);
            }
        }

        // $accessLog->result content could be modified by self::_getLoginUser method
        $result = $this->_getResultFromAccessLog($authResult, $backendType, $user, $needToken, $loginName, $password, $request);

        $this->_updateAccessLogInstance($loginName);

        // Check more failure states
        if (!($user instanceof Tinebase_Model_FullUser) ||
            $result === FALSE ||
            (is_array($result) && $result[0] === FALSE))
        {
            $result = ($result === FALSE) ? array(FALSE, 'Unknown Authentication Error' , Tinebase_Auth::FAILURE_UNCATEGORIZED) : $result;
            $this->_authErrorMessage = $result;
            return NULL;
        }

        return $user;
    }



    /**
     *
     * @param Zend_Auth_Result  $authResult
     * @param string            $_clientIdString
     * @param Zend_Controller_Request_Abstract            $request
     * @return Tinebase_Model_AccessLog
     */
    protected function _getModelAccessLog(Zend_Auth_Result $authResult, $_clientIdString, $request)
    {
        return new Tinebase_Model_AccessLog(array(
                'sessionid'     => Tinebase_Core::get(Tinebase_Session::SESSIONID),
                'ip'            => $request->getServer('REMOTE_ADDR'),
                'li'            => Tinebase_DateTime::now()->get(Tinebase_Record_Abstract::ISO8601LONG),
                'result'        => $authResult->getCode(),
                'clienttype'    => $_clientIdString,
        ), TRUE);
    }

    /**
     *
     * @param Zend_Auth_Result          $authResult
     * @param string                    $backendType
     * @return Tinebase_Model_FullUser | NULL
     */
    protected function _getAuthenticatedUser(Zend_Auth_Result $authResult, $backendType)
    {
        $user = NULL;
        $authResultIdentity = $authResult->getIdentity();
        if ($this->_accessLog->result === Tinebase_Auth::SUCCESS) {
            if (Tinebase_Core::get(Tinebase_Core::CUSTOMEXPIRABLECACHE)
                    && (Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) == 'ActiveSync_Server_Http')) {
                $params = array('getLoginUser_authResultIdentity_', $authResultIdentity);
                $cacheId = Tinebase_Helper::arrayHash($params);
                $cache = Tinebase_Core::getCache();
                if ($cache->test($cacheId)) {
                    $user = $cache->load($cacheId);
                }
            }

            if ($user === NULL) {
                $user = $this->_getLoginUser($authResultIdentity);
                if($this->_accessLog->result === Tinebase_Auth::SUCCESS) {
                    if (Tinebase_Core::get(Tinebase_Core::CUSTOMEXPIRABLECACHE)
                            && (Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) == 'ActiveSync_Server_Http')) {
                        $cache->save($user, $cacheId, array('ASlogin', $user->getId()));
                    }
                } else {
                    if($this->_accessLog->result === Tinebase_Auth::FAILURE_IDENTITY_NOT_FOUND) {
                        if (Tinebase_Config_Manager::isMultidomain()) {
                            $errMsg =  "Wrong email or password!";
                        } else {
                            $errMsg =  "Wrong username or password!";
                        }
                        $this->_authErrorMessage = array(false, $errMsg, Tinebase_Auth::FAILURE_IDENTITY_NOT_FOUND);
                        $user = NULL;
                    } else {
                        $this->_authErrorMessage = array(false, 'The server is currently unable to handle the request due to a temporary overloading, maintenance or misconfiguration of the server. Please try again or contact your administrator.', $this->_accessLog->result);
                        $user = NULL;
                    }
                }
            }
        }
        if($user !== null) {
            if(Zend_Auth_Adapter_ModSsl::hasModSsl()) {
                $clientCert = !empty($_SERVER['SSL_CLIENT_CERT']) ? $_SERVER['SSL_CLIENT_CERT'] : $_SERVER['HTTP_SSL_CLIENT_CERT'];
                $certificate = Custom_Auth_ModSsl_Certificate_Factory::buildCertificate($clientCert);
                $certificateIdentity = $certificate->getCpf();
                if(!$certificate instanceof Custom_Auth_ModSsl_Certificate_ICPBrasil ||
                    $user->accountLoginName != $certificateIdentity)
                {
                        $this->_accessLog->result = Tinebase_Auth::FAILURE_IDENTITY_AMBIGUOUS;
                        $this->_authErrorMessage = array(false, 'Login with digital certificate has failed. Please contact your system administrator.', Tinebase_Auth::FAILURE_IDENTITY_AMBIGUOUS);
                        Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . " User data in digital certificate ($certificateIdentity) not correspond to the same user in database ({$user->accountLoginName}).");
                }
            }
        }
        return $user;
    }

    /**
     *
     * @param string                            $backendType
     * @param Tinebase_Model_FullUser | NULL    $user
     * @return boolean
     */
    protected function _checkIfUserIsEnforcedToLoginWithDigitalCertificate($backendType, $user)
    {
        $needToken = FALSE;
        if($user != NULL) {
            //check if user is enforced to login with digital certificate and,
            //if this is true, if a token was detected ($backendType == 'ModSsl');
            //ActiveSync and DAV users can login always;
            //users that are system administrators can login always;
            if (Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) === 'ActiveSync_Server_Http' ||
                Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) === 'Tinebase_Server_WebDAV') {
                return $needToken;
            }
            $loginWithCertificate = $user->hasRight($this->_applicationName, Tinebase_Acl_Rights::LOGIN_WITH_CERTIFICATE);
            $isAdmin = $user->hasRight($this->_applicationName, Tinebase_Acl_Rights::ADMIN);
            if($loginWithCertificate == TRUE && $backendType !== 'ModSsl' && $isAdmin !== TRUE){
                $needToken = TRUE;
            }
        }
        return $needToken;
    }

    /**
     *
     * @param Zend_Auth_Result                  $authResult
     * @param string                            $backendType
     * @param Tinebase_Model_FullUser | NULL    $user
     * @param boolean                           $needToken
     * @param string                            $_loginname
     * @param string                            $_password
     * @param Zend\Http\Request                 $request
     * @return TRUE | array
     */
    protected function _getResultFromLoginSucceeded($authResult, $backendType, $user, $needToken, $_loginname, $_password, $request)
    {
        if($user != NULL && $user->accountStatus === Tinebase_User::STATUS_ENABLED && $needToken === false) {

            //Check if beckend if setted up to check for password expiration by time
            $setupChecksForExpiredPass = Tinebase_User::getBackendConfiguration("checkExpiredPassword");

            if($setupChecksForExpiredPass &&
                Tinebase_Auth::getConfiguredBackend() !== Tinebase_Auth::MODSSL) // ModSsl don't check expiration
            {
                try {
                    /*
                     * If we get here, we have an LDAP connection bound to the user DN.
                     * Depending on the filter used, we migth not have read permission
                     * to search for the user. So, we force a disconnection to allow
                     * a bind with admin credentials before doing a user DN search by
                     * the backend.
                     */
                    Tinebase_Auth::getInstance()->closeBackendConnection();
                    $userPassIsExpired = $this->login($_loginname, $_password, $request, Tinebase_Frontend_Json::REQUEST_TYPE, NULL, TRUE);
                } catch (Tinebase_Exception_Backend_Ldap $e) {
                    $userPassIsExpired = new Zend_Auth_Result(Zend_Auth_Result::FAILURE_UNCATEGORIZED, $_loginname, array($e->getMessage()));
                    Tinebase_Exception::log($e);
                } catch (Tinebase_Exception $e){
                    $userPassIsExpired = new Zend_Auth_Result(Zend_Auth_Result::FAILURE_UNCATEGORIZED, $_loginname, array($e->getMessage()));
                    Tinebase_Exception::log($e);
                }

                if($userPassIsExpired->getCode() != Zend_Auth_Result::SUCCESS) {
                    return $this->_getResultForExpirationFailure($userPassIsExpired);
                }
            }

            //Login with certificate, must lock at user vocation access block
            if(($result = $this->_getResultForModSsl($user->accountLoginName, $backendType)) !== true) {
                return $result;
            }

            try {
                $this->_initUser($user, $_password);
            } catch (Tinebase_Exception_AccessDenied $tead) {
                return array(FALSE, "Access denied", -1);
            } catch (Exception $exception) {
                Tinebase_Exception::log($exception);
            }

            if ($backendType == Tinebase_Auth::MODSSL){
                return $this->_getResultForModSsl($user->accountLoginName);
            }
            return $authResult;
        }
        // user needs token to login
        return array(FALSE, "User is enforced to login with digital certificate", -1);
    }

    /**
     *
     * @param Zend_Auth_Result  $userPassIsExpired
     * @return true | array (failure)
     */
    protected function _getResultForExpirationFailure($userPassIsExpired)
    {
        $code = $userPassIsExpired->getCode();
        if(($code == Zend_Auth_Result::FAILURE)){
            $msgr = $userPassIsExpired->getMessages();
            $this->logout(Tinebase_AccessLog::getRemoteIpAddress());
            $result = array(FALSE, $msgr[0], $code);
        }
        else
        {
            $result = TRUE;
        }
        return $result;
    }

    /**
     *
     * @param string            $accountLoginName
     * @return true | array (failure)
     */
    protected function _getResultForModSsl($accountLoginName)
    {
        //Login with certificate, must lock at user vocation access block
        $vacationStatus = $this->_getUserVacationStatus($accountLoginName);
        if($vacationStatus !== TRUE && is_array($vacationStatus) && $vacationStatus[0] === TRUE){
            $this->logout(Tinebase_AccessLog::getRemoteIpAddress());
            return array(FALSE, $userv[1], -1);
        } else {
            return TRUE;
        }
    }

    /**
     *
     * @param $authResult                       Zend_Auth_Result
     * @param Tinebase_Model_FullUser | NULL    $user
     * @param string                            $_loginname
     * @param string                            $_password
     * @return TRUE | array
     */
    protected function _getResultFromLoginFailed($authResult, $user, $_loginname,$_password)
    {
        $result = TRUE;

        $this->_hashCheck = FALSE; // login failed default is false
        if (Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) == 'ActiveSync_Server_Http'){
            $this->_hashCheck = Tinebase_AccessLog::getInstance()->testPasswordHash($_loginname,$_password);
            $this->_accessLog->passwordhash = sha1($_password);
        }

        if($user != NULL) {
            //account still enabled
            if($user->accountStatus === Tinebase_User::STATUS_ENABLED){
                $this->_loginFailed($_loginname ? $_loginname : $authResult->getIdentity());
                $result = $this->_authErrorMessage;
            }
            //account status is not enabled -> using default message
            else{
                $this->_loginFailed($_loginname ? $_loginname : $authResult->getIdentity());
                if (Tinebase_Config_Manager::isMultidomain()) {
                    $errMsg =  "Wrong email or password!";
                } else {
                    $errMsg =  "Wrong username or password!";
                }
                $result = array(FALSE, $errMsg, -1);
            }
        } else {
            if($authResult->getCode() == Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID) {
                $this->_loginFailed($_loginName ? $_loginName : $authResult->getIdentity());
                $result = array(FALSE, 'Wrong username or password!', -1);
            } else {
                $this->_loginFailed($_loginname ? $_loginname : $authResult->getIdentity());
                $result = array(FALSE, 'The server is currently unable to handle the request due to a temporary overloading, maintenance or misconfiguration of the server. Please try again or contact your administrator.', $this->_accessLog->result);
            }
        }
        // return must be the same of _getResultFromUserWithAccountStatusEnabledButWithoutToken
        return $result;
    }

    /**
     *
     * @param Zend_Auth_Result                  $authResult
     * @param string                            $backendType
     * @param Tinebase_Model_FullUser | NULL    $user
     * @param boolean                           $needToken
     * @param string                            $_loginname
     * @param string                            $_password
     * @param Zend\Http\Request                 $request
     * @return TRUE | array
     */
    protected function _getResultFromAccessLog(Zend_Auth_Result $authResult, $backendType, $user, $needToken, $_loginname, $_password, $request)
    {
        if($user !== NULL && $this->_accessLog->result === Tinebase_Auth::SUCCESS) {
            $result = $this->_getResultFromLoginSucceeded($authResult, $backendType, $user, $needToken, $_loginname, $_password, $request);

        } else {
            $result = $this->_getResultFromLoginFailed($authResult, $user, $_loginname, $_password);
        }
        return $result;
    }

    /**
     *
     * @param string $_loginname
     */
    protected function _updateAccessLogInstance($loginName)
    {
        if ($this->_writeAccessLog || ($this->_hashCheck === FALSE)) {
            $this->_accessLog->login_name = $loginName;
            Tinebase_AccessLog::getInstance()->create($this->_accessLog);
        }
    }

    /**
     * get login user
     * @param string                    $_username
     * @return Tinebase_Model_FullUser|NULL
     */
    protected function _getLoginUser($_username)
    {
        $accountsController = Tinebase_User::getInstance();
        $user = NULL;
        try {
            // does the user exist in the user database?
            if ($accountsController instanceof Tinebase_User_Interface_SyncAble) {
                /**
                 * catch all exceptions during user data sync
                 * either it's the first sync and no user data get synchronized or
                 * we can work with the data synced during previous login
                 */
                try {
                    if (Tinebase_Session_Abstract::getSessionEnabled() &&
                       !isset(Tinebase_User_Session::getSessionNamespace()->currentAccount) &&
                       (Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) !== 'ActiveSync_Server_Http')) {
                          Tinebase_User::syncUser($_username,array('syncContactData' => TRUE));
                    }
                } catch (Tinebase_Exception_NotFound $e) {
                    throw $e;
                } catch (Exception $e) {
                    Tinebase_Core::getLogger()->crit(__METHOD__ . '::' . __LINE__ . ' Failed to sync user data for: ' . $_username . ' reason: ' . $e->getMessage());
                    if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . $e->getTraceAsString());
                }
            }
            $user = Tinebase_User::getInstance()->getFullUser($_username);
        } catch (Tinebase_Exception_ContainerNotFound $e){
            if (Tinebase_Core::isLogLevel(Zend_Log::CRIT)) Tinebase_Core::getLogger()->crit(__METHOD__ . '::' . __LINE__ . ' User ' . $_username . ' not synced in backend.');
            $this->_accessLog->result = Tinebase_Auth::FAILURE_USER_NOT_SYNCED;
        } catch (Tinebase_Exception_NotFound $e) {
            if (Tinebase_Core::isLogLevel(Zend_Log::CRIT)) Tinebase_Core::getLogger()->crit(__METHOD__ . '::' . __LINE__ . ' Account ' . $_username . ' not found in account storage.');
            $this->_accessLog->result = Tinebase_Auth::FAILURE_IDENTITY_NOT_FOUND;
        } catch (Zend_Db_Adapter_Exception $zdae) {
            if (Tinebase_Core::isLogLevel(Zend_Log::CRIT)) Tinebase_Core::getLogger()->crit(__METHOD__ . '::' . __LINE__ . ' Some database connection failed: ' . $zdae->getMessage());
            $this->_accessLog->result = Tinebase_Auth::FAILURE_DATABASE_CONNECTION;
        }

        return $user;
    }

    /**
     * check user status
     *
     * @param Tinebase_Model_FullUser   $_user
     */
    protected function _checkUserStatus(Tinebase_Model_FullUser $_user)
    {
        // is the user enabled?
        if ($this->_accessLog->result == Tinebase_Auth::SUCCESS && $_user->accountStatus !== Tinebase_User::STATUS_ENABLED) {
            // is the account enabled?
            if ($_user->accountStatus == Tinebase_User::STATUS_DISABLED) {
                if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' Account: '. $_user->accountLoginName . ' is disabled');
                $this->_accessLog->result = Tinebase_Auth::FAILURE_DISABLED;
            }

            // is the account expired?
            else if ($_user->accountStatus == Tinebase_User::STATUS_EXPIRED) {
                if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' Account: '. $_user->accountLoginName . ' password is expired');
                $this->_accessLog->result = Tinebase_Auth::FAILURE_PASSWORD_EXPIRED;
            }

            // too many login failures?
            else if ($_user->accountStatus == Tinebase_User::STATUS_BLOCKED) {
                if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' Account: '. $_user->accountLoginName . ' is blocked');
                $this->_accessLog->result = Tinebase_Auth::FAILURE_BLOCKED;
            }
        }
    }

    /**
     * init user session
     *
     * @param Tinebase_Model_FullUser $_user
     * @param string $_password
     */
    protected function _initUser(Tinebase_Model_FullUser $_user, $_password)
    {
        if ($this->_accessLog->result === Tinebase_Auth::SUCCESS && $_user->accountStatus === Tinebase_User::STATUS_ENABLED) {

            Tinebase_Core::set(Tinebase_Core::USER, $_user);
            if ((Tinebase_Session_Abstract::getSessionEnabled() && !isset(Tinebase_User_Session::getSessionNamespace()->currentAccount))
                    || Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) === 'ActiveSync_Server_Http'){
                $credentialCache = Tinebase_Auth_CredentialCache::getInstance()->cacheCredentials($_user->accountLoginName, $_password);
                Tinebase_Core::set(Tinebase_Core::USERCREDENTIALCACHE, $credentialCache);
            }
            if (Tinebase_Session_Abstract::getSessionEnabled()) {
                $this->_initUserSession($_user);
            }

            // need to set locale again and because locale might not be set correctly during loginFromPost
            // use 'auto' setting because it is fetched from cookie or preference then
            Tinebase_Core::setupUserLocale('auto');

            // need to set userTimeZone again
            $userTimezone = Tinebase_Core::getPreference()->getValue(Tinebase_Preference::TIMEZONE);
            Tinebase_Core::setupUserTimezone($userTimezone);

            if (Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) !== 'ActiveSync_Server_Http' ||
               Tinebase_Core::get(Tinebase_Core::CUSTOMEXPIRABLECACHE) === FALSE) {
                $_user->setLoginTime($this->_accessLog->ip);
            }

            $this->_accessLog->sessionid = Tinebase_Core::get(Tinebase_Session::SESSIONID);
            $this->_accessLog->login_name = $_user->accountLoginName;
            $this->_accessLog->account_id = $_user->getId();
        }
    }

    /**
     * init session after successful login
     *
     * @param Tinebase_Model_FullUser $_user
     */
    protected function _initUserSession(Tinebase_Model_FullUser $_user)
    {
        if (Tinebase_Config::getInstance()->get(Tinebase_Config::SESSIONUSERAGENTVALIDATION, TRUE)) {
            Tinebase_Session::registerValidatorHttpUserAgent();
        } else {
            Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' User agent validation disabled.');
        }

        if (Tinebase_Config::getInstance()->get(Tinebase_Config::SESSIONIPVALIDATION, TRUE)) {
            Tinebase_Session::registerValidatorIpAddress();
        } else {
            Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' Session ip validation disabled.');
        }

        if (Tinebase_Session::isStarted() && !isset(Tinebase_User_Session::getSessionNamespace()->currentAccount)) {
            /**
             * fix php session header handling http://forge.tine20.org/mantisbt/view.php?id=4918
             * -> search all Set-Cookie: headers and replace them with the last one!
             **/
            $cookieHeaders = array();
            foreach (headers_list() as $headerString) {
                if (strpos($headerString, 'Set-Cookie: TINE20SESSID=') === 0) {
                    array_push($cookieHeaders, $headerString);
                }
            }
            header(array_pop($cookieHeaders), true);
            /** end of fix **/

            Tinebase_User_Session::getSessionNamespace()->currentAccount = $_user;
        } else {
            $userSession = Tinebase_User_Session::getSessionNamespace();
            if(isset($userSession->currentAccount) && $userSession->currentAccount->getId() != $_user->getId()) {
                Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__
                        . ":: The user:${$_user->getId()} session was compromised. Destroying session");
                Tinebase_Auth_CredentialCache::getInstance()->getCacheAdapter()->resetCache();
                Tinebase_Session::destroyAndRemoveCookie();
                throw new Tinebase_Exception_AccessDenied(
                        "The user:${$_user->getId()} session was compromised. Destroying session");
            }
        }
    }

    /**
     * login failed
     *
     * @param unknown_type $_loginname
     * @TODO verify the necessity of the sleep at the end of this method
     */
    protected function _loginFailed($_loginname)
    {
        if ($this->_hashCheck === FALSE){
            Tinebase_User::getInstance()->setLastLoginFailure($_loginname);
        }
        $this->_accessLog->login_name = $_loginname;
        $this->_accessLog->lo = Tinebase_DateTime::now()->get(Tinebase_Record_Abstract::ISO8601LONG);

        sleep(mt_rand(2,5));
    }

    /**
     * Conditionally disable accesslog generation for some clients
     *
     * @return bool
     */
    protected function _isAccessLogEnabled() {
        $config = Tinebase_Config::getInstance()->get(Tinebase_Config::ACCESS_LOG, TRUE);

        if(Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) == 'ActiveSync_Server_Http'
                && !is_bool($config) && isset($config->disableactivesyncaccesslog)
                && $config->disableactivesyncaccesslog === true){
            return false;
        } elseif(Tinebase_Core::get(Tinebase_Core::SERVER_CLASS_NAME) == 'Tinebase_Server_WebDAV'
                && !is_bool($config) && isset($config->disabledavaccesslog)
                && $config->disabledavaccesslog === true){
            return false;
        }

        return true;
    }

     /**
     * renders and send to browser one captcha image
     *
     * @return void
     */
    public function makeCaptcha()
    {
        return $this->_makeImage();
    }

    /**
     * renders and send to browser one captcha image
     *
     * @return void
     */
    protected function _makeImage()
    {
        $result = array();
        $width='170';
        $height='40';
        $characters= mt_rand(5,7);
        $possible = '123456789aAbBcCdDeEfFgGhHIijJKLmMnNpPqQrRstTuUvVwWxXyYZz';
        $code = '';
        $i = 0;
        while ($i < $characters) {
            $code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
            $i++;
        }
        $font = './fonts/Milonga-Regular.ttf';
        /* font size will be 70% of the image height */
        $font_size = $height * 0.67;
        try {
            $image = @imagecreate($width, $height);
            /* set the colours */
            $background_color = imagecolorallocate($image, 255, 255, 255);
            $text_color = imagecolorallocate($image, 20, 40, 100);
            $noise_color = imagecolorallocate($image, 100, 120, 180);
            /* generate random dots in background */
            for( $i=0; $i<($width*$height)/3; $i++ ) {
                imagefilledellipse($image, mt_rand(0,$width), mt_rand(0,$height), 1, 1, $noise_color);
            }
            /* generate random lines in background */
            for( $i=0; $i<($width*$height)/150; $i++ ) {
                imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $noise_color);
            }
            /* create textbox and add text */
            $textbox = imagettfbbox($font_size, 0, $font, $code);
            $x = ($width - $textbox[4])/2;
            $y = ($height - $textbox[5])/2;
            imagettftext($image, $font_size, 0, $x, $y, $text_color, $font , $code);
            ob_start();
            imagejpeg($image);
            $image_code = ob_get_contents ();
            ob_end_clean();
            imagedestroy($image);
            $result = array();
            $result['1'] = base64_encode($image_code);
            Tinebase_Session::getSessionNamespace()->captcha['code'] = $code;
        } catch (Exception $e) {
            if (Tinebase_Core::isLogLevel(Zend_Log::NOTICE)) Tinebase_Core::getLogger()->notice(__METHOD__ . '::' . __LINE__ . ' ' . $e->getMessage());
        }
        return $result;
    }

    /**
     * authenticate user but don't log in
     *
     * @param   string $_loginname
     * @param   string $_password
     * @param   string $_ipAddress
     * @param   string $_clientIdString
     * @return  bool
     */
    public function authenticate($_loginname, $_password, $_ipAddress, $_clientIdString = NULL)
    {
        $request = Tinebase_Core::get(Tinebase_Core::REQUEST);
        $result = $this->login($_loginname, $_password, $request, $_clientIdString);

        /**
         * we unset the Zend_Auth session variable. This way we keep the session,
         * but the user is not logged into Tine 2.0
         * we use this to validate passwords for OpenId for example
         */
        $coreSession = Tinebase_Session::getSessionNamespace();
        unset($coreSession->Zend_Auth);

        $userSession = Tinebase_User_Session::getSessionNamespace();
        unset($userSession->currentAccount);

        return $result;
    }
    /**
     * change user password
     *
     * @param string $_oldPassword
     * @param string $_newPassword
     * @throws  Tinebase_Exception_AccessDenied
     * @throws  Tinebase_Exception_InvalidArgument
     */
    public function changePassword($_oldPassword, $_newPassword)
    {
        $translate = Tinebase_Translation::getTranslation('Tinebase');
        if (! Tinebase_Config::getInstance()->get(Tinebase_Config::PASSWORD_CHANGE, TRUE)) {
            throw new Tinebase_Exception_AccessDenied($translate->_('Password change not allowed'));
        }
        $loginName = Tinebase_Config_Manager::getInstance()->getUsername(Tinebase_Core::getUser());
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " change password for $loginName");

        if (!Tinebase_Auth::getInstance()->isValidPassword($loginName, $_oldPassword)) {
            throw new Tinebase_Exception_InvalidArgument($translate->_('Old password is wrong'));
        }

        Tinebase_User::getInstance()->setPassword(Tinebase_Core::getUser(), $_newPassword, true, false, $_oldPassword);
    }

    /**
     * logout user
     *
     * @return void
     */
    public function logout($_ipAddress)
    {
        if ($this->_writeAccessLog) {
            if (Tinebase_Core::isRegistered(Tinebase_Core::USER) && is_object(Tinebase_Core::getUser())) {
                Tinebase_AccessLog::getInstance()->setLogout(Tinebase_Core::get(Tinebase_Session::SESSIONID),  $_ipAddress);
            }
        }
    }

    /**
     * gets image info and data
     *
     * @param   string $_application application which manages the image
     * @param   string $_identifier identifier of image/record
     * @param   string $_location optional additional identifier
     * @return  Tinebase_Model_Image
     * @throws  Tinebase_Exception_NotFound
     * @throws  Tinebase_Exception_UnexpectedValue
     */
    public function getImage($_application, $_identifier, $_location = '')
    {
        $appController = Tinebase_Core::getApplicationInstance($_application);
        if (!method_exists($appController, 'getImage')) {
            throw new Tinebase_Exception_NotFound("$_application has no getImage function.");
        }
        $image = $appController->getImage($_identifier, $_location);

        if (!$image instanceof Tinebase_Model_Image) {
            throw new Tinebase_Exception_UnexpectedValue("$_application returned invalid image.");
        }
        return $image;
    }

    /**
     * remove obsolete/outdated stuff from cache
     * notes: CLEANING_MODE_OLD -> removes obsolete cache entries (files for file cache)
     *        CLEANING_MODE_ALL -> removes complete cache structure (directories for file cache) + cache entries
     *
     * @param string $_mode
     */
    public function cleanupCache($_mode = Zend_Cache::CLEANING_MODE_OLD)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(
            __METHOD__ . '::' . __LINE__ . ' Cleaning up the cache (mode: ' . $_mode . ')');

        Tinebase_Core::getCache()->clean($_mode);
    }

    /**
     * cleanup old sessions files => needed only for filesystems based sessions
     */
    public function cleanupSessions()
    {
        $config = Tinebase_Core::getConfig();

        $backendType = ($config->session && $config->session->backend) ? ucfirst($config->session->backend) : 'File';

        if (strtolower($backendType) == 'file') {
            $maxLifeTime = ($config->session && $config->session->lifetime) ? $config->session->lifetime : 86400;
            $path = ini_get('session.save_path');

            $unlinked = 0;
            try {
                $dir = new DirectoryIterator($path);
            } catch (UnexpectedValueException $uve) {
                if (Tinebase_Core::isLogLevel(Zend_Log::NOTICE)) Tinebase_Core::getLogger()->notice(
                    __METHOD__ . '::' . __LINE__ . " Could not cleanup sessions: " . $e->getMessage());
                return;
            }

            foreach ($dir as $fileinfo) {
                if (!$fileinfo->isDot() && !$fileinfo->isLink() && $fileinfo->isFile()) {
                    if ($fileinfo->getMTime() < Tinebase_DateTime::now()->getTimestamp() - $maxLifeTime) {
                        unlink($fileinfo->getPathname());
                        $unlinked++;
                    }
                }
            }

            if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(
                __METHOD__ . '::' . __LINE__ . " Deleted $unlinked expired session files");

            Tinebase_Config::getInstance()->set(Tinebase_Config::LAST_SESSIONS_CLEANUP_RUN, Tinebase_DateTime::now()->toString());
        }
    }

    /**
     * spy function for unittesting of queue workers
     *
     * this function writes the number of executions of itself in the given
     * file and optionally sleeps a given time
     *
     * @param string  $filename
     * @param int     $sleep
     * @param int     $fail
     */
    public function testSpy($filename=NULL, $sleep=0, $fail=NULL)
    {
        $filename = $filename ? $filename : ('/tmp/'.__METHOD__);
        $counter = file_exists($filename) ? (int) file_get_contents($filename) : 0;

        file_put_contents($filename, ++$counter);

        if ($sleep) {
            sleep($sleep);
        }

        if ($fail && (int) $counter <= $fail) {
            throw new Exception('spy failed on request');
        }

        return;
    }

    /**
     * handle events for Tinebase
     *
     * @param Tinebase_Event_Abstract $_eventObject
     */
    protected function _handleEvent(Tinebase_Event_Abstract $_eventObject)
    {
        switch (get_class($_eventObject)) {
            case 'Admin_Event_DeleteGroup':
                foreach ($_eventObject->groupIds as $groupId) {
                    if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                        . ' Removing role memberships of group ' .$groupId );

                    $roleIds = Tinebase_Acl_Roles::getInstance()->getRoleMemberships($groupId, Tinebase_Acl_Rights::ACCOUNT_TYPE_GROUP);
                    foreach ($roleIds as $roleId) {
                        Tinebase_Acl_Roles::getInstance()->removeRoleMember($roleId, array(
                            'id'   => $groupId,
                            'type' => Tinebase_Acl_Rights::ACCOUNT_TYPE_GROUP,
                        ));
                    }
                }
                break;
        }
    }

    /**
     * change user expired password at login screen
     *
     * @param string $_userName
     * @param string $_oldPassword
     * @param string $_newPassword
     * @throws  Zend_Auth_Result
     * @throws  Tinebase_Exception_AccessDenied
     */
    public function changeExpiredPassword($_userName, $_oldPassword, $_newPassword)
    {
        try
        {
            $translate = Tinebase_Translation::getTranslation('Tinebase');
            if (! Tinebase_Config::getInstance()->get(Tinebase_Config::PASSWORD_CHANGE, TRUE)) {
                Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . " System is not configured to password change. Aborting password expired change routine for user '$_userName'");
                throw new Tinebase_Exception_AccessDenied($translate->_('Password change not allowed at this environment'));
            }

            $loginName = $_userName;

            if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . " Starting change expired password routine for user '$loginName'");

            $oldLogin = $this->login($_userName, $_oldPassword, Tinebase_Core::get(Tinebase_Core::REQUEST), Tinebase_Frontend_Json::REQUEST_TYPE, NULL, NULL);

            if($oldLogin){
                try
                {
                    Tinebase_User::getInstance()->setPassword(Tinebase_User::getInstance()->getFullUser($_userName), $_newPassword, true, false, $_oldPassword);
                    if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . " changing expired password for user '$loginName' successfully done");
                    Tinebase_Session::destroyAndRemoveCookie();
                    return(array(true, 'Your password has been changed'));
                }catch (Tinebase_Exception_PasswordPolicyViolation $ex){
                    Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . " changing expired password for user '$loginName' fails. Policy failure");
                    Tinebase_Session::destroyAndRemoveCookie();
                    return(array(false, $translate->_("Sorry, but there was a problem while processing your password expirated change") . ':' . $ex->getMessage()));
                } catch (Exception $ex) {
                    Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . " changing expired password for user '$loginName' fails");
                    Tinebase_Session::destroyAndRemoveCookie();
                    return(array(false, $translate->_('Unable to change your password. Look for the system administrator')));
                }
            }
            else{
                if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . " password expired change logon for user '$loginName' with current(expired) password fails");
                $code = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
                $messages[0] = $translate->_('Your logon with current(expired) password failed');
                Tinebase_Session::destroyAndRemoveCookie();
                return new Zend_Auth_Result($code, '', $messages);
            }
        } catch (Exception $ex) {
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " changing expired password for user '$loginName' fails!");
            Tinebase_Session::destroyAndRemoveCookie();
            return(array(false,$ex->getMessage()));
        }
    }

     /**
     * check for account restriction by vacation when login with digital certificate
     *
     * @param string $userName
     * @returns array
     */
    protected function _getUserVacationStatus($username){
        try {
            $vacationStatus = Tinebase_User::getInstance()->getUserVacationState($username);
            return $vacationStatus;
        } catch (Exception $e) {
            Tinebase_Core::getLogger()->crit(__METHOD__ . '::' . __LINE__ . ' Failed to fetch vacation attributes account data for: ' . $username . ' reason: ' . $e->getMessage());
            Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . $e->getTraceAsString());
            return false;
        }
    }
}
