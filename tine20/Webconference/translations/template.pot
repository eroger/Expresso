#: Controler/BigBlueButton.php:156
msgid "ERROR (%s): %s"
msgstr ""

#: Controler/BigBlueButton.php:149
msgid "Welcome to the Webconference %s by %s"
msgstr ""

#: Controler/Room.php:46
msgid "Webconference Invite"
msgstr ""

#: views/eventNotification.php:41
msgid "The user %s is inviting you to a Webconference"
msgstr ""

#: Controler/Room.php:41
msgid "Users invited successfully"
msgstr ""

#: js/EmailDetailsPanel.js:52
msgid "You received an webconference invitation."
msgstr ""

#: js/WebconferenceGridPanel.js:28
msgid "Room name"
msgstr ""

#: js/AttendeeGridPanel.js:161
msgid "Type"
msgstr ""

#: js/EmailDetailsPanel.js:36
msgid "Enter"
msgstr ""

#: js/ConferenceDialog.js:60
msgid "Exit"
msgstr ""

#: js/EmailDetailsPanel.js:70
msgid "Room Title"
msgstr ""

#: js/AdminPanel.js:81
msgid "Apply"
msgstr ""

#: js/AdminPanel.js:77
msgid "Webconference Configuration"
msgstr ""

#: js/AdminPanel.js:183
msgid "BBB Url"
msgstr ""

#: js/AdminPanel.js:188
msgid "Security Salt"
msgstr ""

#: js/AdminPanel.js:194
msgid "Description"
msgstr ""

#: js/Webconference.js:25
msgid "Webconference"
msgstr ""

#: js/ConferenceDialog.js:25
msgid "Add User"
msgstr ""

#: js/ConferenceDialog.js:29
msgid "Finish"
msgstr ""

#: js/AttendeeFilterModel.js:39
msgid "Attendee"
msgstr ""

msgid "Moderator"
msgstr ""

#: js/Webconference.js:16
msgid "You are already in a webconference. Accepting this invitation will make you leave the existing one."
msgstr ""

#: js/Webconference.js:16
msgid "Proceed"
msgstr ""

#: js/ConferenceDialog.js:124
msgid "This will kick all participants out of the meeting. Terminate webconference"
msgstr ""

#: js/ConferenceDialog.js:54
msgid "Invite an User to the Webconference"
msgstr ""

#: js/ConferenceDialog.js:63
msgid "Left Webconference"
msgstr ""

#: js/ConferenceDialog.js:33
msgid "Terminate Webconference kicking all users out"
msgstr ""

#: Frontend/Json.php:201
msgid "The Webconference server is offline"
msgstr ""

#: Frontend/Json.php:17
msgid "The Webconference you are trying to join no longer exists"
msgstr ""

#: Controller/Room.php:248
msgid "An error has occured inviting users"
msgstr ""

#: Controller/BigBlueButton.php:153
msgid "ERROR (the webconference server is unreachable)"
msgstr ""

#: Controller/BigBlueButton.php:141
msgid "ERROR (no webconference server available or the room limit has been reached)"
msgstr ""

#: js/ConfigEditDialog:37
msgid "Room Limit"
msgstr ""

#: js/WebconferenceGridPanel:82
msgid "Title"
msgstr ""

#: js/Webconference:21
msgid "New Webconference"
msgstr ""

#: js/WebconferenceGridPanel:172
msgid "Do you want to join the room {0}"
msgstr ""

#: js/WebconferenceGridPanel:58
msgid "Double Click to join"
msgstr ""

#: js/Webconference.js:350
msgid "Do you want to create a new webconference room with title: {0}"
msgstr ""

msgid "Active"
msgstr ""

msgid "Expired"
msgstr ""

#: views/eventNotification.php:43
msgid "Join the webconference"
msgstr ""

#: js/Model.js:182
msgid "status"
msgstr ""

#: js/WebconferenceGridPanel.js:95
msgid "Status"
msgstr ""

#: js/Model.js:179
msgid "title"
msgstr ""

#: js/WebconferenceGridPanel.js:101
msgid "Creation Time"
msgstr ""

#: js/Model.js:32
msgid "Attendee Role"
msgstr ""

#: js/RoomEditDialog.js:166
msgid "Organizer"
msgstr ""

#: js/AttendeeGridPanel.js:38
msgid "Role"
msgstr ""

msgid "Name"
msgstr ""

#: js/WebconferenceDetailsPanel.js:55
msgid "Total Webconference Records"
msgstr ""

#: js/WebconferenceGridPanel.js:217
msgid "Room Expired"
msgstr ""

#: js/WebconferenceGridPanel.js:128
msgid "Start Conference"
msgstr ""

#: js/AttendeeGridPanel.js:28
msgid "Click here to invite another attender..."
msgstr ""

#: js/AttendeeGridPanel.js:28
msgid "Details"
msgstr ""

#: js/AttendeeGridPanel.js:516
msgid "No Information"
msgstr ""

#: js/AttendeeGridPanel.js:44
msgid "Remove Attender"
msgstr ""

#: js/AttendeeGridPanel.js:364
msgid "Delegate Attendance"
msgstr ""

#: js/AttendeeGridPanel.js:364
msgid "Webconferences"
msgstr ""

#: js/Model.js:109
msgid "Room"
msgid_plural "Rooms"
msgstr[0] ""
msgstr[1] ""

#: js/Model.js:110
msgid "Rooms"
msgstr ""

#: js/Model.js:113
msgid "room list"
msgid_plural "room lists"
msgstr[0] ""
msgstr[1] ""

#: js/Model.js:110
msgid "User"
msgstr ""

#: js/Model.js:110
msgid "creation_time"
msgstr ""

#: js/Webconference.js:166
msgid "Save Room Error"
msgstr ""

#: js/Webconference.js:167
msgid "Could not save Room."
msgstr ""

#: js/Model.js:373
msgid "Attender"
msgid_plural "Attendee"
msgstr[0] ""
msgstr[1] ""

#: js/RoomEditDialog.js:92
msgid "View"
msgstr ""

#: js/RoomEditDialog.js:92
msgid "Displayed in"
msgstr ""

#: js/Model.js:113
#: Controller.php:94
msgid "%s's webconference records"
msgstr ""

#: Attender.php:400
msgid "This contact has been automatically added by the system as a room attender"
msgstr ""

#: js/AttendeeGridPanel.js:220
#: js/AttendeeGridPanel.js:221
#: js/RoomEditDialog.js:92
msgid "Do not you an attender in Webconference!"
msgstr ""

#: js/RoomEditDialog.js:92
msgid "All my rooms"
msgstr ""

#: js/RoomEditDialog.js:92
msgid "All Rooms I attend"
msgstr ""

#: js/RoomEditDialog.js:92
msgid "I'm organizer"
msgstr ""

#: js/RoomEditDialog.js:92
msgid "Rooms I'm the organizer of"
msgstr ""

msgid "External Attendee"
msgstr ""

msgid "You have selected an external attendee!"
msgstr ""

msgid "shared desktop"
msgstr ""

msgid "show module shared desktop"
msgstr ""