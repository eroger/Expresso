<?php
/**
 * Tine 2.0
 * 
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2009 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * Webconference Container Filter
 * 
 * @package Webconference
 */
class Webconference_Model_WebconferenceFilter extends Tinebase_Model_Filter_Container
{

    /**
     * appends sql to given select statement
     * 
     * @param  Zend_Db_Select                    $_select
     * @param  Tinebase_Backend_Sql_Abstract     $_backend
     * @throws Tinebase_Exception_NotFound
     */
    public function appendFilterSql($_select, $_backend)
    {
        $this->_resolve();
        $quotedDisplayContainerIdentifier = $_backend->getAdapter()->quoteIdentifier('attendee.displaycontainer_id');
        $_select->where($this->_getQuotedFieldName($_backend) . ' IN (?)', empty($this->_containerIds) ? new Zend_Db_Expr('NULL') : $this->_containerIds);
        $_select->orWhere($quotedDisplayContainerIdentifier  .  ' IN (?)', empty($this->_containerIds) ? new Zend_Db_Expr('NULL') : $this->_containerIds);
    }
    
    public function setRequiredGrants(array $_grants)
    {
        $this->_requiredGrants = $_grants;
    }
    
    /**
     * create a attendee filter of users affected by this filter
     * 
     * @return Webconference_Model_AttenderFilter
     */
    public function getRelatedAttendeeFilter()
    {
        // allways set currentaccount
        $userIds = array(Tinebase_Core::getUser()->getId());
        
        // rip users from pathes
        foreach ((array) $this->getValue() as $value) {
            if (preg_match("/^\/personal\/([0-9a-z_\-]+)/i", $value, $matches)) {
                // transform current user 
                $userIds[] = $matches[1] == Tinebase_Model_User::CURRENTACCOUNT ? Tinebase_Core::getUser()->getId() : $matches[1];
            }
        }
        
        // get contact ids
        $users = Tinebase_User::getInstance()->getMultiple(array_unique($userIds));
        $attendeeFilterData = array();
        foreach ($users as $user) {
            $attendeeFilterData[] = array(
                'user_type' => Webconference_Model_Attender::USERTYPE_USER,
                'user_id'   => $user->contact_id
            );
        }
        $attenderFilter = new Webconference_Model_AttenderFilter('attender', 'in', $attendeeFilterData);
        return $attenderFilter;
    }

    /**
     * fetch shared containers
     *
     * NOTE: this is needed because we don't want external organizers events to be visible if 'shared' node is requested
     *
     * @param $currentAccount
     * @param $appName
     * @return mixed
     */
    protected function _getSharedContainer($currentAccount, $appName)
    {
        return Tinebase_Container::getInstance()->getSharedContainer($currentAccount, $appName, array(
                    Tinebase_Model_Grants::GRANT_READ
                ))->getId();
    }

}
