<?php

/**
 * Tine 2.0
 *
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 */
class Webconference_Setup_Update_Release1 extends Setup_Update_Abstract
{

    /**
     * update to 1.1 - Change config key from Tinebase to Webconference
     * @return void
     */
    public function update_0()
    {
        $cb = new Tinebase_Backend_Sql(array(
            'modelName' => 'Tinebase_Model_Config',
            'tableName' => 'config',
        ));
        $appId = Tinebase_Application::getInstance()->getApplicationByName('Webconference')->getId();
        $filter = new Tinebase_Model_ConfigFilter(array(array('field' => 'name', 'operator' => 'equals', 'value' => 'attendeeRoles'),
            array('field' => 'application_id', 'operator' => 'equals', 'value' => $appId)));
        $wRole = $cb->search($filter)->getFirstRecord();
        $val = json_decode($wRole->value);
        $existing = $val->records;
        $rolesConfig = array(
            'name' => Webconference_Config::ATTENDEE_ROLES,
            'records' => $existing,
        );
        $wRole->value = json_encode($rolesConfig);
        $wRole->name = Webconference_Config::ATTENDEE_ROLES;
        $cb->update($wRole);
        $this->setApplicationVersion('Webconference', '1.1');
    }

}
