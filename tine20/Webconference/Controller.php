<?php
/**
 * Tine 2.0
 * 
 * @package     Webconference
 * @subpackage  Controller
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * Webconference Controller (composite)
 * 
 * The Webconference 2.0 Controller manages access (acl) to the different backends and supports
 * a common interface to the servers/views
 * 
 * @package Webconference
 * @subpackage  Controller
 */
class Webconference_Controller extends Tinebase_Controller_Event implements Tinebase_Container_Interface
{
    
    protected static $_defaultModel = 'Webconference_Model_Room';
    
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */
    private function __construct() {
        $this->_applicationName = 'Webconference';
        $this->_currentAccount = Tinebase_Core::getUser();
    }
    
    /**
     * don't clone. Use the singleton.
     *
     */
    private function __clone() 
    {        
    }
    
    /**
     * holds self
     * @var Webconference_Controller
     */
    private static $_instance = NULL;
    
    /**
     * singleton
     *
     * @return Webconference_Controller
     */
    public static function getInstance() 
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Webconference_Controller();
        }
        return self::$_instance;
    }
    
    /**
     * temporaray function to get a default container]
     * 
     * @param string $_referingApplication
     * @return Tinebase_Model_Container container
     * 
     * @todo replace this by Tinebase_Container::getDefaultContainer
     */
    public function getDefaultContainer($_referingApplication = 'webconference')
    {
	$webconfConfig = Webconference_Config::getInstance();
        $configString = 'defaultcontainer_' . ( empty($_referingApplication) ? 'webconferences' : $_referingApplication );
        
        if (isset($webconfConfig->$configString)) {
            $defaultContainer = Tinebase_Container::getInstance()->getContainerById((int)$webconfConfig->$configString);
        } else {
            $defaultContainer = Tinebase_Container::getInstance()->getDefaultContainer('Webconference');
        }   
        return $defaultContainer;
    }
           
     /*
     * @param mixed[int|Tinebase_Model_User] $_account   the accountd object
     * @return Tinebase_Record_RecordSet                            of subtype Tinebase_Model_Container
     */
    public function createPersonalFolder($_accountId)
    {
        $translation = Tinebase_Translation::getTranslation('Webconference');      
        $account = Tinebase_User::getInstance()->getUserById($_accountId);
        $newContainer = new Tinebase_Model_Container(array(
            'name'              => sprintf($translation->_("%s's webconference records"), $account->accountFullName),
            'type'              => Tinebase_Model_Container::TYPE_PERSONAL,
            'owner_id'          => $_accountId,
            'backend'           => 'Sql',
            'application_id'    => Tinebase_Application::getInstance()->getApplicationByName('Webconference')->getId(),
            'model'             => static::$_defaultModel
        ));
        $personalContainer = Tinebase_Container::getInstance()->addContainer($newContainer);
        $container = new Tinebase_Record_RecordSet('Tinebase_Model_Container', array($personalContainer));
        return $container;
    }

    /*
    protected function _handleEvent(Tinebase_Event_Abstract $_eventObject)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . ' (' . __LINE__ . ') handle event of type ' . get_class($_eventObject));
        
        switch(get_class($_eventObject)) {
            case 'Admin_Event_AddAccount':
                $this->createPersonalFolder($_eventObject->account);
                break;
            case 'Admin_Event_DeleteAccount':
                $this->deletePersonalFolder($_eventObject->account);
                break;
        }
    }
*/
    

    protected function _handleEvent(Tinebase_Event_Abstract $_eventObject)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . ' (' . __LINE__ . ') handle event of type ' . get_class($_eventObject));
        
        switch(get_class($_eventObject)) {
            case 'Admin_Event_AddAccount':
                //$this->createPersonalFolder($_eventObject->account);
                Tinebase_Core::getPreference('Webconference')->getValueForUser(Webconference_Preference::DEFAULTCONFERENCE, $_eventObject->account->getId());
                break;
                
            case 'Admin_Event_DeleteAccount':
                // not a good idea, as it could be the originaters container for invitations
                // we need to move all contained events first
                //$this->deletePersonalFolder($_eventObject->account);
                break;
                
            case 'Admin_Event_UpdateGroup':
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . ' (' . __LINE__ . ') updated group ' . $_eventObject->group->name);
                Tinebase_ActionQueue::getInstance()->queueAction('Webconference.onUpdateGroup', $_eventObject->group->getId());
                break;
            case 'Admin_Event_AddGroupMember':
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . ' (' . __LINE__ . ') add groupmember ' . (string) $_eventObject->userId . ' to group ' . (string) $_eventObject->groupId);
                Tinebase_ActionQueue::getInstance()->queueAction('Webconference.onUpdateGroup', $_eventObject->groupId);
                break;
                
            case 'Admin_Event_RemoveGroupMember':
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . ' (' . __LINE__ . ') removed groupmember ' . (string) $_eventObject->userId . ' from group ' . (string) $_eventObject->groupId);
                Tinebase_ActionQueue::getInstance()->queueAction('Webconference.onUpdateGroup', $_eventObject->groupId);
                break;
                
            case 'Tinebase_Event_Container_BeforeCreate':
                $this->_handleContainerBeforeCreateEvent($_eventObject);
                break;
        }
    }    
    
    /**
     * handler for Tinebase_Event_Container_BeforeCreate
     * - give owner of personal container all grants
     * 
     * @param Tinebase_Event_Container_BeforeCreate $_eventObject
     */ 
    protected function _handleContainerBeforeCreateEvent(Tinebase_Event_Container_BeforeCreate $_eventObject)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->INFO(__METHOD__ . ' (' . __LINE__ . ') about to handle Tinebase_Event_Container_BeforeCreate' );
        
        if ($_eventObject->container && 
            $_eventObject->container->type === Tinebase_Model_Container::TYPE_PERSONAL &&
            $_eventObject->container->application_id === Tinebase_Application::getInstance()->getApplicationByName('Webconference')->getId() &&
            $_eventObject->grants instanceof Tinebase_Record_RecordSet
            ) {
            // get owner from initial initial grants
            $grants = $_eventObject->grants;
            $grants->removeAll();
            
            $grants->addRecord(new Tinebase_Model_Grants(array(
                'account_id'     => $_eventObject->accountId,
                'account_type'   => Tinebase_Acl_Rights::ACCOUNT_TYPE_USER,
                Tinebase_Model_Grants::GRANT_READ      => true,
                Tinebase_Model_Grants::GRANT_ADD       => true,
                Tinebase_Model_Grants::GRANT_EDIT      => true,
                Tinebase_Model_Grants::GRANT_DELETE    => true,
                Tinebase_Model_Grants::GRANT_EXPORT    => true,
                Tinebase_Model_Grants::GRANT_SYNC      => true,
                Tinebase_Model_Grants::GRANT_ADMIN     => true,
                Tinebase_Model_Grants::GRANT_PRIVATE   => true,
            ), TRUE));
           
            if (! Tinebase_Config::getInstance()->get(Tinebase_Config::ANYONE_ACCOUNT_DISABLED)) {
                $grants->addRecord(new Tinebase_Model_Grants(array(
                    'account_id'      => '0',
                    'account_type'    => Tinebase_Acl_Rights::ACCOUNT_TYPE_ANYONE
                        ), TRUE));
            }
        }
    }
    
    /**
     * update group events
     * 
     * @param string $_groupId
     * @return void
     */    
    public function onUpdateGroup($_groupId)
    {
        Webconference_Controller_Room::getInstance()->onUpdateGroup($_groupId);
    }
}
