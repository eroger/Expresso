/*
 * Tine 2.0
 * 
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */
 
Ext.ns('Tine.Webconference');

/**
 * @namespace   Tine.Webconference
 * @class       Tine.Webconference.PerspectiveCombo
 * @extends     Ext.form.ComboBox
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 */
Tine.Webconference.PerspectiveCombo = Ext.extend(Ext.form.ComboBox, {
    
    /**
     * @cfg {Tine.Webconference.RoomEditDialog}
     */
    editDialog: null,
    
    defaultValue: 'origin',
    
    typeAhead: true,
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local',
    valueField: 'id',
    displayField: 'displayText',
    
    initComponent: function() {
        this.app = Tine.Tinebase.appMgr.get('Webconference');
        
        this.initStore();
        
        this.on('beforeselect', function(field, record, index) {
            var oldValue = this.getValue(),
                newValue = this.store.getAt(index).id;
                
            this.onPerspectiveChange.defer(50, this, [field, newValue, oldValue]);
        } , this);
        Tine.Webconference.PerspectiveCombo.superclass.initComponent.call(this);
    },
    
    getAttendeeContainerField: function() {
        if (! this.attendeeContainerField) {
            this.attendeeContainerField = new Tine.widgets.container.selectionComboBox({
                fieldLabel: this.app.i18n._('Displayed in'),
                name: 'attendeeContainer',
                recordClass: this.editDialog.recordClass,
                appName: this.app.appName,
                requiredGrant: this.editDialog.record.data.id ? ['editGrant'] : ['addGrant'],
                disabled: true
            });
        }
        return this.attendeeContainerField;
    },
    
    initStore: function() {
        this.store = new Ext.data.Store({
            fields: Tine.Webconference.Model.Attender.getFieldDefinitions().concat([{name: 'displayText'}])
        });
        
        this.originRecord = new Tine.Webconference.Model.Attender({id: 'origin', displayText: this.app.i18n._('Organizer')}, 'origin');
        
        this.editDialog.attendeeStore.on('add', this.syncStores, this);
        this.editDialog.attendeeStore.on('update', this.syncStores, this);
        this.editDialog.attendeeStore.on('remove', this.syncStores, this);
    },
    
    syncStores: function() {
        this.store.removeAll();
        this.store.add(this.originRecord);
        
        this.editDialog.attendeeStore.each(function(attendee) {
            if (attendee.get('user_id')) {
                var attendee = attendee.copy(),
                    displayName = Tine.Webconference.AttendeeGridPanel.prototype.renderAttenderName.call(Tine.Webconference.AttendeeGridPanel.prototype, attendee.get('user_id'), false, attendee);
                    
                attendee.set('displayText', displayName + ' (' + Tine.Webconference.Model.Attender.getRecordName() + ')');
                attendee.set('id', attendee.id);
                this.store.add(attendee);
            }
        }, this);
        
        // reset if perspective attendee is gone
        if (! this.store.getById(this.getValue())) {
            this.setValue(this.originRecord.id);
        }        
    },
    
    onPerspectiveChange: function(field, newValue, oldValue) {
        if (newValue != oldValue) {
            this.updatePerspective(oldValue);
            this.loadPerspective(newValue);
        }
    },
    
    /**
     * load perspective into dialog
     */
    loadPerspective: function(perspective) {
        if (! this.perspectiveIsInitialized) {
            this.syncStores();
            
            var myAttendee = Tine.Webconference.Model.Attender.getAttendeeStore.getMyAttenderRecord(this.store),
                imOrganizer = this.editDialog.record.get('organizer').id == Tine.Tinebase.registry.get('currentAccount').contact_id;
                
            perspective = imOrganizer || ! myAttendee ? 'origin' : myAttendee.id;
            this.perspectiveIsInitialized = true;
            this.setValue(perspective);
        }
        
        var perspective = perspective || this.getValue(),
            perspectiveRecord = this.store.getById(perspective),
            isOriginPerspective = perspective == 'origin',
            attendeeContainerField = this.getAttendeeContainerField(),
            
            containerField = this.editDialog.getForm().findField('container_id');
            
        attendeeContainerField.setValue(isOriginPerspective ? this.editDialog.record.get('container_id') : perspectiveRecord.get('displaycontainer_id'));
        attendeeContainerField.startPath = isOriginPerspective ? '/' : ('/personal/' + perspectiveRecord.getUserAccountId());
        attendeeContainerField.setVisible(!isOriginPerspective);
        containerField.setVisible(isOriginPerspective);
        
        // (de)activate fields 
        var fs = this.editDialog.record.fields;
        fs.each(function(f){
            var field = this.editDialog.getForm().findField(f.name);
            if(field){
                    field.setDisabled(! isOriginPerspective || (field.requiredGrant && ! this.editDialog.record.get(field.requiredGrant)));
            }
        }, this);
        
        attendeeContainerField.setDisabled(isOriginPerspective || !perspectiveRecord.get('displaycontainer_id').account_grants[containerField.requiredGrant]);
        this.editDialog.attendeeGridPanel.setDisabled(! isOriginPerspective || ! this.editDialog.record.get('editGrant'));
    },
    
    /**
     * update perspective from dialog
     */
    updatePerspective: function(perspective) {
        var perspective = perspective || this.getValue(),
            perspectiveRecord = this.editDialog.attendeeStore.getById(perspective),
            isOriginPerspective = perspective == 'origin',
            attendeeContainerField = this.getAttendeeContainerField(),
            containerField = this.editDialog.getForm().findField('container_id');
        if (! isOriginPerspective) {
            perspectiveRecord.set('displaycontainer_id', attendeeContainerField.selectedContainer);
        }
    }
});