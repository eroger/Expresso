<?php

/**
 * Tine 2.0
 *
 * @package     Messenger
 * @license     http://www.gnu.org/licenses/agpl.html AGPL3
 * @author      Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 */

class Messenger_Setup_Update_Release1 extends Setup_Update_Abstract
{
    /**
     * update to 1.1 - Change config key from Tinebase to Messenger
     * @return void
     */
    public function update_0()
    {
	
        
	$config = Tinebase_Config::getInstance()->get('messenger');
	if(!is_null($config)){
	    // create status config
	    $cb = new Tinebase_Backend_Sql(array(
		'modelName' => 'Tinebase_Model_Config', 
		'tableName' => 'config',
	    ));
	    
	    $config = $config->toArray();
	    $newConfig = 
		array(
		    "domain" => $config['messenger']['domain'] ? $config['messenger']['domain'] : '',
		    "resource" => $config['messenger']['resource'] ? $config['messenger']['resource'] : '',
		    "format" => $config['messenger']['format'] ? $config['messenger']['format'] : '',
		    "rtmfpServerUrl" => $config['messenger']['rtmfpServerUrl'] ? $config['messenger']['rtmfpServerUrl'] : '',
		    "tempFiles" => $config['messenger']['tempFiles'] ? $config['messenger']['tempFiles'] : ''
		);
	    
	     $cb->create(new Tinebase_Model_Config(array(
		'application_id'    => Tinebase_Application::getInstance()->getApplicationByName('Messenger')->getId(),
		'name'              => 'messenger',
		'value'             => json_encode($newConfig),
	    )));
	}
	
        $this->setApplicationVersion('Messenger', '1.1');
    }
}