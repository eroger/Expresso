<?php
/**
 * PreferenceRecord controller for Messenger application
 * 
 * @package     Messenger
 * @subpackage  Controller
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 *
 */

/**
 * PreferenceRecord controller class for Messenger application
 * 
 * @package     Messenger
 * @subpackage  Controller
 */
class Messenger_Controller_PreferenceRecord extends Tinebase_Controller_Record_Abstract
{
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */
    private function __construct() {
        $this->_applicationName = 'Messenger';
        $this->_backend = new Messenger_Backend_PreferenceRecord();
        $this->_modelName = 'Messenger_Model_PreferenceRecord';
        $this->_doContainerACLChecks = false;
    }
    
    /**
     * holds the instance of the singleton
     *
     * @var Messenger_Controller_PreferenceRecord
     */
    private static $_instance = NULL;
    
    /**
     * the singleton pattern
     *
     * @return Messenger_Controller_PreferenceRecord
     */
    public static function getInstance() 
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Messenger_Controller_PreferenceRecord();
        }
        
        return self::$_instance;
    }        

}
