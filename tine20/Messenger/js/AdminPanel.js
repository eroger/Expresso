/*
 * Tine 2.0
 * 
 * @package     Messenger
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>, Jeferson Miranda <jeferson.miranda@serpro.gov.br>
 *
 */

Ext.namespace('Tine.Messenger');


/**
 * admin settings panel
 * 
 * @namespace   Tine.Messenger
 * @class       Tine.Messenger.AdminPanel
 * @extends     Tine.widgets.dialog.EditDialog
 * 
 * <p>Messenger Admin Panel</p>
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>, Jeferson Miranda <jeferson.miranda@serpro.gov.br>
 
 * 
 * @param       {Object} config
 * @constructor
 * Create a new Tine.Messenger.AdminPanel
 */
Tine.Messenger.AdminPanel = Ext.extend(Tine.widgets.dialog.EditDialog, {
    
    appName: 'Messenger',
    recordClass: Tine.Messenger.Model.Settings,
    recordProxy: Tine.Messenger.settingsBackend,
    evalGrants: false,

    onRecordLoad: function() {
	Tine.Messenger.AdminPanel.superclass.onRecordLoad.call(this);
	this.window.setTitle(String.format(_('Change settings for application {0}'), this.appName));
    },
    
    updateToolbars: function() {
    },

    /**
     * is called from onApplyChanges
     * @param {Boolean} closeWindow
     */
    doApplyChanges: function(closeWindow) {
        // we need to sync record before validating to let (sub) panels have 
        // current data of other panels
        this.onRecordUpdate();
        
        // quit copy mode
        this.copyRecord = false;
        
        if (this.isValid()) {
	    Ext.Ajax.request({
			params: {
			    method: 'Messenger.saveSettings',
			    _data: this.record.data
			},
			scope: this,
			success: function(_result, _request){
			    //this.record = Ext.util.JSON.decode(_result.responseText);
			    
			    Tine.Messenger.registry.replace('domain', this.record.data.domain);
			    Tine.Messenger.registry.replace('resource', this.record.data.resource);
			    Tine.Messenger.registry.replace('format', this.record.data.format);
			    Tine.Messenger.registry.replace('rtmfpServerUrl', this.record.data.rtmfpServerUrl);
			    Tine.Messenger.registry.replace('tempFiles', this.record.data.tempFiles);
			    
			    if (! Ext.isFunction(this.window.cascade)) {
			    
				// update form with this new data
				// NOTE: We update the form also when window should be closed,
				//       cause sometimes security restrictions might prevent
				//       closing of native windows
				this.onRecordLoad();
			    }
			    var ticketFn = this.onAfterApplyChanges.deferByTickets(this, [closeWindow]),
				wrapTicket = ticketFn();

			    this.fireEvent('update', Ext.util.JSON.encode(this.record.data), this.mode, this, ticketFn);
			    wrapTicket();
			},
			failure: this.onRequestFailed,
			timeout: 300000 // 5 minutes

		    });
	    
            
        } else {
            this.saving = false;
            this.loadMask.hide();
            Ext.MessageBox.alert(_('Errors'), this.getValidationErrorMessage());
        }
    },
    
    getFormItems: function() {
        return {
            layout: 'form',
            border: false,
            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'fieldset',
                    autoHeight: 'auto',
                    defaults: {width: 300},
                    title: this.app.i18n._('Messenger Service'),
                    id: 'setup-messenger-service-group',
                    items: [
                        {
                            allowBlank: false,
                            name: 'domain',
                            //dataIndex: 'domain',
                            fieldLabel: this.app.i18n._('Domain'),
                            xtype: 'textfield'
                        },
                        {
                            allowBlank: false,
                            name: 'resource',
                            //dataIndex: 'resource',
                            fieldLabel: this.app.i18n._('Resource'),
                            xtype: 'textfield'
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    autoHeight: 'auto',
                    defaults: {width: 300},
                    title: this.app.i18n._('Messenger Users'),
                    id: 'setup-messenger-users-group',
                    items: [
                        {
                            name: 'format',
                            //dataIndex: 'format',
                            fieldLabel: this.app.i18n._('Format'),
                            xtype: 'combo',
                            editable: false,
                            store: [
                                ['email', this.app.i18n._('E-mail Name')],
                                ['login', this.app.i18n._('Login')],
                                ['custom', this.app.i18n._('Custom Name')]
                            ]     
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    autoHeight: 'auto',
                    defaults: {width: 300},
                    title: this.app.i18n._('Messenger Videochat'),
                    id: 'setup-messenger-videochat',
                    items: [
                        {
                            allowBlank: true,
                            name: 'rtmfpServerUrl',
                            //dataIndex: 'rtmfpServerUrl',
                            fieldLabel: this.app.i18n._('Videochat Server address'),
                            xtype: 'textfield'
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    autoHeight: 'auto',
                    defaults: {width: 300},
                    title: this.app.i18n._('Temporary files'),
                    id: 'setup-messenger-tmp-files',
                    items: [
                        {
                            allowBlank: true,
                            name: 'tempFiles',
                            //dataIndex: 'tempFiles',
                            fieldLabel: this.app.i18n._('Temporary Uploaded Files Path'),
                            xtype: 'textfield'
                        }
                    ]
                }
            ]
        };
    }

});

/**
 * Messenger admin settings popup
 * 
 * @param   {Object} config
 * @return  {Ext.ux.Window}
 */
Tine.Messenger.AdminPanel.openWindow = function (config) {
    var window = Tine.WindowFactory.getWindow({
        width: 600,
        height: 400,
        id: 'messenger-admin-panel',
	name: 'messenger-admin-panel',
        contentPanelConstructor: 'Tine.Messenger.AdminPanel',
        contentPanelConstructorConfig: config
    });
    return window;
};