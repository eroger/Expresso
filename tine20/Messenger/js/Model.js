/*
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2010 Metaways Infosystems GmbH (http://www.metaways.de)
 */
Ext.ns('Tine.Messenger', 'Tine.Messenger.Model');

Tine.Messenger.Model.Settings = Tine.Tinebase.data.Record.create([
        {name: 'id'},
        {name: 'domain'},
        {name: 'resource'},
        {name: 'format'},
        {name: 'rtmfpServerUrl'},
        {name: 'tempFiles'},
    ], {
    appName: 'Messenger',
    modelName: 'Settings',
    idProperty: 'id',
    titleProperty: 'title',
    recordName: 'Settings',
    recordsName: 'Settingss',
    containerName: 'Settings',
    containersName: 'Settings',
    getTitle: function() {
        return this.recordName;
    }
});

Tine.Messenger.Model.AddBuddy = Ext.data.Record.create([
    {name: 'id'},
    {name: 'full_name'},
    {name: 'email'},
    {name: 'login_name'}
]);

Tine.Messenger.settingsBackend = new Tine.Tinebase.data.RecordProxy({
    appName: 'Messenger',
    modelName: 'Settings',
    recordClass: Tine.Messenger.Model.Settings
});