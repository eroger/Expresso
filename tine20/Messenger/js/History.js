Ext.ns('Tine.Messenger');

Tine.Messenger.HistoryWindow = function (config) {
    Tine.Messenger.HistoryWindow.superclass.constructor.call(this,
        Ext.applyIf(config || {}, {
            id: 'messenger-history-window',
            title: Tine.Tinebase.appMgr.get('Messenger').i18n._('History Chat with') + ' ' + Tine.Messenger.RosterHandler.getContactElement(config.contact).text,
            layout: 'border',
            labelSeparator: '',
            width: 320,
            minWidth: 260,
            height: 240,
            closeAction: 'close',
            buttons: [
                {
                    id: 'messenger-history-download',
                    text: Tine.Tinebase.appMgr.get('Messenger').i18n._('Download'),
                    hidden: true,
                    handler: function (button) {
                        Tine.Messenger.HistoryDownload(config.jid, config.contact, button.date);
                    }
                }
            ],
            items: [
                {
                    xtype: 'panel',
                    id: 'messenger-history-dates',
                    autoScroll: true,
                    width: 100,
                    region: 'west',
                    border: false,
                    frame: false,
                    bodyStyle: 'background: transparent',
                    columns: [ { header: 'Chat Date' } ],
                    minHeight: 240,
                    items: [
                        {
                            xtype: 'panel',
                            id: 'history-loading',
                            border: false,
                            frame: false,
                            bodyStyle: 'background: transparent',
                            html: '<img src="Messenger/res/loading_animation_liferay.gif" />'
                        }
                    ],
                    listeners: {
                        render: function (panel) {
                            Ext.Ajax.request({
                                params: {
                                    method: 'Messenger.listHistory',
                                    jid: config.jid,
                                    contact: config.contact
                                },
                                success: function (res) {
                                    var result = JSON.parse(res.responseText),
                                        dates = result.content;

                                    Tine.Messenger.HistoryList(dates, panel, config.jid, config.contact);
                                },
                                failure: function (err, details) {
                                    console.log(err);
                                    console.log(details);
                                }
                            });
                        }
                    }
                },
                {
                    xtype: 'panel',
                    id: 'messenger-history-chat',
                    autoScroll: true,
                    region: 'center'
                }
            ]
        })
    );
};
Ext.extend(Tine.Messenger.HistoryWindow, Ext.Window);

Tine.Messenger.HistoryList = function (dates, panel, jid, contact) {
    Ext.getCmp('history-loading').hide();
                                    
    for (var i = 0; i < dates.length; i++) {
        var date_panel = new Ext.Panel({
            xtype: 'panel',
            id: dates[i],
            border: false,
            frame: false,
            bodyStyle: 'background: transparent',
            cls: 'history-list-button',
            html: dates[i].replace(/(\d{4})-(\d{2})-(\d{2})/, '$3/$2/$1'),
            listeners: {
                render: function (p) {
                    p.body.on('click', function () {
                        // Show download button
                        var downloadBT = Ext.getCmp('messenger-history-download');
                        downloadBT.date = p.id;
                        downloadBT.show();
                        
                        // Stylize the button clicked
                        var buttons = Ext.query('.history-list-button');
                        for (var i = 0; i < buttons.length; i++) {
                            Ext.getCmp(buttons[i].id).removeClass('history-list-button-chosen');
                        }
                        p.addClass('history-list-button-chosen');
                        
                        // Clear chat history panel and show loading image
                        Tine.Messenger.HistoryClearAndLoad();
                        
                        // Request the history file
                        Tine.Messenger.HistoryDisplay(Ext.getCmp('messenger-history-chat'), jid, contact, p.id);
                    });
                }
            }
        });

        panel.add(date_panel);
    }
    panel.doLayout();
    if (dates.length > 0) {
        Tine.Messenger.HistoryDisplay(Ext.getCmp('messenger-history-chat'), jid, contact, dates[0]);
    }
};

Tine.Messenger.HistoryClearAndLoad = function () {
    var history_chat = Ext.getCmp('messenger-history-chat');
    history_chat.removeAll();
    history_chat.add({
        xtype: 'panel',
        border: false,
        html: '<img style="margin: 20px;" src="Messenger/res/loading_animation_liferay.gif" />'
    });
    history_chat.doLayout();
}

Tine.Messenger.HistoryDisplay = function (panel, jid, contact, date) {
    var nick = Tine.Messenger.RosterHandler.getContactElement(contact).text;
    Ext.Ajax.request({
        params: {
            method: 'Messenger.getHtmlHistory',
            jid: jid,
            contact: contact,
            nick: nick,
            date: date
        },
        success: function (result) {
            var response = JSON.parse(result.responseText);
            var message = response.content;

            panel.removeAll();
            panel.add({
                xtype: 'panel',
                id: 'message-panel',
                html: message,
                border: false
            });
            panel.doLayout();
        },
        failure: function (err, details) {
            console.log(err);
            console.log(details);
        }
    });
};

Tine.Messenger.HistoryDownload = function (jid, contact, date) {
    
    var msgPanel = Ext.getCmp('message-panel');
    var msg = (msgPanel != undefined) ? msgPanel.body.dom.innerHTML : '';
    var nick = Tine.Messenger.RosterHandler.getContactElement(contact).text;
    var downloader = new Ext.ux.file.Download({
        params: {
            method: 'Messenger.downloadHistory',
            jid: jid,
            contact: contact,
            nick: nick,
            date: date
        }
    });
    downloader.start();
};