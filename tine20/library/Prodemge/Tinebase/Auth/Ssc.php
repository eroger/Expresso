<?php
/**
* Tine 2.0
*
* @package     Tinebase
* @subpackage  Auth
* @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
* @copyright   Copyright (c) 2015 Prodemge (http://www.prodemge.gov.br)
* @author      Victor Pinheiro <victor.pinheiro@prodemge.gov.br>
*/

/**
* LDAP authentication backend
*
* @package     Tinebase
* @subpackage  Auth
*/

 class Prodemge_Tinebase_Auth_Ssc implements Tinebase_Auth_Interface
 {
         
    /*********************************************************************
    *   ATTRIBUTES
    **********************************************************************/

    /**
     * The username of the account being authenticated.
     *
     * @var string
     */
    protected $_username = null;

    /**
     * The password of the account being authenticated.
     *
     * @var string
     */
    protected $_password = null;


    /*********************************************************************
    *   METHODS
    **********************************************************************/

    /**
     * Constructor
     *
     * @param  array  $options  An array of arrays of Zend_Ldap options
     * @return void
     */
    public function __construct(array $options = array(),  $username = null, $password = null)
    {
        $this->_username = $username;
        $this->_password = $password; 
    }

    /**
     * set loginname
     *
     * @param string $_identity
     * @return Tinebase_Auth_Ssc
     */
    public function setIdentity($_identity)
    {
        $this->_username = (string) $_identity;
        return $this;
    }

    /**
     * set password
     *
     * @param string $_credential
     * @return Tinebase_Auth_Ssc
     */
    public function setCredential($_credential)
    {
        $this->_password = (string) $_credential;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public static function getType()
    {
        return self::TYPE;
    }

    /**
     * 
     * @return array
     */
    public static function getBackendConfigurationDefaults()
    {
        return array(
                'host' => '',
                'username' => '',
                'password' => '',
                'bindRequiresDn' => true,
                'baseDn' => '',
                'accountFilterFormat' => NULL,
                'accountCanonicalForm' => '2',
                'accountDomainName' => '',
                'accountDomainNameShort' => '',
                'tryUsernameSplit' => '1'
        );
    }

    /**
     * 
     * @param array $_options
     * @throws Tinebase_Exception_Backend_Ldap
     * @return Tinebase_Ldap
     */
    public static function getBackendConnection(array $_options = array())
    {
        try {
            $ldap = new Tinebase_Ldap($_options);
            $ldap->bind();
        } catch (Zend_Ldap_Exception $zle) {
            throw new Tinebase_Exception_Backend_Ldap("Cannot connect to ldap: ".$zle->getMessage());
        }
        return $ldap;
    }

    /**
     * 
     * @param mixed $_authBackend (expects Tinebase_Ldap)
     * @return boolean
     */
    public static function isValid($_authBackend)
    {
        return ($_authBackend instanceof Tinebase_Ldap);
    }

    /**
     * close LDAP connection
     */
    public function closeConnection()
    {
        $ldap = $this->getLdap();
        $ldap->disconnect();
    }

    /**
     * Authenticate the user
     *
     * @throws Zend_Auth_Adapter_Exception
     * @return Zend_Auth_Result
     */
    public function authenticate()
    {
        $username = $this->_username;

        if( $username ) {
            $code = Zend_Auth_Result::SUCCESS;
            $messages[0] = 'Authentication successful.';
        } else {
            $code = Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
            $messages[0] = 'Authentication fail!';
        }

        return new Zend_Auth_Result($code, $username, $messages);
    }
}
