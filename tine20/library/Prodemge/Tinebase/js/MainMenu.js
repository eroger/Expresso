Ext.onReady(function(){
	
	Ext.override(Tine.Tinebase.MainMenu, {

		/**
	     * logout user & redirect SSCloginLogo: 'images/tine_logo.png'
	     */
	    _doLogout : function() {  
			Ext.MessageBox.wait(_('Logging you out...'),
					_('Please wait!'));
			var SSC = {}; 
			Ext.Ajax.request({
						params : {
							url : 'library/Prodemge/Tinebase/Frontend/',
							method : 'Tinebase.getUrlLogoutSSC',
						},
						callback : function(options, Success, response) {
							var responseDecode = Ext.decode(response.responseText); 
							if (Success && responseDecode.success) {
								SSC.urlLogout = responseDecode.urlLogout; 
							} else {
								SSC.urlLogout = "";   
							}			
							Ext.Ajax.request({
										params : {
											url : 'Tinebase/Frontend/',
											method : 'Tinebase.logout'
										},
										callback : function(options, Success, response) { 
											var redirect = (Tine.Tinebase.registry.get('redirectUrl'));  
											if (redirect && redirect != '') {
												window.location = Tine.Tinebase.registry.get('redirectUrl');
											} else if (SSC.urlLogout) {
												window.location = SSC.urlLogout;
											} else {
												window.location.reload();
											}
										}
									});
						}
					});
		}

	});

});
