<?php
/**
 * Tine 2.0
 *
 * @package     Prodemge
 * @subpackage  Tinebase 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Victor Pinheiro <victor.prodemge.gov.br>
 *
 */

/**
 *
 * @package     Prodemge
 * @subpackage  Tinebase 
 */
class Prodemge_Tinebase_SscHelper
{
    /**
     * Monta a url de chamada do Sistema Segurança Corporativo com os parametros exigidos
     * @param string $operacao:
     * @param string $type: 'full' - completa, 'base' - apenas a url base, 'params' - apenas os parametros
     * @param string $refererFull: se retorna a url completa ou apenas o contexto
     * @return string da url
     */
    public function getUrlSSC( $operacao = 'login', $type = 'full') {
        $referer = "";

        // url do IDP do SSC cadastrado em config.inc.php
        $urlIDP = Tinebase_Core::getConfig()->global->plugins->ssc->hostIDP;

        // url do IDP do SSC cadastrado em config.inc.php
        $referer = Tinebase_Core::getConfig()->global->plugins->ssc->referer;
        // encode base64 exigido pelo SSC
        $referer = base64_encode( $referer );

        // xml do SAMLRequest codificado na base64
        switch ( $operacao ) {
           case "login":
                $xmlBase64 = $this->getXmlLoginBase64();
                break;
           case "logout":
               $xmlBase64 = $this->getXmlLogoutBase64();
               break;
           default:
               break;
        }

        // formata a url com a url do idp mais os parametros exigigos pelo SSC
        switch ( $type ) {
            // a url completa
            case "full":
                $urlSSC  = $urlIDP;
                $urlSSC .= "?Referer=". $referer;
                $urlSSC .= "&SAMLRequest=". $xmlBase64;
                break;
            // penas a url base
            case 'base':
                $urlSSC = $urlIDP;
                break;
            // apenas os parametros formato GET
            case 'params':
                $urlSSC = "Referer=". $referer;
                $urlSSC .= "&SAMLRequest=". $xmlBase64;
                break;
            // a url completa
            default:
                $urlSSC  = $urlIDP;
                $urlSSC .= "?Referer=". $referer;
                $urlSSC .= "&SAMLRequest=". $xmlBase64;
                break;
        }

        return $urlSSC;
    }

    /**
     * Monta o XML esperado no Sistema Seguran�a Corporativo (SSC) ao solicitar o login
     * @return string do xml na base64
     */
    public function getXmlLoginBase64() {

        // parametros necessários para o SAMLRequest
        $assertion = Tinebase_Core::getConfig()->global->plugins->ssc->appUrl;
        $destination = Tinebase_Core::getConfig()->global->plugins->ssc->hostIDP;
        $id = 'ID_'. uniqid();
        $issuer = Tinebase_Core::getConfig()->global->plugins->ssc->appUrl;
        $datetime = date('Y-m-d\TH:i:s');

        $xml = sprintf('<samlp:AuthnRequest xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" xmlns="urn:oasis:names:tc:SAML:2.0:assertion" AssertionConsumerServiceURL="%s" Destination="%s" ID="%s" IssueInstant="%s.724Z" ProtocolBinding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Version="2.0"><saml:Issuer xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">%s</saml:Issuer><samlp:NameIDPolicy AllowCreate="true" Format="urn:oasis:names:tc:SAML:2.0:nameid-format:transient"/></samlp:AuthnRequest>',
                $assertion, $destination, $id, $datetime, $issuer);

        return base64_encode($xml);
    }


    /**
     * Monta o XML esperado no Sistema Segurança Corporativo (SSC) ao solicitar o logout
     * @return string do xml na base64
     */
    public function getXmlLogoutBase64() {

        // parametros necessários para o SAMLRequest
        $username = Tinebase_Core::getUser();
        $id = 'ID_'. session_id();
        $issuer = Tinebase_Core::getConfig()->global->plugins->ssc->appUrl;
        $datetime = date('Y-m-d\TH:i:s');
        $sessionIndex = 0;

        $xml = sprintf('<samlp:LogoutRequest xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" xmlns="urn:oasis:names:tc:SAML:2.0:assertion" ID="%s" IssueInstant="%s.724Z" ProtocolBinding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Destination="%s" Version="2.0"> <saml:Issuer xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">%s</saml:Issuer> <saml:NameID xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">%s</saml:NameID></samlp:LogoutRequest>',
                $id, $datetime, $issuer, $issuer, $username);

        return base64_encode($xml);
    }

    /**
     * @return string
     */
    public function getRefererBase64() {
        return base64_encode(Tinebase_Core::getConfig()->global->plugins->ssc->referer);        
    }

    /**
     * @return string
     */
    public function getHostIdpBase64() {
        return Tinebase_Core::getConfig()->global->plugins->ssc->hostIDP;
    }          

    /**
     * @return string
     */
    public function getUrlLogout(){
        return Tinebase_Core::getConfig()->global->plugins->ssc->urlLogout;
    }
}