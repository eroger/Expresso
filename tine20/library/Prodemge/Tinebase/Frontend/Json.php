<?php
/**
 * Tine 2.0
 *
 * @package     Prodemge
 * @subpackage  Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Prodemge (http://www.prodemge.gov.br)
 * @author      Emerson Roger <emerson.bustamante@prodemge.gov.br>
 *
 */

/**
 * Json interface to Tinebase
 *
 * @package     Prodemge
 * @subpackage  Tinebase
 */
class Prodemge_Tinebase_Frontend_Json extends Tinebase_Frontend_Json
{
    
     /**
     * método para recuperar a url para deslogar do serviço do Sistema de Segurança Corporativo SSC
     */
    public function getUrlLogoutSSC()
    {   

        if ( Tinebase_Core::getConfig()->global->plugins->ssc->active === true ) {

            $ssc = new Prodemge_Tinebase_SscHelper();
            $urlLogout = $ssc->getUrlLogout();

            $result = array(
                'success'=> true,
                'urlLogout' => $urlLogout
            );

        } else {   
            $result = array(
                'success'=> false,
                'urlLogout' => ''
            );
        }

        return $result;
    }
    
     /**
     * metodo para recuperar a url 
     */
    public function getUrlHostIdp()
    {   

        if ( Tinebase_Core::getConfig()->global->plugins->ssc->active === true ) { 

            $ssc = new Prodemge_Tinebase_SscHelper();
            $urlHostIdp = $ssc->getHostIdpBase64();

            $result = array(
                'success'=> true,
                'urlHostIdp' => $urlHostIdp
            );

        } else {
            $result = array(
                'success'=> false,
                'urlHostIdp' => ''
            );
        }

        return $result;
    }
}
