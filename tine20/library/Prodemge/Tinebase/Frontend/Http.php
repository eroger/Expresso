<?php
/**
 * Tine 2.0
 *
 * @package     Prodemge
 * @subpackage  Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Prodemge (http://www.prodemge.gov.br)
 * @author      Emerson Roger <emerson.bustamante@prodemge.gov.br>
 *
 */

/**
 * Customize HTTP Server class
 *
 * @package     Prodemge
 * @subpackage  Tinebase
 */

class Prodemge_Tinebase_Frontend_Http extends Tinebase_Frontend_Http
{
    /**
     * ativa a sessão do usuário no Expresso com o retorno da autenticação via Portal de Segurança (SSC)
     * @todo: verificar quando o login não ocorrer o q fazer ou quando o username não for encontrado
     */
    public function validaLoginSSC()
    {
        Tinebase_Core::startCoreSession('tinebase');

        // decodifica o xml de resposta do base64 (tambem limpa os tags com o texto "saml:" e "samlp:")
        $xmlResponse = base64_decode( $_REQUEST['SAMLResponse'] );
        $xmlResponse = str_replace('samlp:', '', $xmlResponse);
        $xmlResponse = str_replace('saml:', '', $xmlResponse);

        // ler xml com Xml nativo
        $xmlResponse = simplexml_load_string( $xmlResponse );

        // obter os dados retornados no SAMLResponse
        $username = $xmlResponse->Assertion->Subject->NameID;
        $autenticou = $xmlResponse->Assertion->AttributeStatement->Attribute->AttributeValue;
    
        // valida se a tag
        if ( in_array(strtolower($autenticou), array('autenticado', 'true', '1' ) ) ) {
            // tenta ativar a sessão do usuario (sem senha pq foi autenticado via SSC)
            $success = (Tinebase_Controller::getInstance()->login($username, '', Tinebase_Core::get(Tinebase_Core::REQUEST), 'TineSSC') === TRUE);
        } else {
            $success = FALSE;
        }

        // ativa o sessão do usuário
        if ($success === TRUE) {
			// save in cookie (expires in 2 weeks)
			setcookie ( 'TINE20LASTUSERID', $username, time () + 60 * 60 * 24 * 14 );

            if (Tinebase_Core::isRegistered(Tinebase_Core::USERCREDENTIALCACHE)) {
                Tinebase_Auth_CredentialCache::getInstance()->getCacheAdapter()->setCache(Tinebase_Core::get(Tinebase_Core::USERCREDENTIALCACHE));
            } else {
                Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' Something went wrong with the CredentialCache / no CC registered.');
            }
        }

        // authentication failed
        if ($success !== TRUE) {
            $_SESSION = array();

            // redirect back to loginurl if needed
            $defaultUrl = (array_key_exists('HTTP_REFERER', $_SERVER)) ? $_SERVER['HTTP_REFERER'] : '';
            if (! empty($defaultUrl)) {
                header('Location: ' . $defaultUrl);
            }
            header('Location: logout_usuario_nao_cadastrado.php');
            return;
        }
        
        $this->mainScreen();
    }
}