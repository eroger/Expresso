<?php
    $time_start = microtime(true);

    require_once '../bootstrap.php';

    include_once 'Prodemge/Tinebase/SscHelper.php';

    $ssc = new Prodemge_Tinebase_SscHelper();

    $hostIdpBase64 = $ssc->getHostIdpBase64();
    $refererBase64 = $ssc->getRefererBase64();
    $xmlLogoutBase64 = $ssc->getXmlLogoutBase64();
    $html = <<<HTML
<HTML>
    <script language="javascript">
        function mensagem() {
            alert("Usuário não cadastrado ou bloqueado no ExpressoMG, gentileza informar o Service Desk Prodemge utilizando um dos canais abaixo: \nTelefone: (31) 3339-1600 \nE-mail: atendimento@prodemge.gov.br    ");
            document.forms[0].submit();
        }
    </script>
    <BODY Onload="mensagem();">
        <FORM METHOD="POST" ACTION="$hostIdpBase64">
            <INPUT TYPE="HIDDEN" NAME="Referer" VALUE="$refererBase64"/>
            <INPUT TYPE="HIDDEN" NAME="SAMLRequest" VALUE="$xmlLogoutBase64"/>
        </FORM>
    </BODY>
</HTML>
HTML;
    echo $html;