<?php
    $time_start = microtime(true);

    require_once '../../bootstrap.php';

    include_once 'Tinebase/SscHelper.php';

    $ssc = new Prodemge_Tinebase_SscHelper();

    $hostIdpBase64 = $ssc->getHostIdpBase64();  
    $refererBase64 = $ssc->getRefererBase64();
    $xmlLogoutBase64 = $ssc->getXmlLogoutBase64();  
    
    $html = '<HTML><BODY Onload="document.forms[0].submit()">';
    $html .= '<FORM METHOD="POST" ACTION="'.$hostIdpBase64.'">';
    $html .= '<INPUT TYPE="HIDDEN" NAME="Referer" VALUE="'.$refererBase64.'"/>';
    $html .= '<INPUT TYPE="HIDDEN" NAME="SAMLRequest" VALUE="'.$xmlLogoutBase64.'"/>';
    $html .= '</FORM></BODY></HTML>'; 
    echo $html;
    
    