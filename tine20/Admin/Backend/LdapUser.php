<?php
/**
 * Tine 2.0
 *
 * @package     Admin
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2009 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2015 SERPRO (https://www.serpro.gov.br)
 * @author      Philipp Schuele <p.schuele@metaways.de>
 * @author      Bruno Vieira Costa <bruno.vieira-costa@serpro.gov.br>
 */

/**
 * Admin User ldap backend
 *
 * @package     Admin
 * @subpackage  User
 */
class Admin_Backend_LdapUser extends Tinebase_User_Ldap
{
    /**
     * delete an user in ldap backend
     *
     * @param int $_userId
     */
    public function deleteUserInSyncBackend($_userId)
    {
        if (!$this->_useLdapMaster($ldap)) {
            if ($this->_isReadOnlyBackend) {
                return;
            }
        }

        try {
            $metaData = $this->_getMetaData($_userId);
            // user does not exist in ldap anymore
            if (!empty($metaData['dn'])) {
                $ldap->update($metaData['dn'], array('Phpgwaccountstatus' => array(),'Phpgwaccounttype' => array(),'accountstatus' => array(),));
            }
        } catch (Tinebase_Exception_NotFound $tenf) {
            Tinebase_Core::getLogger()->crit(__METHOD__ . '::' . __LINE__ . ' user not found in sync backend: ' . $_userId);
        }
    }
}