<?php
/**
 * Tine 2.0
 *
 * @package     Admin
 * @subpackage  Controller
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Fernando Wendt <fernando-alberto.wendt@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014 SERPRO (https://www.serpro.gov.br)
 *
 */

/**
 * MailList Controller for Admin application
 *
 * @package     Admin
 * @subpackage  Controller
 */
class Admin_Controller_MailList extends Tinebase_Controller_Abstract implements Tinebase_Controller_SearchInterface
{
    /**
     * holds the instance of the singleton
     *
     * @var Admin_Controller_MailList
     */
    private static $_instance = NULL;

    /**
     * the constructor
     *
     * don't use the constructor. use the singleton
     */
    private function __construct()
    {
        $this->_applicationName = 'Admin';
    }

    /**
     * don't clone. Use the singleton.
     *
     */
    private function __clone()
    {
    }

    /**
     * the singleton pattern
     *
     * @return Admin_Controller_MailList
     */
    public static function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Admin_Controller_MailList;
        }
        return self::$_instance;
    }

    /**
     *Check the availability of the email on the user backend
     *
     * @param array $_arrEmails
     * @return array
     */
    public function checkEmail(array $_arrEmails)
    {
        $this->checkRight('MANAGE_ACCOUNTS');
        $return = Admin_MailList::getInstance()->checkEmail($_arrEmails);
        return $return;
    }

    /**
     * get list of mail list entries
     *
     * @param Tinebase_Model_Filter_FilterGroup|optional $_filter
     * @param Tinebase_Model_Pagination|optional $_pagination
     * @param string $_columns
     * @param boolean $_doCache
     * @return Tinebase_Record_RecordSet $result
     * @throws Exception $ex
     */
    public function search(Tinebase_Model_Filter_FilterGroup $_filter = NULL, Tinebase_Record_Interface $_pagination = NULL, $_columns = '*', $_doCache = TRUE)
    {
        $this->checkRight('MANAGE_LDAP_MAILLISTS');

        if ($_filter === NULL) {
            $_filter = new Tinebase_Model_Filter_FilterGroup();
        }

        try
        {
            $result = Admin_MailList::getInstance()->search($_filter, $_pagination, $_columns, $_doCache);
            return $result;
        }
        catch(Exception $ex)
        {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  MailList fails to perform a search operation.\n" . print_r($ex, true));
            throw $ex;
        }
    }

    /**
     * returns the total number of mail lists
     *
     * @param Tinebase_Model_Filter_FilterGroup $_filter
     * @param Tinebase_Record_Interface $_pagination
     * @return integer $count
     */
    public function searchCount(Tinebase_Model_Filter_FilterGroup $_filter = NULL, Tinebase_Record_Interface $_pagination = NULL)
    {
        $this->checkRight('MANAGE_LDAP_MAILLISTS');
        $count =  Admin_MailList::getInstance()->searchCount($_filter);
        return($count);
    }

    /**
     * fetch one mail list identified by maillistId
     *
     * @param int $_maillistId
     * @return Tinebase_Model_Group $mailList
     */
    public function get($_maillistId)
    {
        try
        {
            $this->checkRight('MANAGE_LDAP_MAILLISTS');
            $mailList = Admin_MailList::getInstance()->getById($_maillistId);
            return $mailList;
        }
        catch (Exception $ex)
        {
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  MailList fails to get ID for deletion.\n" . print_r($ex, true));
            return(false);
        }
    }

    /**
     * delete one/multiple mail lists entries
     *
     * @param array $_maillistIds
     * @return array $del_action
     * @throws Tinebase_Exception_Backend
     */
    public function delete($_maillistIds)
    {
        try
        {
            $this->checkRight('MANAGE_LDAP_MAILLISTS');
            Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  MailList will start to perform a delete operation.");

            if (empty($_maillistIds)) {
                throw new Tinebase_Exception_UnexpectedValue('Maillist identificator is empty for deletion.');
            }

            $eventBefore = new Admin_Event_BeforeDeleteMailList();
            $eventBefore->mailListIds = $_maillistIds;
            Tinebase_Event::fireEvent($eventBefore);

            $listIds = array();

            foreach ($_maillistIds as $mailListId) {
                $mailList[] = $this->get($mailListId);
                if (!empty($mailList->uidnumber))
                {
                    $listIds[] = $mailList->uidnumber;
                }
            }

            if (!empty($listIds))
            {
                $listBackend = new Admin_MailList_Ldap();
                $listBackend->delete($listIds);
            }

            try
            {
                $del_action = Admin_MailList::getInstance()->deleteMailList($_maillistIds);

                //Add system note del event
                if ($del_action[0] === true){
                    $baseMessage = 'deleted';
                    $noteModel = Tinebase_Model_Note::SYSTEM_NOTE_NAME_CHANGED;
                    $mailCount = count($_maillistIds);
                    for($i=0; $i<$mailCount; $i++){
                        $objectMailList = $del_action[1][$i];
                        $translate = Tinebase_Translation::getTranslation('Admin');
                        $noteMessage = $translate->_('Mail list ' . $baseMessage);
                        Tinebase_Notes::getInstance()->addSystemNote($objectMailList, Tinebase_Core::getUser(), $noteModel, $noteMessage, 'Sql', 'Admin_Model_MailList');
                    }
                }
            }
            catch(Tinebase_Exception_Backend_Ldap $ex)
            {
                Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  MailList delete command exception raised:" . print_r($ex, true));
                throw new Tinebase_Exception_Backend($ex->getMessage());
            }

            $event = new Admin_Event_DeleteMailList();
            $event->mailListIds = $_maillistIds;
            Tinebase_Event::fireEvent($event);
        }
        catch (Tinebase_Exception_Backend $tbeb)
        {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  MailList delete fails: " . print_r($tbeb, true));
            throw new Tinebase_Exception_Backend($tbeb->getMessage());
        }
        return($del_action);
    }

    /**
     * save a mail lists entry
     *
     * @param   array $_maillist
     * @return  boolean
     * @throws Tinebase_Exception_Backend
     */
    public function save($_maillist) {
        try
        {
            $this->checkRight('MANAGE_LDAP_MAILLISTS');
            $save_action = Admin_MailList::getInstance()->saveMailList($_maillist);
            if($save_action[0]){
                try
                {
                    $newMailList = $save_action[1];
                    Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " Saved Admin Mail List Model Object data:  " . print_r($newMailList, true));
                    Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " Mail list data:  " . print_r($_maillist, true));

                    $noteMailList = $this->_jsonDataValid($_maillist);

                    Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " Adding mail list event:  " . print_r($noteMailList, true));
                    //Create
                    if ($save_action[2] === 0){
                        $baseMessage = 'created';
                        $noteModel = Tinebase_Model_Note::SYSTEM_NOTE_NAME_CREATED;
                        Tinebase_Timemachine_ModificationLog::getInstance()->setRecordMetaData($newMailList, 'create');
                    }
                    //Update
                    else if($save_action[2] === 1){
                        $baseMessage = 'updated';
                        $noteModel = Tinebase_Model_Note::SYSTEM_NOTE_NAME_CHANGED;
                        $oldMailList = new Admin_Model_MailList($this->get($newMailList->uidnumber));
                        Tinebase_Timemachine_ModificationLog::getInstance()->setRecordMetaData($newMailList, 'update');
                        Tinebase_Timemachine_ModificationLog::getInstance()->writeModLog($newMailList, $oldMailList, get_class($newMailList), 'Sql', $newMailList->uidnumber);
                    }

                    $translate = Tinebase_Translation::getTranslation('Admin');
                    $noteMessage = $translate->_('Mail list ' . $baseMessage) . ' | ' . $translate->_('User input data') . ': ';
                    $noteMessage .= json_encode($noteMailList);
                    Tinebase_Notes::getInstance()->addSystemNote($newMailList, Tinebase_Core::getUser(), $noteModel, $noteMessage, 'Sql', 'Admin_Model_MailList');
                    return true;
                }
                catch (Tinebase_Exception_UnexpectedValue $tmv){
                    Tinebase_Core::getLogger()->error(__METHOD__ . '::' . __LINE__ . " Mail list history entry fails to be created." . print_r($tmv, true));
                    throw new Tinebase_Exception_InvalidArgument("History log create entry for mail list fails.");
                }
            }
            else{
                Tinebase_Core::getLogger()->error(__METHOD__ . '::' . __LINE__ . " Mail list entry fails to be saved.");
                return false;
            }
        }
        catch (Tinebase_Exception_Backend $tbeb)
        {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  MailList save fails: " . print_r($tbeb, true));
            throw new Tinebase_Exception_Backend($tbeb->getMessage());
        }
    }

     /**
     * validate json non-empty keys
     *
     * @param string $_jsonObject the JavaScript JSON structured data
     * @return array $result array data with originary JSON structure wich does have data
     */
    protected function _jsonDataValid($_jsonObject)
    {
        $result = array();
        foreach ($_jsonObject as $k=>$v){
            if ((is_array($v)) || (is_object($v))){
                $recursive = $this->_jsonDataValid($v);
                if(count($recursive))
                    $result[$k]=$recursive;
            }
            else{
                if (
                     (($v !== NULL) && ($v !== ''))
                     && (strlen(trim($v))>0)
                ){
                    $result[$k] = $v;
                }
            }
        }
        return $result;
    }

    /**
     * add email addresses to a maillist
     *
     * @param array $_tmpFile
     * @return array
     */
    public function importEmails(array $_tmpFile){

        $csv = file_get_contents($_tmpFile['path']);
        $csv = str_replace(' ','',$csv);
        $csv = str_replace("\n",'',$csv);
        $emails = explode(',',$csv);
        $error = array();
        $results = array();
        foreach ($emails as $email) {
            if($this->_validateEmailAddress($email)){
                $results[] = array('address' => $email);
            }else{
                $error[] = $email;
            }
        }
        $valid = array('results' => $results);
        return array('Valid' => $valid,'Errors' => $error);
    }

    /**
     * validate a given email address
     *
     * @param string $email
     * @return boolean
     */
    protected function _validateEmailAddress($email) {

        $account = "^[a-zA-Z0-9\._-]+@";
        $domain = "[a-zA-Z0-9\._-]+.";
        $extension = "([a-zA-Z]{2,4})$";
        $pattern = '/'.$account.$domain.$extension.'/';
        return (boolean)preg_match($pattern, $email);
    }
}