/*
 * Tine 2.0
 *
 * @package     Admin
 * @subpackage  MailLists
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Fernando Wendt <fernando-alberto.wendt@serpro.gov.br>
 * @copyright   Copyright (c) 2014 SERPRO - Serviço Federal de Processamento de Dados (https://www.serpro.gov.br)
 *
 */

Ext.ns('Tine.Admin.MailLists');

Tine.Admin.MailLists.show = function () {
    var app = Tine.Tinebase.appMgr.get('Admin');
    if (!Tine.Admin.MailLists.gridPanel) {
        Tine.Admin.MailLists.gridPanel = new Tine.Admin.MailLists.GridPanel({
            app: app
        });
    }
    else {
        Tine.Admin.MailLists.gridPanel.loadGridData.defer(100, Tine.Admin.MailLists.gridPanel, []);
    }

    Tine.Tinebase.MainScreen.setActiveContentPanel(Tine.Admin.MailLists.gridPanel, true);
    Tine.Tinebase.MainScreen.setActiveToolbar(Tine.Admin.MailLists.gridPanel.actionToolbar, true);
};

/************** models *****************/
Ext.ns('Tine.Admin.Model');

/**
 * Model of a mail list
 */
Tine.Admin.Model.MailListArray = [
    {name: 'uid'},
    {name: 'uidnumber'},
    {name: 'gidnumber'},
    {name: 'mail'},
    {name: 'name'},
    {name: 'description'},
    {name: 'moderation'},
    {name: 'notmoderated'},
    {name: 'admins'},
    {name: 'members'},
    {name: 'externalmembers'},
    {name: 'result'},
    {name: 'clienttype'}
];

Tine.Admin.Model.MailList = Tine.Tinebase.data.Record.create(Tine.Admin.Model.MailListArray, {
    appName: 'Admin',
    modelName: 'MailList',
    idProperty: 'uidnumber',
    titleProperty: 'name',
    recordName: 'Mail List',
    recordsName: 'Mail Lists'
});

Tine.Admin.Model.MailListMemberArray = [
    {name: 'results'},
    {name: 'totalcount'}
];

Tine.Admin.Model.MailListMember = Tine.Tinebase.data.Record.create(Tine.Admin.Model.MailListMemberArray, {
    appName: 'Admin',
    modelName: 'MailListMember',
    idProperty: 'address',
    recordName: 'Mail List Member',
    recordsName: 'Mail List Members'
});

/************** backends *****************/
/**
 * MailListsBackend data proxy
 *
 * @type Tine.Tinebase.data.RecordProxy
 */
Tine.Admin.MailListsBackend = new Tine.Tinebase.data.RecordProxy({
    appName: 'Admin',
    modelName: 'MailList',
    recordClass: Tine.Admin.Model.MailList,
    idProperty: 'uidnumber'
});

/**
 * MailListmembersBackend data proxy
 *
 * @type Tine.Tinebase.data.RecordProxy
 */
Tine.Admin.MailListMembersBacked = new Tine.Tinebase.data.RecordProxy({
    appName: 'Admin',
    modelName: 'MailListMember',
    recordClass: Tine.Admin.Model.MailListMember,
    idProperty: 'address'
});