/*
 * Tine 2.0
 *
 * @package     Admin
 * @subpackage  MailLists
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Fernando Wendt <fernando-alberto.wendt@serpro.gov.br>
 * @copyright   Copyright (c) 2014 SERPRO - Serviço Federal de Processamento de Dados (https://www.serpro.gov.br)
 *
 */
Ext.ns('Tine.Admin.MailLists');

/**
 * MailList grid panel
 *
 * @namespace   Tine.Admin.MailLists
 * @class       Tine.Admin.MailLists.GridPanel
 * @extends     Tine.widgets.grid.GridPanel
 */
Tine.Admin.MailLists.GridPanel = Ext.extend(Tine.widgets.grid.GridPanel, {
    newRecordIcon: 'action_addMailList',
    recordClass: Tine.Admin.Model.MailList,
    recordProxy: Tine.Admin.MailListsBackend,
    defaultSortInfo: {field: 'mail', direction: 'ASC'},
    evalGrants: false,
    gridConfig: {
        id: 'gridAdminMailLists',
        autoExpandColumn: 'email'
    },
    initComponent: function () {
        this.gridConfig.columns = this.getColumns();
        Tine.Admin.MailLists.GridPanel.superclass.initComponent.call(this);
    },

    /**
     * initActions with actionToolbar, contextMenu and actionUpdater
     *
     * @private
     */
    initActions: function () {
        this.supr().initActions.call(this);
    },

    /**
     * initFilterPanel filter toolbar
     */
    initFilterPanel: function () {
        this.filterToolbar = new Tine.widgets.grid.FilterToolbar({
            filterModels: [
                {label: this.app.i18n._('Mail Lists'), field: 'query', operators: ['contains']},
                {label: this.app.i18n._('Address'), field: 'mail'},
                {label: this.app.i18n._('Name'), field: 'name'},
                {label: this.app.i18n._('Description'), field: 'description'},
                {label: this.app.i18n._('Type'), field: 'moderation'},
                {label: this.app.i18n._('Administrators'), field: 'admins'}
            ],
            defaultFilter: 'query',
            plugins: [
                new Tine.widgets.grid.FilterToolbarQuickFilterPlugin()
            ]
        });
        this.plugins = this.plugins || [];
        this.plugins.push(this.filterToolbar);
    },

    /**
     * getColumns returns grid column model (cm)
     * @private
     */
    getColumns: function () {
        return [
            {header: this.app.i18n._('ID'), id: 'uid', dataIndex: 'uid', width: 50, hidden: true, sortable: true},
            {header: this.app.i18n._('Address'), id: 'email', dataIndex: 'mail', hidden: false, sortable: true},
            {header: this.app.i18n._('Name'), id: 'name', dataIndex: 'name', width: 50, hidden: false, sortable: true},
            {header: this.app.i18n._('Description'), id: 'description', dataIndex: 'description', width: 50, hidden: false, sortable: true},
            {header: this.app.i18n._('Administrators'), id: 'admins', dataIndex: 'admins', hidden: true, sortable: true},
            {header: this.app.i18n._('UID Number'), id: 'uidnumber', dataIndex: 'uidnumber', width: 50, hidden: true, sortable: true}
        ];
    },

    /**
     * onDeleteRecords delete mail list handler
     *
     * @return {void}
     */
    onDeleteRecords: function (btn, e) {
        var sm = this.grid.getSelectionModel();
        if (sm.isFilterSelect && !this.filterSelectionDelete) {
            Ext.MessageBox.show({
                title: _('Not Allowed'),
                msg: _('You are not allowed to delete all pages at once'),
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO
            });
            return;
        }

        var records = sm.getSelections();

        if (Tine[this.app.appName].registry.containsKey('preferences')
                && Tine[this.app.appName].registry.get('preferences').containsKey('confirmDelete')
                && Tine[this.app.appName].registry.get('preferences').get('confirmDelete') == 0)
        {
            // don't show confirmation question for record deletion
            this.deleteRecords(sm, records);
        }
        else
        {
            var recordNames = records[0].get(this.recordClass.getMeta('titleProperty'));

            if (records.length > 1)
            {
                recordNames += ', ...';
            }

            var i18nQuestion = this.i18nDeleteQuestion ?
                    this.app.i18n.n_hidden(this.i18nDeleteQuestion[0], this.i18nDeleteQuestion[1], records.length) :
                    String.format(Tine.Tinebase.translation.ngettext('Do you really want to delete the selected record ({0})?',
                            'Do you really want to delete the selected records ({0})?', records.length), recordNames);
            Ext.MessageBox.confirm(_('Confirm'), i18nQuestion, function (btn) {
                if (btn == 'yes') {
                    this.deleteRecords(sm, records);
                }
            }, this);
        }
    },

    /**
     * deleteRecords
     *
     * @param {SelectionModel} sm
     * @param {Array} records
     */
    deleteRecords: function (sm, records) {
        Ext.MessageBox.wait(_('Please wait a moment...'), this.app.i18n.gettext('Deleting mail list(s)'));

        // directly remove records from the store (only for non-filter-selection)
        if (Ext.isArray(records) && !(sm.isFilterSelect && this.filterSelectionDelete))
        {
            Ext.each(records, function (record) {
                this.store.remove(record);
            });
            // if nested in an editDialog, just change the parent record
            if (this.editDialog) {
                var items = [];
                this.store.each(function (item) {
                    items.push(item.data);
                });
                this.editDialog.record.set(this.editDialogRecordProperty, items);
                this.editDialog.fireEvent('updateDependent');
                return;
            }
        }

        if (this.recordProxy) {
            if (this.usePagingToolbar) {
                this.pagingToolbar.refresh.disable();
            }

            var i18nItems = this.app.i18n.n_hidden(this.recordClass.getMeta('recordName'), this.recordClass.getMeta('recordsName'), records.length),
                    recordIds = [].concat(records).map(function (v) {
                return v.id;
            });

            if (sm.isFilterSelect && this.filterSelectionDelete) {
                if (!this.deleteMask) {
                    this.deleteMask = new Ext.LoadMask(this.grid.getEl(), {
                        msg: String.format(_('Deleting {0}'), i18nItems) + _(' ... This may take a long time!')
                    });
                }
                this.deleteMask.show();
            }

            this.deleteQueue = this.deleteQueue.concat(recordIds);

            var options = {
                scope: this,
                success: function (response) {
                    var jsonData = Ext.util.JSON.decode(response.response);
                    if ((jsonData.status == 'failure') && (!jsonData.success))
                    {
                        Ext.MessageBox.show({
                            title: this.app.i18n.gettext('Mail list(s) delete error'),
                            msg: this.app.i18n.gettext(jsonData.errormsg),
                            buttons: Ext.MessageBox.OK,
                            fn: function () {
                                Ext.MessageBox.hide();
                            },
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                    else {
                        Ext.MessageBox.show({
                            title: this.app.i18n.gettext('Mail List(s) Delete Succeeded'),
                            msg: this.app.i18n.gettext('The selected mail list(s) where dropped.'),
                            buttons: Ext.MessageBox.OK,
                            fn: function () {
                                Ext.MessageBox.hide();
                            },
                            icon: Ext.MessageBox.INFO
                        });
                    }
                    this.refreshAfterDelete(recordIds);
                    this.onAfterDelete(recordIds);
                },
                failure: function (exception) {
                    this.refreshAfterDelete(recordIds);
                    this.loadGridData();
                    Ext.MessageBox.hide();
                    Tine.Tinebase.ExceptionHandler.handleRequestException(exception);
                },
                doReresh: function () {
                    this.refreshAfterDelete(recordIds);
                    this.onAfterDelete(recordIds);
                    Ext.MessageBox.hide();
                }
            };

            if (sm.isFilterSelect && this.filterSelectionDelete) {
                this.recordProxy.deleteRecordsByFilter(sm.getSelectionFilter(), options);
            } else {
                this.recordProxy.deleteRecords(records, options);
            }
        }
    }
});