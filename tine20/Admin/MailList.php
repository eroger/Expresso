<?php
/**
 * Tine 2.0
 *
 * @package     Admin
 * @subpackage  MailList
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2014 SERPRO (https://www.serpro.gov.br)
 * @author      Fernando Wendt <fernando-alberto.wendt@serpro.gov.br>
 */

/**
 * MailList class
 *
 * @package     Admin
 * @subpackage  MailList
 */
class Admin_MailList extends Tinebase_Controller_Record_Abstract
{
    /**
     * backend constants
     *
     * @var string
     */
    const LDAP = 'Ldap';

    /**
     * user status constants
     *
     * @var string
     *
     */
    const STATUS_DISABLED = 'disabled';
    const STATUS_ENABLED  = 'enabled';

    /**
     * holds the instance of the singleton
     *
     * @var Tinebase_User_Interface
     */
    private static $_instance = NULL;

    /**
     * Holds the accounts backend type (e.g. Ldap or Sql.
     * Property is lazy loaded on first access via getter {@see getConfiguredBackend()}
     *
     * @var array | optional
     */
    private static $_backendType;

    /**
     * Holds the backend configuration options.
     * Property is lazy loaded from {@see Tinebase_Config} on first access via
     * getter {@see getBackendConfiguration()}
     *
     * @var array | optional
     */
    private static $_backendConfiguration;

    /**
     * the constructor
     *
     * don't use the constructor. use the singleton
     */
    private function __construct() {}

    /**
     * don't clone. Use the singleton.
     */
    private function __clone() {}

    /**
     * Holds the backend configuration options.
     * Property is lazy loaded from {@see Tinebase_Config} on first access via
     * getter {@see getBackendConfiguration()}
     *
     * @var array | optional
     */
    private static $_backendConfigurationDefaults = array(
        self::LDAP => array(
            'host' => '',
            'username' => '',
            'password' => '',
            'bindRequiresDn' => true,
            'useRfc2307bis' => false,
            'baseDn' => '',
            'userFilter' => 'objectclass=posixaccount',
            'userSearchScope' => Zend_Ldap::SEARCH_SCOPE_SUB,
            'pwEncType' => 'CRYPT',
            'minUserId' => '10000',
            'maxUserId' => '29999',
            'userUUIDAttribute' => 'entryUUID',
            'readonly' => false,
            'masterLdapHost' => '',
            'masterLdapUsername' => '',
            'masterLdapPassword' => '',
            'masterLdapReadOnly' => false,
            'mailListDn' => '',
            'mailListGid' => '',
            'mailListFilter' => '',
            'mailListSchemas' => '',
            'mailListAttributes' => '',
         )
    );

    /**
     * the singleton pattern
     *
     * @return Admin_MailList_Abstract
     * @throws Exception
     */
    public static function getInstance()
    {
        try {
            $backendType = Tinebase_User::getConfiguredBackend();

            if (self::$_instance === NULL) {
                $backendType = Tinebase_User::getConfiguredBackend();
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ .' mail list backend: ' . $backendType);

                self::$_instance = self::factory($backendType);
            }

            return self::$_instance;
        } catch (Tinebase_Exception_Backend $tbeb) {
            if (Tinebase_Core::isLogLevel(Zend_Log::ERR)) Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ .' mail list getting instance performs failure situation.' . print_r($tbeb, true));
            throw new Exception($tbeb->getMessage());
        }
    }

    /**
     * return an instance of the current mail list backend
     *
     * @param   string $_backendType name of the mail lists backend
     * @return  Admin_MailList_Abstract
     * @throws  Tinebase_Exception_InvalidArgument, Tinebase_Exception>Backend
     */
    public static function factory($_backendType)
    {
        try {
            switch($_backendType){
                case self::LDAP:
                    $options = Tinebase_User::getBackendConfiguration();
                    $result = new Admin_MailList_Ldap($options);
                    break;

                default:
                    throw new Tinebase_Exception_InvalidArgument("Mail List backend type $_backendType not implemented.");
            }

            return $result;
        } catch (Tinebase_Exception_Backend $tbeb) {
            if (Tinebase_Core::isLogLevel(Zend_Log::ERR)) Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ .' mail list factory fails to start up.' . print_r($tbeb, true));
            throw $tbeb;
        }
    }
}