/*
 * TODO: Licensing
 */
package br.gov.serpro.expresso.security.applet;

import java.awt.Frame;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.security.AccessController;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivilegedAction;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileTypeMap;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.JApplet;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.picocontainer.DefaultPicoContainer;
import com.google.common.base.Throwables;
import br.gov.serpro.expresso.security.applet.openmessage.Converter;
import br.gov.serpro.expresso.security.applet.openmessage.Decrypter;
import br.gov.serpro.expresso.security.applet.openmessage.ExpressoMessage;
import br.gov.serpro.expresso.security.applet.openmessage.Opener;
import br.gov.serpro.expresso.security.applet.openmessage.SignatureChecker;
import br.gov.serpro.expresso.security.applet.openmessage.TokenBroker;
import br.gov.serpro.expresso.security.applet.openmessage.WebServer;
import br.gov.serpro.expresso.security.cert.DigitalCertificate;
import br.gov.serpro.expresso.security.mail.SMIMEMailGenerator;
import br.gov.serpro.expresso.security.setup.Setup;
import netscape.javascript.JSException;
import netscape.javascript.JSObject;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Level.WARNING;
import static org.picocontainer.Characteristics.CACHE;
import static br.gov.serpro.expresso.security.applet.Utils.asJsonString;
import static br.gov.serpro.expresso.security.applet.Utils.asString;
import static br.gov.serpro.expresso.security.applet.openmessage.CryptoException.raise;

//TODO: Logs de depuração e interface para usuário reportar problemas.
public class SmimeApplet extends JApplet {

    private static final MutableInt runningApplets = new MutableInt(0);
    private static final Map<String, StringBuffer> incomingBuffers = Collections.synchronizedMap(new HashMap<String, StringBuffer>());
    private static final Logger logger;

    static {

        Properties logProps = new Properties();
        logProps.setProperty("handlers", "java.util.logging.FileHandler, java.util.logging.ConsoleHandler");

        logProps.setProperty("java.util.logging.FileHandler.level", "FINE");
        logProps.setProperty("java.util.logging.FileHandler.formatter", "java.util.logging.SimpleFormatter");
        logProps.setProperty("java.util.logging.FileHandler.encoding", "UTF-8");
        logProps.setProperty("java.util.logging.FileHandler.count", "10");
        logProps.setProperty("java.util.logging.FileHandler.pattern", "%t/expresso_crypto%u.%g.txt");

        logProps.setProperty("java.util.logging.ConsoleHandler.level", "INFO");
        logProps.setProperty("java.util.logging.ConsoleHandler.formatter", "java.util.logging.SimpleFormatter");

        logProps.setProperty("java.util.logging.SocketHandler.level", "OFF");
        logProps.setProperty("java.util.logging.SocketHandler.formatter", "java.util.logging.XMLFormatter");
        logProps.setProperty("java.util.logging.SocketHandler.host", "127.0.0.1");
        logProps.setProperty("java.util.logging.SocketHandler.port", "4448"); //Chainsaw log viewer XMLSocketReceiver default port
        //logProps.setProperty("java.util.logging.SocketHandler.port", "11020"); //Lilith log viewer default port
        //logProps.setProperty("java.util.logging.SocketHandler.port", "50505"); //OtrosLogViewer default port

        // 1:date, 2:source, 3:logger, 4:level, 5:message, 6:thrown
        logProps.setProperty("java.util.logging.SimpleFormatter.format", "#%1$tY.%<tm.%<td %<tH:%<tM:%<tS [%2$s]%n%4$s: %5$s%6$s%n");

        logProps.setProperty("br.gov.serpro.expresso.security.level", "FINE");

        File p = new File(System.getProperty("user.home"), ".expresso_crypto_logging.properties");
        if (p.canRead()) {
            Properties userLogProps = new Properties();
            try {
                userLogProps.load(new FileInputStream(p));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            for (String k : userLogProps.stringPropertyNames()) {
                logProps.setProperty(k, userLogProps.getProperty(k));
            }
        }
        class Buffer extends ByteArrayOutputStream {

            public byte[] getBuf() {
                return buf;
            }

            public Buffer() {
                super(4096);
            }
        }
        Buffer buf = new Buffer();
        try {
            logProps.store(buf, "crypto");
            LogManager.getLogManager().readConfiguration(new ByteArrayInputStream(buf.getBuf(), 0, buf.size()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        logger = Logger.getLogger(SmimeApplet.class.getName());
    }
    protected static final DefaultPicoContainer pico;

    static {

        pico = new DefaultPicoContainer();

        // registering BC as a cryptographyc provider is a privileged action
        AccessController.doPrivileged(new PrivilegedAction() {

            @Override
            public Object run() {
                pico.as(CACHE).addComponent(WebServer.class).as(CACHE).addComponent(TokenBroker.class).as(CACHE).addComponent(Decrypter.class).as(CACHE).addComponent(SignatureChecker.class).as(CACHE).addComponent(Converter.class).as(CACHE).addComponent(Opener.class);
                return null;
            }
        });

    }
    /**
     * Variáveis de instância
     */
    protected WebServer webServer;
    protected Opener opener;
    private Setup setup;
    private Frame parentFrame;
    private SmimeApplet applet = this;
    private FileDownloader downloader;
    private String currentAccountFullName;
    private String currentAccountEmailAddress;
    /**
     * Variáveis de classe
     */
    private static final long serialVersionUID = 4797603392324194391L;

    @Override
    public void init() {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            logger.log(WARNING, e.getMessage(), e);
        }

        try {
            SwingUtilities.invokeAndWait(new Runnable() {

                @Override
                public void run() {
                    setSize(0, 0);
                }
            });
        } catch (Exception e) {
            logger.log(WARNING, e.getMessage() + "- createGUI didn't complete successfully", e);
        }

        setup = new Setup(this);
        setup.addLanguageResource("ExpressoCertMailMessages");
        parentFrame = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, this);

        AccessController.doPrivileged(new PrivilegedAction() {

            @Override
            public Object run() {
                webServer = pico.getComponent(WebServer.class);
                webServer.setCodebase(getCodeBase());
                opener = pico.getComponent(Opener.class);
                return null;
            }
        });

        try {
            JSObject window = JSObject.getWindow(this);
            window.eval("Tine.Expressomail.afterAppletLoad();");
            logger.log(INFO, "browser notified of applet initialization");
        } catch (JSException jse) {
            logger.log(SEVERE, "javascript callback after applet load", jse);
        }

        try {
            JSObject window = JSObject.getWindow(this);
            JSObject currentAccount = (JSObject) window.eval("Tine.Tinebase.registry.get('currentAccount');");

            currentAccountFullName = (String) currentAccount.getMember("accountFullName");
            logger.log(INFO, "currentAccountFullName: {0}", currentAccountFullName);

            currentAccountEmailAddress = (String) currentAccount.getMember("accountEmailAddress");
            logger.log(INFO, "currentAccountEmailAddress: {0}", currentAccountEmailAddress);
        } catch (JSException jse) {
            logger.log(SEVERE, "get current account", jse);
        }
    }

    @Override
    public void start() {
        logger.log(INFO, "Starting applet {0}", hashCode());
        super.start();
        synchronized (runningApplets) {
            if (runningApplets.getValue() == 0) {
                try {
                    webServer.start();
                } catch (IOException ioe) {
                    raise(ioe);
                }
            }
            runningApplets.increment();
        }
    }

    public ExpressoMessage openMessage(String message) {
        return opener.process(message);
    }

    @Override
    public void stop() {
        logger.log(INFO, "Stopping applet {0}", hashCode());
        super.stop();
        synchronized (runningApplets) {
            runningApplets.decrement();
            if (runningApplets.getValue() == 0) {
                webServer.stop();
            }
        }
    }

    @Override
    /**
     * Retorna Informações sobre os parâmetros que essa applet aceita
     *
     * @author Mário César Kolling <mario.kolling@serpro.gov.br>
     * @return String[][] Uma matriz de Strings relacionando cada parâmetro e
     * sua descrição
     */
    public String[][] getParameterInfo() {
        return setup.getParameterInfo();
    }

    private enum OpType {

        SIGN, ENCRYPT, SIGN_AND_ENCRYPT,
        DECRYPT, VERIFY;
    }

    abstract class SmimeOperation implements Callable<Void>, ExceptionHandler {

        final String id;
        final OpType type;
        DigitalCertificate digitalCertificate;
        JsonResult result;

        SmimeOperation(String id, OpType type) {
            this.id = id;
            this.type = type;
        }

        @Override
        public Void call() throws Exception {
            try {
                startup();
                perform(result);
                finish();
            } finally {
                if (digitalCertificate != null) {
                    digitalCertificate.destroy();
                }
            }

            return null;
        }

        protected void startup() {
            digitalCertificate = new DigitalCertificate(parentFrame, setup,
                    currentAccountFullName, currentAccountEmailAddress);

            result = new JsonResult();
            result.setType(type.name());
        }

        protected abstract void perform(JsonResult result) throws Exception;

        protected void finish() {
            result.setSuccess(true);
            reply();
        }

        // Must be privileged
        @Override
        public void onException(final Exception ex) {
            logger.log(SEVERE, "SmimeOperation exception: \n" + Throwables.getStackTraceAsString(ex), ex);

            AccessController.doPrivileged(new PrivilegedAction() {

                @Override
                public Object run() {
                    result.setSuccess(false);
                    result.setException(new JsonException(ex));
                    reply();
                    return null;
                }
            });
        }

        protected void reply() {
            String r = null;
            try {
                r = asJsonString(result);
            } catch (IOException ex) {
                logger.log(SEVERE, "SmimeOperation reply exception: \n" + Throwables.getStackTraceAsString(ex), ex);
                return;
            }

            int chunkSize = 0x100000; //1MB

            for (int i = 0, length = r.length(), chunks = 1; i < length; i += chunkSize, chunks++) {
                String isLast = String.valueOf(Math.min(length, i + chunkSize) == length);
                JSObject.getWindow(applet).call("appletStub",
                        new String[]{isLast, id, r.substring(i, Math.min(length, i + chunkSize))});
                logger.log(INFO, "{0} Chunks sent!", chunks);
            }
        }

        protected SMIMEMailGenerator createGenerator(String userAgent, String jsonMessageModel) throws IOException {

            // TODO: Convert to ExpressoMessage.class
            ObjectMapper mapper = new ObjectMapper(new JsonFactory());
            Map<String, Object> message = mapper.readValue(jsonMessageModel, Map.class);

            return new SMIMEMailGenerator(applet, digitalCertificate, message, userAgent);
        }
    }

    //--------------------------------------------------------------------------
    public void signMessage(final String userAgent, final String id, final String jsonMessageModel) {

        logger.log(INFO, "Calling test: ");
        logger.log(INFO, "User Agent: {0}", userAgent);
        logger.log(INFO, "Operation Type: SIGN");
        logger.log(INFO, "id: {0}", id);
        logger.log(INFO, "body: {0}", jsonMessageModel);

        SmimeOperation op = new SmimeOperation(id, OpType.SIGN) {

            @Override
            public void perform(JsonResult result) throws Exception {
                SMIMEMailGenerator generator = createGenerator(userAgent, jsonMessageModel);
                logger.log(INFO, "Iniciando assinatura digital da mensagem");
                MimeMessage m = generator.generateSignedMail();
                result.setSignerCertificate(generator.getSignerCertificate());
                result.setMimeMsg(asString(m));
            }
        };

        new AsynchronousPrivilegedTask(op).start();
    }

    //--------------------------------------------------------------------------
    public void encryptMessage(final String userAgent, final String id, final String jsonMessageModel) {

        logger.log(INFO, "Calling test: ");
        logger.log(INFO, "User Agent: {0}", userAgent);
        logger.log(INFO, "Operation Type: ENCRYPT");
        logger.log(INFO, "id: {0}", id);
        logger.log(INFO, "body: {0}", jsonMessageModel);

        SmimeOperation op = new SmimeOperation(id, OpType.ENCRYPT) {

            @Override
            public void perform(JsonResult result) throws Exception {
                SMIMEMailGenerator generator = createGenerator(userAgent, jsonMessageModel);
                logger.log(INFO, "Iniciando cifragem da mensagem");
                MimeMessage m = generator.generateEncryptedMail();
                result.setMimeMsg(asString(m));
            }
        };

        new AsynchronousPrivilegedTask(op).start();
    }

    //--------------------------------------------------------------------------
    public void signAndEncryptMessage(final String userAgent, final String id, final String jsonMessageModel) {

        logger.log(INFO, "Calling test: ");
        logger.log(INFO, "User Agent: {0}", userAgent);
        logger.log(INFO, "Operation Type: SIGN_AND_ENCRYPT");
        logger.log(INFO, "id: {0}", id);
        logger.log(INFO, "body: {0}", jsonMessageModel);

        SmimeOperation op = new SmimeOperation(id, OpType.SIGN_AND_ENCRYPT) {

            @Override
            public void perform(JsonResult result) throws Exception {
                SMIMEMailGenerator generator = createGenerator(userAgent, jsonMessageModel);
                logger.log(INFO, "Iniciando assinatura digital e cifragem da mensagem");
                MimeMessage m = generator.generateSignedAndEncryptedMail();
                result.setSignerCertificate(generator.getSignerCertificate());
                result.setMimeMsg(asString(m));
            }
        };

        new AsynchronousPrivilegedTask(op).start();
    }

    //--------------------------------------------------------------------------
    public void decryptMessage(String userAgent, String id, String jsonMessageModel, boolean dispatch) {
        StringBuffer sb = incomingBuffers.get(id);
        if (dispatch) {
            if (sb != null) {
                sb.append(jsonMessageModel);
                jsonMessageModel = sb.toString();
                incomingBuffers.remove(id);
            }
            decryptMessage(userAgent, id, jsonMessageModel);
        } else {
            if (sb == null) {
                sb = new StringBuffer(0x4000); //16KB
                incomingBuffers.put(id, sb);
            }
            sb.append(jsonMessageModel);
        }
    }

    public void decryptMessage(final String userAgent, final String id, final String jsonMessageModel) {

        logger.log(INFO, "Calling test: ");
        logger.log(INFO, "User Agent: {0}", userAgent);
        logger.log(INFO, "Operation Type: {0}", "DECRYPT");
        logger.log(INFO, "id: {0}", id);
        logger.log(INFO, "body: {0}", jsonMessageModel);

        SmimeOperation op = new SmimeOperation(id, OpType.DECRYPT) {

            @Override
            public void perform(JsonResult result) throws Exception {

                // TODO: Don't get MessageModel, only smimeEml attribute
                ObjectMapper mapper = new ObjectMapper(new JsonFactory());
                Map<String, Object> message = mapper.readValue(jsonMessageModel, Map.class);

                logger.log(INFO, "Iniciando decifragem da mensagem");
                opener.setDigitalcertificate(digitalCertificate);
                String encryptedMessage = (String) message.get("smimeEml");
//                logger.log(FINEST, encryptedMessage);

                ExpressoMessage m = opener.process(encryptedMessage);
                result.setExprMsg(m);
            }
        };

        new AsynchronousPrivilegedTask(op).start();
    }

    //--------------------------------------------------------------------------
    public void verifyMessage(String userAgent, String id, String jsonMessageModel, boolean dispatch) {
        StringBuffer sb = incomingBuffers.get(id);
        if (dispatch) {
            if (sb != null) {
                sb.append(jsonMessageModel);
                jsonMessageModel = sb.toString();
                incomingBuffers.remove(id);
            }
            verifyMessage(userAgent, id, jsonMessageModel);
        } else {
            if (sb == null) {
                sb = new StringBuffer(0x4000); //16KB
                incomingBuffers.put(id, sb);
            }
            sb.append(jsonMessageModel);
        }
    }

    public void verifyMessage(final String userAgent, final String id, final String jsonMessageModel) {

        logger.log(INFO, "Calling test: ");
        logger.log(INFO, "User Agent: {0}", userAgent);
        logger.log(INFO, "Operation Type: {0}", "VERIFY");
        logger.log(INFO, "id: {0}", id);
        logger.log(INFO, "body: {0}", jsonMessageModel);

        SmimeOperation op = new SmimeOperation(id, OpType.VERIFY) {

            @Override
            public void perform(JsonResult result) throws Exception {

                // TODO: Don't get MessageModel, only smimeEml attribute
                ObjectMapper mapper = new ObjectMapper(new JsonFactory());
                Map<String, Object> message = mapper.readValue(jsonMessageModel, Map.class);

                logger.log(INFO, "Iniciando verificação da mensagem");
                opener.setDigitalcertificate(digitalCertificate);
                String signedMessage = (String) message.get("smimeEml");
//                logger.log(FINEST, signedMessage);

                ExpressoMessage m = opener.process(signedMessage);
                result.setExprMsg(m);
            }
        };

        new AsynchronousPrivilegedTask(op).start();
    }

    public FileDownloader getDownloader(String userAgent) throws KeyManagementException, NoSuchAlgorithmException {
        if (this.downloader == null) {
            this.downloader = new FileDownloader(userAgent);
        }
        return this.downloader;
    }

    @Override
    public void destroy() {
        // shutdown Downloader and other resources
        if (this.downloader != null) {
            this.downloader.shutdown();
        }
        super.destroy();
    }

    /**
     * @todo Implements parallel downloads
     */
    public class FileDownloader {

        private final AbstractHttpClient client;
        private final JSObject balanceId;

        private FileDownloader(String userAgent) throws NoSuchAlgorithmException, KeyManagementException {

            this.client = new DefaultHttpClient();
            this.client.getParams().setParameter(CoreProtocolPNames.USER_AGENT, userAgent);
            URL documentBase = applet.getDocumentBase();
            JSObject window = JSObject.getWindow(applet);
            balanceId = (JSObject) window.eval("Tine.Expressomail.registry.get('balanceId');");

            // permissive Trust Manager
            X509TrustManager tm = new X509TrustManager() {

                @Override
                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            X509HostnameVerifier verifier = new X509HostnameVerifier() {

                @Override
                public void verify(String string, SSLSocket ssls) throws IOException {
                }

                @Override
                public void verify(String string, X509Certificate xc) throws SSLException {
                }

                @Override
                public void verify(String string, String[] strings, String[] strings1) throws SSLException {
                }

                @Override
                public boolean verify(String string, SSLSession ssls) {
                    return true;
                }
            };

            if (documentBase.getProtocol().equalsIgnoreCase("https")) {
                SSLContext ctx = SSLContext.getInstance("TLS");
                ctx.init(null, new TrustManager[]{tm}, null);
                SSLSocketFactory ssf = new SSLSocketFactory(ctx, verifier);
                ClientConnectionManager ccm = this.client.getConnectionManager();
                SchemeRegistry sr = ccm.getSchemeRegistry();
                int port = documentBase.getPort();
                sr.register(new Scheme("https", port == -1 ? 443 : port, ssf));
            }

        }

        public MimeBodyPart downloadFile(URI url, String sessionId) throws ClientProtocolException, IOException {
            System.out.println("arquivo: " + url.toString());

            String sessionCookieName = "TINE20SESSID";
            CookieStore cookieStore = this.client.getCookieStore();
            if (sessionId != null) {
                BasicClientCookie tine20SessId = new BasicClientCookie(sessionCookieName, sessionId);
                tine20SessId.setDomain(url.getHost());
                tine20SessId.setPath("/");
                cookieStore.addCookie(tine20SessId);
            }

            if (balanceId != null) {
                String balanceIdCookieName = (String) balanceId.getMember("cookieName");
                String balanceIdCookieValue = (String) balanceId.getMember("cookieValue");
                BasicClientCookie balanceIdCookie = new BasicClientCookie(balanceIdCookieName, balanceIdCookieValue);
                balanceIdCookie.setDomain(url.getHost());
                balanceIdCookie.setPath("/");
                cookieStore.addCookie(balanceIdCookie);
            }

            // Configura um Post request
            HttpGet get = new HttpGet(url);

            System.out.println("Executou o request");
            HttpResponse response = this.client.execute(get);

            System.out.println("Getting entity");
            HttpEntity httpEntity = response.getEntity();

            String contentType;
            // Verify why httpEntity.getContentType().getValue() is returning the wrong contentType (text/html)
            // download response should just only answer one content-disposition header with one element
            NameValuePair filenameValuePair = null;
            if (response.containsHeader("Content-Disposition")){
                Header headerContentDisposition = response.getHeaders("Content-Disposition")[0];
                // If header "Content-Disposition" exists, the first element should exist.
                if (headerContentDisposition.getElements().length != 0) {
                    filenameValuePair = headerContentDisposition.getElements()[0].getParameterByName("filename");
                }
            }
            if (filenameValuePair != null) {
                String filename = filenameValuePair.getValue();
                contentType = FileTypeMap.getDefaultFileTypeMap().getContentType(filename.toLowerCase());
            } else {
                contentType = httpEntity.getContentType().getValue();
            }

            //OutputStream content = this._writeToOS(httpEntity.getContent());
            MimeBodyPart part = new MimeBodyPart();

            try {

                System.out.println("Setting data on MimeBodyPart");
                logger.log(INFO, "contentType: {0}", contentType);
                part.setDataHandler(new DataHandler(new ByteArrayDataSource(
                        EntityUtils.toByteArray(httpEntity), contentType)));
                part.setHeader("Content-Type", contentType);
                part.setHeader("Content-Transfer-Encoding", "base64");

            } catch (Exception e) {
                logger.log(SEVERE, null, e);
            } finally {
                EntityUtils.consumeQuietly(httpEntity);
            }

            return part;
        }

        void shutdown() {
            if (this.client != null) {
                this.client.getConnectionManager().shutdown();
            }
        }
    }
}
