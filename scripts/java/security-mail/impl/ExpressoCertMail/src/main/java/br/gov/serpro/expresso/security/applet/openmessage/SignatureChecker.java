package br.gov.serpro.expresso.security.applet.openmessage;

import java.io.IOException;
import java.security.Security;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.cms.SignerInformationVerifier;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.mail.smime.SMIMEException;
import org.bouncycastle.mail.smime.SMIMESigned;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.util.Store;
import br.gov.serpro.expresso.security.util.Base64Utils;
import static java.util.logging.Level.*;
import static br.gov.serpro.expresso.security.applet.openmessage.CryptoException.*;

public class SignatureChecker {

    private static final Logger logger = Logger.getLogger(SignatureChecker.class.getName());

    private final BouncyCastleProvider bcProvider;
    private final JcaX509CertificateConverter x509CertificateConverter;
    private final JcaSimpleSignerInfoVerifierBuilder simpleSignerInfoVerifierBuilder;

    public SignatureChecker() {

        bcProvider = new BouncyCastleProvider();
        Security.addProvider(bcProvider);

        x509CertificateConverter = new JcaX509CertificateConverter();
        x509CertificateConverter.setProvider(bcProvider);

        simpleSignerInfoVerifierBuilder = new JcaSimpleSignerInfoVerifierBuilder();
        simpleSignerInfoVerifierBuilder.setProvider(bcProvider);
    }

    public void process(MimePart mimePart, ExpressoMessage exprMsg) {

        SMIMESigned s = null;
        try {
            s = (mimePart.isMimeType("multipart/signed"))
                    ? new SMIMESigned((MimeMultipart) mimePart.getContent())
                 // "application/pkcs7-mime" || "application/x-pkcs7-mime"
                    : new SMIMESigned(mimePart);
        }
        catch (MessagingException ex) {
            raise(ex);
        }
        catch (CMSException ex) {
            raise(ex);
        }
        catch (SMIMEException ex) {
            raise(ex);
        }
        catch (IOException ex) {
            raise(ex);
        }

        Store certs = s.getCertificates();

        SignerInformationStore signes = s.getSignerInfos();
        Collection<SignerInformation> signerCol = signes.getSigners();
        Iterator<SignerInformation> signerIt = signerCol.iterator();

        while (signerIt.hasNext()) {

            ExpressoSignature exprSig = new ExpressoSignature();

            SignerInformation signer = signerIt.next();

            Collection certCol = certs.getMatches(signer.getSID());
            Iterator certIt = certCol.iterator();

            X509CertificateHolder certHolder = (X509CertificateHolder) certIt.next();

            X509Certificate cert = null;
            try {
                cert = x509CertificateConverter.getCertificate(certHolder);

                if(logger.isLoggable(FINE)) {
                    logger.log(FINE, "Signer certificate:\n{0}", cert.toString());
                }
            }
            catch (CertificateException ex) {
                raise(ex);
            }

            try {
                exprSig.setCertificate(Base64Utils.der2pem(cert.getEncoded(), true));
            }
            catch (CertificateEncodingException ex) {
                raise(ex);
            }

            try {
                addAddresses(cert, exprSig);
            }
            catch (IOException ex) {
                raise(ex);
            }
            catch (CertificateEncodingException ex) {
                raise(ex);
            }

            SignerInformationVerifier verifier = null;
            try {
                verifier = simpleSignerInfoVerifierBuilder.build(cert);
            }
            catch (OperatorCreationException ex) {
                raise(ex);
            }

            try {
                exprSig.setVerified(signer.verify(verifier));
            }
            catch (CMSException ex) {
                raise(ex);
            }

            exprMsg.addSignature(exprSig);
            exprMsg.setHasSignature(true);
        }
    }

    public void addAddresses(X509Certificate cert, ExpressoSignature exprSig)
            throws IOException, CertificateEncodingException {

        X509Principal name;
        {
            byte[] t1 = cert.getTBSCertificate();
            ASN1Primitive t2 = ASN1Primitive.fromByteArray(t1);
            TBSCertificateStructure t3 = TBSCertificateStructure.getInstance(t2);
            X500Name t4 = t3.getSubject();
            X500Name t5 = X500Name.getInstance(t4);
            name = new X509Principal(t5);
        }

        Vector oids = name.getOIDs();
        Vector names = name.getValues();

        for (int i = 0; i < oids.size(); i++) {
            if (oids.get(i).equals(BCStyle.EmailAddress)) {
                String email = ((String) names.get(i)).toLowerCase();
                exprSig.addAddress(email);
                break;
            }
        }

        byte[] ext = cert.getExtensionValue(X509Extension.subjectAlternativeName.getId());
        if (ext != null) {

            ASN1Sequence altNames;
            {
                ASN1InputStream aIn = new ASN1InputStream(ext);
                ASN1OctetString octs = (ASN1OctetString) aIn.readObject();
                aIn = new ASN1InputStream(octs.getOctets());
                ASN1Primitive a = aIn.readObject();
                altNames = ASN1Sequence.getInstance(a);
            }

            for (int j = 0; j < altNames.size(); j++) {
                ASN1TaggedObject o = (ASN1TaggedObject) altNames.getObjectAt(j);
                if (o.getTagNo() == 1) {
                    String email = DERIA5String.getInstance(o, false).getString().toLowerCase();
                    exprSig.addAddress(email);
                }
            }
        }
    }
}
