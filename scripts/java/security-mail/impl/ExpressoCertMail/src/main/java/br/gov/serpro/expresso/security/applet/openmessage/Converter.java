package br.gov.serpro.expresso.security.applet.openmessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.james.mime4j.dom.BinaryBody;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.SingleBody;
import org.apache.james.mime4j.dom.TextBody;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.address.Group;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.field.AddressListField;
import org.apache.james.mime4j.dom.field.ContentDescriptionField;
import org.apache.james.mime4j.dom.field.ContentIdField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.dom.field.MailboxListField;
import org.apache.james.mime4j.dom.field.UnstructuredField;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.util.CharsetUtil;
import net.htmlparser.jericho.Attributes;
import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.OutputDocument;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;
import net.htmlparser.jericho.Tag;
import static java.util.logging.Level.FINE;
import static java.util.logging.Level.SEVERE;
import static br.gov.serpro.expresso.security.applet.openmessage.CryptoException.raise;
import static br.gov.serpro.expresso.security.applet.openmessage.OpenUtils.getContentDescription;
import static br.gov.serpro.expresso.security.applet.openmessage.OpenUtils.getContentId;
import static br.gov.serpro.expresso.security.applet.openmessage.OpenUtils.getContentType;
import static br.gov.serpro.expresso.security.applet.openmessage.OpenUtils.getExpressoData;
import java.text.MessageFormat;

public class Converter {

    private static final Logger logger = Logger.getLogger(Converter.class.getName());

    public static final Charset Windows_1252 = Charset.forName("windows-1252");

    private static final String CR = "\015";
    private static final String LF = "\012";
    private static final String CRLF = "\015\012";

    private static final Pattern controlCharactersPattern = Pattern.compile("[\\x00-\\x09\\x0B\\x0C\\x0E-\\x1F]");

    private final WebServer webServer;

    public Converter(WebServer webServer) {
        this.webServer = webServer;
    }

    public void process(final Message message, final ExpressoMessage exprMsg) throws IOException {

        OpenUtils.traverse(message, new EntityHandler() {
            @Override
            public void entity(Entity entity, Deque<Integer> level) {
                new ExpressoDataField(entity);
            }
        });

        //TODO: reverter antes da distribuição
        //if(logger.isLoggable(FINER)) {
        //    logger.log(FINER, "mime message: \n{0}", OpenUtils.dump(message));
        //}

        final StringBuilder tb = new StringBuilder();

        EntityHandler handler;
        handler = new EntityHandler() {

            @Override
            public void header(Header header, Deque<Integer> level) {

                if(level.size() == 1) {
                    MessageDigest md = null;
                    try {
                        md = MessageDigest.getInstance("MD5");
                    } catch (NoSuchAlgorithmException ex) {}
                    try {
                        md.update(header.toString().getBytes("UTF-8"));
                    } catch (UnsupportedEncodingException ex) {}

                    char[] hexDigest = new char[32];
                    int i = 0;
                    for(byte b : md.digest()) {
                        hexDigest[i++] = Character.forDigit((char)((b & 0xf0) >>> 4), 16);
                        hexDigest[i++] = Character.forDigit((char)(b & 0x0f), 16);
                    }

                    exprMsg.setEid(new String(hexDigest));
                    logger.log(FINE, "exprMsg eid: {0}", exprMsg.getEid());
                }
            }

            @Override
            public void field(Field field, Deque<Integer> level) {
                if(level.size() == 1) {
                    exprMsg.addHeader(field.getName() + ": " + field.getBody());
                }
            }

            @Override
            public void subject(UnstructuredField field, Deque<Integer> level) {
                if(level.size() == 1) {
                    exprMsg.setSubject(field.getBody());
                }
            }

            @Override
            public void from(MailboxListField field, Deque<Integer> level) {
                if(level.size() == 1) {
                    Mailbox from = field.getMailboxList().get(0);
                    exprMsg.setFromName(from.getName());
                    exprMsg.setFromEmail(from.getAddress());
                }
            }

            @Override
            public void sender(MailboxField field, Deque<Integer> level) {
                if(level.size() == 1) {
                    Mailbox sender = field.getMailbox();
                    exprMsg.setSender(sender.getName());
                    exprMsg.setSenderAccount(sender.getAddress());
                }
            }

            @Override
            public void to(AddressListField field, Deque<Integer> level) {
                if(level.size() == 1) {
                    for(Address to : field.getAddressList()) {
                        if(to instanceof Mailbox) {
                            Mailbox toMailbox = (Mailbox) to;
                            exprMsg.addTo(toMailbox.getAddress());
                        }
                        else if(to instanceof Group) {
                            Group toGroup = (Group) to;
                            exprMsg.addTo(toGroup.toString());
                        }
                    }
                }
            }

            @Override
            public void cc(AddressListField field, Deque<Integer> level) {
                if(level.size() == 1) {
                    for(Address cc : field.getAddressList()) {
                        if(cc instanceof Mailbox) {
                            Mailbox ccMailbox = (Mailbox) cc;
                            exprMsg.addCc(ccMailbox.getAddress());
                        }
                        else if(cc instanceof Group) {
                            Group ccGroup = (Group) cc;
                            exprMsg.addCc(ccGroup.toString());
                        }
                    }
                }
            }

            @Override
            public void bcc(AddressListField field, Deque<Integer> level) {
                if(level.size() == 1) {
                    for(Address bcc : field.getAddressList()) {
                        if(bcc instanceof Mailbox) {
                            Mailbox bccMailbox = (Mailbox) bcc;
                            exprMsg.addBcc(bccMailbox.getAddress());
                        }
                        else if(bcc instanceof Group) {
                            Group bccGroup = (Group) bcc;
                            exprMsg.addBcc(bccGroup.toString());
                        }
                    }
                }
            }

            @Override
            public void textBody(TextBody textBody, Deque<Integer> level) {
                try {
                    tb.append("    <div id='text-body");
                    for(int n : level) {
                        tb.append("-").append(n);
                    }
                    tb.append("'>\n").append(getAdaptedText(message, textBody)).append("\n    </div>\n");
                }
                catch(IOException e) {
                    logger.log(SEVERE, e.getMessage(), e);
                    raise(e);
                }
            }

            @Override
            public void inlineBody(SingleBody singleBody, Deque<Integer> level) {
                addAttachment(exprMsg, singleBody, level);
            }

            @Override
            public void attachmentBody(SingleBody singleBody, Deque<Integer> level) {
                addAttachment(exprMsg, singleBody, level);
            }


        };

        tb.append("<span>\n");

        OpenUtils.traverseWithDefaultAlternativePriority(message, handler);

        tb.append("</span>\n");

        exprMsg.setBody(controlCharactersPattern.matcher(tb.toString()).replaceAll(""));

        //logger.log(FINE, "exprMsg: \n{0}", Utils.asJsonString(exprMsg));
    }

    private void addAttachment(ExpressoMessage exprMsg, SingleBody singleBody, Deque<Integer> level) {

        ExpressoAttachment att = new ExpressoAttachment();

        ContentDescriptionField contentDescription = getContentDescription(singleBody);
        //ContentDispositionField contentDisposition = getContentDisposition(singleBody);
        ContentIdField contentId = getContentId(singleBody);
        //ContentLanguageField contentLanguage = getContentLanguage(singleBody);
        //ContentLengthField contentLength = getContentLength(singleBody);
        //ContentLocationField contentLocation = getContentLocation(singleBody);
        //ContentMD5Field contentMD5 = getContentMD5(singleBody);
        //ContentTransferEncodingField contentTransferEncoding = getContentTransferEncoding(singleBody);
        ContentTypeField contentType = getContentType(singleBody);
        //UnstructuredField messageId = getMessageId(singleBody);
        ExpressoDataField expressoData = getExpressoData(singleBody);

        att.setCid(contentId != null ? contentId.getBody() : null);
        att.setContentType(contentType != null ? contentType.getBody() : null);
        att.setDescription(contentDescription != null ? contentDescription.getDescription() : null);

        String cid = att.getCid();
        att.setEid(cid == null ? expressoData.getEid() : cid.substring(1, cid.length()-1));
        att.setBody(expressoData.getEntitySingleBodyDecoded());
        att.setSize(att.getBody().length);
        att.setFileName(expressoData.getFilename());

        exprMsg.addAttachment(att);
        exprMsg.setHasAttachment(true);

        webServer.publish(exprMsg.getEid(), att);
    }


    private String fixAnchors(String text){

        Source htmlSource = new Source(text);
        htmlSource.fullSequentialParse();
        OutputDocument htmlOutputDocument = new OutputDocument(htmlSource);

        List<Element> anchors = htmlSource.getAllElements("a");

        for (Element anchor : anchors){

            StartTag startTag = anchor.getStartTag();
            Map<String, String> attrs = new HashMap<String, String>();
            startTag.getAttributes().populateMap(attrs, true);
            attrs.put("target", "_blank"); // adds or overwrites target value
            String newTag = MessageFormat.format("<a {0}>", Attributes.generateHTML(attrs));
            htmlOutputDocument.replace(startTag, newTag);

        }

        return htmlOutputDocument.toString();
    }

    private String getAdaptedText(Message message, TextBody textBody) throws IOException {

        InputStream is = textBody.getInputStream();

//        Task #9613 - decoding an already decoded quoted-printable text, is breaking
//        an image data-URI base64 when terminated with ==
//        String contentTransferEncoding = textBody.getParent().getContentTransferEncoding();
//
//        ContentTransferEncodingField x = getContentTransferEncoding(textBody);
//
//        if("base64".equalsIgnoreCase(contentTransferEncoding)) {
//            is = new Base64InputStream(is);
//        }
//        else if("quoted-printable".equalsIgnoreCase(contentTransferEncoding)) {
//            is = new QuotedPrintableInputStream(is);
//        }

        Charset cs = CharsetUtil.lookup(textBody.getMimeCharset());
        if((cs == null) || (cs == CharsetUtil.US_ASCII)) {
            cs = Windows_1252;
        }

        InputStreamReader isr = new InputStreamReader(is, cs);
        BufferedReader reader = new BufferedReader(isr);

        StringBuilder sb = new StringBuilder();

        char[] cbuf = new char[0x1000];
        int len;
        while((len = reader.read(cbuf)) > 0) {
            sb.append(cbuf, 0, len);
        }

        reader.close();
        isr.close();
        is.close();

        String text = sb.toString();

        if("text/plain".equalsIgnoreCase(textBody.getParent().getMimeType())) {
            text = text.replaceAll(CRLF + "|" + CR + "|" + LF, "<br>" + LF);
        }

        text = HTMLSanitiser.stripInvalidMarkup(text, true);
        text = StringEscapeUtils.unescapeHtml4(text);
        text = replaceMailToRef(text);
        text = replaceContentIdRef(message, text);
        text = replaceUrlRef(text);
        text = fixAnchors(text);

        return text;
    }


    //--------------------------------------------------------------------------
//    private static final String contentIdRegex = "src\\s*=(.*(?!cid))(" +
//            "'\\s*cid:\\s*([^'\\s]+)\\s*'" +
//            "|" +
//            "\"\\s*cid:\\s*([^\"\\s]+)\\s*\"" +
//            "|" +
//            "cid:([^\\s]+)" +
//            ")";
//
//    private static final Pattern contentIdPattern = Pattern.compile(contentIdRegex, Pattern.CASE_INSENSITIVE);

    private String replaceContentIdRef(Message message, String text) {

        Source textSource = new Source(text);
        textSource.fullSequentialParse();
        OutputDocument textOutputDocument = new OutputDocument(textSource);
        List<Element> images = textSource.getAllElements("img");

        for (Element img : images) {
            StartTag startTag = img.getStartTag();
            Map<String, String> attrs = new HashMap<String, String>();
            startTag.getAttributes().populateMap(attrs, true);

            String src = attrs.get("src");
            if (src == null) {
                continue;
            }

            Pattern cidPattern = Pattern.compile("cid:(.*)", Pattern.CASE_INSENSITIVE);
            Matcher m = cidPattern.matcher(src);
            if (!m.matches()) {
                continue;
            }

            final String cid = m.group(1);
            final StringBuilder newSrc = new StringBuilder("https://local.expressov3.serpro.gov.br:8998/");

            final MutableBoolean binaryBodyFound = new MutableBoolean(false);

            OpenUtils.traverseWithDefaultAlternativePriority(message, new BodyHandler() {
                @Override
                public void binaryBody(BinaryBody binaryBody, Deque<Integer> level) {
                    ContentIdField contentId = (ContentIdField) binaryBody.getParent().getHeader().getField("content-id");
                    if(contentId != null) {
                        if(contentId.getBody().equalsIgnoreCase("<"+cid+">")) {
                            ExpressoDataField expressoData = (ExpressoDataField) binaryBody.getParent().getHeader().getField("Expresso-Data");
                            newSrc.append(expressoData.getEid()).append("/").append(expressoData.getFilename());
                            binaryBodyFound.setValue(true);
                            quit();
                        }
                    }
                }
            });

            if (binaryBodyFound.isTrue()) {
                attrs.put("src", newSrc.toString());
                textOutputDocument.replace(startTag, "<img " + Attributes.generateHTML(attrs) + " />");
            }

        }

        return textOutputDocument.toString();
    }

    //--------------------------------------------------------------------------
    private static final String mailToRegex = "href\\s*=\\s*(" +
            "'\\s*mailto:\\s*([^'\\s<>]*)\\s*'" +
            "|" +
            "\"\\s*mailto:\\s*([^\"\\s<>]*)\\s*\"" +
            "|" +
            "mailto:\\s*([^\\s<>]+)" +
            ")";
    private static final Pattern mailToPattern = Pattern.compile(mailToRegex, Pattern.CASE_INSENSITIVE);

    private String replaceMailToRef(String text) {
        Matcher m = mailToPattern.matcher(text);
        StringBuilder sb = new StringBuilder();
        int lastEnd = 0;
        while(m.find()) {
            sb.append(text.substring(lastEnd, m.start()))
              .append("name=\"_mailto\" href=\"javascript:new_message_to('");

            if(m.group(2) != null) {
                sb.append(m.group(2));
            }
            else if(m.group(3) != null) {
                sb.append(m.group(3));
            }
            else if(m.group(4) != null) {
                sb.append(m.group(4));
            }
            sb.append("');\"");
            lastEnd = m.end();
        }
        sb.append(text.substring(lastEnd));
        return sb.toString();
    }

    //--------------------------------------------------------------------------
    private static final String urlRegex = "\\b(" +
            "(?:(https?://)|www\\d{0,3}[.]|[a-z0-9.\\-]+[.](?=(?:com|org|net|gov|mil|info)(?:[.]br)?)|[a-z0-9.\\-]+[.][a-z]{2,4}/)" +
            "(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+" +
            "(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>\\?«»“”‘’]))";
    private static final Pattern urlPattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);

    private String replaceUrlRef(String text) {

        Source source = new Source(text);
        source.fullSequentialParse();

        Matcher m = urlPattern.matcher(text);
        StringBuilder sb = new StringBuilder();
        int lastEnd = 0;
        find:
        while(m.find()) {

            Element element = source.getEnclosingElement(m.start());
            while(element != null) {
                String elementName = element.getName();

                if(elementName == HTMLElementName.A
                || elementName == HTMLElementName.LINK
                || elementName == HTMLElementName.AREA
                || elementName == HTMLElementName.BASE
                || elementName == HTMLElementName.META
                || elementName == HTMLElementName.SCRIPT
                || elementName == HTMLElementName.IMG
                || elementName == HTMLElementName.VIDEO
                || elementName == HTMLElementName.AUDIO
                || elementName == HTMLElementName.SOURCE
                || elementName == HTMLElementName.BLOCKQUOTE
                || elementName == HTMLElementName.DEL
                || elementName == HTMLElementName.INS
                || elementName == HTMLElementName.Q
                || elementName == HTMLElementName.BUTTON
                || elementName == HTMLElementName.INPUT
                || elementName == HTMLElementName.OBJECT
                || elementName == HTMLElementName.EMBED
                || elementName == HTMLElementName.COMMAND)
                {
                    continue find;
                }

                Tag startTag = element.getStartTag();

                if((startTag.getBegin() < m.start()) && (m.start() < startTag.getEnd())) {
                    continue find;
                }

                element = element.getParentElement();
            }

            sb.append(text.substring(lastEnd, m.start()))
              .append("<a href=\"");

            if(m.group(2) == null) {
                sb.append("http://");
            }

            sb.append(m.group()).append("\">").append(m.group(1)).append("</a>");
            lastEnd = m.end();
        }
        sb.append(text.substring(lastEnd));
        return sb.toString();
    }

}
