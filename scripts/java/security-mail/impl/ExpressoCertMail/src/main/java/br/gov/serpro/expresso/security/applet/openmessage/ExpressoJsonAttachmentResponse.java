/**
 * @todo this header.
 */

package br.gov.serpro.expresso.security.applet.openmessage;

import java.util.logging.Logger;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 *
 * @author Mário César Kolling <mario.kolling@serpro.gov.br>
 */
@JsonPropertyOrder({
    "status", "tempFile"})
public class ExpressoJsonAttachmentResponse {
    public static final String CONTENT_TYPE = "application/json";
    private static final Logger logger = Logger.getLogger(ExpressoJsonAttachmentResponse.class.getName());
    
    public enum Status { success, failed };
    
    private Status status;
    @JsonProperty("status")
    public void setStatus(Status value) {
        status = value;
    }
    public Status getStatus() {
        return status;
    }
    
    private ExpressoAttachment tempFile;
    @JsonProperty("tempFile")
    public void setTempFile(ExpressoAttachment value) {
        tempFile = value;
    }
    public ExpressoAttachment getTempFile() {
        return tempFile;
    }
    
}
