/*
 * TODO: Licensing
 */
package br.gov.serpro.expresso.security.mail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.util.ByteArrayBuffer;
import org.bouncycastle.mail.smime.SMIMEException;
import br.gov.serpro.expresso.security.applet.SmimeApplet;
import br.gov.serpro.expresso.security.cert.DigitalCertificate;
import br.gov.serpro.expresso.security.exception.ExpressoCertificateException;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.util.*;
import javax.mail.*;
import net.htmlparser.jericho.Attributes;
import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.OutputDocument;
import net.htmlparser.jericho.Renderer;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;
import br.gov.serpro.expresso.security.applet.SmimeApplet;
import br.gov.serpro.expresso.security.cert.DigitalCertificate;
import br.gov.serpro.expresso.security.exception.ExpressoCertificateException;

/**
 *
 * @author 75779242020
 */
public class SMIMEMailGenerator {

    private static final Logger logger = Logger.getLogger(SMIMEMailGenerator.class.getName());
    private SmimeApplet applet;
    private DigitalCertificate digitalCertificate;
    private Map<String, Object> messageModel;
    private String userAgent;

    /**
     *
     * @param applet
     * @param dc O certifiado digital
     * @param messageModel
     * @param userAgent
     *
     * @todo Set Opener class here
     */
    public SMIMEMailGenerator(SmimeApplet applet, DigitalCertificate dc, Map<String, Object> messageModel, String userAgent) {
        this.applet = applet;
        this.digitalCertificate = dc;
        this.messageModel = messageModel;
        this.userAgent = userAgent;
    }

    private Map.Entry<String, Part> _createAttachmentFromJsonData(Map<String, Object> tempFile) throws UnsupportedEncodingException, MessagingException, IOException{

        String partId = tempFile.get("partId") != null ? tempFile.get("partId").toString() : null;

        String cid = tempFile.get("cid") != null ? ((String)tempFile.get("cid")).replaceAll("[<>]", "") : null;
        String encoding = tempFile.get("encoding") != null ? tempFile.get("encoding").toString() : null;
        String fileData = tempFile.get("fileData") != null ? tempFile.get("fileData").toString() : null;
        String contentType = tempFile.get("type") != null ? tempFile.get("type").toString() : null;
        String fileName = tempFile.get("name") != null ? tempFile.get("name").toString() : null;

        InputStream is = new ByteArrayInputStream(fileData.getBytes());
        if("base64".equalsIgnoreCase(encoding)) {
            is = new Base64InputStream(is);
        } else if("quoted-printable".equalsIgnoreCase(encoding)) {
            is = new QuotedPrintableInputStream(is);
        } else {
            throw new UnsupportedEncodingException("Encoding not Supported: " + encoding);
        }

        int len;
        byte[] buf = new byte[0x1000];
        ByteArrayBuffer buffer = new ByteArrayBuffer(0x1000);
        while((len = is.read(buf)) > 0) {
            buffer.append(buf, 0, len);
        }

        MimeBodyPart bodyPart = new MimeBodyPart();
        bodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(buffer.toByteArray(), contentType)));
        bodyPart.setHeader("Content-Type", contentType);
        bodyPart.setHeader("Content-Transfer-Encoding", encoding);
        bodyPart.setFileName(fileName);

        if (tempFile.containsKey("cid")) {
            bodyPart.setHeader("Content-ID", "<"+cid+">");
        }

        return new AbstractMap.SimpleEntry<String, Part>(partId, bodyPart);

    }

    /**
     *
     * @param attachmentsRef
     * @param userAgent
     * @return Map<String, Part>
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws MessagingException
     * @todo Separate download code (applet | server)
     */
    private Map<String, Part> _getAttachments(boolean isEmbeddedImage) throws URISyntaxException, UnsupportedEncodingException, IOException,
            KeyManagementException, MessagingException, NoSuchAlgorithmException {

        List<Map<String, Object>> attachmentsRef = isEmbeddedImage
                ? (List<Map<String, Object>>) messageModel.get("embedded_images")
                : (List<Map<String, Object>>) messageModel.get("attachments");


        Map<String, Part> attachments = new HashMap<String, Part>(attachmentsRef.size());
        for (Map<String, Object> attach : attachmentsRef) {
            String  id,
                    session_id,
                    name;

            // TODO: throw exceptions if some values don't exists
            URL page = applet.getDocumentBase();
            URI url;
            String port = page.getPort() == -1 ? "" : ":" + page.getPort();
            Map<String, Object> tempFile = (Map<String, Object>) (attach.containsKey("tempFile") ? attach.get("tempFile") : attach);
            if (tempFile.containsKey("eid") && tempFile.get("eid") != null) {
                id = (String) tempFile.get("eid");
                url = new URI("https://local.expressov3.serpro.gov.br:8998/download/"+id);
                session_id = null;
            } else if (tempFile.containsKey("encoding") && tempFile.containsKey("fileData")) {
                Map.Entry<String, Part> entry = _createAttachmentFromJsonData(tempFile);
                attachments.put(entry.getKey(), entry.getValue());
                continue;
            } else if (isEmbeddedImage) {
                id = (String) attach.get("id");
                session_id = (String) attach.get("session_id");
                url = new URI(page.getProtocol() + "://" + page.getHost() + port + "/index.php?method=Expressomail.showTempImage&tempImageId=" + id);
            } else {
                id = (String) tempFile.get("id");
                session_id = (String) tempFile.get("session_id");
                url = new URI(page.getProtocol() + "://" + page.getHost() + port + "/index.php?method=Tinebase.getTempFile&id=" + id);
            }
            name = (String) attach.get("name");

            SmimeApplet.FileDownloader downloader = this.applet.getDownloader(this.userAgent);

            MimeBodyPart part = downloader.downloadFile(url, session_id);
            part.setDisposition(isEmbeddedImage ? "inline" : "attachment");
            if (name != null) {
                part.setFileName(MimeUtility.encodeText(name, "UTF-8", null));
            }
            if (isEmbeddedImage) {
                part.setHeader("Content-ID", "<" + id + ">");
            }
            attachments.put(id, part);

        }

        return attachments;
    }

    private MimeBodyPart _buildUnsignedBodyPart(Map<String, Part> embeddedImages, Map<String, Part> attachments) throws MessagingException, UnsupportedEncodingException, IOException, URISyntaxException {

        String body = (String) this.messageModel.get("body");
        String contentType = (String) messageModel.get("content_type");

        MimeBodyPart completeBody = new MimeBodyPart();
        MimeMultipart mm = null;
        MimeBodyPart firstPart = new MimeBodyPart();

        MimeMultipart bodyContent = new MimeMultipart("alternative");

        MimeBodyPart textPlainPart = new MimeBodyPart();

        // get body source as UTF-8
        Source bodyHtmlSource = new Source(new String(body.getBytes("UTF-8"), "UTF-8"));
        String textPlainBody = new Renderer(bodyHtmlSource).toString();
        textPlainPart.setDataHandler(new DataHandler(new ByteArrayDataSource(textPlainBody, "text/plain; charset=UTF-8")));
        //textPlainPart.setHeader("Content-Type", "text/plain; charset=UTF-8");
        textPlainPart.setHeader("Content-Transfer-Encoding", "quoted-printable");

        if (contentType.equalsIgnoreCase("text/html")) {
            // text/html body

            bodyHtmlSource.fullSequentialParse();
            OutputDocument outputBody = new OutputDocument(bodyHtmlSource);

            List<Element> images = bodyHtmlSource.getAllElements("img");

            for (Element img : images) {
                StartTag startTag = img.getStartTag();
                Map<String, String> attrs = new HashMap<String, String>();
                startTag.getAttributes().populateMap(attrs, true);

                String src = attrs.get("src");
                if (src == null) {
                    continue;
                }

                URI imgUri = new URI(src);

                try {

                    String regexp = "";
                    if ("local.expressov3.serpro.gov.br".equalsIgnoreCase(imgUri.getHost()) && imgUri.getPort() == 8998) {
                        regexp =  ".*/local.expressov3.serpro.gov.br:8998/(.+)/.*";
                    } else if (imgUri.getQuery().contains("showTempImage")) {
                        regexp = ".*method=\\w+\\.showTempImage&tempImageId=(.+)";
                    } else if (imgUri.getQuery().contains("Expressomail.downloadAttachment")) {
                        regexp = ".*method=Expressomail.downloadAttachment&messageId=.+&partId=([0-9\\.]+)(&getAsJson=.+){0,1}";
                    }

                    boolean local = "local.expressov3.serpro.gov.br".equalsIgnoreCase(imgUri.getHost()) && imgUri.getPort() == 8998 ? true : false;
                    String value = local ? src : imgUri.getQuery();

                    // get image name
                    Pattern pattern = Pattern.compile(regexp);
                    Matcher m = pattern.matcher(value);

                    if (!m.matches()) {
                        continue;
                    }

                    String id = m.group(1);
                    Part part = embeddedImages.get(id);
                    Header contentId = (Header) part.getMatchingHeaders(new String[]{"content-id"}).nextElement();

                    attrs.put("src", "cid:" + contentId.getValue().replaceAll("[<>]", ""));

                    String newTag = "<img " + Attributes.generateHTML(attrs) + " />";
                    outputBody.replace(startTag, newTag);

                } catch (NullPointerException ex) {
                    logger.log(Level.INFO, "Ignoring Img tag: {0}\nCaused by: {1}",
                            new Object[]{img.toString(), ex});
                }

            }

            // add text/plain
            bodyContent.addBodyPart(textPlainPart);

            MimeBodyPart textHtmlPart = new MimeBodyPart();

            textHtmlPart.setDataHandler(new DataHandler(new ByteArrayDataSource(outputBody.toString(), contentType + "; charset=UTF-8")));
            //textHtmlPart.setHeader("Content-Type", contentType + "; charset=UTF-8");
            textHtmlPart.setHeader("Content-Transfer-Encoding", "quoted-printable");

            if (!embeddedImages.isEmpty()) {

                MimeMultipart relatedMultipart = new MimeMultipart("related");
                relatedMultipart.addBodyPart(textHtmlPart);

                for (String key : embeddedImages.keySet()) {
                    relatedMultipart.addBodyPart((MimeBodyPart) embeddedImages.get(key));
                }

                MimeBodyPart relatedBodyPart = new MimeBodyPart();
                relatedBodyPart.setContent(relatedMultipart);
                bodyContent.addBodyPart(relatedBodyPart);

            } else {
                bodyContent.addBodyPart(textHtmlPart);
            }

            firstPart.setContent(bodyContent);
        } else {
            // text/plain body
            firstPart = textPlainPart;

        }

        // use MimeUtility.encode with a ByteArrayOutputStream
        if (!attachments.isEmpty()) {
            mm = new MimeMultipart("mixed");
            mm.addBodyPart(firstPart);
            for (Part attach : attachments.values()) {
                mm.addBodyPart((MimeBodyPart) attach);
            }

            completeBody.setContent(mm);
        } else if (contentType.equalsIgnoreCase("text/html")) {
            completeBody.setContent(bodyContent);
        } else {
            completeBody = firstPart;
        }

        return completeBody;
    }

    // todo: need more tests
    private InternetAddress _tine2Rfc822Adress(String address) throws AddressException {
        logger.log(Level.SEVERE, "Address: {0}", address);

        Pattern regex = Pattern.compile("^(.*)\\((.*@.*)\\)$");
        Matcher match = regex.matcher(address.trim());
        String name = "";
        String mail = "";

        if (match.matches()) {
            logger.log(Level.SEVERE, "match: {0}", match.group());
            name = match.group(1).trim();
            mail = match.group(2).trim();
        }

        //name = name.trim().matches("^\".*\"$") ? name : ("\"" + name + "\"");
        InternetAddress rfc822Adress = new InternetAddress(name + " <" + mail + ">");

        return rfc822Adress;
    }

    private MimeMessage _buildMessage(MimeBodyPart signedBody) throws ExpressoCertificateException {
        try {
            MimeMessage message = new MimeMessage(Session.getInstance(System.getProperties()));

            message.setContent(signedBody.getContent(), signedBody.getContentType());

            // Subject
            String subject = (String) this.messageModel.get("subject");
            message.setSubject(new String(subject.getBytes("UTF-8"),"UTF-8"), "UTF-8");

            // From
            String from = (String) this.messageModel.get("from_name");
            Address fromAddress = new InternetAddress((String) this.messageModel.get("from_email"),
                new String(from.getBytes("UTF-8"),"UTF-8"), "UTF-8");
            message.setFrom(fromAddress);

            // To comply with the rest of Tine we'll have to set those headers ourselves.
            // format used: name (mail)
            ArrayList<String> addressArray = (ArrayList<String>) this.messageModel.get("to");
            for (String to : addressArray) {
                try {
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
                } catch (AddressException ex) {
                    message.addRecipient(Message.RecipientType.TO, this._tine2Rfc822Adress(to));
                }
            }

            // CC
            addressArray = (ArrayList<String>) this.messageModel.get("cc");
            for (String cc : addressArray) {
                try {
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(cc));
                } catch (AddressException ex) {
                    message.addRecipient(Message.RecipientType.TO, this._tine2Rfc822Adress(cc));
                }
            }

            // BCC
            addressArray = (ArrayList<String>) this.messageModel.get("bcc");
            for (String bcc : addressArray) {
                try {
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(bcc));
                } catch (AddressException ex) {
                    message.addRecipient(Message.RecipientType.TO, this._tine2Rfc822Adress(bcc));
                }
            }

            //{ name: 'importance' },
            Object value = this.messageModel.get("importance");
            if (value != null ? ((Boolean) value).booleanValue() : false) {
                message.addHeader("Importance", "high");
            }

            // reading_conf
            value = this.messageModel.get("reading_conf");
            if (value != null ? ((Boolean) value).booleanValue() : false) {
                message.addHeader("Disposition-Notification-To", from.toString());
            }

            return message;
        } catch (MessagingException e) {
            throw new ExpressoCertificateException(e.getMessage(), e.getCause());
        } catch (IOException e) {
            throw new ExpressoCertificateException(e.getMessage(), e.getCause());
        }

    }

    /**
     *
     * @return
     * @throws CertificateException
     * @todo Create model for Custom_Model_CertificateValidation
     * @todo Get key escrow settings directly, don't trust data from browser
     * @todo Verify matches of digital certificates and e-mail addresses.
     */
    private List<X509Certificate> _getCertificatesForEncryption() throws CertificateException{
        
        List<X509Certificate> certificates = new ArrayList<X509Certificate>();
        CertificateFactory cf = CertificateFactory.getInstance("X509");

        // Corrige linhas que são terminadas por \n (\x0A) e deveriam ser terminadas por \r\n (\x0D\x0A)
        Pattern p = Pattern.compile("(?<!\\r)\\n");

        // Get recipients certificates
        List<String> pemCertificates = (List<String>) messageModel.get("certificadosDestinatarios");

        // Get key escrow certificates
        if (messageModel.containsKey("keyEscrowCertificates")) {
            for (Map<String, Object> keyEscrowCertificate : (List<Map<String, Object>>) messageModel.get("keyEscrowCertificates")) {
                if (((Boolean)keyEscrowCertificate.get("success")) == true) {
                    Map<String, Object> certificateMap = (Map<String, Object>) keyEscrowCertificate.get("certificate");
                    pemCertificates.add((String) certificateMap.get("pemCertificateData"));
                }
            }
        }

        for (String pemCertificate : pemCertificates) {
            Matcher matcher = p.matcher(pemCertificate);
            pemCertificate = matcher.replaceAll("\r\n");

            InputStream is = new ByteArrayInputStream(pemCertificate.getBytes());
            certificates.addAll((List<X509Certificate>) cf.generateCertificates(is));
        }

        return certificates;
    }

    public Certificate getSignerCertificate() throws KeyStoreException{
        return digitalCertificate.getDigitalCertificate();
    }

    /**
     *
     * @param dc
     * @param messageModel
     * @param userAgent
     * @return
     * @throws MessagingException
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws GeneralSecurityException
     * @throws SMIMEException @todo implements text/plain body
     */
    public MimeMessage generateSignedMail() throws ExpressoCertificateException {
        try {
            long begin = System.currentTimeMillis();

            // download localAttachments
            Map<String, Part> embbededImages = this._getAttachments(true);
            Map<String, Part> attachmentParts = this._getAttachments(false);
            long end = System.currentTimeMillis();
            logger.log(Level.INFO, "download time: {0} seconds", ((end - begin) / 1000));

            // get the bodyPart of the unsigned message body.
            MimeBodyPart unsignedBodyPart = _buildUnsignedBodyPart(embbededImages, attachmentParts);

            // Complete the message
            digitalCertificate.init();

            // TODO: Verify DigitalCertificate
            // X509Certificate senderCertificate =  (X509Certificate) digitalCertificate.getDigitalCertificate();

            MimeBodyPart signedMultipartBody = digitalCertificate.signMail(unsignedBodyPart);

            if (signedMultipartBody != null) {
                // pass model, and signed multipart body
                return _buildMessage(signedMultipartBody);
            }
        } catch (MessagingException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (URISyntaxException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (UnsupportedEncodingException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (IOException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (NoSuchAlgorithmException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (KeyManagementException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (GeneralSecurityException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        }
        return null;
    }

    /**
     * Efetua a encriptação de um e-mail
     *
     * @return MimeMessage
     * @throws ExpressoCertificateException
     */
    public MimeMessage generateEncryptedMail() throws ExpressoCertificateException {
        try {
            long begin = System.currentTimeMillis();

            // download localAttachments
            Map<String, Part> embbededImages = this._getAttachments(true);
            Map<String, Part> attachmentParts = this._getAttachments(false);
            long end = System.currentTimeMillis();
            logger.log(Level.INFO, "download time: {0} seconds", ((end - begin) / 1000));

            // get the bodyPart of the unsigned message body.
            MimeBodyPart openBodyPart = this._buildUnsignedBodyPart(embbededImages, attachmentParts);

            List<X509Certificate> certificates = _getCertificatesForEncryption();

            // Complete the message
            MimeBodyPart encryptedMultipartBody = digitalCertificate.createEncryptedBodyPart(certificates, openBodyPart);


            if (encryptedMultipartBody != null) {
                // pass model, and signed multipart body
                return this._buildMessage(encryptedMultipartBody);
            }

        } catch (MessagingException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (URISyntaxException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (UnsupportedEncodingException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (IOException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (NoSuchAlgorithmException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (KeyManagementException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (CertificateException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        }
        return null;
    }

    /**
     * Efetua a assinatura digital e criptografia de um e-mail
     *
     * @return
     */
    public MimeMessage generateSignedAndEncryptedMail() throws ExpressoCertificateException {
        try {
            // download localAttachments
            Map<String, Part> embbededImages = _getAttachments(true);
            Map<String, Part> attachmentParts = _getAttachments(false);

            // get the bodyPart of the unsigned message body.
            MimeBodyPart openBodyPart = this._buildUnsignedBodyPart(embbededImages, attachmentParts);
            digitalCertificate.init();

            // Verify DigitalCertificate
            X509Certificate senderCertificate =  (X509Certificate) digitalCertificate.getDigitalCertificate();
            MimeBodyPart signedBodyPart = digitalCertificate.signMail(openBodyPart);

            List<X509Certificate> certificates = _getCertificatesForEncryption();

            // Complete the message
            MimeBodyPart encryptedMultipartBody = digitalCertificate.createEncryptedBodyPart(certificates, signedBodyPart);

            if (encryptedMultipartBody != null) {
                // pass model, and signed multipart body
                return _buildMessage(encryptedMultipartBody);
            }
            return null;

        } catch (KeyStoreException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (CertificateException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (KeyManagementException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (NoSuchAlgorithmException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (URISyntaxException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (UnsupportedEncodingException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (IOException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        } catch (MessagingException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex.getCause());
        }
    }
}
