package br.gov.serpro.expresso.security.applet.openmessage;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class TokenBroker {

    private static final Logger logger = Logger.getLogger(TokenBroker.class.getName());

    private List<PKCS11Config> tokenProviders;

    private void scanTokenProviders() {

        tokenProviders = new ArrayList<PKCS11Config>();

        PKCS11Config ePass2000 = PKCS11ConfigBuilder.create()
                .name("ePass2000")
                .library("ngp11v211.dll")
                .library("libepsng_p11.so")
                .showInfo(false)
                .build();

        if(ePass2000 != null) {
            tokenProviders.add(ePass2000);
        }

        PKCS11Config watchKey = PKCS11ConfigBuilder.create()
                .name("WatchKey")
                .scanPath("C:/Windows/System32/WatchData/Watchdata ICP CSP v1.0")
                .scanPath("/usr/lib/watchdata/ICP/lib")
                .library("WDPKCS.dll")
                .library("libwdpkcs_icp.so")
                .showInfo(false)
                .build();

        if(watchKey != null) {
            tokenProviders.add(watchKey);
        }

        PKCS11Config safenet = PKCS11ConfigBuilder.create()
                .name("Safenet")
                .library("eTPkcs11.dll")
                .library("libeTPkcs11.so")
                .showInfo(false)
                .build();

        if(safenet != null) {
            tokenProviders.add(safenet);
        }

        PKCS11Config crypToken = PKCS11ConfigBuilder.create()
                .name("CrypToken")
                .scanPath("c:/windows/SYSWOW764")
                .scanPath("c:/Windows/SysWOW64")
                .library("aetpkss1.dll")
                .showInfo(false)
                .build();

        if(crypToken != null) {
            tokenProviders.add(crypToken);
        }
    }

    public PKCS11Config pickTokenProvider() {

        if(tokenProviders == null) {
            scanTokenProviders();
        }

        List<PKCS11Config> connectedTokenProviders = new ArrayList<PKCS11Config>();

        for(PKCS11Config pkcs11Config : tokenProviders) {
            pkcs11Config.update();
            if(pkcs11Config.isConnected()) {
                connectedTokenProviders.add(pkcs11Config);
            }
        }

        switch(connectedTokenProviders.size()) {
            case 0:
                return null;

            case 1:
                return connectedTokenProviders.get(0);

            default:

                final JDialog dialog = new JDialog((JDialog)null, "Provider", Dialog.ModalityType.TOOLKIT_MODAL);

                class ProviderButton extends JButton {
                    private final PKCS11Config pkcs11Config;
                    ProviderButton(PKCS11Config pkcs11Config) {
                        this.pkcs11Config = pkcs11Config;
                        this.setText(pkcs11Config.getName());
                        this.setToolTipText(pkcs11Config.toHtmlString());
                        this.setBackground(Color.WHITE);
                        this.setMargin(new Insets(15, 15, 15, 15));
                    }
                    PKCS11Config getPKCS11Config() {
                        return pkcs11Config;
                    }
                }

                final JOptionPane optionPane = new JOptionPane(
                        "Selecione um token:",
                        JOptionPane.PLAIN_MESSAGE,
                        JOptionPane.DEFAULT_OPTION,
                        null);

                ProviderButton[] options = new ProviderButton[connectedTokenProviders.size()];

                for(int i = 0; i < connectedTokenProviders.size(); i++) {
                    final ProviderButton b = new ProviderButton(connectedTokenProviders.get(i));
                    options[i] = b;
                    b.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            optionPane.setValue(b);
                            dialog.setVisible(false);
                            dialog.dispose();
                        }
                    });
                }

                optionPane.setOptions(options);

                dialog.setContentPane(optionPane);
                dialog.setLocationRelativeTo(null);
                dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                dialog.pack();
                dialog.setVisible(true);

                return ((ProviderButton) optionPane.getValue()).getPKCS11Config();
        }
    }

    public LoginStatus login(PKCS11Config pkcs11Config) {
        return login(pkcs11Config, 1);
    }

    public LoginStatus login(PKCS11Config pkcs11Config, int maxAttempts) {

        int attempts = 0;

        while(true) {
            ++attempts;

            String password = null;
            JPasswordField passField = new JPasswordField();
            int passRet = JOptionPane.showConfirmDialog(null, passField,
                    "Informe o PIN para o token '" + pkcs11Config.getName() + "'",
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (passRet == JOptionPane.OK_OPTION) {
                password = new String(passField.getPassword());
            }

            if(password == null) {
                return LoginStatus.CANCELLED;
            }
            else {

                if(pkcs11Config.load(password)) {
                    return LoginStatus.SUCCESS;
                }
                else {
                    if(attempts < maxAttempts) {
                        JOptionPane.showMessageDialog(null, "PIN Errado", "PIN", JOptionPane.ERROR_MESSAGE);
                        continue;
                    }
                    return LoginStatus.FAILURE;
                }
            }
        }
    }

    public enum LoginStatus {
        SUCCESS, FAILURE, CANCELLED;
    }

}
