/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.serpro.expresso.security.cert;

import br.gov.serpro.expresso.security.exception.DialogCancelledException;
import br.gov.serpro.expresso.security.exception.ExpressoCertificateException;
import br.gov.serpro.expresso.security.setup.Setup;
import br.gov.serpro.expresso.security.ui.DialogBuilder;
import java.awt.Frame;
import java.io.IOException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 *
 * @author Mário César Kolling <mario.kolling@serpro.gov.br>
 */
public class PinCallbackHandler implements CallbackHandler{

    private final Frame parent;
    private final Setup setup;
    
    public PinCallbackHandler() {
        this(null, null);
    }
    
    public PinCallbackHandler(Frame parent, Setup setup) {
        this.parent = parent;
        this.setup = setup;
    }
    
    @Override
    public void handle(Callback[] clbcks) throws IOException, UnsupportedCallbackException {
        for (Callback callback : clbcks){
            if (callback instanceof PasswordCallback){
                PasswordCallback pc = (PasswordCallback)callback;
                String sPin = DialogBuilder.showPinDialog(parent, setup);
                if (sPin != null) {
                    pc.setPassword(sPin.toCharArray());
                    // TODO: clear pin
                } else {
                    throw new IOException(new DialogCancelledException());
                }
            } else {
                throw new UnsupportedCallbackException(callback);
            }
        }
    }
}
