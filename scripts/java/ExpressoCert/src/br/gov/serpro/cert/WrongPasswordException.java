/*
 * TODO: Licensing
 */
package br.gov.serpro.cert;

/**
 *
 * @author Mário César Kolling <mario.kolling@serpro.gov.br>
 * @todo i18n
 */
public class WrongPasswordException extends Exception {

    public WrongPasswordException(String string) {
        super(string);
    }
    
    public WrongPasswordException() {
        this("Senha Incorreta");
    }
    
}
