#!/usr/bin/env php

<?php
/**
 * Reset all ActiveSync devices
 *
 * @package     HelperScripts
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @version     $Id$
 *
 */

// for a folder different of tine20, append t=[folder name] to commmand line, for example, t=expressov3
define('APPNAME', getAppName($argv));

main();

/**
 * Start of process
 */
function main()
{
    set_time_limit(0);
    ini_set('memory_limit','256M');

    prepareEnvironment();

    try {
        $opts = new Zend_Console_Getopt(array(
                'domain|d-s'=>'To get database from a specified DOMAIN config file',
                'global|g'=>'To get database from the global config file',
                'help|h'=>'help option with no required parameter'
        )
        );
        $opts->parse();
    } catch (Zend_Console_Getopt_Exception $e) {
        echo $e->getUsageMessage() . "\nType t=[target] for a folder different of tine20\n";
        exit;
    }

    if($opts->getOption('h')) {
        die("ERROR: ".$opts->getUsageMessage() . "\nType t=[target] for a folder different of tine20\n");
    }

    $domain = $opts->getOption('d');
    if(empty($domain)) $domain = 'default';
    if($opts->getOption('g')) $domain = 'global';

    $dbConfig = getDbConfig($domain);

    $result = resetAllDevices($dbConfig);

    return $result;
}

/**
 * Sets the include path and loads autoloader classes
 */
function prepareEnvironment()
{
    $paths = array(
            realpath(dirname(__FILE__) . '/../' . APPNAME),
            realpath(dirname(__FILE__) . '/../' . APPNAME . '/library'),
            get_include_path()
    );
    set_include_path(implode(PATH_SEPARATOR, $paths));

    require_once 'Zend/Loader/Autoloader.php';
    $autoloader = Zend_Loader_Autoloader::getInstance();
    $autoloader->setFallbackAutoloader(true);
    Tinebase_Autoloader::initialize($autoloader);
}

/**
 * Delete all devices from DB
 *
 * @param Zend_Config $_dbConfig
 * @return boolean
 */
function resetAllDevices($_dbConfig)
{
    echo "Be pacient! This can take a long time if you have a large database...\n";

    $adapterConfig = array(
        'username'    => $_dbConfig->username,
        'password'    => $_dbConfig->password,
        'host'        => $_dbConfig->host,
        'dbname'      => $_dbConfig->dbname,
        'port'        => !empty($_dbConfig->port) ? $_dbConfig->port : 5432,
    );

    try {
        $db = @Zend_Db::factory($_dbConfig->adapter, $adapterConfig);
    } catch(Exception $e) {
        die("ERROR: ".$e->getMessage()."\n");
    }
    echo "Connected to Database ".$_dbConfig->dbname."...\n";

    $adapterConfig['adapter'] = $_dbConfig->adapter;
    $adapterConfig['tableprefix'] = $tablePrefix = $_dbConfig->tableprefix;

    echo "Resetting devices ...\n";
    try {
        $numberOfRowsDeleted = $db->delete($tablePrefix . 'acsync_device', array());
    } catch (Exception $e) {
        die("ERROR: ".$e->getMessage()."\n");
    }

    echo $numberOfRowsDeleted. " devices had resetting successfully!\n";

    return true;
}

/**
 * Get dbConfig
 * @param string $domain
 */
function getDbConfig($domain)
{
    echo "Gettind database configurations from domain \"$domain\" ...\n";

    if ($domain == 'global'){
        $configPath = realpath(__DIR__ . '/../' . APPNAME);
    } else {
        $configPath = realpath(__DIR__ . '/../' . APPNAME . '/domains') . "/$domain";
    }

    $configFile = $configPath . '/config.inc.php';
    if(!file_exists($configFile)){
        echo "ERROR: file ".$configFile." doesn't exist...\n";
        die("WARNING: Fill \"$configFile\" with correct database information.\n");
    }

    $formerConfig = array();
    echo "Loading file \"$configFile\"...";
    if(file_exists($configFile)) {
        $config = new Zend_Config(require $configFile, TRUE);
        if ($domain == 'global') {
            $formerConfig = require $configFile;
        }
    } else {
        die("ERROR: Config file: $configFile not found!\n");
    }
    echo "OK!\n";

    if (isset($config->asdatabase) && ($config->asdatabase->active === true)) {
        return $config->asdatabase;
    }

    return $config->database;
}

/**
 * get installation folder name
 *
 * @param array $argv
 * @return string
 */
function getAppName(array $argv)
{
    $appName = 'tine20';
    foreach ($argv as $arg){
        if (substr($arg, 0, 2) == 't='){
            $appName = trim(substr($arg, 2));
            break;
        }
    }

    return $appName;
}
