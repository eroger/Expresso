<?php
/**
 * Tine 2.0 - http://www.tine20.org
 * 
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2007-2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Matthias Greiling <m.greiling@metaways.de>
 */
/**
 * Test helper
 */
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'TestHelper.php';

class AllTests
{
    public static function main()
    {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }
    
    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('ExpressoV3 All Tests');
        
        $suite->addTest(Tinebase_AllTests::suite());
        $suite->addTest(Calendar_AllTests::suite());
        $suite->addTest(Addressbook_AllTests::suite());
        $suite->addTest(Tasks_AllTests::suite());
        $suite->addTest(Expressomail_AllTests::suite());
        $suite->addTest(ActiveSync_AllTests::suite());
        $suite->addTest(Zend_AllTests::suite());
        $suite->addTest(Scheduler_AllTests::suite());
        $suite->addTest(Admin_AllTests::suite());
        //$suite->addTest(Expresso_Performance_AllTests::suite()); TODO: Fix this test. (task13912)
        
        return $suite;
    }
}
