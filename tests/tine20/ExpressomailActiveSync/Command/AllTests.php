<?php
/**
 * Tine 2.0 - http://www.tine20.org
 * 
 * @package     ExpressomailActiveSync
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2015 Serpro <http://www.serpro.gov.br>
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

if (! defined('PHPUnit_MAIN_METHOD')) {
    define('PHPUnit_MAIN_METHOD', 'ExpressomailActiveSync_Command_AllTests::main');
}

class ExpressomailActiveSync_Command_AllTests
{
    public static function main ()
    {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }
    
    public static function suite ()
    {
        $suite = new PHPUnit_Framework_TestSuite('Tine 2.0 ActiveSync All Command Tests');
        
        $suite->addTestSuite('ExpressomailActiveSync_Command_PingTests');
        $suite->addTestSuite('ExpressomailActiveSync_Command_SyncTests');
        
        return $suite;
    }
}

if (PHPUnit_MAIN_METHOD == 'ExpressomailActiveSync_Command_AllTests::main') {
    ExpressomailActiveSync_Command_AllTests::main();
}
